//
//  SaveAddressModel.swift
//  Peps
//
//  Created by Jay on 20/12/19.
//  Copyright © 2019 JAY BHADJA. All rights reserved.
//

import Foundation
import ObjectMapper

struct SaveAddressModel : Mappable {
    var address : String = ""
    var address_type : String = ""
    var id : String = ""
    var lat : String = ""
    var lng : String = ""
    var user_id : String = ""

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        address <- map["address"]
        address_type <- map["address_type"]
        id <- map["id"]
        lat <- map["lat"]
        lng <- map["lng"]
        user_id <- map["user_id"]
    }

}
