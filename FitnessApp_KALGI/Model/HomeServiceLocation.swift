//
//  HomeServiceLocation.swift
//  FitnessApp
//
//  Created by Ashish on 11/05/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//





import UIKit

class HomeServiceLocation: NSObject {
    
    var str_id:String! = ""
    var str_lat:String! = ""
    var str_lon:String! = ""
    var str_address:String! = ""
    var str_name:String! = ""
    var is_selected:Bool! = false
    
    
    /*
     address = "Noida, Uttar Pradesh, India";
     "created_at" = "2020-05-10 07:13:43";
     id = 7;
     lat = "28.570848";
     "location_name" = Noida;
     lon = "77.32593";
     "updated_at" = "<null>";
     "user_id" = ANGNQSENSPRZ;
     
     
     */
    
    
    init(dictUserInfo: NSDictionary) {
        
        str_name = String(format:dictUserInfo.value(forKey: "location_name") as! String)
        str_address = String(format:dictUserInfo.value(forKey: "address") as! String)
        
        str_lat = String(format:dictUserInfo.value(forKey: "lat") as! String)
        str_lon = String(format:dictUserInfo.value(forKey: "lon") as! String)
        
        
        if let result_number = dictUserInfo.value(forKey: "id") as? NSNumber
        {
            str_id = "\(result_number)"
        }
        
        
        
        
    }
    
    
    override init() {
        str_id = ""
        str_lat = ""
        str_lon = ""
        str_address = ""
        str_name = ""
        is_selected = false
        
    }
    
}
