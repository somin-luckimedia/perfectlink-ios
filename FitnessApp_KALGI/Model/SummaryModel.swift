import Foundation
import ObjectMapper

struct SummaryModel : Mappable {
	var Service : String = ""
	var Subservice : [Subservice] = []
	var Total : String = ""
	var Description : String = ""
	var LadderRequired : String = ""
    var Location : String = ""
    var Date : String = ""
    var Time : String = ""

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
	}
}

struct Subservice : Mappable {
    var Name : String = ""
    var Cost : String = ""

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
    }

}
