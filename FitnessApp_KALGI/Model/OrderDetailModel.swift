//
//  OrderDetailModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 16/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//


import UIKit

class OrderDetailModel: NSObject {
    
    
    var obj_address:ShippingAddressModel = ShippingAddressModel()
    var obj_accessories_detail:AccessoriesDetailModel = AccessoriesDetailModel()
    var str_order_id:String = ""
    var str_transaction_id:String = ""
    var str_order_status:String = ""
    var str_total_amount_paid:String = ""
    var str_payment_method:String = ""
    var str_quantity:String = ""
    
    
    var str_payer_status:String = ""
    var str_payer_email:String = ""
    var str_is_coupon_applied:Bool = false
    var str_coupon_id:String = ""
    var str_coupon_code:String = ""
    var str_membership_amount:String = "0"
    var str_discounted_amount:String = "0"
    
    init(dictUserInfo: NSDictionary) {
        if let shippingValue = dictUserInfo.value(forKey:"shipping_detail"){
            obj_address = ShippingAddressModel(dictUserInfo: shippingValue as! NSDictionary)
        }
        if let shippingValue = dictUserInfo.value(forKey:"shipment_detail"){
            obj_accessories_detail = AccessoriesDetailModel(dictUserInfo: shippingValue as! NSDictionary)
        }
        if let result_number = dictUserInfo.value(forKey: "order_id") as? String
        {
            str_order_id = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "transaction_id") as? String
        {
            str_transaction_id = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "order_status") as? String
        {
            str_order_status = "\(result_number)"
        }
        if let shippingValue = dictUserInfo.value(forKey:"payment_detail"){
            let obj_dict:NSDictionary = shippingValue as! NSDictionary
            if let result_number = obj_dict.value(forKey: "total_amount_paid") as? String
            {
                str_total_amount_paid = "\(result_number)"
            }
            if let result_number = obj_dict.value(forKey: "payment_method") as? String
            {
                str_payment_method = "\(result_number)"
            }
            if let result_number = obj_dict.value(forKey: "quantity") as? NSNumber
            {
                str_quantity = "\(result_number)"
            }
            if let result_number = obj_dict.value(forKey: "payer_status") as? String
            {
                str_payer_status = "\(result_number)"
            }
            if let result_number = obj_dict.value(forKey: "payer_email") as? String
            {
                str_payer_email = "\(result_number)"
            }
            if let result_number = obj_dict.value(forKey: "is_coupon_applied") as? Bool
            {
                str_is_coupon_applied = result_number
            }
            if let result_number = obj_dict.value(forKey: "coupon_id") as? String
            {
                str_coupon_id = "\(result_number)"
            }
            if let result_number = obj_dict.value(forKey: "coupon_code") as? String
            {
                str_coupon_code = "\(result_number)"
            }
            if let result_number = obj_dict.value(forKey: "membership_amount") as? String
            {
                str_membership_amount = "\(result_number)"
            }
            if let result_number = obj_dict.value(forKey: "discounted_amount") as? String
            {
                str_discounted_amount = "\(result_number)"
            }
            
        }
      }
    
    override init() {
        obj_address = ShippingAddressModel()
        obj_accessories_detail = AccessoriesDetailModel()
        str_order_id = ""
        str_transaction_id = ""
        str_order_status = ""
        str_total_amount_paid = ""
        str_payment_method = ""
        str_quantity = ""
    }
    
}

/*
 {
 "STATUS": "true",
 "message": "Subscription Info.",
 "response": {
 "order_id": "2pjO7zEwrMJo",
 "transaction_id": "3WA07698X4182153S",
 "order_status": "Active",
 "shipment_detail": {
 "id": "OSJGJGBASJUB",
 "product_type": "GYM",
 "name": "Silver gym",
 "brief_description": "sadfasdf",
 "full_description": "sdfasdf",
 "cover_image": "http://perfectlinkfitness.com/assets/images/gym_cover_image/1574792422.jpg",
 "amount": "369.00",
 "currency": "$",
 "subscription_amount": "123",
 "start_date": "2020-02-24",
 "months": 3,
 "end_date": "2020-05-24",
 "member_count": 1,
 "preferred_time": null,
 "membership_type": "SINGLE",
 "valid_upto": "90 days"
 },
 "payment_detail": {
 "total_amount_paid": "369.00",
 "payer_status": "UNVERIFIED",
 "payment_method": "paypal",
 "payment_created_at": "2020-02-23T06:54:54Z",
 "payer_email": "seatmvbuyer@gmail.com",
 "is_coupon_applied": false,
 "coupon_id": null,
 "coupon_code": null,
 "membership_amount": null,
 "discounted_amount": null
 }
 }
 }
 
 
 
 */
