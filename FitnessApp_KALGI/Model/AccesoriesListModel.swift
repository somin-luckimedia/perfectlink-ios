//
//  AccesoriesListModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 22/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit

class AccesoriesListModel: NSObject {
    
    var str_id:String! = ""
    var str_accessorie_id:String! = ""
    var str_accessorie_title:String! = ""
    var str_cover_image:String! = ""
    var str_brief_description:String! = ""
    var str_star_rating:String! = ""
    var str_currency:String! = ""
    var str_amount:String! = ""
    var str_per:String! = ""
    
    init(dictUserInfo: NSDictionary) {

        
        str_accessorie_title = String(format:dictUserInfo.value(forKey: "accessorie_title") as! String)
        str_cover_image = String(format:dictUserInfo.value(forKey: "cover_image") as! String)
        str_brief_description = String(format:dictUserInfo.value(forKey: "brief_description") as! String)
        
        
        if let result_number = dictUserInfo.value(forKey: "accessorie_id") as? String
        {
            str_accessorie_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "star_rating") as? String
        {
            str_star_rating = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "amount") as? String
        {
            str_amount = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "per") as? String
        {
            str_per = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "currency") as? String
        {
            str_currency = "\(result_number)"
        }
        
        
    }
}
