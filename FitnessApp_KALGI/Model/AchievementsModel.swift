//
//  AchievementsModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 12/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class AchievementsModel: NSObject {
    
    var str_achievement:String! = ""
    var str_id:String! = ""
    
    init(dictUserInfo: NSDictionary) {
        str_achievement = String(format:dictUserInfo.value(forKey: "achievement") as! String)
        if let result_number = dictUserInfo.value(forKey: "id") as? NSNumber
        {
            str_id = "\(result_number)"
        }
    }
}
