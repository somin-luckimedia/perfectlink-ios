//
//  TrainerListModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 21/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class TrainerListModel: NSObject {
    
    var str_trainer_id:String! = ""
    var followers : String! = ""
    var videos : String! = ""
    var courses : String! = ""
    var str_name:String! = ""
    var str_profile_image:String! = ""
    var str_about:String! = ""
    var str_star_rating:String! = ""
    var str_distance:String! = ""
    var array_locations: NSMutableArray  = NSMutableArray()
    
    
    init(dictUserInfo: NSDictionary) {
        str_name = String(format:dictUserInfo.value(forKey: "name") as! String)
        str_profile_image = String(format:dictUserInfo.value(forKey: "profile_picture") as! String)
        
        if let result_number = dictUserInfo.value(forKey: "about") as? String
        {
        str_about = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "user_id") as? String
        {
            str_trainer_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "courses") as? NSNumber
        {
            courses = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "videos") as? NSNumber
        {
            videos = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "followers") as? NSNumber
        {
            followers = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "star_rating") as? String
        {
            str_star_rating = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "distance") as? String
        {
            str_distance = "\(result_number)"
        }
        
        
        if let array:NSArray = dictUserInfo["location_service"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                let obj_model:LocationServiceModel = LocationServiceModel.init(dictUserInfo: dict)
                self.array_locations.add(obj_model)
            }
        }
        
    }
    
    override init() {
        str_trainer_id = ""
        followers = ""
        videos = ""
        courses = ""
        str_name = ""
        str_profile_image = ""
        str_about = ""
        str_star_rating = ""
        str_distance = ""


    }
}
