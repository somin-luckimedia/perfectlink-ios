//
//  GymListModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 26/11/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit

class GymListModel: NSObject {
    
    var str_gym_id:String! = ""
    var str_gym_title:String! = ""
    var str_cover_image:String! = ""
    var str_brief_description:String! = ""
    var str_star_rating:String! = ""
    var str_distance:String! = ""
    var array_open_close: NSMutableArray  = NSMutableArray()
    var str_open_close:String! = ""
    
 
    
    
    
    init(dictUserInfo: NSDictionary) {
        
       
        
        
        
        str_gym_title = String(format:dictUserInfo.value(forKey: "gym_title") as! String)
        str_cover_image = String(format:dictUserInfo.value(forKey: "cover_image") as! String)
        str_brief_description = String(format:dictUserInfo.value(forKey: "brief_description") as! String)
        
        
        if let result_number = dictUserInfo.value(forKey: "gym_id") as? String
        {
            str_gym_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "star_rating") as? NSNumber
        {
            str_star_rating = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "distance") as? NSNumber
        {
            str_distance = "\(result_number)"
        }
        

        
        if let array:NSArray = dictUserInfo["open_close"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                self.array_open_close.add(dict)
            }
        }
        
        if let result_number = dictUserInfo.value(forKey: "open_close") as? String
        {
            str_open_close = "\(result_number)"
        }
        
        
       
    }
}
