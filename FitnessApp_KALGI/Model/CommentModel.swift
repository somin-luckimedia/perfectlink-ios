//
//  CommentModel.swift
//  FitnessApp
//
//  Created by Ashish on 26/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class CommentModel: NSObject {
    
    /*
     "id": 3,
     "user_id": "IJDNBSRDSDOK",
     "wall_id": 30,
     "comment": "I Like this video2",
     "created_at": "2020-04-25 11:32:55",
     "posted_at": 1587814375
     
     */
    
    
    var str_id:String! = ""
    var str_name:String! = ""
    var str_profile_picture:String! = ""
    var str_comment:String! = ""
    var str_user_id:String! = ""
    var str_posted_at:String! = ""
    var str_wall_id:String! = ""
    
    init(dictUserInfo: NSDictionary) {
        
        
        if let result_number = dictUserInfo.value(forKey: "id") as? NSNumber
        {
            str_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "name") as? String
        {
            str_name = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "user_id") as? String
        {
            str_user_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "comment") as? String
        {
            str_comment = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "profile_picture") as? String
        {
            str_profile_picture = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "posted_at") as? NSNumber
        {
            str_posted_at = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "wall_id") as? NSNumber
        {
            str_wall_id = "\(result_number)"
        }
        
        
        //likes
    }
}
