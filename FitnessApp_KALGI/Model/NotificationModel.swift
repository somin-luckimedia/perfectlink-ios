//
//  NotificationModel.swift
//  Peps
//
//  Created by Jay on 25/12/19.
//  Copyright © 2019 JAY BHADJA. All rights reserved.
//

import Foundation
import ObjectMapper

struct NotificationModel : Mappable {
    var id : String = ""
    var datetime : String = ""
    var message : String = ""
    var title : String = ""
    var type : String = ""

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        id <- map["id"]
        datetime <- map["datetime"]
        message <- map["message"]
        title <- map["title"]
        type <- map["type"]
    }

}
