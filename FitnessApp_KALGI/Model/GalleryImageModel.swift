//
//  GalleryImageModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 12/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class GalleryImageModel: NSObject {
    
    var str_image:String! = ""
    var str_id:String! = ""
    
    init(dictUserInfo: NSDictionary) {
        str_image = String(format:dictUserInfo.value(forKey: "image") as! String)
        if let result_number = dictUserInfo.value(forKey: "id") as? NSNumber
        {
            str_id = "\(result_number)"
        }
    }
}
