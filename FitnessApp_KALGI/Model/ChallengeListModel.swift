//
//  ChallengeListModel.swift
//  FitnessApp
//
//  Created by Ashish on 12/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//


import UIKit

class ChallengeListModel: NSObject {
    
    var str_file:String! = ""
    var str_file_type:String! = ""
    var str_challenge_id:String! = ""
    var str_name:String! = ""
    var str_profile_picture:String! = ""
    var str_text:String! = ""
    var str_user_id:String! = ""
    var str_challenge_status:String! = "0"
    var image_video:UIImage? = nil
    var str_thumb_image:String! = ""
    var array_participant:NSMutableArray = NSMutableArray()
    var array_participant_requested:NSMutableArray = NSMutableArray()
    var array_participant_completed:NSMutableArray = NSMutableArray()
    var array_participant_accepted:NSMutableArray = NSMutableArray()
    var array_participant_pager:NSMutableArray = NSMutableArray()
    
    init(dictUserInfo: NSDictionary) {
        if let result_number = dictUserInfo.value(forKey: "file") as? String
        {
            str_file = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "file_type") as? String
        {
            str_file_type = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "thumbnail") as? String
        {
            str_thumb_image = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "challange_id") as? NSNumber
        {
            str_challenge_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "name") as? String
        {
            str_name = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "user_id") as? String
        {
            str_user_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "text") as? String
        {
            str_text = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "profile_image") as? String
        {
            str_profile_picture = "\(result_number)"
        }
        
        /*
         str_file = ""
         str_file_type = ""
         str_challenge_id = ""
         str_name = ""
         str_profile_picture = ""
         str_text = ""
         str_user_id = ""
         str_completed_url = ""
         str_challenge_status = ""
         */
        
        let obj_initialParticipant:ParticipantModel = ParticipantModel()
        obj_initialParticipant.str_file = self.str_file
        obj_initialParticipant.str_file_type = self.str_file_type
        obj_initialParticipant.str_challenge_id = self.str_challenge_id
        obj_initialParticipant.str_name = self.str_name
        obj_initialParticipant.str_profile_picture = self.str_profile_picture
        obj_initialParticipant.str_text = self.str_text
        obj_initialParticipant.str_user_id = self.str_user_id
        obj_initialParticipant.str_challenge_status = self.str_challenge_status
        obj_initialParticipant.str_thumb_image = self.str_thumb_image
        self.array_participant_pager.add(obj_initialParticipant)

        if let array:NSArray = dictUserInfo.value(forKey: "participant") as? NSArray {
            for item in array{
                let dict:NSDictionary = item as! NSDictionary
                let obj_bookingmodal:ParticipantModel = ParticipantModel(dictUserInfo: dict)
                if (DataUtil.appdelegate().objUserInfo?.str_trainer_user_id) == obj_bookingmodal.str_user_id{
                    self.str_challenge_status = obj_bookingmodal.str_challenge_status
                }
                obj_bookingmodal.str_file_type = self.str_file_type
                if obj_bookingmodal.str_challenge_status == "0"{
                    self.array_participant_requested.add(obj_bookingmodal)
                }else if obj_bookingmodal.str_challenge_status == "1"{
                    self.array_participant_accepted.add(obj_bookingmodal)
                }else{
                    self.array_participant_completed.add(obj_bookingmodal)
                    self.array_participant_pager.add(obj_bookingmodal)
                }
                self.array_participant.add(obj_bookingmodal)
            }
        }
        
    }
    
    override init() {
        str_file = ""
        str_file_type = ""
        str_challenge_id = ""
        str_name = ""
        str_profile_picture = ""
        str_text = ""
        str_user_id = ""
        str_challenge_status = "0"
        array_participant = NSMutableArray()
        array_participant_requested = NSMutableArray()
        array_participant_completed = NSMutableArray()
        array_participant_accepted = NSMutableArray()
    }
}
