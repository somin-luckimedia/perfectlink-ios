//
//  FriendDetailModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 16/03/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit

class FriendDetailModel: NSObject {
    
    var str_id:String! = ""
    var str_user_id:String! = ""
    var str_recipient_name:String! = ""
    var str_city:String! = ""
    var str_state:String! = ""
    var str_postal_code:String! = ""
    var str_country_code:String! = ""
    var str_phone_number:String! = ""
    var str_line1:String! = ""
    var str_line2:String! = ""
    var str_land_mark:String! = ""
    var str_age:String! = ""
    var str_request_status:String! = ""
    var str_request_id:String! = ""
    var str_is_friend : String! = ""
    
    var str_name:String! = ""
    var str_height:String! = ""
    var str_weight:String! = ""
    var str_about:String! = ""
    var str_email:String! = ""
    var str_profile_picture:String! = ""
    var str_gender:String! = ""
    var array_gyms: NSMutableArray  = NSMutableArray()
    var array_gyms_id: NSMutableArray  = NSMutableArray()

    var array_vistedpark: NSMutableArray  = NSMutableArray()
    var array_gallery: NSMutableArray  = NSMutableArray()
    var array_videos: NSMutableArray  = NSMutableArray()

    
    
    init(dictUserInfo: NSDictionary) {
        //age
        
        if let result_number = dictUserInfo.value(forKey: "request_id") as? NSNumber
        {
            str_request_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "is_friend") as? NSNumber
        {
            str_is_friend = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "request_status") as? NSNumber
        {
            str_request_status = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "age") as? NSNumber
        {
            str_age = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "gender") as? String
        {
            str_gender = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "height") as? String
        {
            str_height = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "weight") as? String
        {
            str_weight = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "name") as? String
        {
            str_name = "\(result_number)"
        }
        //gender
        if let result_number = dictUserInfo.value(forKey: "user_id") as? String
        {
            str_user_id = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "about") as? String
        {
            str_about = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "email") as? String
        {
            str_email = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "profile_picture") as? String
        {
            str_profile_picture = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "state") as? String
        {
            str_state = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "postal_code") as? String
        {
            str_postal_code = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "country_code") as? String
        {
            str_country_code = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "phone_number") as? String
        {
            str_phone_number = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "line1") as? String
        {
            str_line1 = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "line2") as? String
        {
            str_line2 = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "land_mark") as? String
        {
            str_land_mark = "\(result_number)"
        }
        
        if let array:NSArray = dictUserInfo["subscribed_gym"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                let obj_model:GymDetailModel = GymDetailModel.init(dictUserInfo: dict.value(forKey: "shipment_detail") as! NSDictionary)
                self.array_gyms.add(obj_model)
                self.array_gyms_id.add(dict)
            }
        }
        
        if let array:NSArray = dictUserInfo["park_info"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                self.array_vistedpark.add(dict)
            }
        }
        if let array:NSArray = dictUserInfo["gallery"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                self.array_gallery.add(dict)
            }
        }
        if let array:NSArray = dictUserInfo["videos"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                self.array_videos.add(dict)
            }
        }
        
        
        //full_description
    }
    
    
    override init() {
        str_id = ""
        str_gender = ""
        str_name = ""
        str_user_id = ""
        str_recipient_name = ""
        str_city = ""
        str_state = ""
        str_postal_code = ""
        str_country_code = ""
        str_phone_number = ""
        str_line1 = ""
        str_line2 = ""
        str_land_mark = ""
        str_height = "5.5"
        str_weight = "100"
    }
}

