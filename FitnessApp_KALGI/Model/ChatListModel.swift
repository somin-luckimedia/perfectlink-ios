import Foundation
import ObjectMapper

struct ChatListModel : Mappable {
    var user_id : String = ""
    var order_id : String = ""
	var service_id : String = ""
    var receiver_id : String = ""
	var service_name : String = ""
	var last_message : String = ""
	var unread_count : String = ""
	var strdate : String = ""
	var strtime : String = ""
    var message_id : String = ""

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
        user_id <- map["user_id"]
        order_id <- map["order_id"]
        service_id <- map["service_id"]
		receiver_id <- map["receiver_id"]
		service_name <- map["service_name"]
		last_message <- map["last_message"]
		unread_count <- map["unread_count"]
		strdate <- map["strdate"]
		strtime <- map["strtime"]
        message_id <- map["message_id"]
	}

}
