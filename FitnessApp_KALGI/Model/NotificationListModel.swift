//
//  NotificationListModel.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 22/10/20.
//

import UIKit

class NotificationListModel: NSObject {
    
    var str_id:String! = ""
    var str_user_id:String! = ""
    var str_friend_id:String! = ""
    var str_type:String! = ""
    var str_transaction_id:String! = ""
    var str_tracking_id:String! = ""
    var str_message : String! = ""
    var str_create_at :String! = ""
    
    
    
    
    
    init(dictUserInfo: NSDictionary) {
        if let result_number = dictUserInfo.value(forKey: "id") as? NSNumber
        {
            str_id = "\(result_number)"
        }
//       str_id = String(format:dictUserInfo.value(forKey: "id") as! String)
        str_user_id = String(format:dictUserInfo.value(forKey: "user_id") as! String)
        str_friend_id = String(format:dictUserInfo.value(forKey: "friend_id") as! String)
        str_type = String(format:dictUserInfo.value(forKey: "type") as! String)
        str_transaction_id = String(format:dictUserInfo.value(forKey: "transaction_id") as! String)
        str_tracking_id = String(format:dictUserInfo.value(forKey: "tracking_id") as! String)
        str_message = String(format:dictUserInfo.value(forKey: "message") as! String)
        str_create_at = String(format:dictUserInfo.value(forKey: "created_at") as! String)
    }
    
    override init() {
        str_id = ""
    }

}
