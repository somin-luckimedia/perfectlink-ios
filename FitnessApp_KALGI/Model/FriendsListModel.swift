//
//  FriendsListModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 23/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class FriendsListModel: NSObject {
    
    
    
    var str_id:String! = ""
    var str_user_id:String! = ""
    var str_recipient_name:String! = ""
    var str_city:String! = ""
    var str_state:String! = ""
    var str_postal_code:String! = ""
    var str_country_code:String! = ""
    var str_phone_number:String! = ""
    var str_line1:String! = ""
    var str_line2:String! = ""
    var str_land_mark:String! = ""

    var str_name:String! = ""
    var str_about:String! = ""
    var str_email:String! = ""
    var str_profile_picture:String! = ""

    var str_request_status:String! = ""
    var str_request_id:String! = ""
    var is_selected:Bool! = false
    
    var is_about : String = ""
    var gender : String  = ""
    var dob : String = ""
    var active_status : String = ""
    var email : String = ""
    var name : String = ""
    
    var profile_picture : String = ""


    /*
     //name
     //about
     //email
     //profile_picture
     
     
     "request_id" = 5;
     "request_status" = 3;
     
     */
    
    
    init(dictUserInfo: NSDictionary) {
        
        if let is_about1 = dictUserInfo.value(forKey: "is_about") as? String
        {
            is_about = "\(is_about1)"
        }
        
        if let active_status1 = dictUserInfo.value(forKey: "active_status") as? String
        {
            active_status = "\(active_status1)"
        }
        
        if let str_request_status1 = dictUserInfo.value(forKey: "request_status") as? Int
        {
            str_request_status = "\(str_request_status1)"
        }
        
        if let request_id1 = dictUserInfo.value(forKey: "request_id") as? Int
        {
            str_request_id = "\(request_id1)"
        }
        
        if let email1 = dictUserInfo.value(forKey: "email") as? String
        {
            email = "\(email1)"
        }
        
        if let name1 = dictUserInfo.value(forKey: "name") as? String
        {
            name = "\(name1)"
        }
        if let profile_picture1 = dictUserInfo.value(forKey: "profile_picture") as? String
        {
            profile_picture = "\(profile_picture1)"
        }
        if let dob1 = dictUserInfo.value(forKey: "dob") as? String
        {
            dob = "\(dob1)"
        }
        if let gender1 = dictUserInfo.value(forKey: "gender") as? String
        {
            gender = "\(gender1)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "name") as? String
        {
            str_name = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "user_id") as? String
        {
            str_user_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "about") as? String
        {
            str_about = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "email") as? String
        {
            str_email = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "profile_picture") as? String
        {
            str_profile_picture = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "state") as? String
        {
            str_state = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "postal_code") as? String
        {
            str_postal_code = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "country_code") as? String
        {
            str_country_code = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "phone_number") as? String
        {
            str_phone_number = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "line1") as? String
        {
            str_line1 = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "line2") as? String
        {
            str_line2 = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "land_mark") as? String
        {
            str_land_mark = "\(result_number)"
        }
        
        
        
        //full_description
    }
    
    
    override init() {
        is_about = ""
        active_status = ""
        email = ""
        name = ""
        profile_picture = ""
        dob = ""
        gender = ""
        str_user_id = ""
        str_id = ""
        str_request_status = ""
        str_user_id = ""
        str_recipient_name = ""
        str_city = ""
        str_state = ""
        str_postal_code = ""
        str_country_code = ""
        str_phone_number = ""
        str_line1 = ""
        str_line2 = ""
        str_land_mark = ""
    }
}



