//
//  VideosModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 12/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class VideosModel: NSObject {
    
    var str_title:String! = ""
    var str_id:String! = ""
    var str_video:String! = ""
    
    init(dictUserInfo: NSDictionary) {
        
        if let result_number = dictUserInfo.value(forKey: "title") as? String
        {
            str_title = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "video") as? String
        {
            str_video = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "id") as? NSNumber
        {
            str_id = "\(result_number)"
        }
    }
}
