//
//  LocationServiceModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 12/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class LocationServiceModel: NSObject {
    
    var str_id:String! = ""
    var str_lat:String! = ""
    var str_lon:String! = ""
    var str_park_address:String! = ""
    var str_park_id:String! = ""
    var str_park_image:String! = ""
    var str_park_name:String! = ""
    var str_park_type:String! = ""
    var is_selected:Bool! = false
    
    init(dictUserInfo: NSDictionary) {

        str_park_name = String(format:dictUserInfo.value(forKey: "park_name") as! String)
        str_park_image = String(format:dictUserInfo.value(forKey: "park_image") as! String)
        str_park_address = String(format:dictUserInfo.value(forKey: "park_address") as! String)
        
        str_lat = String(format:dictUserInfo.value(forKey: "lat") as! String)
        str_lon = String(format:dictUserInfo.value(forKey: "lon") as! String)
        
        
        if let result_number = dictUserInfo.value(forKey: "id") as? NSNumber
        {
            str_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "park_id") as? NSNumber
        {
            str_park_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "park_type") as? NSNumber
        {
            str_park_type = "\(result_number)"
        }

        
    }
    
    
    override init() {
        str_id = ""
        str_lat = ""
        str_lon = ""
        str_park_address = ""
        str_park_id = ""
        str_park_image = ""
        str_park_name = ""
        str_park_type = ""
        is_selected = false
        
    }
    
}


