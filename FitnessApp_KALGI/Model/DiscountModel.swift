//
//  DiscountModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 22/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit

class DiscountModel: NSObject {
    
    var str_coupon_code:String! = ""
    var str_coupon_title:String! = ""
    var str_brief_description:String! = ""
    var str_currency:String! = ""
    var str_amount:String! = ""
    var str_type:String! = ""
    var str_percentage:String! = ""
    var str_coupon_for:String! = ""
    var str_couponId:String! = ""
    
    init(dictUserInfo: NSDictionary) {
        //obj_bookingmodal.str_coupon_title!.replacingOccurrences(of: "%", with: "%%")
        str_coupon_title = (dictUserInfo.value(forKey: "title") as! String)
        str_coupon_code = String(format:dictUserInfo.value(forKey: "coupon_code") as! String)
        str_brief_description = String(format:dictUserInfo.value(forKey: "brief_description") as! String)
        str_type = String(format:dictUserInfo.value(forKey: "coupon_type") as! String)
       // str_coupon_for = String(format:dictUserInfo.value(forKey: "coupon_for") as! String)
        
        
        if let result_number = dictUserInfo.value(forKey: "amount") as? String
        {
            str_amount = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "percentage") as? String
        {
            str_percentage = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "currency") as? String
        {
            str_currency = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "id") as? NSNumber
        {
            str_couponId = "\(result_number)"
        }
        
        
    }
}


/*
 "id": 10,
 "title": "25% Off on trainer",
 "coupon_code": "TR25",
 "brief_description": "<p>This is test brief description<br></p>",
 "coupon_type": "percentage",
 "coupon_for": "trainer",
 "amount": null,
 "percentage": "25"
 */
