import Foundation
import ObjectMapper

struct LoginModel : Mappable {
	var user_id : String = ""
    var role_id : String = ""
	var username : String = ""
	var mobile : String = ""
	var image : String = ""
	var email : String = ""
	var otp : String = ""
    var fb_id : String = ""
    var address : String = ""
    var lat : String = ""
    var lng : String = ""

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
		user_id <- map["user_id"]
        role_id <- map["role_id"]
		username <- map["username"]
		mobile <- map["mobile"]
		image <- map["image"]
		email <- map["email"]
		otp <- map["otp"]
        address <- map["address"]

	} 
}
 
