 //
//  ParticipantModel.swift
//  FitnessApp
//
//  Created by Ashish on 12/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//


  import UIKit
 
 class ParticipantModel: NSObject {

    var str_file:String! = ""
    var str_file_type:String! = ""
    var str_challenge_id:String! = ""
    var str_name:String! = ""
    var str_profile_picture:String! = ""
    var str_text:String! = ""
    var str_user_id:String! = ""
    var str_completed_url:String! = ""
    var str_challenge_status:String! = ""
    var image_video:UIImage? = nil
    var str_thumb_image:String! = ""
    
    init(dictUserInfo: NSDictionary) {
        if let result_number = dictUserInfo.value(forKey: "file") as? String
        {
            str_file = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "file_type") as? String
        {
            str_file_type = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "thumbnail") as? String
        {
            str_thumb_image = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "challange_id") as? NSNumber
        {
            str_challenge_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "name") as? String
        {
            str_name = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "user_id") as? String
        {
            str_user_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "text") as? String
        {
            str_text = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "profile_image") as? String
        {
            str_profile_picture = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "challange_status") as? String
        {
            str_challenge_status = "\(result_number)"
        }

        if let result_number = dictUserInfo.value(forKey: "completed_url") as? String
        {
            if self.str_challenge_status == "2"{
            str_file = "\(result_number)"
                if let result_number = dictUserInfo.value(forKey: "completed_thumbnail") as? String
                {
                    str_thumb_image = "\(result_number)"
                }
            }
        }
        
        
        
    }
    
    override init() {
        str_file = ""
        str_file_type = ""
        str_challenge_id = ""
        str_name = ""
        str_profile_picture = ""
        str_text = ""
        str_user_id = ""
        str_completed_url = ""
        str_challenge_status = ""
        str_thumb_image = ""
    }
 }

