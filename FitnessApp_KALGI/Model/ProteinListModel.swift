//
//  ProteinListModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 20/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit

class ProteinListModel: NSObject {
    
    var str_id:String! = ""
    var str_product_id:String! = ""
    var str_product_title:String! = ""
    var str_cover_image:String! = ""
    var str_brief_description:String! = ""
    var str_star_rating:String! = ""
    var str_currency:String! = ""
    var str_amount:String! = "0"
    var str_per:String! = ""
    
    
    
    
    
    init(dictUserInfo: NSDictionary) {
        
        
        
        
        
        str_product_title = String(format:dictUserInfo.value(forKey: "product_title") as! String)
        str_cover_image = String(format:dictUserInfo.value(forKey: "cover_image") as! String)
        str_brief_description = String(format:dictUserInfo.value(forKey: "brief_description") as! String)
        
        
        if let result_number = dictUserInfo.value(forKey: "product_id") as? String
        {
            str_product_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "star_rating") as? NSNumber
        {
            str_star_rating = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "amount") as? String
        {
            str_amount = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "per") as? String
        {
            str_per = "\(result_number)"
        }
     
        if let result_number = dictUserInfo.value(forKey: "currency") as? String
        {
            str_currency = "\(result_number)"
        }

        
    }
}
