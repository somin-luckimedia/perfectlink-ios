//
//  HomeCountModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 25/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class HomeCountModel: NSObject {
    
    
    
    var str_gym_count:String = "0"
    var str_discount_coupon_count:String = "0"
    var str_meals_count:String = "0"
    var str_protein_count:String = "0"
    var str_accessories_count:String = "0"
    var str_trainer_count:String = "0"
    var str_free_run_count:String = "0"
    var str_home_service_count:String = "0"
    var str_park_link_count:String = "0"
    let array_home:[String] = ["+ Gyms", "+ Personal Trainer", "+ Parks","","+ Offer", "+ Meals", "+ Products", "+ Accessories"]
    var array_quantity: NSMutableArray  = NSMutableArray()
    
    init(dictUserInfo: NSDictionary) {
        
        if let result_number = dictUserInfo.value(forKey: "gym_count") as? NSNumber
        {
            str_gym_count = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "discount_coupon_count") as? NSNumber
        {
            str_discount_coupon_count = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "meals_count") as? NSNumber
        {
            str_meals_count = "\(result_number)"
        }
        
        /*
         {
         "gym_count": 3,
         "discount_coupon_count": 2,
         "meals_count": 10,
         "protein_count": 2,
         "accessories_count": 1,
         "trainer_count": 6,
         "tariners_type_count": {
         "free_run_count": 6,
         "home_service_count": 0,
         "park_link_count": 0
         }
         }
         */
        
        if let result_number = dictUserInfo.value(forKey: "protein_count") as? NSNumber
        {
            str_protein_count = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "accessories_count") as? NSNumber
        {
            str_accessories_count = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "trainer_count") as? NSNumber
        {
            str_trainer_count = "\(result_number)"
        }
        
        if let dict:NSDictionary = dictUserInfo["tariners_type_count"] as? NSDictionary{
            
            if let result_number = dict.value(forKey: "free_run_count") as? NSNumber
            {
                str_free_run_count = "\(result_number)"
            }
            
            if let result_number = dict.value(forKey: "home_service_count") as? NSNumber
            {
                str_home_service_count = "\(result_number)"
            }
            
            if let result_number = dict.value(forKey: "park_link_count") as? NSNumber
            {
                str_park_link_count = "\(result_number)"
            }
            
        }
        
        
        
        var index:Int = 0
        for str in array_home{
            var str_count:String = ""
            switch index {
            case 0:
                str_count = String(format: "%@%@", self.str_gym_count, str)
                break
            case 1:
                str_count = String(format: "%@%@", self.str_trainer_count, str)
                break
            case 2:
                str_count = String(format: "%@%@", self.str_park_link_count, str)
                
            case 3:
                str_count = String(format: "%@%@", "", str)
                break
                
            case 4:
                str_count = String(format: "%@%@", self.str_discount_coupon_count, str)
                break
            case 5:
                str_count = String(format: "%@%@", self.str_meals_count, str)
                break
            case 6:
                str_count = String(format: "%@%@", self.str_protein_count, str)
                break
            case 7:
                str_count = String(format: "%@%@", self.str_accessories_count, str)
                break
            default:
                break
            }
            array_quantity.add(str_count)
            index = index + 1
        }
        
        
        
        
    }
    
    override init() {
        str_gym_count = "0"
        str_discount_coupon_count = "0"
        str_meals_count = "0"
        str_protein_count = "0"
        str_accessories_count = "0"
        str_trainer_count = "0"
        str_free_run_count = "0"
        str_home_service_count = "0"
        str_park_link_count = "0"
        var index:Int = 0
        for str in array_home{
            var str_count:String = ""
            switch index {
            case 0:
                str_count = String(format: "%@%@", self.str_gym_count, str)
                break
            case 1:
                str_count = String(format: "%@%@", self.str_trainer_count, str)
                break
            case 2:
                str_count = String(format: "%@%@", self.str_park_link_count, str)
                
            case 3:
                str_count = String(format: "%@%@", "", str)
                break
            case 4:
                str_count = String(format: "%@%@", self.str_discount_coupon_count, str)
                break
            case 5:
                str_count = String(format: "%@%@", self.str_meals_count, str)
                break
            case 6:
                str_count = String(format: "%@%@", self.str_protein_count, str)
                break
            case 7:
                str_count = String(format: "%@%@", self.str_accessories_count, str)
                break
            default:
                break
            }
            array_quantity.add(str_count)
            index = index + 1
        }
    }
    
}
