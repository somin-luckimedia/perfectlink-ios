//
//  WalletModel.swift
//  FitnessApp
//
//  Created by Ashish on 10/05/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class WalletModel: NSObject {
    
    var str_order_id:String = ""
    var str_name:String = ""
    var str_user_id:String = ""
    var str_transaction_id:String = ""
    var str_order_status:String = ""
    var str_total_amount_paid:String = ""
    var str_payment_method:String = ""
    var str_quantity:String = ""
    
    
    var str_product_id:String = ""
    var str_product_type:String = ""
    var str_product_category:String = ""
    var str_product_title:String = ""
    var str_sub_title:String = ""
    var str_brief_description:String = ""
    var str_full_description:String = ""
    var str_cover_image:String = ""
    var str_profile_pic:String = ""
    var str_purchase_date:String = ""
    var str_transaction_date:String = ""

    var str_amount:String = ""
    var str_currency:String = ""
    var str_per:String = ""
    
    var str_start_date:String = ""
    var str_months:String = ""
    var str_end_date:String = ""
    var str_member_count:String = ""
    var str_preferred_time:String = ""
    var str_membership_type:String = ""
    var str_membership_amount:String = ""
    var str_discounted_amount:String = ""
    var str_valid_upto:String = ""
    var str_days:String = ""
    var str_wallet_status:String = ""
    
    
    
    
    init(dictUserInfo: NSDictionary) {
        
        
        if let result_number = dictUserInfo.value(forKey: "order_id") as? String
        {
            str_order_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "user_id") as? String
        {
            str_user_id = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "name") as? String
        {
            str_name = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "transaction_id") as? String
        {
            str_transaction_id = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "order_status") as? String
        {
            str_order_status = "\(result_number)"
        }
        
            if let result_number = dictUserInfo.value(forKey: "id") as? String
            {
                str_product_id = "\(result_number)"
            }
            if let result_number = dictUserInfo.value(forKey: "product_type") as? String
            {
                str_product_type = "\(result_number)"
            }
            if let result_number = dictUserInfo.value(forKey: "product_category") as? String
            {
                str_product_category = "\(result_number)"
            }
            
            if let result_number = dictUserInfo.value(forKey: "recived_from") as? String
            {
                str_product_title = "\(result_number)"
            }
            if let result_number = dictUserInfo.value(forKey: "recived_money_for") as? String
            {
                str_sub_title = "\(result_number)"
            }
            if let result_number = dictUserInfo.value(forKey: "brief_description") as? String
            {
                str_brief_description = "\(result_number)"
            }
            
            if let result_number = dictUserInfo.value(forKey: "full_description") as? String
            {
                str_full_description = "\(result_number)"
            }
            if let result_number = dictUserInfo.value(forKey: "cover_image") as? String
            {
                str_cover_image = "\(result_number)"
            }
        
        if let result_number = dictUserInfo.value(forKey: "profile_pic") as? String
        {
            str_profile_pic = "\(result_number)"
        }
            if let result_number = dictUserInfo.value(forKey: "final_amount") as? String
            {
                str_amount = "\(result_number)"
            }
        if let result_number = dictUserInfo.value(forKey: "total_amount_paid") as? String
        {
            str_total_amount_paid = "\(result_number)"
        }
            if let result_number = dictUserInfo.value(forKey: "currency") as? String
            {
                str_currency = "\(result_number)"
            }
            if let result_number = dictUserInfo.value(forKey: "per") as? String
            {
                str_per = "\(result_number)"
            }
            if let result_number = dictUserInfo.value(forKey: "start_date") as? String
            {
                str_start_date = "\(result_number)"
            }
            if let result_number = dictUserInfo.value(forKey: "months") as? NSNumber
            {
                str_months = "\(result_number)"
            }
        
        if let result_number = dictUserInfo.value(forKey: "wallet_status") as? NSNumber
        {
            str_wallet_status = "\(result_number)"
        }
        
        
        //wallet_status
            if let result_number = dictUserInfo.value(forKey: "days") as? String
            {
                str_days = "\(result_number)"
            }
        
        
            
            if let result_number = dictUserInfo.value(forKey: "end_date") as? String
            {
                str_end_date = "\(result_number)"
            }
            if let result_number = dictUserInfo.value(forKey: "member_count") as? NSNumber
            {
                str_member_count = "\(result_number)"
            }
            if let result_number = dictUserInfo.value(forKey: "preferred_time") as? String
            {
                str_preferred_time = "\(result_number)"
            }
        if let result_number = dictUserInfo.value(forKey: "purchase_date") as? String
        {
            str_purchase_date = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "transaction_date") as? String
        {
            str_transaction_date = "\(result_number)"
        }
            if let result_number = dictUserInfo.value(forKey: "membership_type") as? String
            {
                str_membership_type = "\(result_number)"
            }
        if let result_number = dictUserInfo.value(forKey: "membership_amount") as? String
        {
            str_membership_amount = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "discounted_amount") as? String
        {
            str_discounted_amount = "\(result_number)"
        }
        
            if let result_number = dictUserInfo.value(forKey: "valid_upto") as? String
            {
                str_valid_upto = "\(result_number)"
            }
    }
    
    override init() {
        str_order_id = ""
        str_transaction_id = ""
        str_order_status = ""
        str_total_amount_paid = ""
        str_payment_method = ""
        str_quantity = ""
    }
    
}
