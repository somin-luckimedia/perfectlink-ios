//
//  TrainerDetailModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 21/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

/*
 {
 STATUS = true;
 message = "Trainer Information.";
 response =     {
 about = "This is test1 jjj";
 achievements =         (
 {
 achievement = achievement2;
 id = 1;
 },
 {
 achievement = achievement3;
 id = 2;
 },
 {
 achievement = achievement3;
 id = 4;
 },
 {
 achievement = achievement3;
 id = 5;
 },
 {
 achievement = achievement3;
 id = 7;
 }
 );
 "active_status" = 1;
 age = 19;
 "country_code" = "+99";
 "cover_image" = "http://perfectlinkfitness.com/assets/images/gym_cover_image/157929096755808567.png";
 "device_type" = IOS;
 distance = 2;
 dob = "2001-12-18";
 email = "dasbeatty142@gmail.com";
 "email_verified_at" = "<null>";
 "galary_image" =         (
 {
 id = 2;
 image = "http://perfectlinkfitness.com/uploads/trainer_galary_image/1574798707775269630.png";
 title = "<null>";
 }
 );
 gender = Male;
 "group_membership_rate" = "11.99";
 height = "<null>";
 lat = 13;
 "location_service" =         (
 {
 id = 2;
 lat = "11.12";
 lon = "12.11";
 "park_address" = "New Delhi, Chattarpur, 110068";
 "park_id" = 5;
 "park_image" = "http://perfectlinkfitness.com/assets/images/parks/1578241960.png";
 "park_name" = "Subash Park";
 "park_type" = 1;
 }
 );
 lon = 14;
 name = AvniKumar;
 "phone_number" = 99999999;
 "preffered_date_time" = "2019-12-28";
 "profile_picture" = "157929096755808567.png";
 "single_membership_rate" = "12.99";
 "star_rating" = "4.5";
 "user_id" = 1Q3MAZCC4DXB;
 videos =         (
 {
 id = 2;
 title = "<null>";
 video = "http://perfectlinkfitness.com/uploads/trainer_video/15756551801970280970.mp4";
 },
 {
 id = 5;
 title = video1;
 video = "http://perfectlinkfitness.com/uploads/trainer_video/1575658904882792869.mp4";
 }
 );
 weight = "<null>";
 };
 }
 
 */


import UIKit

class TrainerDetailModel: NSObject {
    
    var name:String?
    var phone:String?
    var email:String?
    var gender:String?
    var userId:String?
    var address:String?
    var str_lattitude:String = ""
    var str_longitude:String = ""
    var profilePic:String = ""
    var str_countryCode:String = ""
    var str_height:String = ""
    var str_weight:String = ""
    var str_dob:String = ""
    var str_accessToken:String = ""
    var zipcode:String = ""
    var str_userType:String = ""
    var str_currency:String = ""
    var str_count:Int = 1
    var follower_count: Int = 0
    var video_count: Int = 0
    var courses_count: Int = 0
    // currency = "$";
    
    
    var str_about:String = ""
    var str_rating:String = ""
    var str_age:String = ""
    var str_coverImage:String = ""
    var str_single_rate:String = ""
    var str_gruop_rate:String = ""
    var str_trainer_user_id:String = ""
    var str_trainer_category:String = "0"
    var array_gallery_image: NSMutableArray  = NSMutableArray()
    var array_achievements: NSMutableArray  = NSMutableArray()
    var array_locations: NSMutableArray  = NSMutableArray()
    var array_videos: NSMutableArray  = NSMutableArray()
    
    
    
    
    
    
    init(dictUserInfo: NSDictionary) {
        name = dictUserInfo.value(forKey: "name") as? String
        phone = dictUserInfo.value(forKey: "phone_number") as? String
        email = dictUserInfo.value(forKey: "email") as? String
        gender = dictUserInfo.value(forKey: "gender") as? String
        userId = dictUserInfo.value(forKey: "id") as? String
        address = dictUserInfo.value(forKey: "address") as? String
        profilePic = (dictUserInfo.value(forKey: "profile_picture") as? String)!
        
        str_countryCode = (dictUserInfo.value(forKey: "country_code") as? String)!
        
        if let result_number = dictUserInfo.value(forKey: "height") as? String
        {
            str_height = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "currency") as? String
        {
            str_currency = "\(result_number)"
        }
        //currency
        if let result_number = dictUserInfo.value(forKey: "weight") as? String
        {
            str_weight = "\(result_number)"
        }
        
        //str_height = (dictUserInfo.value(forKey: "height") as? String)!
        //str_weight = (dictUserInfo.value(forKey: "weight") as? String)!
        //group_count
        str_dob = (dictUserInfo.value(forKey: "dob") as? String)!
        
        follower_count = Int(truncating: (dictUserInfo.value(forKey: "followers") as? NSNumber)!)
        video_count = Int(truncating: (dictUserInfo.value(forKey: "video_count") as? NSNumber)!)
        courses_count = Int(truncating: (dictUserInfo.value(forKey: "courses") as? NSNumber)!)
        
        if let result_number = dictUserInfo.value(forKey: "user_type") as? NSNumber
        {
            str_userType = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "group_count") as? NSNumber
        {
            str_count = result_number.intValue
        }
        
        
        if let result_number = dictUserInfo.value(forKey: "user_id") as? String
        {
            userId = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "user_id") as? String
        {
            str_trainer_user_id = "\(result_number)"
        }
        //user_id
        if let result_number = dictUserInfo.value(forKey: "about") as? String
        {
            str_about = "\(result_number)"
        }
        
        //trainer_category
        if let result_number = dictUserInfo.value(forKey: "star_rating") as? String
        {
            str_rating = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "trainer_category") as? NSNumber
        {
            str_trainer_category = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "age") as? NSNumber
        {
            str_age = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "cover_image") as? String
        {
            str_coverImage = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "single_membership_rate") as? String
        {
            str_single_rate = "\(result_number)"
            str_single_rate = str_single_rate.replacingOccurrences(of: "$", with: "")
            str_single_rate = str_single_rate.replacingOccurrences(of: " ", with: "")
//            print("Double value 1", str_single_rate.doubleValue())
        }
        if let result_number = dictUserInfo.value(forKey: "group_membership_rate") as? String
        {
            str_gruop_rate = "\(result_number)"
            str_gruop_rate = str_gruop_rate.replacingOccurrences(of: "$", with: "")
            str_gruop_rate = str_gruop_rate.replacingOccurrences(of: " ", with: "")
//            print("Double value 2", str_gruop_rate.doubleValue())
        }
        
        
        if let array:NSArray = dictUserInfo["achievements"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                let obj_model:AchievementsModel = AchievementsModel.init(dictUserInfo: dict)
                self.array_achievements.add(obj_model)
            }
        }
        
        if let array:NSArray = dictUserInfo["galary_image"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                let obj_model:GalleryImageModel = GalleryImageModel.init(dictUserInfo: dict)
                self.array_gallery_image.add(obj_model)
            }
        }
        
        
        
        if let array:NSArray = dictUserInfo["videos"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                let obj_model:VideosModel = VideosModel.init(dictUserInfo: dict)
                self.array_videos.add(obj_model)
            }
        }
        
        if let array:NSArray = dictUserInfo["location_service"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                let obj_model:LocationServiceModel = LocationServiceModel.init(dictUserInfo: dict)
                self.array_locations.add(obj_model)
            }
        }
    }
    
    override init() {
        name = ""
        phone = ""
        email = ""
        gender = ""
        userId = ""
        address = ""
        profilePic =  ""
    }
    
}
