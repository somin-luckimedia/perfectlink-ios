//
//  ChatModel.swift
//  Peps
//
//  Created by Jay on 08/12/19.
//  Copyright © 2019 JAY BHADJA. All rights reserved.
//

import Foundation
import ObjectMapper

struct ChatModel : Mappable {
    var from : String = ""
    var message : String = ""
    var type : String = ""
    var image : String = ""
    var strdatetime : String = ""

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        from <- map["from"]
        message <- map["message"]
        type <- map["type"]
        image <- map["image"]
        strdatetime <- map["strdatetime"]
    }

}

