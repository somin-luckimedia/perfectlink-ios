//
//  AccessoriesDetailModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 23/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit

class AccessoriesDetailModel: NSObject {
    var str_id:String! = ""
    var str_accessorie_id:String! = ""
    var str_accessorie_title:String! = ""
    var str_cover_image:String! = ""
    var str_brief_description:String! = ""
    var str_star_rating:String! = ""
    var str_currency:String! = ""
    var str_amount:String! = "0"
    var str_per:String! = ""
    
    var str_sub_title:String! = ""
    var str_full_description:String! = ""
    var str_type:String = ""
    var array_galleryImage: NSMutableArray  = NSMutableArray()
    
    
    var str_subscription_amount:String! = "0"
    var str_start_date:String! = ""
    var str_months:String! = "1"
    var str_end_date:String! = ""
    var str_member_count:String! = "1"
    var str_preferred_time:String! = ""
    var str_membership_type:String! = ""
    var str_valid_upto:String! = ""
    
    
/*
     "subscription_amount": "123",
     "start_date": "2020-02-24",
     "months": 3,
     "end_date": "2020-05-24",
     "member_count": 1,
     "preferred_time": null,
     "membership_type": "SINGLE",
     "valid_upto": "90 days"
     
     */
    
    init(dictUserInfo: NSDictionary) {
    
        
        if let result_number = dictUserInfo.value(forKey: "name") as? String
        {
            str_accessorie_title = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "accessorie_title") as? String{
        
            str_accessorie_title = "\(result_number)"
        
        }
        
        if let result_number = dictUserInfo.value(forKey: "product_title") as? String{
            
            str_accessorie_title = "\(result_number)"
            
        }
        
        if let result_number = dictUserInfo.value(forKey: "product_type") as? String{
            
            str_type = "\(result_number)"
            
        }
        
        str_cover_image = String(format:dictUserInfo.value(forKey: "cover_image") as! String)
        str_brief_description = String(format:dictUserInfo.value(forKey: "brief_description") as! String)
        
        
        if let result_number = dictUserInfo.value(forKey: "accessorie_id") as? String
        {
            str_accessorie_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "id") as? String
        {
            str_accessorie_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "star_rating") as? String
        {
            str_star_rating = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "amount") as? String
        {
            str_amount = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "per") as? String
        {
            str_per = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "currency") as? String
        {
            str_currency = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "sub_title") as? String
        {
            str_sub_title = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "full_description") as? String
        {
            str_full_description = "\(result_number)"
        }
        if let array:NSArray = dictUserInfo["galary_image"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                self.array_galleryImage.add(dict)
            }
        }
        //"name": "Silver gym",
        

        if let result_number = dictUserInfo.value(forKey: "subscription_amount") as? String
        {
            str_subscription_amount = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "start_date") as? String
        {
            str_start_date = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "months") as? NSNumber
        {
            str_months = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "end_date") as? String
        {
            str_end_date = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "member_count") as? NSNumber
        {
            str_member_count = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "preferred_time") as? String
        {
            str_preferred_time = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "membership_type") as? String
        {
            str_membership_type = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "valid_upto") as? String
        {
            str_valid_upto = "\(result_number)"
        }
        
        
    }
    
    override init() {
        str_id = ""
        str_accessorie_id = ""
        str_accessorie_title = ""
        str_cover_image = ""
        str_brief_description = ""
        str_star_rating = ""
        str_currency = ""
        str_amount = "0"
        str_per = ""
        str_sub_title = ""
        str_full_description = ""
        str_type = ""
        array_galleryImage  = NSMutableArray()
    }
}
