//
//  UserInfo.swift
//  WealthGenerators
//
//  Created by Mithilesh on 2/18/17.
//  Copyright © 2017 Mithilesh. All rights reserved.
//

import UIKit

class UserInfo: NSObject {
    
    var name:String?
    var phone:String?
    var email:String?
    var gender:String?
    var userId:String?
    var address:String?
    var str_lattitude:String = ""
    var str_longitude:String = ""
    var profilePic:String = ""
    var str_countryCode:String = ""
    var str_height:String = ""
    var str_weight:String = ""
    var str_dob:String = ""
    var str_accessToken:String = ""
    var zipcode:String = ""
    var str_userType:String = ""
    var devicetoken:String = ""
    var str_paypal_id:String = ""
    
    var str_about:String = ""
    var str_rating:String = ""
    var str_age:String = ""
    var str_coverImage:String = ""
    var str_single_rate:String = ""
    var str_gruop_rate:String = ""
    var str_trainer_user_id:String = ""
    var str_trainer_category:String = "0"
    var array_gallery_image: NSMutableArray  = NSMutableArray()
    var array_achievements: NSMutableArray  = NSMutableArray()
    var array_locations: NSMutableArray  = NSMutableArray()
    var array_home_locations: NSMutableArray  = NSMutableArray()
    var array_videos: NSMutableArray  = NSMutableArray()
    var str_user_name:String = ""
    var str_first_name:String?
    var str_last_name:String?

    
    //user_name
    
   
    init(dictUserInfo: NSDictionary) {
        name = dictUserInfo.value(forKey: "name") as? String
        phone = dictUserInfo.value(forKey: "phone_number") as? String
        email = dictUserInfo.value(forKey: "email") as? String
         gender = dictUserInfo.value(forKey: "gender") as? String
         userId = dictUserInfo.value(forKey: "user_id") as? String
         address = dictUserInfo.value(forKey: "address") as? String
        str_first_name = dictUserInfo.value(forKey: "firstname") as? String
        str_last_name = dictUserInfo.value(forKey: "lastname") as? String

        
        //devicetoken = dictUserInfo.value(forKey: "token") as? String
        
        if let result_number = dictUserInfo.value(forKey: "device_token") as? String
        {
            devicetoken = "\(result_number)"
            let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate
            objAppDelgate1.str_devicetoken = devicetoken
        }
        
        //paypal_id
        
        if let result_number = dictUserInfo.value(forKey: "paypal_id") as? String
        {
            str_paypal_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "profile_picture") as? String
        {
            profilePic = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "user_name") as? String
        {
            str_user_name = "\(result_number)"
        }
        
        str_countryCode = (dictUserInfo.value(forKey: "country_code") as? String)!
        
        if let result_number = dictUserInfo.value(forKey: "height") as? String
        {
            str_height = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "weight") as? String
        {
            str_weight = "\(result_number)"
        }
        
        //str_height = (dictUserInfo.value(forKey: "height") as? String)!
        //str_weight = (dictUserInfo.value(forKey: "weight") as? String)!
        str_dob = (dictUserInfo.value(forKey: "dob") as? String)!
        
        if let result_number = dictUserInfo.value(forKey: "user_type") as? NSNumber
        {
            str_userType = "\(result_number)"
        }
        
        
        if let result_number = dictUserInfo.value(forKey: "user_id") as? String
        {
            userId = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "user_id") as? String
        {
            str_trainer_user_id = "\(result_number)"
        }
        //user_id
        if let result_number = dictUserInfo.value(forKey: "about") as? String
        {
            str_about = "\(result_number)"
        }
        
        //trainer_category
        if let result_number = dictUserInfo.value(forKey: "star_rating") as? String
        {
            str_rating = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "trainer_category") as? NSNumber
        {
            str_trainer_category = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "age") as? NSNumber
        {
            str_age = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "cover_image") as? String
        {
            str_coverImage = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "single_membership_rate") as? String
        {
            str_single_rate = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "group_membership_rate") as? String
        {
            str_gruop_rate = "\(result_number)"
        }
        
        
        if let array:NSArray = dictUserInfo["achievements"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                let obj_model:AchievementsModel = AchievementsModel.init(dictUserInfo: dict)
                self.array_achievements.add(obj_model)
            }
        }
        
        if let array:NSArray = dictUserInfo["galary_image"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                let obj_model:GalleryImageModel = GalleryImageModel.init(dictUserInfo: dict)
                self.array_gallery_image.add(obj_model)
            }
        }
        
        
        
        if let array:NSArray = dictUserInfo["videos"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                let obj_model:VideosModel = VideosModel.init(dictUserInfo: dict)
                self.array_videos.add(obj_model)
            }
        }
        
        if let array:NSArray = dictUserInfo["location_service"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                let obj_model:LocationServiceModel = LocationServiceModel.init(dictUserInfo: dict)
                self.array_locations.add(obj_model)
            }
        }
        
        
        if let array:NSArray = dictUserInfo["home_service_locations"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                let obj_model:HomeServiceLocation = HomeServiceLocation.init(dictUserInfo: dict)
                self.array_home_locations.add(obj_model)
            }
        }
        
        //HomeServiceLocation
        
        //self.userId = "9EAD587QHOWO"
        
    }
    
    override init() {
        name = ""
        phone = ""
        email = ""
        gender = ""
        userId = ""
        address = ""
        profilePic =  ""
        str_user_name = ""
        str_first_name = ""
        str_last_name = ""
    }
    
}
/*
 {
 "access_token" = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjlkZTE5NTI0NDRmMTgwMzk0OGU1NjFkNTEzYzU1OGQxMTA0YjQwYTYzOTg2NTMxNDE1NWU1Nzc2MzkzMDQxNmEzYTQwZjEwYjkwNWYzZmFhIn0.eyJhdWQiOiIxIiwianRpIjoiOWRlMTk1MjQ0NGYxODAzOTQ4ZTU2MWQ1MTNjNTU4ZDExMDRiNDBhNjM5ODY1MzE0MTU1ZTU3NzYzOTMwNDE2YTNhNDBmMTBiOTA1ZjNmYWEiLCJpYXQiOjE1NzExNjQxNTIsIm5iZiI6MTU3MTE2NDE1MiwiZXhwIjoxNjAyNzg2NTUyLCJzdWIiOiIzMiIsInNjb3BlcyI6W119.t96cvjWpage3-eqgDXHKAZ8lTRkvpuObAbTplUTqC75RCrlhjOZER3Z1dRXan0qcEVg8yol5cH3GUzgIAd6Z4QNl09ujlIx_5g7zVujGXQLrJjjg6oj7hc2PFDJSO8tMhb1WVlZH38oWB2Apn0t-C8U8rvyQFKyf0QKNxadVN4SM7rpEVLIRwkoBxPimnXA7Yb8AWF62vayzM8pyODEi5M23cBmCmwBPKrEhR71S1kPO3x2cOmceoP0KcvNmZnoLU2QmLn21mjt7K8S4FTzTYQ__2D306G_hB3YsUOEFv1nBRYyHpskAP4bjsrvtHewBSSU9-OWhdBN2HUk5rChCLq0boCxDIREwoXP5pPp_2ONaWkTonsZdCRmxoaiVuoI4jYZb6w9NS7yl9HQBy21wlm0h3urf-uxBKrySAKTRQtRZqypjftmAGFoqIH2uCNlN4q2HpTclw45zEhRcm2_3I0oLos8nmhtSp6kUhHTyFDzdvjbqzFPZ3E12II5ko_1vt2MCQ2How_Tg_rzOMQbjX3mKGFIMzYTpPv5OavbwgIxJxk8UBHIWZAx_A_uy5JM47pH3x31gxjIsXtTd1pp90tpjrtnGBc0O42KF_kYzojtZnrNgHe8KtRG9Pjy1duIuYSHgVXczhCpjnJkMAagTiAylsyQh7qmUakeVnfIbXgk";
 message = "";
 response =     {
 about = "<null>";
 address = "<null>";
 "country_code" = "+91";
 "created_at" = "2019-10-15 18:29:12";
 "device_token" = "<null>";
 "device_type" = iPhone;
 dob = "2011-10-15";
 email = "a@a.com";
 gender = Male;
 height = 123;
 id = 32;
 lat = "<null>";
 lon = "<null>";
 name = gyyt;
 "phone_number" = 9958186291;
 "profile_picture" = "http://perfectlinkfitness.com/uploads/profile_pic/1571164152197583760.jpeg";
 "updated_at" = "2019-10-15 18:29:12";
 "user_type" = 2;
 weight = 13;
 };
 status = true;
 }
 
 
 */
