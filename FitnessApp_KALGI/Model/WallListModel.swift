//
//  WallListModel.swift
//  FitnessApp
//
//  Created by Ashish on 05/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit

class WallListModel: NSObject {
    
    var str_file:String! = ""
    var str_file_type:String! = ""
    var str_thumb_image:String! = ""
    var str_id:String! = ""
    var str_is_liked:String! = ""
    var str_is_unlike:String! = ""
    var str_likes:String! = ""
    var str_name:String! = ""
    var str_profile_picture:String! = ""
    var str_text:String! = ""
    var str_unlikes:String! = ""
    var str_user_id:String! = ""
    var str_posted_at:String! = ""
    var image_video:UIImage? = nil
    var str_comment_count:String! = ""
    var str_share_count:String! = ""
    var str_is_user : String! = ""

    init(dictUserInfo: NSDictionary) {
        
        if let result_number = dictUserInfo.value(forKey: "file") as? String
        {
            str_file = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "file_type") as? String
        {
            str_file_type = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "thumbnail") as? String
        {
            str_thumb_image = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "posted_at") as? NSNumber
        {
            str_posted_at = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "id") as? NSNumber
        {
            str_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "name") as? String
        {
            str_name = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "user_id") as? String
        {
            str_user_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "text") as? String
        {
            str_text = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "profile_picture") as? String
        {
            str_profile_picture = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "is_liked") as? NSNumber
        {
            str_is_liked = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "is_user") as? String
        {
            str_is_user = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "is_unlike") as? NSNumber
        {
            str_is_unlike = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "unlikes") as? NSNumber
        {
            str_unlikes = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "likes") as? NSNumber
        {
            str_likes = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "comment_count") as? NSNumber
        {
            str_comment_count = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "share_count") as? NSNumber
        {
            str_share_count = "\(result_number)"
        }
        
        
        //likes
    }
}
