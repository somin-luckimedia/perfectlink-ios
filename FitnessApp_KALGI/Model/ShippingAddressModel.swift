//
//  ShippingAddressModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 26/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit



class ShippingAddressModel: NSObject {
    
    var str_id:String! = ""
    var str_user_id:String! = ""
    var str_recipient_name:String! = ""
    var str_city:String! = ""
    var str_state:String! = ""
    var str_postal_code:String! = ""
    var str_country_code:String! = ""
    var str_phone_number:String! = ""
    var str_line1:String! = ""
    var str_line2:String! = ""
    var str_land_mark:String! = ""
    var str_default_shipping1:Int = 0

    
    
    
    init(dictUserInfo: NSDictionary) {
        
        if let result_number = dictUserInfo.value(forKey: "id") as? NSNumber
        {
            str_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "user_id") as? String
        {
            str_user_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "recipient_name") as? String
        {
            str_recipient_name = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "city") as? String
        {
            str_city = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "state") as? String
        {
            str_state = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "postal_code") as? String
        {
            str_postal_code = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "country_code") as? String
        {
            str_country_code = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "phone_number") as? String
        {
            str_phone_number = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "line1") as? String
        {
            str_line1 = "\(result_number)"
        }
        
        if let str_default_shipping = dictUserInfo.value(forKey: "default_shipping") as? Int
        {
            str_default_shipping1 = str_default_shipping
        }
        
        if let result_number = dictUserInfo.value(forKey: "line2") as? String
        {
            str_line2 = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "land_mark") as? String
        {
            str_land_mark = "\(result_number)"
        }
        
        
        
        //full_description
    }
    
    
    override init() {
        str_id = ""
        str_user_id = ""
        str_recipient_name = ""
        str_city = ""
        str_state = ""
        str_postal_code = ""
        str_country_code = ""
        str_phone_number = ""
        str_line1 = ""
        str_line2 = ""
        str_land_mark = ""
        str_default_shipping1 = 0
    }
}
