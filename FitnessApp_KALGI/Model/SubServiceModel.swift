import Foundation
import ObjectMapper

struct SubServiceModel : Mappable {
	var id : String = ""
	var name : String = ""
	var cost : String = ""
	var unit : String = ""
	var isjob : String = ""
    var isQuotation : Int = 0
    var sub_image : String = ""

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		name <- map["name"]
		cost <- map["cost"]
		unit <- map["unit"]
		isjob <- map["isjob"]
        sub_image <- map["sub_image"]
	}

}
