//
//  GymDetailModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 14/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit

class GymDetailModel: NSObject {
    
    var str_gym_id:String! = ""
    var str_gym_title:String! = ""
    var str_email:String! = ""
    var str_address:String! = ""
    var str_cover_image:String! = ""
    var str_brief_description:String! = ""
    var str_full_description:String! = ""
    var str_star_rating:String! = ""
    var str_distance:String! = ""
    var str_open_close:String! = ""
    var str_curency:String! = ""
    var array_galleryImage: NSMutableArray  = NSMutableArray()
    var array_equipments: NSMutableArray  = NSMutableArray()
    var array_subscription: NSMutableArray  = NSMutableArray()
    var array_userList: NSMutableArray  = NSMutableArray()
    var str_lat:String! = ""
    var str_long:String! = ""
    
    //currency
    
    
    
    init(dictUserInfo: NSDictionary) {
       
        
        if let result_number = dictUserInfo.value(forKey: "product_title") as? String
        {
            str_gym_title = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "gym_title") as? String
        {
            str_gym_title = "\(result_number)"
        }
        str_cover_image = String(format:dictUserInfo.value(forKey: "cover_image") as! String)
        str_brief_description = String(format:dictUserInfo.value(forKey: "brief_description") as! String)
        str_full_description = String(format:dictUserInfo.value(forKey: "full_description") as! String)
        
        
        if let result_number = dictUserInfo.value(forKey: "email") as? String
        {
            str_email = "\(result_number)"
        }
        
        
        
        
        if let result_number = dictUserInfo.value(forKey: "lat") as? String
        {
            str_lat = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "lon") as? String
        {
            str_long = "\(result_number)"
        }
        
        
        if let result_number = dictUserInfo.value(forKey: "address") as? String
        {
            str_address = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "gym_id") as? String
        {
            str_gym_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "star_rating") as? String
        {
            str_star_rating = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "distance") as? NSNumber
        {
            str_distance = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "currency") as? String
        {
            str_curency = "\(result_number)"
        }
        
        
        if let array:NSArray = dictUserInfo["galary_image"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                self.array_galleryImage.add(dict)
            }
        }
        
        if let array:NSArray = dictUserInfo["gym_equipment"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                self.array_equipments.add(dict)
            }
        }
        
        if let array:NSArray = dictUserInfo["subscription"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                self.array_subscription.add(dict)
            }
        }
        if let array:NSArray = dictUserInfo["user_list"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                self.array_userList.add(dict)
            }
        }
        
        
        if let result_number = dictUserInfo.value(forKey: "open_close") as? String
        {
            str_open_close = "\(result_number)"
        }
        
        //subscription
        //full_description
    }
    
    
    override init() {
        str_gym_id = ""
        str_gym_title = ""
        str_email = ""
        str_address = ""
        str_cover_image = ""
        str_brief_description = ""
        str_full_description = ""
        str_star_rating = ""
        str_distance = ""
        str_open_close = ""
    }
}


/*
 
 {
 "gym_id": "OSJGJGBASJUB",
 "id": 39,
 "user_id": "OSJGJGBASJUB",
 "gym_category": "1",
 "gym_title": "Silver gym",
 "email": "silver@gmail.com",
 "country_code": "91",
 "contact_number": "8826523624",
 "address": "Chattarpur, Chattarpur",
 "cover_image": "http://perfectlinkfitness.com/assets/images/gym_cover_image/1574792422.jpg",
 "video": null,
 "brief_description": "sadfasdf",
 "full_description": "sdfasdf",
 "lat": "11",
 "lon": "12",
 "active_status": 1,
 "created_at": "2019-11-26 06:20:22",
 "updated_at": null,
 "subscription": {
 "subscription_title": null,
 "subscription_amount": null,
 "subscription_month": null,
 "subscription_details": null
 },
 "star_rating": 4.5,
 "distance": 2,
 "open_close": "12:00Pm-1:00Pm",
 "galary_image": [
 {
 "id": 5,
 "image": "http://perfectlinkfitness.com/assets/images/gym_galary_image/15747924220.png"
 },
 {
 "id": 6,
 "image": "http://perfectlinkfitness.com/assets/images/gym_galary_image/15747924221.png"
 },
 {
 "id": 7,
 "image": "http://perfectlinkfitness.com/assets/images/gym_galary_image/15747924222.jpg"
 },
 {
 "id": 8,
 "image": "http://perfectlinkfitness.com/assets/images/gym_galary_image/15747924223.png"
 },
 {
 "id": 9,
 "image": "http://perfectlinkfitness.com/assets/images/gym_galary_image/15747924224.png"
 }
 ],
 "gym_equipment": [
 {
 "id": 1,
 "user_id": "OSJGJGBASJUB",
 "equipment_id": 1,
 "quantity": null,
 "created_at": "2019-11-07 03:54:46",
 "updated_at": null,
 "equipment_type": "Upper Body",
 "equipment_name": "Dumble",
 "equipment_image": "http://perfectlinkfitness.com/assets/images/equipments/1573098886.jpg",
 "description": "Dumble test",
 "status": 1
 },
 {
 "id": 2,
 "user_id": "OSJGJGBASJUB",
 "equipment_id": 2,
 "quantity": null,
 "created_at": "2019-11-11 05:39:25",
 "updated_at": null,
 "equipment_type": "Upper Body",
 "equipment_name": "Squat Rack",
 "equipment_image": "http://perfectlinkfitness.com/assets/images/equipments/1573450765.png",
 "description": "In fitness and strength training, the squat exercise trains your full body.  All serious strength training regiments should incorporate the squat station gym equipment.",
 "status": 1
 }
 ]
 }
 
 
 */
