/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct HistoryModel : Mappable {
	var cart_count : Int = 0
	var notification_count : String = ""
	var services : [Services_History] = []

	init?(map: Map) {
	}

	mutating func mapping(map: Map) {

		cart_count <- map["cart_count"]
		notification_count <- map["notification_count"]
		services <- map["services"]


	}
}

struct Services_History : Mappable {
    var order_id : String = ""
    var address : String = ""
    var daddress : String = ""

    var cart_id : String = ""
    var description : String = ""
    var ladder : String = ""
    var lat : String = ""
    var lng : String = ""
    var dlat : String = ""
    var dlng : String = ""
    var service_id : String = ""
    var service_name : String = ""
    var strDate : String = ""
    var strTime : String = ""
    var sub_service : [subService_History] = []
    var total : Int = 0
    var order_status : String = ""
    var payment_type : String = ""
    var w_amount : String = ""
    var cash_amount : String = ""
    var partner_id : String = ""
    var partner_name : String = ""
    var partner_phone : String = ""
    var partner_image : String = ""

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        order_id <- map["order_id"]
        order_status <- map["order_status"]
        address <- map["address"]
        daddress <- map["daddress"]

        cart_id <- map["cart_id"]
        description <- map["description"]
        ladder <- map["ladder"]
        lat <- map["lat"]
        lng <- map["lng"]
        dlat <- map["dlat"]
        dlng <- map["dlng"]
        service_id <- map["service_id"]
        service_name <- map["service_name"]
        strDate <- map["strDate"]
        strTime <- map["strTime"]
        sub_service <- map["sub_service"]
        total <- map["total"]
        payment_type <- map["payment_type"]
        w_amount <- map["w_amount"]
        cash_amount <- map["cash_amount"]
        partner_id <- map["partner_id"]
        partner_name <- map["partner_name"]
        partner_phone <- map["partner_phone"]
        partner_image <- map["partner_image"]

    }
}

struct subService_History : Mappable {
    var cost : String = ""
    var id : String = ""
    var isJob : String = ""
    var name : String = ""
    var unit : String = ""


    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        cost <- map["cost"]
        id <- map["id"]
        isJob <- map["isJob"]
        name <- map["name"]
        unit <- map["unit"]

    }
}


