//
//  ProtienDetailModel.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 22/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit

class ProtienDetailModel: NSObject {
    
    var str_id:String! = ""
    var str_product_id:String! = ""
    var str_product_title:String! = ""
    var str_cover_image:String! = ""
    var str_brief_description:String! = ""
    var str_star_rating:String! = "0"
    var str_currency:String! = ""
    var str_amount:String! = "0"
    var str_per:String! = ""
    
    var str_sub_title:String! = ""
    var str_full_description:String! = ""
    var array_galleryImage: NSMutableArray  = NSMutableArray()
    
    
    init(dictUserInfo: NSDictionary) {
        
        
        
        
        
        str_product_title = String(format:dictUserInfo.value(forKey: "product_title") as! String)
        str_cover_image = String(format:dictUserInfo.value(forKey: "cover_image") as! String)
        str_brief_description = String(format:dictUserInfo.value(forKey: "brief_description") as! String)
        
        
        if let result_number = dictUserInfo.value(forKey: "product_id") as? String
        {
            str_product_id = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "star_rating") as? String
        {
            str_star_rating = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "amount") as? String
        {
            str_amount = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "per") as? String
        {
            str_per = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "currency") as? String
        {
            str_currency = "\(result_number)"
        }
        if let result_number = dictUserInfo.value(forKey: "sub_title") as? String
        {
            str_sub_title = "\(result_number)"
        }
        
        if let result_number = dictUserInfo.value(forKey: "full_description") as? String
        {
            str_full_description = "\(result_number)"
        }
        if let array:NSArray = dictUserInfo["galary_image"] as? NSArray{
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                self.array_galleryImage.add(dict)
            }
        }
        
    }
    
    override init() {
        str_id = ""
        str_product_id = ""
        str_product_title = ""
        str_cover_image = ""
        str_brief_description = ""
        str_star_rating = ""
        str_currency = ""
        str_amount = ""
        str_per = ""
        str_sub_title = ""
        str_full_description = ""
        array_galleryImage  = NSMutableArray()
    }
}
