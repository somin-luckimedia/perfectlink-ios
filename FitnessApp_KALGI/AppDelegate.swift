//
//  AppDelegate.swift
//  FitnessApp_KALGI
//
//  Created by Yogesh on 2020-09-29.
//

import UIKit
import IQKeyboardManagerSwift
import CoreLocation
import FBSDKLoginKit
import GoogleSignIn
import UserNotifications
import Firebase
import FirebaseFirestore
import FirebaseDatabase
import FirebaseMessaging
import FirebaseInstanceID
import SideMenuController_Swift4
import SquareInAppPaymentsSDK
import GoogleMaps
import AppTrackingTransparency



@main
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate,CLLocationManagerDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var navigationController:UINavigationController!
    var wakeTime : Date = Date()
    var str_lattitude:String = "40.67541876073912"
    var str_longitude:String = "-73.67198573730674"
    var array_seats:NSMutableArray = NSMutableArray()
    var array_height:[String] = []
    var array_height_final:[String] = []
    var array_weight:[String] = []
    let array_category:[String] = ["Free Run", "Home Service", "Park Link","All"]
    var array_member_plan:[String] = []
    var isFromSignup:Bool = false
    var is_social:Bool = false
    var str_devicetoken: String! = "1234"
    var locationManager: CLLocationManager!
    var objUserInfo : UserInfo?
    var sideMenuIndex : Int = 0
    var array_issues:NSMutableArray = NSMutableArray()
    var objHomeCount:HomeCountModel = HomeCountModel()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        SQIPInAppPaymentsSDK.squareApplicationID = "sq0idp-UPOiSOI_bZvmvS2BXts-PQ"
        Database.database().isPersistenceEnabled = true
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        Messaging.messaging().delegate = self as MessagingDelegate
        Messaging.messaging().isAutoInitEnabled = true
        GMSServices.provideAPIKey("AIzaSyCeH2KyXewo3VCH1ymMyktmSJGBj8JjTkg")
        db.settings = settings
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        let isUserLogedIn = UserDefaults.standard.bool(forKey: "LogedIn")
        if isUserLogedIn{
            objUserInfo?.userId = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
            objUserInfo?.str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        }
        
        UNUserNotificationCenter.current().delegate = self
                if #available(iOS 10.0, *) {
                let center = UNUserNotificationCenter.current()
                center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                    guard granted else{
                        return
                    }
                    // Enable or disable features based on authorization.
                }
                    UNUserNotificationCenter.current().getNotificationSettings { (setting) in
                        print(setting)
                        guard setting.authorizationStatus == .authorized else{
                           UNUserNotificationCenter.current().delegate = self
                           DispatchQueue.main.async {
                               application.registerForRemoteNotifications()
                           }
                           return
                        }
                       UNUserNotificationCenter.current().delegate = self
                        DispatchQueue.main.async {
                            application.registerForRemoteNotifications()
                        }
                    }
                
                 }else{
                    UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
                     application.registerForRemoteNotifications()
                    
                }
        self.isAppTransperncy()
        
//        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu")
//        SideMenuController.preferences.drawing.sidePanelPosition = .underCenterPanelLeft
//        SideMenuController.preferences.drawing.sidePanelWidth = 300
//        SideMenuController.preferences.drawing.centerPanelShadow = true
//        SideMenuController.preferences.animating.statusBarBehaviour = .horizontalPan
//        SideMenuController.preferences.animating.transitionAnimator = FadeAnimator.self

        let objViewController = SignInVC(nibName: "SignInVC", bundle: nil)
        navigationController = UINavigationController(rootViewController: objViewController)
        window?.rootViewController = navigationController
        window!.makeKeyAndVisible()
        self.createHeightArray()
        self.createWeightArray()
        self.createHeightFinalArray()
        self.creatememberplanArray()
        self.isAuthorizedtoGetUserLocation()
        return true
    }
    
    func isAppTransperncy() {
        ATTrackingManager.requestTrackingAuthorization { status in
            switch status {
            case .notDetermined:
                break
                
            case .restricted:
                break
                
            case .denied:
                break
             
            case .authorized:
                print("Allow")
            @unknown default:
                break
            }
        
        }
    }
    
    func isAuthorizedtoGetUserLocation() {
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        else if  CLLocationManager.authorizationStatus() != .authorizedAlways     {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    

    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            print("User allowed us to access location")
            if let locValue: CLLocationCoordinate2D = manager.location?.coordinate{
                self.str_lattitude = "\(locValue.latitude)"
                self.str_longitude = "\(locValue.longitude)"
                print("locations = \(locValue.latitude) \(locValue.longitude)")

            }else
            {
                self.str_lattitude = "40.67541876073912"
                self.str_longitude = "-73.67198573730674"
            }
            //do whatever init activities here.
        }
        else   {
            print("User allowed us to access location")
            //let locValuefinal:CLLocationCoordinate2D
            if let locValue: CLLocationCoordinate2D = manager.location?.coordinate{
                self.str_lattitude = "\(locValue.latitude)"
                self.str_longitude = "\(locValue.longitude)"
                print("locations = \(locValue.latitude) \(locValue.longitude)")}
            else
            {
                self.str_lattitude = ""
                self.str_longitude = ""
            }
        }
    }


    //this method is called by the framework on         locationManager.requestLocation();
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Did location updates is called")
        let userLocation :CLLocation = locations[0] as CLLocation
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        self.str_lattitude = "\(userLocation.coordinate.latitude)"
        self.str_longitude = "\(userLocation.coordinate.longitude)"

        manager.stopUpdatingLocation()
        let notificationCenter = NotificationCenter.default
        notificationCenter.post(name: NSNotification.Name(rawValue: "LocationUpdated"), object: nil)




        //store the user location here to firebase or somewhere
    }
    public func loadSidePanel(objVC:UINavigationController)
    {
        let middleColor:UIColor = UIColor(red: 143/255, green: 147/255, blue: 38/255, alpha: 1.0)
        let endColor:UIColor = UIColor(red: 162/255, green: 106/255, blue: 13/255, alpha: 1.0)
        let startColor:UIColor = UIColor(red: 10/255, green: 135/255, blue: 5/255, alpha: 1.0)
        
        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu_icon")
        let sideMenuViewController = SideMenuController()
        // let homeVC : UIViewController!
        let homeVC : EditProfileVC = EditProfileVC(nibName: "EditProfileVC", bundle: nil)
        let nav = UINavigationController(rootViewController: homeVC)
        nav.navigationBar.barTintColor = UIColor.black
        nav.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let sideController : SideMenuController1 = SideMenuController1(nibName: "SideMenuController", bundle: nil)
        sideMenuViewController.embed(sideViewController: sideController)
        sideMenuViewController.embed(centerViewController: nav)
        objVC.navigationBar.tintColor = UIColor.white
       
        UINavigationBar.appearance().barTintColor = UIColor.black
        UINavigationBar.appearance().tintColor = UIColor.white
       // UINavigationBar.appearance().setBackgroundImage(self.setGradientBackground(colors: [startColor, middleColor, endColor], startPoint: .centerLeft, endPoint: .centerRight), for: .default)
        objVC.pushViewController(sideMenuViewController, animated: true)
        objVC.isNavigationBarHidden = true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        let userref = Database.database().reference(withPath:"online")
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let onlineRef = userref.child(str_userid)
        onlineRef.removeValue { (error, _ ) in
            if error != nil {
                print(error?.localizedDescription)
            }
        }
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        let userref = Database.database().reference(withPath:"online")
        let dict : NSMutableDictionary = [:]
        dict.setValue(DataUtil.appdelegate().objUserInfo?.userId, forKey: "userId")
        dict.setValue(DataUtil.appdelegate().objUserInfo?.profilePic, forKey: "pic")
        dict.setValue(DataUtil.appdelegate().objUserInfo?.name, forKey: "name")
        let currentUser = userref.child((DataUtil.appdelegate().objUserInfo?.userId)!)
        currentUser.setValue(dict)
    }
    
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data){
        Messaging.messaging().apnsToken = deviceToken
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                self.str_devicetoken = "\(result.token)"
            }
        }

    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
    }
     
     func userNotificationCenter(_ center: UNUserNotificationCenter,
                                 didReceive response: UNNotificationResponse,
                                 withCompletionHandler completionHandler: @escaping () -> Void) {
         let userInfo = response.notification.request.content.userInfo
         // Print message ID.
         
         
         // Print full message.
         print("userInfo",userInfo)
//        print(userInfo["flag"] as! )
        flagNotification = Int(userInfo["flag"] as! String) ?? 0
        let notificationCenter = NotificationCenter.default
        notificationCenter.post(name: NSNotification.Name(rawValue: "pushNotification"), object: nil)

         completionHandler()
     }
    
    public func loadSidePanelWithScan(objVC:UINavigationController)
    {
        
        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu")
        let sideMenuViewController = SideMenuController()
        // let homeVC : UIViewController!
        //HomePageTrainerVC
        
         let sideController : SideMenuController1 = SideMenuController1(nibName: "SideMenuController", bundle: nil)
     if (DataUtil.appdelegate().objUserInfo?.str_userType) == "2" {
            let homeVC : SettingVC = SettingVC(nibName: "SettingVC", bundle: nil)
//            homeVC.isFromSignup = self.isFromSignup
            let nav = UINavigationController(rootViewController: homeVC)
        nav.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            sideMenuViewController.embed(sideViewController: sideController)
            sideMenuViewController.embed(centerViewController: nav)
        }
//        else {
//            let homeVC : TrainerHomeVC = TrainerHomeVC(nibName: "TrainerHomeVC", bundle: nil)
//            let nav = UINavigationController(rootViewController: homeVC)
//        nav.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//            sideMenuViewController.embed(sideViewController: sideController)
//            sideMenuViewController.embed(centerViewController: nav)
//        }
      
        
      
        objVC.navigationBar.tintColor = UIColor.white
        
        //UINavigationBar.appearance().setBackgroundImage(self.setGradientBackground(colors: [startColor, middleColor, endColor], startPoint: .centerLeft, endPoint: .centerRight), for: .default)
        objVC.pushViewController(sideMenuViewController, animated: true)
        objVC.isNavigationBarHidden = true
    }
    
    func createHeightArray(){
        for i in 3..<8 {
            var k:Int = 13
            if i == 7{
                k = 6
            }
            for j in 0..<k {
                let str:String = String(format: "%ld'%ld\"", i,j)
                array_height.append(str)
            }
        }
    }
    
    func createHeightFinalArray(){
        for i in 3..<8 {
            var k:Int = 13
            if i == 7{
                k = 6
            }
            for j in 0..<k {
                let str:String = String(format: "%ld.%ld", i,j)
                array_height_final.append(str)
            }
        }
    }
    
    func createWeightArray(){
        for i in 10..<401 {
            let str:String = String(format: "%ld", i)
            array_weight.append(str)
        }
    }
    
    func creatememberplanArray(){
        for i in 1..<181 {
            var str:String = ""
            if i == 1{
                str = String(format: "%ld Day", i)
            }else{
                str = String(format: "%ld Days", i)
            }
            array_member_plan.append(str)
        }
    }




}

extension CAGradientLayer {
    
    enum Point {
        case topRight, topLeft
        case centerLeft, centerRight
        case centerTop, centerBottom
        case bottomRight, bottomLeft
        case custion(point: CGPoint)
        
        var point: CGPoint {
            switch self {
            case .topRight: return CGPoint(x: 1, y: 0)
            case .topLeft: return CGPoint(x: 0, y: 0)
            case .centerLeft: return CGPoint(x: 0, y: 0.5)
            case .centerRight: return CGPoint(x: 1, y: 0.5)
            case .centerTop: return CGPoint(x: 0.5, y: 0)
            case .centerBottom: return CGPoint(x: 0.5, y: 1)
            case .bottomRight: return CGPoint(x: 1, y: 1)
            case .bottomLeft: return CGPoint(x: 0, y: 1)
            case .custion(let point): return point
            }
        }
    }
    
    convenience init(frame: CGRect, colors: [UIColor], startPoint: CGPoint, endPoint: CGPoint) {
        self.init()
        self.frame = frame
        self.colors = colors.map { $0.cgColor }
        self.startPoint = startPoint
        self.endPoint = endPoint
    }
    
    convenience init(frame: CGRect, colors: [UIColor], startPoint: Point, endPoint: Point) {
        self.init(frame: frame, colors: colors, startPoint: startPoint.point, endPoint: endPoint.point)
}

    func createGradientImage() -> UIImage? {
        defer { UIGraphicsEndImageContext() }
        UIGraphicsBeginImageContext(bounds.size)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        render(in: context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

