//
//  PhoneDetailVC.swift
//  Spolu
//
//  Created by Ashish Mishra on 31/03/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire
import SearchTextField

class PhoneDetailVC: UIViewController, IQActionSheetPickerViewDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    
    var str_phone:String = ""
    var dict_phone:NSDictionary!
    var dict_selectedModel:NSDictionary!
    var dict_selectedColor:NSDictionary!
    var dict_selectedIssue:NSDictionary!
    
    
    @IBOutlet weak var imageview1: UIImageView!
    @IBOutlet weak var imageview2: UIImageView!
    @IBOutlet weak var imageview3: UIImageView!

    var selectedImage1:UIImage?
    var selectedImage2:UIImage?
    var selectedImage3:UIImage?

    var array_models:NSMutableArray = NSMutableArray()
    var array_colors:NSMutableArray = NSMutableArray()
    @IBOutlet weak var textfield_issues: SearchTextField!
    
    @IBOutlet weak var lbl_model: UILabel!
    @IBOutlet weak var textfield_model: UITextField!
    @IBOutlet weak var textfield_color: UITextField!
    
    @IBOutlet weak var textfield_issues1: UITextField!
    @IBOutlet weak var imageView_BG: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Select phone details"
        self.addLeftMenuButtonWithImage()
        self.lbl_model.text = dict_phone["name"] as? String
        self.array_models.removeAllObjects()
        let array:NSArray = dict_phone["models"] as! NSArray
        for item in array {
            let dict:NSDictionary =  item as! NSDictionary
            self.array_models.add(dict)
        }
        self.imageView_BG.image = DataUtil.setGradientBackgroundImage()
        self.addLeftMenuButtonWithImage()
        self.configureSimpleSearchTextField()
    }


    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    fileprivate func configureSimpleSearchTextField() {
        textfield_issues.startVisibleWithoutInteraction = false
        let array: NSArray = (DataUtil.appdelegate().array_issues.value(forKey: "issue") as! NSArray)
        textfield_issues.filterStrings(array as! [String])
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func select_model_clicked(_ sender: UIButton){
        let array: NSArray = (array_models.value(forKey: "name") as! NSArray)
        var groups = [[String]]()
        groups.append(array as! [String])
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = sender.tag;
        picker.show()
    }
    
    
    @IBAction func select_color_clicked(_ sender: UIButton) {
       let array: NSArray = (array_colors.value(forKey: "color") as! NSArray)
        var groups = [[String]]()
        groups.append(array as! [String])
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = sender.tag;
        picker.show()
    }
    
    @IBAction func select_issue_clicked(_ sender: UIButton) {
        let array: NSArray = (DataUtil.appdelegate().array_issues.value(forKey: "issue") as! NSArray)
        var groups = [[String]]()
        groups.append(array as! [String])
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = sender.tag;
        picker.show()
    }
    
    
    
    
    
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView, didSelectTitlesAtIndexes indexes: [NSNumber]) {
        let index = indexes[0] as! NSInteger
        
        if pickerView.tag == 101{
            dict_selectedModel = array_models[index] as? NSDictionary
            textfield_model.text = dict_selectedModel["name"] as? String
            self.array_colors.removeAllObjects()
            let array:NSArray = dict_selectedModel["colors"] as! NSArray
            for item in array {
                let dict:NSDictionary =  item as! NSDictionary
                self.array_colors.add(dict)
            }
            dict_selectedColor = nil
            textfield_color.text  = ""
        }else if pickerView.tag == 102{
            dict_selectedColor = array_colors[index] as? NSDictionary
            textfield_color.text  = dict_selectedColor["color"] as? String
        }else{
            dict_selectedIssue = DataUtil.appdelegate().array_issues[index] as? NSDictionary
            textfield_issues1.text  = dict_selectedIssue["issue"] as? String
            //textfield_issues1.text = title
        }
    }
    
    
    @IBAction func attachment_btn_clicked(_ sender: UIButton) {
        let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert) in
            self.openCamera(tag: sender.tag)
            
        }))
        alert.addAction(UIAlertAction(title:"Photo Library", style: .default, handler: { (alert) in
            self.opneGallery(tag: sender.tag)
        }))
        
        alert.addAction(UIAlertAction(title:"Cancel", style: .default, handler: { (alert) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func opneGallery(tag:Int){
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            imagePicker.view.tag = tag
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera(tag:Int){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            imagePicker.view.tag = tag
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    
    
    
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var recievedImage: UIImage?
        if let editedImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")]   as? UIImage {
            recievedImage = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerOriginalImage")] as? UIImage {
            recievedImage = originalImage
        }
        if let selectedImage = recievedImage {
            if picker.view.tag == 101{
                selectedImage1 = selectedImage
                imageview1.image = selectedImage
            }else if picker.view.tag == 102{
                 selectedImage2 = selectedImage
                imageview2.image = selectedImage
            }else{
                 selectedImage3 = selectedImage
                imageview3.image = selectedImage
            }
        }
        
        
        
        /*if let selectedImages = selectedImage {
         
         
         if let data = UIImageJPEGRepresentation(selectedImages,1) {
         let parameters: Parameters = [
         "access_token" : "YourToken"
         ]
         // You can change your image name here, i use NSURL image and convert into string
         let imageURL:NSURL = info[UIImagePickerControllerReferenceURL] as! NSURL
         let fileName = imageURL.absoluteString
         // Start Alamofire
         
         }
         
         
         
         }*/
        
        
        
        
        
        self.dismiss(animated: true, completion: nil)
        
        
    }

}
