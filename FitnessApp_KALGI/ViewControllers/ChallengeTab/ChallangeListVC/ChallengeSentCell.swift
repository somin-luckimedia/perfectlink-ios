//
//  ChallengeSentCell.swift
//  FitnessApp
//
//  Created by Ashish on 12/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit
import AVFoundation
import Photos
import AVKit


protocol ChallengeSentCellDelegate:class {
    func play_btn_clicked(cell: ChallengeSentCell, btn: UIButton)
    func preview_btn_clicked(cell: ChallengeSentCell, btn: UIButton)
    func replace_array(obj: ChallengeListModel, index: Int)
}


class ChallengeSentCell: UITableViewCell {
    
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var imageView_profile_thumb: UIImageView!
    @IBOutlet weak var imageView_file: UIImageView!
    @IBOutlet weak var view_video: UIView!
    @IBOutlet weak var imageview_thumb: UIImageView!
    
    @IBOutlet weak var view_thumb: UIView!
    @IBOutlet weak var constraint_view_thumb: NSLayoutConstraint!
    
    var rowIndex:Int = 0
    weak var delegate:ChallengeSentCellDelegate?
    var obj_challengeModel:ChallengeListModel? = nil
    var selectedIndex:Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_bookingmodal:ChallengeListModel, rowIndex:Int){
        self.rowIndex = rowIndex
        //lbl_name.text = obj_bookingmodal.str_name
        lbl_description.text =  String(format: "%@", obj_bookingmodal.str_text!)
        var url = URL(string: obj_bookingmodal.str_profile_picture)
        
        imageView_profile_thumb.setImageFrom(url)//(stringImageUrl: obj_bookingmodal.str_profile_picture)
        self.imageView_file.image = nil
        url = URL(string: obj_bookingmodal.str_file)

        imageView_file.setImageFrom(url)//(stringImageUrl: obj_bookingmodal.str_file)
        
        if obj_bookingmodal.str_file_type == "VIDEO"{
            self.view_video.isHidden = false
            self.imageView_file.isHidden = true
            let url = URL(string: obj_bookingmodal.str_thumb_image)
            imageview_thumb.setImageFrom(url)//(stringImageUrl: obj_bookingmodal.str_thumb_image)
            //self.imageview_thumb.image = nil
            //let url = URL(string: obj_bookingmodal.str_file)
            
            /*if self.obj_challengeModel?.image_video != nil{
               self.imageview_thumb.image = self.obj_challengeModel?.image_video
            }
            else{
            self.getThumbnailImageFromVideoUrl(url: url!) { (thumbImage) in
                self.imageview_thumb.image = thumbImage
                if thumbImage != nil{
                    if self.obj_challengeModel?.image_video == nil{
                        self.obj_challengeModel?.image_video = thumbImage
                        self.delegate?.replace_array(obj: self.obj_challengeModel!, index: self.rowIndex)
                    }
                }
            }
            }*/
        }else{
            self.view_video.isHidden = true
            self.imageView_file.isHidden = false
        }
        
        if (DataUtil.appdelegate().objUserInfo?.str_trainer_user_id) == obj_bookingmodal.str_user_id{
            self.view_thumb.isHidden = true
            self.constraint_view_thumb.constant = 0
        }else{
                self.view_thumb.isHidden = false
                self.constraint_view_thumb.constant = 40
            //lbl_description.text = String(format: "%@ would like to challenge you to complete this activity.", obj_bookingmodal.str_name)
        }
        self.obj_challengeModel = obj_bookingmodal
    }
    
    
    
    
    @IBAction func play_btn_clicked(_ sender: UIButton) {
        self.delegate?.play_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func preview_btn_clicked(_ sender: UIButton) {
        self.delegate?.preview_btn_clicked(cell: self, btn: sender)
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
    
}
