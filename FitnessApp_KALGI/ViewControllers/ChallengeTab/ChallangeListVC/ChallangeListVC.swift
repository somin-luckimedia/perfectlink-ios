//
//  ChallangeListVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 11/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import Photos
import AVKit
import Firebase

class ChallangeListVC: UIViewController, UITableViewDelegate, UITableViewDataSource, ChallengeReceivedCellDelegate, ChallengeSentCellDelegate {
    func replace_array_received(obj: ChallengeListModel, index: Int) {
        self.array_received.replaceObject(at: index, with: obj)
    }
    
    @IBOutlet var btnMenu: UIButton!
    func replace_array(obj: ChallengeListModel, index: Int) {
        if self.selectedIndex == 0{
            self.array_completed.replaceObject(at: index, with: obj)
        }else{
            self.array_requested.replaceObject(at: index, with: obj)
        }
    }
    
    func play_btn_clicked(cell: ChallengeSentCell, btn: UIButton) {
    }
    
    func preview_btn_clicked(cell: ChallengeSentCell, btn: UIButton) {
        
    }
    
    
    
    @IBOutlet var tblOnline: UITableView!
    var arrOnlineFriends : NSMutableArray = []
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")
    
    @IBOutlet var btnReceived: UIButton!
    @IBOutlet var btnCompleted: UIButton!
    @IBOutlet var btnRequested: UIButton!
    var selectedIndex:Int = 0
    fileprivate var array_completed:NSMutableArray = NSMutableArray()
    fileprivate var array_received:NSMutableArray = NSMutableArray()
    fileprivate var array_requested:NSMutableArray = NSMutableArray()
    @IBOutlet weak var imageView_BG: UIImageView!
    @IBOutlet weak var tableview_bookings: UITableView!
    let transiton = SlideInTransition()
    var topView: UIView?
    
    @IBAction func btnMakeChallengeTapped(_ sender: UIButton) {
        let obj_phonelistvc:MakeChallengeVC = MakeChallengeVC(nibName: "MakeChallengeVC", bundle: nil)
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
    
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        flagChallenge = true
        
        let rootVC : SideMenuController1 = SideMenuController1(nibName: "SideMenuController", bundle: nil)
        if DataUtil.appdelegate().objUserInfo?.str_userType == "2" {
            rootVC.didTapMenuType = { menuType in
                self.transitionToNew(menuType)
            }
        }
        else {
            rootVC.didTapMenuType1 = {menuTypeTrainer in
                self.transitionTrainer(menuTypeTrainer)
            }
        }
        rootVC.modalPresentationStyle = .overCurrentContext
        rootVC.transitioningDelegate = self
        present(rootVC, animated: true)
    }
    
    func transitionToNew(_ menuType: MenuType) {
        switch menuType {
        case .FitnessWall:
            let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Challenges:
            let rootVC : OrderHistoryVC = OrderHistoryVC(nibName: "OrderHistoryVC", bundle: nil)
            self.present(rootVC, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.dismissControllerAnimated()
            }
        case .Chat:
            let rootVC : FriendListVC = FriendListVC(nibName: "FriendListVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.dismissControllerAnimated()
            }
        //            self.present(rootVC, animated: true, completion: nil)
        //            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
        //                self.dismissControllerAnimated()
        //            }
        default:
            break
        }
    }
    
    func transitionTrainer(_ menuType : MenuTypeTrainer) {
        switch menuType {
        case .FitnessWall:
            let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
            
        case .MyOrders:
            let rootVC : OrderHistoryVC = OrderHistoryVC(nibName: "OrderHistoryVC", bundle: nil)
            self.present(rootVC, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Friends:
            let rootVC : FriendListVC = FriendListVC(nibName: "FriendListVC", bundle: nil)
            //            self.present(rootVC, animated: true, completion: nil)
            //            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            //                self.dismissControllerAnimated()
            //            }
            // self.present(rootVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Achievements:
            let rootVC : AchievementsVC = AchievementsVC(nibName: "AchievementsVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .MyTips:
            let rootVC : FreeTipsVCViewController = FreeTipsVCViewController(nibName: "FreeTipsVCViewController", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
            
        case .Wallet:
            let rootVC : WalletNewVC = WalletNewVC(nibName: "WalletNewVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
            
        default:
            break
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        //  self.title = "CHALLENGE"
        tableview_bookings.delegate  = self as UITableViewDelegate
        tableview_bookings.dataSource = self as UITableViewDataSource
        //        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        //self.getChallengeCompleted()
        self.navigationController?.navigationBar.isHidden = true
        //        self.addRightMenuButtonWithImage()
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        
        // if DataUtil.appdelegate().objUserInfo?.str_userType == "2" {
        btnMenu.setImage(UIImage(named: "menu"), for: .normal)
        //        }
        //        else {
        //            btnMenu.setImage(UIImage(named: "Back_icon"), for: .normal)
        //
        //        }
        
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
                //                if snapshot.childrenCount == 1 {
                //                    self.constWidthTbl.constant = 0
                ////                    self.constLeadingColl.constant = 20
                //                    self.lblOnline.isHidden = true
                //                }
                //                else {
                //                    self.constWidthTbl.constant = 60
                ////                    self.constLeadingColl.constant = 0
                //                    self.lblOnline.isHidden = false
                //                }
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }
                
                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
        }
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(dismissChallenge),
                                       name: NSNotification.Name(rawValue: "DismissChallenge"),
                                       object: nil)
        
    }
    
    @objc func dismissChallenge(){
        //  flagChallenge = false
        self.dismissControllerAnimated()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.getMyBookings()
        super.viewWillAppear(true)
        self.dismissControllerAnimated()
        flagMenu = true
        
        if self.selectedIndex == 0{
            self.getChallengeCompleted()
        }else if self.selectedIndex == 1{
            self.getChallengeReceived()
        }else{
            self.getChallengeRequested()
        }
    }
    
    @IBAction func btnNotificationTapped(_ sender: UIButton) {
        let obj_NotificationVC:NotificationVC = NotificationVC(nibName: "NotificationVC", bundle: nil)
        self.navigationController?.pushViewController(obj_NotificationVC, animated: true)
        
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "add_icon"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.add_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func add_btn_clicked(){
        let obj_phonelistvc:MakeChallengeVC = MakeChallengeVC(nibName: "MakeChallengeVC", bundle: nil)
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
    
    
    @IBAction func header_btn_clicked(_ sender: UIButton) {
        let imageview1:UIImageView = self.view.viewWithTag(201) as! UIImageView
        let imageview2:UIImageView = self.view.viewWithTag(202) as! UIImageView
        let imageview3:UIImageView = self.view.viewWithTag(203) as! UIImageView
        let btn1:UIButton = self.view.viewWithTag(101) as! UIButton
        let btn2:UIButton = self.view.viewWithTag(102) as! UIButton
        let btn3:UIButton = self.view.viewWithTag(103) as! UIButton
        btn1.setTitleColor(.lightGray, for: .normal)
        btn2.setTitleColor(.lightGray, for: .normal)
        btn3.setTitleColor(.lightGray, for: .normal)
        sender.setTitleColor(.white, for: .normal)
        
        let imageview_selected:UIImageView = self.view.viewWithTag(sender.tag + 100) as! UIImageView
        imageview1.backgroundColor = UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        
        imageview2.backgroundColor = UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        imageview3.backgroundColor = UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        imageview_selected.backgroundColor = UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
        if sender.tag == 101{
            self.selectedIndex = 0
            self.getChallengeCompleted()
        }else if sender.tag == 102{
            self.selectedIndex = 1
            self.getChallengeReceived()
        }else{
            self.selectedIndex = 2
            self.getChallengeRequested()
        }
        
    }
    
    func play_btn_clicked(cell: ChallengeReceivedCell, btn: UIButton) {
        
    }
    
    func preview_btn_clicked(cell: ChallengeReceivedCell, btn: UIButton) {
        
    }
    
    func accept_btn_clicked(cell: ChallengeReceivedCell, btn: UIButton) {
        let obj_requestmodal:ChallengeListModel = array_received[cell.rowIndex] as! ChallengeListModel
        if obj_requestmodal.str_challenge_status == "0"{
            self.accept_challenge(obj_challengModel: obj_requestmodal, rowIndex: cell.rowIndex)
        }else{
            let obj_phonelistvc:MakeChallengeVC = MakeChallengeVC(nibName: "MakeChallengeVC", bundle: nil)
            obj_phonelistvc.is_fromComplete = true
            obj_phonelistvc.obj_challengeModel = obj_requestmodal
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
    }
    
    func accept_challenge(obj_challengModel:ChallengeListModel, rowIndex:Int){
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam: Parameters = ["user_id":str_userid, "challange_id":obj_challengModel.str_challenge_id!]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.accept_challange, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                obj_challengModel.str_challenge_status = "1"
                self.array_received.replaceObject(at: rowIndex, with: obj_challengModel)
                self.tableview_bookings.reloadData()
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    func getChallengeCompleted(){
        self.array_completed.removeAllObjects()
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        //let str_userid:String = "ERUYCBI8EV5P"
        let postParam: Parameters = ["user_id":str_userid]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.challange_completed_list, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let dict : NSDictionary = arr as! NSDictionary
                    
                    let array:NSArray = dict["challanges"] as! NSArray
                    let array_trainerCount : NSArray = dict["challange_count"] as! NSArray
                    let dict_trainerCount = array_trainerCount[0] as! NSDictionary
                    let completed  = dict_trainerCount["completed"] as! Int
                    let completed_ : String = "COMPLETED (\(completed))"
                    self.btnCompleted.setTitle(completed_, for: .normal)
                    
                    let received = dict_trainerCount["received"] as! Int
                    let received_ : String = "RECEIVED (\(received))"
                    self.btnReceived.setTitle(received_, for: .normal)
                    
                    let requested = dict_trainerCount["requested"] as! Int
                    let requested_ : String = "REQUESTED (\(requested))"
                    self.btnRequested.setTitle(requested_, for: .normal)
                    
                    //                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:ChallengeListModel = ChallengeListModel(dictUserInfo: dict)
                        self.array_completed.add(obj_bookingmodal)
                    }
                    self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func getChallengeReceived(){
        self.array_received.removeAllObjects()
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam: Parameters = ["user_id":str_userid]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.challange_received_list, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let dict : NSDictionary = arr as! NSDictionary
                    
                    let array:NSArray = dict["challanges"] as! NSArray
                    let array_trainerCount : NSArray = dict["challange_count"] as! NSArray
                    let dict_trainerCount = array_trainerCount[0] as! NSDictionary
                    let completed  = dict_trainerCount["completed"] as! Int
                    let completed_ : String = "COMPLETED (\(completed))"
                    self.btnCompleted.setTitle(completed_, for: .normal)
                    
                    let received = dict_trainerCount["received"] as! Int
                    let received_ : String = "RECEIVED (\(received))"
                    self.btnReceived.setTitle(received_, for: .normal)
                    
                    let requested = dict_trainerCount["requested"] as! Int
                    let requested_ : String = "REQUESTED (\(requested))"
                    self.btnRequested.setTitle(requested_, for: .normal)
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:ChallengeListModel = ChallengeListModel(dictUserInfo: dict)
                        self.array_received.add(obj_bookingmodal)
                    }
                    self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    func getChallengeRequested(){
        self.array_requested.removeAllObjects()
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        //let str_userid:String = "ERUYCBI8EV5P"
        let postParam: Parameters = ["user_id":str_userid]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.challange_sent_list, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    //                    let array:NSArray = arr as! NSArray
                    let dict : NSDictionary = arr as! NSDictionary
                    let array:NSArray = dict["challanges"] as! NSArray
                    let array_trainerCount : NSArray = dict["challange_count"] as! NSArray
                    let dict_trainerCount = array_trainerCount[0] as! NSDictionary
                    let completed  = dict_trainerCount["completed"] as! Int
                    let completed_ : String = "COMPLETED (\(completed))"
                    self.btnCompleted.setTitle(completed_, for: .normal)
                    
                    let received = dict_trainerCount["received"] as! Int
                    let received_ : String = "RECEIVED (\(received))"
                    self.btnReceived.setTitle(received_, for: .normal)
                    
                    let requested = dict_trainerCount["requested"] as! Int
                    let requested_ : String = "REQUESTED (\(requested))"
                    self.btnRequested.setTitle(requested_, for: .normal)
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:ChallengeListModel = ChallengeListModel(dictUserInfo: dict)
                        self.array_requested.add(obj_bookingmodal)
                    }
                    self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == tblOnline {
            return 60.0
        }
        else {
            return 277
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if tableView == tblOnline {
            return arrOnlineFriends.count
        }
        else {
            //return self.array_mybookings.count
            if selectedIndex == 0{
                return self.array_completed.count
            }else if selectedIndex == 1{
                return self.array_received.count
            }else{
                return self.array_requested.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblOnline {
            let identifier = "onlineTVCTableViewCell"
            var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
            
            if logincell == nil {
                tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
            }
            logincell?.selectionStyle = .none
            //        logincell?.layer.cornerRadius = 25
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            let url = URL(string: dict["pic"] as! String)
            logincell?.imgOnlineFriends.setImageFrom(url)
            logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
            
            return logincell!
        }
        else {
            if selectedIndex == 1{
                let identifier = "ChallengeReceivedCell"
                var logincell: ChallengeReceivedCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? ChallengeReceivedCell
                if logincell == nil {
                    tableView.register(UINib(nibName: "ChallengeReceivedCell", bundle: nil), forCellReuseIdentifier: identifier)
                    logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? ChallengeReceivedCell)!
                }
                
                var obj_requestmodal:ChallengeListModel?  = nil
                if selectedIndex == 0{
                    obj_requestmodal = array_completed[indexPath.row] as? ChallengeListModel
                }else if selectedIndex == 1{
                    obj_requestmodal = array_received[indexPath.row] as? ChallengeListModel
                }else{
                    obj_requestmodal = array_requested[indexPath.row] as? ChallengeListModel
                }
                logincell?.delegate = self
                logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal!, rowIndex: indexPath.row)
                return logincell!
            }
            
            
            
            let identifier = "ChallengeSentCell"
            var logincell: ChallengeSentCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? ChallengeSentCell
            if logincell == nil {
                tableView.register(UINib(nibName: "ChallengeSentCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? ChallengeSentCell)!
            }
            var obj_requestmodal:ChallengeListModel?  = nil
            if selectedIndex == 0{
                obj_requestmodal = array_completed[indexPath.row] as? ChallengeListModel
            }else if selectedIndex == 1{
                obj_requestmodal = array_received[indexPath.row] as? ChallengeListModel
            }else{
                obj_requestmodal = array_requested[indexPath.row] as? ChallengeListModel
            }
            logincell?.selectedIndex = self.selectedIndex
            logincell?.delegate = self
            logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal!, rowIndex: indexPath.row)
            return logincell!
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblOnline {
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            flagPark = false
            flagGym = false
            flagGallery = false
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            obj_phonelistvc.str_user_id = dict["userId"] as! String
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
        else {
            var obj_requestmodal:ChallengeListModel?  = nil
            if selectedIndex == 0{
                obj_requestmodal = array_completed[indexPath.row] as? ChallengeListModel
            }else if selectedIndex == 1{
                obj_requestmodal = array_received[indexPath.row] as? ChallengeListModel
            }else{
                obj_requestmodal = array_requested[indexPath.row] as? ChallengeListModel
            }
            let obj_phonelistvc:ChallengeDetailVC = ChallengeDetailVC(nibName: "ChallengeDetailVC", bundle: nil)
            if obj_requestmodal?.image_video != nil{
                let obj_participantModel:ParticipantModel = obj_requestmodal?.array_participant_pager[0] as! ParticipantModel
                obj_participantModel.image_video = obj_requestmodal?.image_video
                obj_requestmodal?.array_participant_pager.replaceObject(at: 0, with: obj_participantModel)
            }
            obj_phonelistvc.obj_challengeDetail = obj_requestmodal
            obj_phonelistvc.str_requestType = selectedIndex
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
    }
    
    
}

extension ChallangeListVC: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = true
        return transiton
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = false
        return transiton
    }
}
