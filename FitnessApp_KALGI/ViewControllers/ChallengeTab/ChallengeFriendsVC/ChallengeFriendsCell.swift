//
//  ChallengeFriendsCell.swift
//  FitnessApp
//
//  Created by Ashish on 12/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//




import UIKit

class ChallengeFriendsCell: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var imageView_thumb: UIImageView!
    @IBOutlet weak var imageView_check: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_bookingmodal:FriendsListModel){
        lbl_title.text = String(format: "%@", obj_bookingmodal.str_name!)
       // lbl_description.text = String(format: "%@", obj_bookingmodal.str_about!)
        let url = URL(string: obj_bookingmodal.str_profile_picture)
        imageView_thumb.setImageFrom(url)
//        lbl_price.text = String(format: "%@", obj_bookingmodal.str_email)
        let age = getAgeFromDOF(date: obj_bookingmodal.dob)
        print(age)
//        imageView_thumb.setImageFromURL(stringImageUrl: (URL(string: (obj_bookingmodal.str_profile_picture))), placeholderImage: UIImage(named: "appLogo"))
//        lbl_price.text = String(format: "%@", obj_bookingmodal.email)
        lbl_description.text = "\(obj_bookingmodal.gender) \(age) years"
    }
    
    func getAgeFromDOF(date: String) -> (Int) {

        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "YYYY-MM-dd"
        let dateOfBirth = dateFormater.date(from: date)

        let calender = Calendar.current

        let dateComponent = calender.dateComponents([.year], from:
        dateOfBirth!, to: Date())

        return (dateComponent.year!)
    }
    
}


