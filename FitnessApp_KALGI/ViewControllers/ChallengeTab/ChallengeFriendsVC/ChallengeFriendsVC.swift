//
//  ChallengeFriendsVC.swift
//  FitnessApp
//
//  Created by Ashish on 12/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit
import Alamofire
import Foundation
import Contacts
import ContactsUI

class ChallengeFriendsVC: UIViewController , UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, SwiftMultiSelectDelegate,SwiftMultiSelectDataSource  {
    func reload_list() {
        self.getMyBookings()
    }
    
    
    var items:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    var initialValues:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    var selectedItems:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    fileprivate var array_mybookings:NSMutableArray = NSMutableArray()
    fileprivate var array_selectedFriends:NSMutableArray = NSMutableArray()
    @IBOutlet weak var imageView_BG: UIImageView!
    
    @IBOutlet weak var tableview_bookings: UITableView!
    var dict_item:NSDictionary!
    var my_switch:UISwitch = UISwitch()
    var array_emails:[String] = [String]()
    var video_url:URL? = nil
    var selectedImage:UIImage? = nil
    var str_fileType:String! = ""
    var str_message:String! = ""
    var freeRunId : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        tableview_bookings.delegate  = self as UITableViewDelegate
        tableview_bookings.dataSource = self as UITableViewDataSource
        self.tableview_bookings.register(UINib(nibName: "ChallengeFriendsCell", bundle: nil), forCellReuseIdentifier: "ChallengeFriendsCell")
        self.getMyBookings()

        // self.addRightSearchButtonWithImage()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        my_switch = UISwitch(frame: .zero)
        my_switch.tintColor =  UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
        my_switch.onTintColor =  UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
        my_switch.isOn = false // or false
        my_switch.addTarget(self, action: #selector(switchToggled(_:)), for: .valueChanged)
        let switch_display = UIBarButtonItem(customView: my_switch)
        
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "search"), for: UIControl.State.normal)
//        button.addTarget(self, action: #selector(self.search_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        
        
        navigationItem.rightBarButtonItems = [barButton, switch_display]
        // navigationItem.rightBarButtonItem = switch_display
    }
    
//    @objc func search_btn_clicked(){
//        let rootVC : FriendSearchVC = FriendSearchVC(nibName: "FriendSearchVC", bundle: nil)
//        self.navigationController?.pushViewController(rootVC, animated: true)
//    }
    
    
//    func addRightSearchButtonWithImage(){
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//        let button = UIButton.init(type: .custom)
//        button.setImage(UIImage(named: "search"), for: UIControl.State.normal)
//        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
//        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
//        let barButton = UIBarButtonItem(customView: button)
//        self.navigationItem.rightBarButtonItem = barButton
//    }
    @objc func switchToggled(_ sender: UISwitch) {
        if sender.isOn {
            print( "The switch is now true!" )
            SwiftMultiSelect.initialSelected = []
            SwiftMultiSelect.Show(to: self)
        }
        else{
            print( "The switch is now false!" )
        }
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func post_btn_clicked(_ sender: UIButton) {
            if self.str_fileType == "VIDEO"{
                self.upload_btn_clicked()
            }else if self.str_fileType == "IMAGE"{
                self.upload_image()
            }else{
                DataUtil.alertMessage("Please slect video or image", viewController: self)
            }
    }
    
    func getMyBookings(){
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam: Parameters = ["user_id":str_userid]
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        self.array_mybookings.removeAllObjects()
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.challange_friends_list, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.array_mybookings.removeAllObjects()
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:FriendsListModel = FriendsListModel(dictUserInfo: dict)
                        self.array_mybookings.add(obj_bookingmodal)
                    }
                    self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    @objc func upload_btn_clicked(){
        
        if array_selectedFriends.count == 0{
            DataUtil.alertMessage("Please select freind", viewController: self)
            return
        }
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
//        let headers: HTTPHeaders = [
//            "Authorization": String(format: "Bearer %@",str_accessToken),
//            "Accept": "application/json"
//        ]

        DataUtil.loadIndicatorView()
        let postParam: Parameters = ["text":str_message, "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!, "file_type":"VIDEO", "friends_id":array_selectedFriends.componentsJoined(by: ",")]
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(self.video_url!, withName: "file")
            for (key, value) in postParam {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
            
        },
                         usingThreshold:UInt64.init(),
                         to:ConstantFiles.add_challange,
                         method:.post,
                         headers:[
                            "Authorization": String(format: "Bearer %@",str_accessToken),
                            "Accept": "application/json"
            ],
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    guard let successDict = response.result.value as? NSDictionary else
                                    {
                                        DataUtil.removeIndicatorView()
                                        return
                                    }
                                    DataUtil.removeIndicatorView()
                                    if let status = successDict["STATUS"] as? String{
                                        if status == "true"{
                                            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                                                
                                                
                                                
                                                let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                                                
                                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                                                    //self.delegate?.parkadded_clicked()
                                                    DispatchQueue.main.async {
                                                        let rootVC : ChallangeListVC = ChallangeListVC(nibName: "ChallangeListVC", bundle: nil)
                                                        let nav = UINavigationController(rootViewController: rootVC)
                                                        self.sideMenuController?.embed(centerViewController: nav)
                                                    }
                                                    
                                                    
                                                }))
                                                
                                                self.present(alert, animated: true, completion: nil)
                                                
                                                
                                                print("Success ------", successDict)
                                                if let array:NSArray = successDict["response"] as? NSArray{
                                                    DataUtil.appdelegate().objUserInfo?.array_videos.removeAllObjects()
                                                    for item in array {
                                                        // let dict:NSDictionary =  item as! NSDictionary
                                                        // let obj_model:VideosModel = VideosModel.init(dictUserInfo: dict)
                                                        //DataUtil.appdelegate().objUserInfo?.array_videos.add(obj_model)
                                                    }
                                                    //self.delegate?.videoadded_clicked()
                                                    //  self.dismiss(animated: true, completion: nil)
                                                }
                                                
                                                
                                                
                                                
                                                //  self.present(alert, animated: true, completion: nil)
                                                
                                                // DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                                                //self.back_btn_clicked()
                                                
                                                /*   if let arr = successDict.value(forKey:"response"){
                                                 
                                                 
                                                 }else{
                                                 
                                                 }*/
                                            }else{
                                                
                                                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                                                self.back_btn_clicked()
                                            }
                                            
                                        }
                                    }
                                    debugPrint(response)
                                }
                            case .failure(let encodingError):
                                DataUtil.removeIndicatorView()
                                print(encodingError)
                            }
        })
    }
    
    func upload_image(){
        
        if array_selectedFriends.count == 0{
            DataUtil.alertMessage("Please select freind", viewController: self)
            return
        }
        
        let postParam: Parameters = ["text":str_message!, "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!, "file_type":"IMAGE","freerun_id" : freeRunId, "friends_id":array_selectedFriends.componentsJoined(by: ",")]
        print("Login Post Parameter : \(postParam)")
        
        ServerCommunication.getDataWithPostImageForWall(url: ConstantFiles.add_challange, image: selectedImage as Any,parameter: postParam, viewController: self, isImage:true,success: { (successDict) in
            print("Success ------", successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    //self.delegate?.parkadded_clicked()
                    DispatchQueue.main.async {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
            
            
            // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
        
        
    }
    
    
    func syncContacts(){
        //friends_email
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam: Parameters = ["user_id":str_userid, "friends_email":array_emails.joined(separator: ",")]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]

        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.contact_sync, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.getMyBookings()
                /* if let arr = successDict.value(forKey:"response"){
                 let array:NSArray = arr as! NSArray
                 for item in array {
                 let dict:NSDictionary =  item as! NSDictionary
                 let obj_bookingmodal:FriendsListModel = FriendsListModel(dictUserInfo: dict)
                 self.array_mybookings.add(obj_bookingmodal)
                 }
                 self.tableview_bookings.reloadData()
                 }else{
                 DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                 }*/
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        let obj_requestmodal:FriendsListModel = array_mybookings[indexPath.row] as! FriendsListModel
        if obj_requestmodal.str_request_status == "1"{
            return 120
        }
        return 110
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.array_mybookings.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj_requestmodal:FriendsListModel = array_mybookings[indexPath.row] as! FriendsListModel
            let identifier = "ChallengeFriendsCell"
            var logincell: ChallengeFriendsCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? ChallengeFriendsCell
            if logincell == nil {
                tableView.register(UINib(nibName: "ChallengeFriendsCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? ChallengeFriendsCell)!
            }
        
        
        if obj_requestmodal.is_selected == true{
            logincell?.imageView_check.image = UIImage(named: "default_shipping")
        }else{
            logincell?.imageView_check.image = UIImage(named: "icn_select")
        }
        
            logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal)
            return logincell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:ChallengeFriendsCell = tableView.cellForRow(at: indexPath) as! ChallengeFriendsCell
        cell.imageView_check.isHidden = false
        let freindListModel:FriendsListModel = (array_mybookings[indexPath.row] as! FriendsListModel)
        freindListModel.is_selected = true
        array_selectedFriends.add(freindListModel.str_user_id)
        array_mybookings.replaceObject(at: indexPath.row, with: freindListModel)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell:ChallengeFriendsCell = tableView.cellForRow(at: indexPath) as! ChallengeFriendsCell
        cell.imageView_check.isHidden = true
        let freindListModel:FriendsListModel = (array_mybookings[indexPath.row] as! FriendsListModel)
        freindListModel.is_selected = false
        array_selectedFriends.remove(freindListModel.str_user_id)
        array_mybookings.replaceObject(at: indexPath.row, with: freindListModel)
    }
    
    
    
    /// Create a custom items set
    func createItems(){
        
        self.items.removeAll()
        self.initialValues.removeAll()
        for i in 0..<50{
            items.append(SwiftMultiSelectItem(row: i, title: "test\(i)", description: "description for: \(i)", imageURL : (i == 1 ? "https://randomuser.me/api/portraits/women/68.jpg" : nil)))
        }
        self.initialValues = [self.items.first!,self.items[1],self.items[2]]
        self.selectedItems = items
        SwiftMultiSelect.dataSourceType = .phone
    }
    
    
    /// selector for switch addressbook
    ///
    /// - Parameter sender
    @IBAction func useAddr(_ sender: Any) {
        
        SwiftMultiSelect.dataSourceType = .phone
    }
    
    
    /// Function to launch selector from button
    ///
    /// - Parameter sender
    @IBAction func launch(_ sender: Any) {
        
        //Example to start a selector with initial values
        
        
    }
    
    
    //MARK: - SwiftMultiSelectDelegate
    
    func userDidSearch(searchString: String) {
        if searchString == ""{
            selectedItems = items
        }else{
            selectedItems = items.filter({$0.title.lowercased().contains(searchString.lowercased()) || ($0.description != nil && $0.description!.lowercased().contains(searchString.lowercased())) })
        }
    }
    
    func numberOfItemsInSwiftMultiSelect() -> Int {
        return selectedItems.count
    }
    
    func swiftMultiSelect(didUnselectItem item: SwiftMultiSelectItem) {
        print("row: \(item.title) has been deselected!")
    }
    
    func swiftMultiSelect(didSelectItem item: SwiftMultiSelectItem) {
        print("item: \(item.title) \(String(describing: item.userInfo)) has been selected!")
        
        
        //CNContact
    }
    
    func didCloseSwiftMultiSelect() {
        //badge.isHidden = true
        //badge.text = ""
        my_switch.isOn = false
    }
    
    func swiftMultiSelect(itemAtRow row: Int) -> SwiftMultiSelectItem {
        return selectedItems[row]
    }
    
    func swiftMultiSelect(didSelectItems items: [SwiftMultiSelectItem]) {
        
        initialValues   = items
        // badge.isHidden  = (items.count <= 0)
        ///badge.text      = "\(items.count)"
        my_switch.isOn = false
        print("you have been selected: \(items.count) items!")
        
        for item in items{
            
            if let contact = item.userInfo as? CNContact
            {
                if let emailValue : CNLabeledValue = contact.emailAddresses.first
                {
                    self.array_emails.append(emailValue.value as String)
                    print("Email : ", emailValue.value)
                }
            }
            
        }
        self.syncContacts()
        
    }
    
    
    
}

