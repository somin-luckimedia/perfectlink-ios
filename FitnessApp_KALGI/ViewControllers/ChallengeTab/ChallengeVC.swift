//
//  ChallengeVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 10/10/20.
//

import UIKit

class ChallengeVC: UIViewController {
    
    let transiton = SlideInTransition()
    var topView: UIView?

    let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        dismissControllerAnimated()
        self.navigationController?.navigationBar.isHidden = true
        let notificationCenter = NotificationCenter.default
               notificationCenter.addObserver(self,
                                              selector: #selector(dismissChallenge),
                                              name: NSNotification.Name(rawValue: "DismissChallenge"),
                                              object: nil)

    }
    
    @objc func dismissChallenge(){
      //  flagChallenge = false
        self.dismissControllerAnimated()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        objAppDelgate1.sideMenuIndex = 0
    }
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        let rootVC : SideMenuController1 = SideMenuController1(nibName: "SideMenuController", bundle: nil)
        rootVC.didTapMenuType = { menuType in
            self.transitionToNew(menuType)
        }
        rootVC.modalPresentationStyle = .overCurrentContext
        rootVC.transitioningDelegate = self
        present(rootVC, animated: true)


    }
    
    func transitionToNew(_ menuType: MenuType) {
        switch menuType {
        case .FitnessWall:
            let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Challenges:
            let rootVC : OrderHistoryVC = OrderHistoryVC(nibName: "OrderHistoryVC", bundle: nil)
            self.present(rootVC, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.dismissControllerAnimated()
                
            }
        case .Chat:
            let rootVC : FriendListVC = FriendListVC(nibName: "FriendListVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.dismissControllerAnimated()
            }
//            self.present(rootVC, animated: true, completion: nil)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                self.dismissControllerAnimated()
//            }
        default:
            break
        }
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChallengeVC: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = true
        return transiton
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = false
        return transiton
    }
}
