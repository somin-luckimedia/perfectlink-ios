//
//  MyTabbarController.swift
//  Squat Challenge
//
//  Created by khushbu Bhavsar on 11/05/20.
//  Copyright © 2020 khushbu Bhavsar. All rights reserved.
//

import UIKit

class MyTabbarController: UITabBarController,UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        // Do any additional setup after loading the view.
    }
    //    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    //
    //           if viewController is DashboardVC {
    //               print("First tab")
    //           } else if viewController is DietRecipeVC {
    //               print("Second tab")
    //           }
    //       }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if(item.tag == 0) {
            item.title = ""
            //workout
            print("Code for item 0")
            if flagChallenge {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissChallenge"), object: nil)
            }
            if flagHome {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissHome"), object: nil)
            }
            if flagCChat {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissChat"), object: nil)
            }
            if flagProfile {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissProfile"), object: nil)
            }
            if flagSetting {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissSetting"), object: nil)
            }
        }
        else if(item.tag == 1) {
            item.title = ""
             //StaticClass.save(toUserDefaults: "YES", "strProductFull")
            if flagChallenge {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissChallenge"), object: nil)
            }
            if flagHome {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissHome"), object: nil)
            }
            if flagCChat {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissChat"), object: nil)
            }
            if flagProfile {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissProfile"), object: nil)
            }
            if flagSetting {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissSetting"), object: nil)
            }
//            self.dismissControllerAnimated()
            print("Code for item 1")
           // self.PresentUpgradeVC()
            
        }
        else if(item.tag == 2) {
            item.title = ""
            //progress
            if flagChallenge {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissChallenge"), object: nil)
            }
            if flagHome {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissHome"), object: nil)
            }
            if flagCChat {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissChat"), object: nil)
            }
            if flagProfile {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissProfile"), object: nil)
            }
            if flagSetting {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissSetting"), object: nil)
            }
            self.dismissControllerAnimated()

            print("Code for item 2")
            
        }
        else if(item.tag == 3) {
            item.title = ""
            //Community
            if flagChallenge {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissChallenge"), object: nil)
            }
            if flagHome {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissHome"), object: nil)
            }
            if flagCChat {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissChat"), object: nil)
            }
            if flagProfile {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissProfile"), object: nil)
            }
            if flagSetting {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissSetting"), object: nil)
            }
            self.dismissControllerAnimated()

            print("Code for item 3")
            
        }
        else if(item.tag == 4) {
            item.title = ""
            //more
            if flagChallenge {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissChallenge"), object: nil)
            }
            if flagHome {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissHome"), object: nil)
            }
            if flagCChat {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissChat"), object: nil)
            }
            if flagProfile {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissProfile"), object: nil)
            }
            if flagSetting {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissSetting"), object: nil)
            }
            self.dismissControllerAnimated()

            print("Code for item 4")
            
        }
        else if(item.tag == 5) {
            item.title = "5"
            //more
            if flagChallenge {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissChallenge"), object: nil)
            }
            if flagHome {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissHome"), object: nil)
            }
            if flagCChat {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissChat"), object: nil)
            }
            if flagProfile {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissProfile"), object: nil)
            }
            if flagSetting {
                let notificationCenter = NotificationCenter.default
                notificationCenter.post(name: NSNotification.Name(rawValue: "DismissSetting"), object: nil)
            }
            self.dismissControllerAnimated()

            print("Code for item 5")
            
        }
    }
    
//    func PresentUpgradeVC()
//    {
//        let strInapp = StaticClass.retrieve(fromUserDefaults: "strProductFull")
//        if (strInapp == "YES")
//        {
//        }
//        else {
//            self.prsentUpgradeVC()
//        }
//    }
//    func prsentUpgradeVC()
//    {
//        let ObjUpgradePremVC = UpgradeVC(nibName: "UpgradeVC", bundle: nil)
//
//
//        if #available(iOS 13.0, *) {
//            ObjUpgradePremVC.modalPresentationStyle = .fullScreen
//        } else {
//            // Fallback on earlier versions
//        }
//
//        self.present(ObjUpgradePremVC, animated: true, completion: nil)
//        CFRunLoopWakeUp(CFRunLoopGetCurrent())
//        //}
//    }
    
}

class TabBar: UITabBar {
  override var traitCollection: UITraitCollection {
    guard UIDevice.current.userInterfaceIdiom == .pad else {
      return super.traitCollection
    }

    return UITraitCollection(horizontalSizeClass: .compact)
  }
}
