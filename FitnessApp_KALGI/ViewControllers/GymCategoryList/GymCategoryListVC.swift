//
//  GymCategoryListVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 25/11/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire

class GymCategoryListVC: UIViewController {
    
    @IBOutlet weak var collectioneview_city: UICollectionView!
    var array_city:NSMutableArray = NSMutableArray(objects: "", "", "", "", "", "")
    let array_icons:[String] = ["Gym_icon", "Trainer_icon", "Discount_icon", "Meals_icon", "Protien_icon", "Acceessories_icon"]
    let array_title:[String] = ["Gym", "Trainer", "Discount", "Meals", "Protien", "Acceessories"]
    let array_quantity:[String] = ["300+ Gyms", "80+ Personal Trainer", "214+ Offer", " 100+ Meals", "30+ Products", "100+ Acceessories"]
    var isFromHome:Bool = false
     fileprivate var array_categories: NSMutableArray = NSMutableArray()
    
   // @IBOutlet weak var lbl_name: UILabel!
    
  //  @IBOutlet weak var lbl_weight: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectioneview_city.register(UINib(nibName: "GymCategoryCell", bundle: nil), forCellWithReuseIdentifier: "GymCategoryCell")
        self.collectioneview_city.reloadData()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.get_categories_list()
        DataUtil.appdelegate().isAuthorizedtoGetUserLocation()
        
        
        self.addLeftMenuButtonWithImage()
        //        if isFromHome == true{
        //            self.addLeftMenuButtonWithImage()
        //        }
        // Do any additional setup after loading the view.
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     static let gym_categories =  baseURL+"gym_category"
     static let gym_listing =  baseURL+"gym_list"
     
   
     
     */
    
    
    func get_categories_list(){
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        print(headers)
        
        ServerCommunication.getData(url: ConstantFiles.gym_categories, viewController: self,headers:headers   ,success: { (successDict) in
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                let array: NSArray = successDict["response"] as! NSArray
                for item in array{
                    let dict_item:NSDictionary = (item as? NSDictionary)!
                    if dict_item != nil {
                        self.array_categories.add(dict_item)
                    }
                }
                self.collectioneview_city.reloadData()
                print(self.array_categories)
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
            
            
            print(successDict)
            // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
}



extension GymCategoryListVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_categories.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:GymCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GymCategoryCell", for: indexPath) as! GymCategoryCell
        let dict_item:NSDictionary = array_categories[indexPath.row] as! NSDictionary
        
        cell.lbl_title.text = dict_item["category_name"] as? String
        //cell.lbl_title.text = dict_item["category_image"] as? String
        cell.lbl_quantity.text = String(format: "%ld options", dict_item["options"] as! Int)
        //    cell.imageview_phone.sd_setImage(with: url, placeholderImage: UIImage(named: "phone"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenwidth:CGFloat = UIScreen.main.bounds.size.width/2
        return CGSize(width: screenwidth, height: 170)
    }
    
    
    
  /*  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let obj_phonelistvc:GymListVC = GymListVC(nibName: "GymListVC", bundle: nil)
        let dict_item:NSDictionary = array_categories[indexPath.row] as! NSDictionary
        obj_phonelistvc.dict_item = dict_item
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        
    }*/
    
}
