//
//  GymCategoryCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 25/11/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit

class GymCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var imageview_city: UIImageView!
    
    @IBOutlet weak var view_inner: UIView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_quantity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //view_inner.layer.shadowColor = UIColor.black.cgColor
        //view_inner.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        //view_inner.layer.shadowOpacity = 0.4
        //view_inner.layer.shadowRadius = 6.0
        //layer.masksToBounds = true
        // view_inner.dropShadow()
        // Initialization code
    }
    
}

