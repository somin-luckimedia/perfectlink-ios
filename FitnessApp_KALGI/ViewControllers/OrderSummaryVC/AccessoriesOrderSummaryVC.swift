//
//  AccessoriesOrderSummaryVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 16/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit
import Alamofire
import SquareInAppPaymentsSDK


class AccessoriesOrderSummaryVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, PromocodeCellDelegate, DiscountControllerDelegate,OrderViewControllerDelegate, UIViewControllerTransitioningDelegate, SQIPCardEntryViewControllerDelegate  {
    
    @IBOutlet var imgOrderSuccessful: UIImageView!
    @IBAction func funcDoneTapped(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)

    }
    
    func didRequestPayWithApplyPay() {
        
    }
    
    func cardEntryViewController(_ cardEntryViewController: SQIPCardEntryViewController, didObtain cardDetails: SQIPCardDetails, completionHandler: @escaping (Error?) -> Void) {
        guard serverHostSet else {
            printCurlCommand(nonce: cardDetails.nonce)
            completionHandler(nil)
            return
        }
//        getMyBookings(cardDetails.nonce)
        processPayment(cardDetails.nonce) { (transactionID, errorDescription) in
            guard let errorDescription = errorDescription else {
                // No error occured, we successfully charged
                completionHandler(nil)
                return
            }
            
            // Pass error description
            let error = NSError(domain: "com.example.supercookie", code: 0, userInfo:[NSLocalizedDescriptionKey : errorDescription])
            completionHandler(error)
        }
    }
    
    func processPayment(_ nonce: String, completion: @escaping (String?, String?) -> Void) {
//        self.getMyBookings(nonce)
       // let str_id:String = self.obj_gymdetail.str_trainer_user_id
        var str_id:String = ""
        var str_type:String = ""
        if isfromProtien == true{
            str_id = self.obj_protienDetailModel.str_product_id
            str_type = "PRODUCT"
        }else{
            str_id = self.obj_accessoriesModel.str_accessorie_id
            str_type = "ACCESSORIES"
        }
        
//        showOrderSheet()
        
        
      //  let string:String =    String(format:"user_id=%@&subscription_for=%@&trainer_id=%@&months=%ld&discount_id=%@&start_date=%@&subscription_amount=%.2f&preferred_time=%@&membership_type=%@&member_count=%ld",((DataUtil.appdelegate().objUserInfo?.userId)!),str_type,str_id, str_member_plan,self.str_couponId, self.str_start_date, self.totalCost, self.str_start_time, self.str_membership_type, self.str_member_count)
        let postParam = ["nonce": nonce,"user_id" : ((DataUtil.appdelegate().objUserInfo?.userId)!),"product_type" : str_type,"product_id" : str_id,"quantity" : self.productCount,"shipping_address_id" : self.obj_selected_address.str_id!, "discount_id" : self.str_couponId,"email" : ((DataUtil.appdelegate().objUserInfo?.email)!)] as [String : Any]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Accept": "application/json"
        ]
        let url = URL(string: ConstantFiles.Square.CHARGE_URL_Accessories)!

        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.Square.CHARGE_URL_Accessories, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
//                self.tableview_detail.isHidden = false
                if let arr = successDict.value(forKey:"response"){
                    
//                    self.didChargeSuccessfully()
                    
                    DispatchQueue.main.async {
                        completion("success", nil)
                    }
                    /* let array:NSArray = arr as! NSArray
                     for item in array {
                     let dict:NSDictionary =  item as! NSDictionary
                     let obj_bookingmodal:GymListModel = GymListModel(dictUserInfo: dict)
                     self.array_mybookings.add(obj_bookingmodal)
                     }*/
                    //GymDetailModel
                    // self.obj_gymDetail = GymDetailModel(dictUserInfo: arr as! NSDictionary)
                    // self.tableview_detail.reloadData()
                }else{
//                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
//                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
//                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
//    func getMyBookings(_ nonce: String){
////        let str_id:String = self.obj_gymdetail.str_trainer_user_id
//        let str_type:String = "PRODUCT"
//
////        showOrderSheet()
//
//
//      //  let string:String =    String(format:"user_id=%@&product_type=%@&product_id=%@&quantity=%ld&discount_id=%@&shipping_address_id=%@",((DataUtil.appdelegate().objUserInfo?.userId)!),str_type,str_id, self.productCount,self.str_couponId, self.obj_selected_address.str_id)
//        let postParam = ["nonce": nonce,"user_id" : ((DataUtil.appdelegate().objUserInfo?.userId)!),"product_type" : str_type,"product_id" : self.obj_protienDetailModel.str_product_id!,"quantity" : self.productCount,"shipping_address_id" : self.obj_selected_address.str_id!, "discount_id" : self.str_couponId,"email" : ((DataUtil.appdelegate().objUserInfo?.email)!)] as [String : Any]
//        print("Login Post Parameter : \(postParam)")
//        let headers = [
//            "Accept": "application/json"
//        ]
//        let url = URL(string: ConstantFiles.Square.CHARGE_URL_Accessories)!
//
//        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.Square.CHARGE_URL_Accessories, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
//            print(successDict)
//            if (successDict.value(forKey: "STATUS") as? String) == "true"{
////                self.tableview_detail.isHidden = false
//                if let arr = successDict.value(forKey:"response"){
//                    self.didChargeSuccessfully()
//                    /* let array:NSArray = arr as! NSArray
//                     for item in array {
//                     let dict:NSDictionary =  item as! NSDictionary
//                     let obj_bookingmodal:GymListModel = GymListModel(dictUserInfo: dict)
//                     self.array_mybookings.add(obj_bookingmodal)
//                     }*/
//                    //GymDetailModel
//                   // self.obj_gymDetail = GymDetailModel(dictUserInfo: arr as! NSDictionary)
//                   // self.tableview_detail.reloadData()
//                }else{
////                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
//                }
//
//            }else{
////                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
//            }
//        }) { (dictFailure) in
//            if (dictFailure.value(forKey: "message") as? String) != nil {
////                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
//            }
//        }
//    }
    
    func cardEntryViewController(_ cardEntryViewController: SQIPCardEntryViewController, didCompleteWith status: SQIPCardEntryCompletionStatus) {
        dismiss(animated: true) {
            switch status {
            case .canceled:
                self.showOrderSheet()
                break
            case .success:
                guard self.serverHostSet else {
                    self.showCurlInformation()
                    return
                }
                self.didChargeSuccessfully()
            }
        }
    }
    

    private func didChargeSuccessfully() {
        imgOrderSuccessful.isHidden = false
//        showAlertWithTitleFromVC(vc: self, title: "Your order was successful", andMessage: "Go to your Square dashbord to see this order reflected in the sales tab.", buttons: ["OK"]) { (index) in
//            if index == 0 {
//                self.navigationController?.popToRootViewController(animated: true)
//            }
//        }
    }

    
//    func didRequestPayWithApplyPay() {
//        dismiss(animated: true) {
//            self.requestApplePayAuthorization()
//        }
//    }
    
    func didRequestPayWithCard() {
        dismiss(animated: true) {
            let vc = self.makeCardEntryViewController()
            vc.delegate = self

            let nc = UINavigationController(rootViewController: vc)
            self.present(nc, animated: true, completion: nil)
        }
    }
    var obj_accessoriesModel:AccessoriesDetailModel! = AccessoriesDetailModel()
    var obj_protienDetailModel:ProtienDetailModel! = ProtienDetailModel()
    var obj_selected_address:ShippingAddressModel! =  ShippingAddressModel()
    var productCount:Int = 1
    var totalCost:Double = 0.00
    var discount:Double = 0.00
    var str_promocode:String = ""
    var str_couponId:String = ""
    var str_userType:String = "2"
    var obj_orderSummaryCell: OrdarSummaryCell!
    var obj_orderShippingCell: OrderShippingCell!
    var obj_applyCodeCell: ApplyCodeCell!
    var str_gymId:String!
    var isDiscountApplied:Bool = false
    var isfromProtien:Bool = false
    @IBOutlet weak var tableview_detail: UITableView!
    
    @IBOutlet weak var view_header: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview_detail.delegate  = self as UITableViewDelegate
        tableview_detail.dataSource = self as UITableViewDataSource
        self.title = "ORDER SUMMARY"
        self.tableview_detail.tableFooterView = self.view_header
        self.addLeftMenuButtonWithImage()
        //tableview_detail.rowHeight = 700
        //tableview_detail.estimatedRowHeight = UITableViewAutomaticDimension
        tableview_detail.reloadData()
        imgOrderSuccessful.isHidden = true

        
        
        
        //self.getMyBookings()
        
        
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    private var serverHostSet: Bool {
        return ConstantFiles.Square.CHARGE_SERVER_HOST != "REPLACE_ME"
    }
    func discount_btn_clicked(obj_discount: DiscountModel) {
        if obj_discount.str_type == "percentage"{
            if let cost = Double(obj_discount.str_percentage!) {
                self.discount = self.calculatePercentage(value: self.totalCost , percentageVal: cost)
            } else {
               self.discount = 0.00
            }
        }else{
            self.discount = Double(obj_discount.str_amount) ?? 0.0
        }
        self.str_promocode = obj_discount.str_coupon_code
        self.str_couponId = obj_discount.str_couponId
        self.isDiscountApplied = true
        self.tableview_detail.reloadData()
    }
    
    public func calculatePercentage(value:Double,percentageVal:Double)->Double{
        let val = value * percentageVal
        let valD:Double = val / 100.0
        return valD
    }
    
    private func showOrderSheet() {
        // Open the buy modal
        let orderViewController = OrderViewController()
        orderViewController.delegate = self
        let nc = OrderNavigationController(rootViewController: orderViewController)
        nc.modalPresentationStyle = .custom
        nc.transitioningDelegate = self
        present(nc, animated: true, completion: nil)
    }
    
    private func printCurlCommand(nonce : String) {
        let uuid = UUID().uuidString
        print("curl --request POST https://connect.squareupsandbox.com/v2/payments \\" +
            "--header \"Content-Type: application/json\" \\" +
            "--header \"Authorization: Bearer YOUR_ACCESS_TOKEN\" \\" +
            "--header \"Accept: application/json\" \\" +
            "--data \'{" +
            "\"idempotency_key\": \"\(uuid)\"," +
            "\"autocomplete\": true," +
            "\"amount_money\": {" +
            "\"amount\": 100," +
            "\"currency\": \"USD\"}," +
            "\"source_id\": \"\(nonce)\"" +
            "}\'");
    }
    
    private func showCurlInformation() {
        let alert = UIAlertController(title: "Nonce generated but not charged",
                                      message: "Check your console for a CURL command to charge the nonce, or replace Constants.Square.CHARGE_SERVER_HOST with your server host.",
                                      preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    @IBAction func continue_btn_clicked(_ sender: UIButton) {
        showOrderSheet()

        //  let postParam = ["item_name":obj_requestmodal.str_boatname,  "item_number":obj_requestmodal.str_captainname, "item_description":"payement testing", "item_tax":"4", "item_price":obj_requestmodal.str_quoteprice, "details_tax":"4", "details_subtotal":obj_requestmodal.str_quoteprice, "request_id":self.str_requestId, "boat_user_id":obj_requestmodal.str_driverid]
        
        
        /*
         http://perfectlinkfitness.com/api/creat-payment
         user_id="+appStore.getCachedUser().userId+"&product_type=PRODUCT&product_id="+gymDetailData?.productId+"&quantity="+selectedQuantity
         
         for accessory:- &product_type=ACCESSORIES
         */
        //user_id
        //Octatrades-Maldives
//        var str_id:String = ""
//        var str_type:String = ""
//        if isfromProtien == true{
//            str_id = self.obj_protienDetailModel.str_product_id
//            str_type = "PRODUCT"
//        }else{
//            str_id = self.obj_accessoriesModel.str_accessorie_id
//            str_type = "ACCESSORIES"
//        }
//
//
//        let string:String =    String(format:"user_id=%@&product_type=%@&product_id=%@&quantity=%ld&discount_id=%@&shipping_address_id=%@",((DataUtil.appdelegate().objUserInfo?.userId)!),str_type,str_id, self.productCount,self.str_couponId, self.obj_selected_address.str_id)
//
//        //  let string:String = "item_tax=4&details_tax=4&item_name=Black Pearl&details_subtotal=259&request_id=82&item_number=Avnish&boat_user_id=BOATUSRID0000000002&item_description=payement testing&item_price=259"
//
//        let url = URL(string: "http://perfectlinkfitness.com/api/creat-payment")
//        print("URL: ", string)
//
//        let data = string.data(using: .utf8)
//        var request = URLRequest(url: url!)
//        //AIzaSyB2AX5rToBHRxVO0HW4ROU-A1He5oGLUZE
//        request.httpMethod = "POST"
//        request.httpBody =  data
//        let rootVC : PaymentWebVC = PaymentWebVC(nibName: "PaymentWebVC", bundle: nil)
//        rootVC.request = request
//        rootVC.isfromProtien = self.isfromProtien
//        rootVC.str_type = str_type
//        self.navigationController?.pushViewController(rootVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // self.navigationController?.navigationBar.isHidden = true
        //   self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  /*  func getMyBookings(){
        let postParam: Parameters = ["gym_id":str_gymId!]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",  (DataUtil.appdelegate().objUserInfo?.str_accessToken)!),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.gym_detail, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    /* let array:NSArray = arr as! NSArray
                     for item in array {
                     let dict:NSDictionary =  item as! NSDictionary
                     let obj_bookingmodal:GymListModel = GymListModel(dictUserInfo: dict)
                     self.array_mybookings.add(obj_bookingmodal)
                     }*/
                    //GymDetailModel
                    self.obj_gymDetail = GymDetailModel(dictUserInfo: arr as! NSDictionary)
                    self.tableview_detail.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
 
 */
    
    
    func promocode_btn_clicked(cell: ApplyCodeCell, btn: UIButton) {
        if isDiscountApplied == true{
            self.discount = 0.00
            self.str_promocode = ""
            self.str_couponId = ""
            self.isDiscountApplied = false
            self.tableview_detail.reloadData()
        }else{
                let obj_parkeAddressVC:DiscountsVC = DiscountsVC(nibName: "DiscountsVC", bundle: nil)
                 obj_parkeAddressVC.delegate = self
            obj_parkeAddressVC.forPromoCode = true
            if isfromProtien == true{
                obj_parkeAddressVC.str_type = "3"
            }else{
                obj_parkeAddressVC.str_type = "4"
            }
            
                let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
                self.navigationController?.present(nav, animated: true, completion: nil)
        }
        
    }
   
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 70
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header:OrderSummaryHeaderView =  UINib(nibName: "OrderSummaryHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! OrderSummaryHeaderView
        if section == 1{
            header.lbl_title.text = "Shippping Detail"
        }else if section == 2{
            header.lbl_title.text = "Payment Details"
        }
        
        return header
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0{
            return 160
        }else if indexPath.section == 1{
            return UITableView.automaticDimension
        }else{
            return 55
        }
        
        
        // return 710
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 2{
            
            if isDiscountApplied == true{
                return 3
            }else{
            return 2
            }
        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        ////TrainerHomeAddressCell
        if indexPath.section == 0{
            let identifier = "OrdarSummaryCell"
            obj_orderSummaryCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrdarSummaryCell
            if obj_orderSummaryCell == nil {
                tableView.register(UINib(nibName: "OrdarSummaryCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_orderSummaryCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrdarSummaryCell
            }
            
            if self.isfromProtien == true{
                obj_orderSummaryCell.updateCellwithModal(obj_protienModel: self.obj_protienDetailModel, str_qty: String(format: "%ld", productCount))

            }else{
                obj_orderSummaryCell.updateCellwithModal(obj_accessoryiesModel: self.obj_accessoriesModel, str_qty: String(format: "%ld", productCount))

            }
            
            return obj_orderSummaryCell
        }else if indexPath.section == 1{
            let identifier = "OrderShippingCell"
             obj_orderShippingCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderShippingCell
            if obj_orderShippingCell == nil {
                tableView.register(UINib(nibName: "OrderShippingCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_orderShippingCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderShippingCell
            }
            
            //let obj_locationModel:LocationServiceModel = obj_trainer_detail!.array_locations[indexPath.row] as! LocationServiceModel
            //homeCell.updateCellwithModal(obj_location_model:obj_locationModel)
            //  homeCell.delegate = self
            obj_orderShippingCell.updateCellwithModal(obj_shippingAddress: self.obj_selected_address)
            return obj_orderShippingCell
        }else{
            if indexPath.row == 0{
            let identifier = "ApplyCodeCell"
            obj_applyCodeCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ApplyCodeCell
            if obj_applyCodeCell == nil {
                tableView.register(UINib(nibName: "ApplyCodeCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_applyCodeCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ApplyCodeCell
            }
                if isDiscountApplied == true{
                    obj_applyCodeCell.btn_title.setTitle("Remove Promo Code", for: .normal)
                    obj_applyCodeCell.lbl_promocode.text = self.str_promocode
                }else{
                    obj_applyCodeCell.btn_title.setTitle("Apply Promo Code", for: .normal)
                    obj_applyCodeCell.lbl_promocode.text = ""
                }
                obj_applyCodeCell.delegate = self
            //let obj_locationModel:LocationServiceModel = obj_trainer_detail!.array_locations[indexPath.row] as! LocationServiceModel
            //homeCell.updateCellwithModal(obj_location_model:obj_locationModel)
            //  homeCell.delegate = self
            return obj_applyCodeCell
            }
            
            let identifier = "OrderAmountCell"
            var obj_amountcell: OrderAmountCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderAmountCell
            if obj_amountcell == nil {
                tableView.register(UINib(nibName: "OrderAmountCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_amountcell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderAmountCell)!
            }
            
            if isDiscountApplied{
                if indexPath.row == 1{
                    
                    if isfromProtien == true{
                        obj_amountcell?.updateCellwithModal(str_title: "Total Amount", str_value: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost), str_payable: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost))

                    }else{
                        obj_amountcell?.updateCellwithModal(str_title: "Total Amount", str_value: String(format: "%@%.2f", self.obj_accessoriesModel.str_currency ,self.totalCost), str_payable: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost))

                    }
                }
                    
//                }else if indexPath.row == 2{
//
//                    if isfromProtien == true{
//                        obj_amountcell?.updateCellwithModal(str_title: "Discount", str_value: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency,self.discount), str_payable: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost))
//
//                    }else{
//                        obj_amountcell?.updateCellwithModal(str_title: "Discount", str_value: String(format: "%@%.2f", self.obj_accessoriesModel.str_currency,self.discount), str_payable: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost))
//                    }
//                }
                    
                    
//                }else if indexPath.row == 3{
//                    if isfromProtien == true{
//                        obj_amountcell?.updateCellwithModal(str_title: "Amount Payable", str_value: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency,self.totalCost-self.discount))
//
//                    }else{
//                        obj_amountcell?.updateCellwithModal(str_title: "Amount Payable", str_value: String(format: "%@%.2f", self.obj_accessoriesModel.str_currency,self.totalCost-self.discount))
//
//                    }
                    
                    
               // }
            }else{
            if indexPath.row == 1{
                if isfromProtien == true{
                    obj_amountcell?.updateCellwithModal(str_title: "Total Amount", str_value: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency,self.totalCost), str_payable: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost))

                }else{
                    obj_amountcell?.updateCellwithModal(str_title: "Total Amount", str_value: String(format: "%@%.2f", self.obj_accessoriesModel.str_currency,self.totalCost), str_payable: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost))

                }
            }
                
//            }else if indexPath.row == 2{
//                if isfromProtien == true{
//                    obj_amountcell?.updateCellwithModal(str_title: "Amount Payable", str_value: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency,self.totalCost), str_payable: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost))
//
//                }else{
//                    obj_amountcell?.updateCellwithModal(str_title: "Amount Payable", str_value: String(format: "%@%.2f", self.obj_accessoriesModel.str_currency,self.totalCost), str_payable: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost))
//
//                }
//
//            }
            }
            return obj_amountcell!
            
        }
    }
    
    
    
    /*
     
     func login_btn_clicked(cell: LoginCell, btn: UIButton){
     print("delegate called")
     //  let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
     //  objAppDelgate.loadSidePanel(objVC: self.navigationController!)
     //  return
     let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate
     let strEmail  : String = (logincell.textfield_email.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
     let strPassword  : String = (logincell.textfield_password.text)!
     let strDeviceToken:String = (objAppDelgate1.str_devicetoken)!
     let strDeviceType:String = "IOS"
     let strLat:String = DataUtil.appdelegate().str_lattitude
     let strLong:String = DataUtil.appdelegate().str_longitude
     
     //email, password, user_id, login_type, device_token, device_type, lat, lng
     //let strCountrycode1:String = "+91"
     
     let postParam: Parameters = ["email":strEmail,  "password":strPassword,"device_type":strDeviceType,"device_token":"342423442344","lat":"342423442344", "lon":"342423442344"]
     print("Login Post Parameter : \(postParam)")
     
     
     //let imageTest:UIImage = UIImage(named: "back_image")!
     
     if !loginValidation() {
     return
     }
     
     ServerCommunication.getDataWithPost(url: ConstantFiles.loginUrl, parameter: postParam, viewController: self, success: { (successDict) in
     //   SynapseUserDefault.shared.setGenId(userName: self.txtGenId.text!)
     // SynapseUserDefault.shared.setPassword(password: self.txtFieldPassword.text!)
     // SynapseUserDefault.shared.setUserId(userId: successDict.value(forKey: "userId") as! Int)
     print(successDict)
     if (successDict.value(forKey: "STATUS") as? String) == "true"{
     DataUtil.appdelegate().objUserInfo = UserInfo(dictUserInfo: successDict["response"] as! NSDictionary)
     DataUtil.appdelegate().objUserInfo?.str_accessToken = successDict["access_token"] as! String
     let responseDict:NSDictionary = successDict["response"] as! NSDictionary
     print(responseDict)
     
     let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
     objAppDelgate.loadSidePanelWithScan(objVC: self.navigationController!)
     
     if (responseDict.value(forKey: "verify_status") as? String) == "0" {
     
     //let rootVC : VerifyOTPVC = VerifyOTPVC(nibName: "VerifyOTPVC", bundle: nil)
     //self.navigationController?.pushViewController(rootVC, animated: true)
     
     } else{
     //let rootVC : HomeVC = HomeVC(nibName: "HomeVC", bundle: nil)
     //self.navigationController?.pushViewController(rootVC, animated: true)
     }
     }else{
     DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
     }
     
     
     
     
     
     // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
     }) { (dictFailure) in
     if (dictFailure.value(forKey: "message") as? String) != nil {
     DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
     }
     }
     
     
     
     
     }
     
     
     
     
     */
    
    
    
    
    
    
}

extension AccessoriesOrderSummaryVC {
    func makeCardEntryViewController() -> SQIPCardEntryViewController {
        // Customize the card payment form
        let theme = SQIPTheme()
        theme.errorColor = .red
        theme.tintColor = UIColor(red: 0.14, green: 0.6, blue: 0.55, alpha: 1)
        theme.keyboardAppearance = .light
        theme.messageColor = UIColor(red: 0.48, green: 0.48, blue: 0.48, alpha: 1)
        theme.saveButtonTitle = "Pay"

        return SQIPCardEntryViewController(theme: theme)
    }
}

