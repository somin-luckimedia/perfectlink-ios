//
//  OrdarSummaryCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 16/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//





import UIKit


class OrdarSummaryCell: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_quantitiy: UILabel!
    @IBOutlet weak var lbl_dsecription: UILabel!
    @IBOutlet weak var imageView_thumb: UIImageView!
    var str_type:String = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_accessoryiesModel:AccessoriesDetailModel, str_qty:String){
        if self.str_type == "GYM" || self.str_type == "TRAINER"{
            self.lbl_price.isHidden = true
            self.lbl_quantitiy.isHidden = true
        }
        
        lbl_title.text = String(format: "%@", obj_accessoryiesModel.str_accessorie_title!)
        lbl_price.text = String(format: "%@%@/%@", obj_accessoryiesModel.str_currency!,obj_accessoryiesModel.str_amount!,obj_accessoryiesModel.str_per!)
        lbl_quantitiy.text = String(format: "Qty: %@", str_qty)
        lbl_dsecription.text = String(format: "%@", obj_accessoryiesModel.str_brief_description!)
        let url = URL(string: obj_accessoryiesModel.str_cover_image)
        imageView_thumb.setImageFrom(url)
    }
    
    func updateCellwithModal(obj_protienModel:ProtienDetailModel, str_qty:String){
        lbl_title.text = String(format: "%@", obj_protienModel.str_product_title!)
        lbl_price.text = String(format: "%@%@/%@", obj_protienModel.str_currency!,obj_protienModel.str_amount!,obj_protienModel.str_per!)
        lbl_quantitiy.text = String(format: "Qty: %@", str_qty)
        lbl_dsecription.text = String(format: "%@", obj_protienModel.str_brief_description!)
        let url = URL(string: obj_protienModel.str_cover_image)
        imageView_thumb.setImageFrom(url)
    }
    
    func updateCellwithModalGym(obj_protienModel:GymDetailModel){
        lbl_title.text = String(format: "%@", obj_protienModel.str_gym_title!)
        //lbl_price.text = String(format: "%@%@/%@", obj_protienModel.str_currency!,obj_protienModel.str_amount!,obj_protienModel.str_per!)
       // lbl_quantitiy.text = String(format: "Qty: %@", str_qty)
        lbl_dsecription.text = String(format: "%@", obj_protienModel.str_brief_description!)
        let url = URL(string: obj_protienModel.str_cover_image)
        imageView_thumb.setImageFrom(url)
    }
    
    func updateCellwithModalTrainer(obj_protienModel:TrainerDetailModel){
        lbl_title.text = String(format: "%@", obj_protienModel.name!)
        lbl_dsecription.text = String(format: "%@", obj_protienModel.str_about)
        let url = URL(string: obj_protienModel.str_coverImage)
        imageView_thumb.setImageFrom(url)
    }
    
}

