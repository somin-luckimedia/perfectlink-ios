//
//  OrderShippingCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 16/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit


class OrderShippingCell: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_address: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_shippingAddress:ShippingAddressModel){
        lbl_title.text = String(format: "%@", obj_shippingAddress.str_recipient_name!)
        
        var str_address:String = ""
        if obj_shippingAddress.str_phone_number != "" {
            str_address.append(String(format: "%@-%@\n\n", obj_shippingAddress.str_country_code, obj_shippingAddress.str_phone_number))
        }
        
        if obj_shippingAddress.str_line1 != "" {
            str_address.append(String(format: "%@, %@\n", obj_shippingAddress.str_line1,obj_shippingAddress.str_line2))
        }
//        if obj_shippingAddress.str_line2 != "" {
//            str_address.append(String(format: "%@\n", obj_shippingAddress.str_line2))
//        }
        if obj_shippingAddress.str_land_mark != "" {
            str_address.append(String(format: "Landmark: %@\n", obj_shippingAddress.str_land_mark))
        }
        if obj_shippingAddress.str_city != "" {
            str_address.append(String(format: "%@-%@\n", obj_shippingAddress.str_city, obj_shippingAddress.str_postal_code))
        }
        if obj_shippingAddress.str_state != "" {
            str_address.append(String(format: "%@\n", obj_shippingAddress.str_state))
        }

        lbl_address.text = str_address
    }
    
   
    
}

