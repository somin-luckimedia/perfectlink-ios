//
//  ApplyCodeCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 16/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

protocol PromocodeCellDelegate:class {
    func promocode_btn_clicked(cell: ApplyCodeCell, btn: UIButton)
}

class ApplyCodeCell: UITableViewCell {

    @IBOutlet weak var btn_title: UIButton!
    @IBOutlet weak var lbl_promocode: UILabel!
     weak var delegate:PromocodeCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
     @IBAction func promocode_btn_clicked(_ sender: UIButton) {
        self.delegate?.promocode_btn_clicked(cell: self, btn: sender)
        
    }
    
}
