//
//  OrderAmountCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 16/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class OrderAmountCell: UITableViewCell {
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_value: UILabel!
    
    @IBOutlet var lbl_payable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateCellwithModal(str_title:String, str_value:String,str_payable:String){
        lbl_title.text = str_title
        lbl_value.text = str_value
        lbl_payable.text = str_payable
    }
    
}
