//
//  ForgetPasswordVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 20/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire

class ForgetPasswordVC: UIViewController {
    
    @IBOutlet weak var textfield_email: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
     //   self.title = "FORGOT PASSWORD"
     //   self.addLeftMenuButtonWithImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //   self.navigationController?.isNavigationBarHidden = true
    }
    
//    func addLeftMenuButtonWithImage(){
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//        let button = UIButton.init(type: .custom)
//        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
//        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
//        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
//        let barButton = UIBarButtonItem(customView: button)
//        self.navigationItem.leftBarButtonItem = barButton
//    }
    
    
    
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnForgetPasswordTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func submit_btn_clicked(_ sender: Any) {
        let str_email  : String = (textfield_email.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let postParam: Parameters = [ "email":str_email]

        if !changepasswordValidation() {
            return
        }
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        forgotPWCall()
        
        
//        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.forget_password, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
//                print(successDict)
//                if (successDict.value(forKey: "STATUS") as? String) == "true"{
//                    DispatchQueue.main.async { //8
//                        self.textfield_email.text = ""
//                    }
//
//                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
//                }else{
//                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
//                }
//            }) { (dictFailure) in
//                if (dictFailure.value(forKey: "message") as? String) != nil {
//                    DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
//                }
//        }
        
        
        
        
        
    }
    
    
    
    
    func changepasswordValidation() -> Bool {
        
        if (self.textfield_email.text?.isEmpty)!{
            DataUtil.alertMessage("Email Id is Empty", viewController: self)
            return false
        }
        let isValidEmail = ValidateUtils.isValidEmail(self.textfield_email.text!)
        if isValidEmail == false {
            DataUtil.alertMessage("Please provide the valid Email.", viewController: self)
            return false
        }
        return true
    }
}

//MARK:- API CALL
extension ForgetPasswordVC {
    func forgotPWCall() {
        let str_email  : String = (textfield_email.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
         let params = ["email":str_email]
        ServiceManager.viewController = self
        ServerCommunication.getDataWithPost(url: ConstantFiles.forgetpassword, parameter: params, viewController: self, success: { (successDict) in
            //   SynapseUserDefault.shared.setGenId(userName: self.txtGenId.text!)
            // SynapseUserDefault.shared.setPassword(password: self.txtFieldPassword.text!)
            // SynapseUserDefault.shared.setUserId(userId: successDict.value(forKey: "userId") as! Int)
           print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{

                showAlertWithTitleFromVC(vc: self, title: "", andMessage: (successDict.value(forKey: "message") as? String)!, buttons: ["OK"]) { (index) in
                    if index == 0 {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            } else {
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }){ (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
}
