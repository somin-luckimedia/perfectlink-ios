//
//  VerifyOTPVC.swift
//  IslandTransfer
//
//  Created by Ashish Mishra on 20/03/18.
//  Copyright © 2018 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire

class VerifyOTPVC: UIViewController,UITextFieldDelegate  {

   
    @IBOutlet weak var textfiled_OTP1: UITextField!
    @IBOutlet weak var textfiled_OTP2: UITextField!
    @IBOutlet weak var textfiled_OTP3: UITextField!
    @IBOutlet weak var textfiled_OTP4: UITextField!
    @IBOutlet weak var btn_resend: UIButton!
    var str_otp: String! = ""
    var str_phone: String! = ""


    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLeftMenuButtonWithImage()
        self.textfiled_OTP1.becomeFirstResponder()
        self.textfiled_OTP1.delegate = self
        self.textfiled_OTP2.delegate =  self
        self.textfiled_OTP3.delegate = self
        self.textfiled_OTP4.delegate = self
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
 
        // Do any additional setup after loading the view.
    }
    
   
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "VERIFICATION  "
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func show_verify_view()  {
        DispatchQueue.main.async {
            self.btn_resend.isUserInteractionEnabled = false
        }
    }
    

    
    func countdownFinished(){
        self.btn_resend.isUserInteractionEnabled = true
    }
    
    
  
    
  
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {

    }
    
     public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        let boolvalue:Bool = newString.length <= maxLength
        
        if boolvalue {
            if let nextTextfield:UITextField = self.view.viewWithTag(textField.tag+1) as? UITextField{
                 textField.text = string
                if string != ""{
                nextTextfield.becomeFirstResponder()
                }else{
                    if let previousTextfield:UITextField = self.view.viewWithTag(textField.tag-1) as? UITextField{
                        textField.text = string
                        previousTextfield.becomeFirstResponder()
                    }else{
                        if textField.tag == 101{
                            textField.text = ""
                        }
                    }
                    
                    
                }
               
            }else{
                if textField.tag == 104{
                    textField.text = string
                    if string != ""{
                    textField.resignFirstResponder()
                        return true
                    }
                    
                    if let previousTextfield:UITextField = self.view.viewWithTag(textField.tag-1) as? UITextField{
                        textField.text = string
                        previousTextfield.becomeFirstResponder()
                    }else{
                        if textField.tag == 101{
                            textField.text = ""
                        }
                    }
                    
                    
                    
                }
            }
            
        }
        
        return false
        
    }
     public func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        return true
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        
        return true
    }
    
    
    @IBAction func submit_btn_clicked(_ sender: Any) {
        self.view.endEditing(true)
        
        let otp1  : String = (self.textfiled_OTP1.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let otp2  : String = (self.textfiled_OTP2.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let otp3  : String = (self.textfiled_OTP3.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let otp4  : String = (self.textfiled_OTP4.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        
        let completeOTP:String = otp1+otp2+otp3+otp4

        if(completeOTP.count != 4){
             DataUtil.alertMessage("Please enter valid OTP.", viewController: self)
            return
        }
        
        if (completeOTP != str_otp){
            DataUtil.alertMessage("Please enter valid OTP.", viewController: self)
            return
        }
        
        let postParam: Parameters = ["phone":str_phone!]
        print("Login Post Parameter : \(postParam)")
        //let imageTest:UIImage = UIImage(named: "back_image")!
        
        // DataUtil.appdelegate().objUserInfo = UserInfo(dictUserInfo: successDict["response"] as! NSDictionary)
     //   let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
       // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
        
     
        
       /* ServerCommunication.getDataWithPost(url: ConstantFiles.checkProfileAvailability, parameter: postParam, viewController: self, success: { (successDict) in
            //   SynapseUserDefault.shared.setGenId(userName: self.txtGenId.text!)
            // SynapseUserDefault.shared.setPassword(password: self.txtFieldPassword.text!)
            // SynapseUserDefault.shared.setUserId(userId: successDict.value(forKey: "userId") as! Int)
            if (successDict.value(forKey: "status") as? String) == "true"{
                //let rootVC : HomeVC = HomeVC(nibName: "HomeVC", bundle: nil)
              //  self.navigationController?.pushViewController(rootVC, animated: true)
                let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
                objAppDelgate.loadSidePanel(objVC: self.navigationController!)
            }else{
                 DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
            
       
            print(successDict)
            // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
        }) { (dictFailure) in
            
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }*/
        
        
        
    }
    
    @IBAction func resend_btn_clicked(_ sender: Any) {
        
        
        
    }
    
    
}
