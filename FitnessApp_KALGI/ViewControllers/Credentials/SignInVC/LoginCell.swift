//
//  LoginCell.swift
//  IslandTransfer
//
//  Created by Ashish Mishra on 18/03/18.
//  Copyright © 2018 Ashish Mishra. All rights reserved.
//

import UIKit
import GoogleSignIn



protocol LoginCellDelegate:class {
    func login_btn_clicked(cell: LoginCell, btn: UIButton)
    func signup_btn_clicked(cell: LoginCell, btn: UIButton)
    func btnShowPassword(cell: LoginCell, btn: UIButton)
    func sociallogin_btn_clicked(cell: LoginCell, btn: UIButton)
    func forgetpassword_btn_clicked(cell: LoginCell, btn: UIButton)
    //forgetpassword_btn_clicked
}



class LoginCell: UITableViewCell, UITextFieldDelegate {
    
    weak var delegate: LoginCellDelegate?
    var cellIndex:NSInteger = 0
    
    @IBOutlet weak var googleBtn: GIDSignInButton!
    
    @IBOutlet var vwGoogle: UIView!
    @IBOutlet var vwFaceook: UIView!
    @IBOutlet var vwApple: UIView!

    
    @IBOutlet weak var stacviewSignInwithApple: UIStackView!
    @IBOutlet weak var textfield_email: UITextField!
    @IBOutlet weak var textfield_password: UITextField!
    

    @IBOutlet weak var btn_signup: UIButton!
    
    let yourAttributes : [NSAttributedString.Key: Any] = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13),
        NSAttributedString.Key.foregroundColor : UIColor.white,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
    //.styleDouble.rawValue, .styleThick.rawValue, .styleNone.rawValue
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        vwFaceook.layer.cornerRadius = 5.0
        vwFaceook.layer.borderWidth = 0.5
        vwFaceook.layer.borderColor = UIColor.white.cgColor
        vwGoogle.layer.cornerRadius = 5.0
        vwGoogle.layer.borderWidth = 0.5
        vwGoogle.layer.borderColor = UIColor.white.cgColor
        vwApple.layer.cornerRadius = 5.0
        vwApple.layer.borderWidth = 0.5
        vwApple.layer.borderColor = UIColor.white.cgColor
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configureWithOwner(controller:SignInVC){
        self.textfield_email.delegate = controller;
        self.textfield_password.delegate = controller;
        let attributeString = NSMutableAttributedString(string: "Sign In",
                                                        attributes: yourAttributes)
//        btn_signup.setAttributedTitle(attributeString, for: .normal)
    }
    
    
    @IBAction func login_btn_clicked(_ sender: UIButton) {
        DataUtil.appdelegate().is_social = false
        self.delegate?.login_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func btnAppleSignclicked(_ sender: UIButton) {
        DataUtil.appdelegate().is_social = false
        self.delegate?.sociallogin_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func btnFBTapped(_ sender: UIButton) {
        DataUtil.appdelegate().is_social = true
        self.delegate?.sociallogin_btn_clicked(cell: self, btn: sender)
    }
    @IBAction func btnGoogleTapped(_ sender: UIButton) {
        DataUtil.appdelegate().is_social = true
        self.delegate?.sociallogin_btn_clicked(cell: self, btn: sender)

    }
    
    @IBAction func signup_btn_clicked(_ sender: UIButton) {
        DataUtil.appdelegate().is_social = false
        self.delegate?.signup_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func btnShowPassword(_ sender: UIButton) {
        self.delegate?.btnShowPassword(cell: self, btn: sender)
    }
    
    
    @IBAction func forgetpassword_btn_clicked(_ sender: UIButton) {
        self.delegate?.forgetpassword_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func social_login_clicked(_ sender: UIButton) {
        DataUtil.appdelegate().is_social = true
        self.delegate?.sociallogin_btn_clicked(cell: self, btn: sender)
    }
}
