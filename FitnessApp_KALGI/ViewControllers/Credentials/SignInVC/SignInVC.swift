//
//  SignInVC.swift
//  FitnessApp_KALGI
//
//  Created by Yogesh on 2020-09-29.
//

import UIKit
import FacebookLogin
import FBSDKLoginKit
import GoogleSignIn
import FirebaseFirestore
import AuthenticationServices
import Firebase

import CryptoKit

class SignInVC: UIViewController,UITextFieldDelegate,LoginCellDelegate,GIDSignInDelegate {
   
    @IBOutlet weak var tableview_login: UITableView!
    @IBOutlet var vwSignUp: UIView!
    var logincell: LoginCell!
    var str_userType:String = "2"
    var iconClick = true
    var btnClickSocial : String = "Normal"
    @IBOutlet var btnUser: UIButton!
    @IBOutlet var btnTrainer: UIButton!
    @IBOutlet var imgSplashIcon: UIImageView!
    @IBOutlet var imgSplashImage: UIImageView!
    @IBOutlet var tabBarObj: UITabBarController!
    fileprivate var currentNonce: String?
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")


}

extension SignInVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        tableview_login.delegate  = self as UITableViewDelegate
        tableview_login.dataSource = self as UITableViewDataSource
        tableview_login.reloadData()
        vwSignUp.isHidden = true
        tabBarObj.tabBar.isTranslucent = false
        UITabBar.appearance().barTintColor = .black
        btnUser.backgroundColor = #colorLiteral(red: 1, green: 0.3199719489, blue: 0.2844669521, alpha: 1)
        btnUser.titleLabel?.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        btnTrainer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        btnTrainer.titleLabel?.tintColor = #colorLiteral(red: 1, green: 0.3199719489, blue: 0.2844669521, alpha: 1)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let isUserLogedIn = UserDefaults.standard.bool(forKey: "LogedIn")
        if isUserLogedIn {
            let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
            let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
            
            let postParam = [ "user_id":str_userid]
            
            let headers = [
                "Authorization": String(format: "Bearer %@",  (str_accessToken)),
                "Accept": "application/json"
            ]
            ServerCommunication.getDataWithPostHeader(url: ConstantFiles.get_friend_info, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
                print(successDict)
                if (successDict.value(forKey: "STATUS") as? String) == "true" {
                    DataUtil.appdelegate().objUserInfo = UserInfo(dictUserInfo: successDict["response"] as! NSDictionary)
                    let responseDict:NSDictionary = successDict["response"] as! NSDictionary
                    print(responseDict)
                    UserDefaults.standard.synchronize()
                    self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
                    
//                    self.tabBarController?.selectedIndex = 1

                   // if DataUtil.appdelegate().objUserInfo?.str_userType == "2" {
                       // self.navigationController?.navigationBar.isHidden = true
                    let dict : NSMutableDictionary = [:]
                    dict.setValue(DataUtil.appdelegate().objUserInfo?.userId, forKey: "userId")
                    dict.setValue(DataUtil.appdelegate().objUserInfo?.profilePic, forKey: "pic")
                    dict.setValue(DataUtil.appdelegate().objUserInfo?.name, forKey: "name")
                    
                    let currentUser = self.userref.child((DataUtil.appdelegate().objUserInfo?.userId)!)
                    currentUser.setValue(dict)

                        self.navigationController?.pushViewController(self.tabBarObj, animated: true)
                   // }
//                    else {
//                      //  self.navigationController?.navigationBar.isHidden = false
//                        let obj_trainerHomeVC:TrainerHomeVC = TrainerHomeVC(nibName: "TrainerHomeVC", bundle: nil)
//                        self.navigationController?.pushViewController(obj_trainerHomeVC, animated: true)
//
//                    }
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
            }) { (dictFailure) in
                if (dictFailure.value(forKey: "message") as? String) != nil {
                    DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
                }
            }
        }
        else {
            imgSplashIcon.isHidden = true
            imgSplashImage.isHidden = true
        }
    }
}

//MARK: Button action methods
extension SignInVC {
    func btnShowPassword(cell: LoginCell, btn: UIButton) {
        if(iconClick == true) {
            logincell.textfield_password.isSecureTextEntry = false
        } else {
            logincell.textfield_password.isSecureTextEntry = true
        }
        iconClick = !iconClick
    }
    
    func sociallogin_btn_clicked(cell: LoginCell, btn: UIButton) {
        switch btn.tag {
        case 101:
            vwSignUp.isHidden = false
            btnClickSocial = "FACEBOOK"
            
            break
        case 102:
            vwSignUp.isHidden = false
            btnClickSocial = "GOOGLEPLUS"

            break
            
        case 103:
            vwSignUp.isHidden = false
            btnClickSocial = "APPLE"
            
            break
            
        default:
            break
        }
        
    }
    
    func forgetpassword_btn_clicked(cell: LoginCell, btn: UIButton) {
        let rootVC : ForgetPasswordVC = ForgetPasswordVC(nibName: "ForgetPasswordVC", bundle: nil)
         self.navigationController?.pushViewController(rootVC, animated: true)
    }
    
    func login_btn_clicked(cell: LoginCell, btn: UIButton) {
        let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate
        let strEmail  : String = (logincell.textfield_email.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strPassword  : String = (logincell.textfield_password.text)!
        let strDeviceToken:String = (objAppDelgate1.str_devicetoken)!
        let strDeviceType:String = "IOS"
        let strLat:String = DataUtil.appdelegate().str_lattitude
        let strLong:String = DataUtil.appdelegate().str_longitude
        
        //email, password, user_id, login_type, device_token, device_type, lat, lng
        //let strCountrycode1:String = "+91"
        
        let postParam = ["email":strEmail,  "password":strPassword,"device_type":strDeviceType,"device_token":strDeviceToken,"lat":strLat, "lon":strLong]
        print("Login Post Parameter : \(postParam)")
        if !loginValidation() {
            return
        }
        ServerCommunication.getDataWithPost(url: ConstantFiles.loginUrl, parameter: postParam, viewController: self, success: { (successDict) in
            //   SynapseUserDefault.shared.setGenId(userName: self.txtGenId.text!)
            // SynapseUserDefault.shared.setPassword(password: self.txtFieldPassword.text!)
            // SynapseUserDefault.shared.setUserId(userId: successDict.value(forKey: "userId") as! Int)
           print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.appdelegate().objUserInfo = UserInfo(dictUserInfo: successDict["response"] as! NSDictionary)
                DataUtil.appdelegate().objUserInfo?.str_accessToken = successDict["access_token"] as! String
                let responseDict:NSDictionary = successDict["response"] as! NSDictionary
                print(responseDict)
                self.registernewuser()
                let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
                UserDefaults.standard.set(DataUtil.appdelegate().objUserInfo?.userId, forKey: "LogedInUserID")
                UserDefaults.standard.set( DataUtil.appdelegate().objUserInfo?.str_accessToken, forKey: "LogedInUserAccessToken")
                UserDefaults.standard.set(true, forKey: "LogedIn")
                UserDefaults.standard.set(strEmail, forKey: "Email")
                let dict : NSMutableDictionary = [:]
                dict.setValue(DataUtil.appdelegate().objUserInfo?.userId, forKey: "userId")
                dict.setValue(DataUtil.appdelegate().objUserInfo?.profilePic, forKey: "pic")
                dict.setValue(DataUtil.appdelegate().objUserInfo?.name, forKey: "name")

                let currentUser = self.userref.child((DataUtil.appdelegate().objUserInfo?.userId)!)
                currentUser.setValue(dict)
                
                

                UserDefaults.standard.synchronize()
                self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
               // self.navigationController?.pushViewController(self.tabBarObj, animated: true)
//                if DataUtil.appdelegate().objUserInfo?.str_userType == "2" {
                   // self.navigationController?.navigationBar.isHidden = true
                    self.navigationController?.pushViewController(self.tabBarObj, animated: true)
//                }
//                else {
//                  //  self.navigationController?.navigationBar.isHidden = false
//                    let obj_trainerHomeVC:TrainerHomeVC = TrainerHomeVC(nibName: "TrainerHomeVC", bundle: nil)
//                    self.navigationController?.pushViewController(obj_trainerHomeVC, animated: true)
//
//                }

//                objAppDelgate.loadSidePanelWithScan(objVC: self.navigationController!)
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
            
     
            
            
           
            // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func btnLoginWithAppleIDClicked()
    {
        if #available(iOS 13.0, *) {
//            MBProgressHUD.showAdded(to: self.view, animated: true)
            let nonce = randomNonceString()
            currentNonce = nonce
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            request.nonce = sha256(nonce)

            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
    }
    
    @available(iOS 13, *)
    private func sha256(_ input: String) -> String {
      let inputData = Data(input.utf8)
      let hashedData = SHA256.hash(data: inputData)
      let hashString = hashedData.compactMap {
        return String(format: "%02x", $0)
      }.joined()

      return hashString
    }
    
    private func randomNonceString(length: Int = 32) -> String {
      precondition(length > 0)
      let charset: Array<Character> =
          Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
      var result = ""
      var remainingLength = length

      while remainingLength > 0 {
        let randoms: [UInt8] = (0 ..< 16).map { _ in
          var random: UInt8 = 0
          let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
          if errorCode != errSecSuccess {
            fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
          }
          return random
        }

        randoms.forEach { random in
          if remainingLength == 0 {
            return
          }

          if random < charset.count {
            result.append(charset[Int(random)])
            remainingLength -= 1
          }
        }
      }

      return result
    }
    
    func registernewuser(){
        
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let str_name:String = String(format:(DataUtil.appdelegate().objUserInfo?.name)!)
        let str_profile:String = String(format:(DataUtil.appdelegate().objUserInfo?.profilePic)!)
        let str_token:String = String(format:(DataUtil.appdelegate().objUserInfo?.devicetoken)!)
        
        let db = Firestore.firestore()
        let id = str_userid
        db.collection("users").document(id).setData([
            "id":id,"name":str_name,"profileImage":str_profile,"registrationTokens":str_token])
    }
    
    func signup_btn_clicked(cell: LoginCell, btn: UIButton) {
        vwSignUp.isHidden = false
        btnClickSocial = "NORMAL"

    }
    @IBAction func btnTrainerTapped(_ sender: UIButton) {
        btnTrainer.backgroundColor = #colorLiteral(red: 1, green: 0.3199719489, blue: 0.2844669521, alpha: 1)
        btnTrainer.setTitleColor(.white, for: .normal)
        
        btnUser.backgroundColor = .white
        btnUser.setTitleColor(#colorLiteral(red: 1, green: 0.3199719489, blue: 0.2844669521, alpha: 1), for: .normal)
        str_userType = "3"
        vwSignUp.isHidden = true
        if btnClickSocial == "NORMAL" {
            let rootVC : SignUpVC = SignUpVC(nibName: "SignUpVC", bundle: nil)
            rootVC.str_provider = btnClickSocial
            rootVC.strUserType = self.str_userType
            self.navigationController?.pushViewController(rootVC, animated: true)
        }
        else if btnClickSocial == "FACEBOOK" {
            loginButtonClicked()
        }
        else if btnClickSocial == "GOOGLEPLUS" {
            initiateGooglePlus()
        }
        else if btnClickSocial == "APPLE" {
            btnLoginWithAppleIDClicked()
        }
    }
    @IBAction func btnUserTapped(_ sender: UIButton) {
        btnUser.backgroundColor = #colorLiteral(red: 1, green: 0.3199719489, blue: 0.2844669521, alpha: 1)
        btnUser.setTitleColor(.white, for: .normal)
        
        btnTrainer.backgroundColor = .white
        btnTrainer.setTitleColor(#colorLiteral(red: 1, green: 0.3199719489, blue: 0.2844669521, alpha: 1), for: .normal)
        str_userType = "2"
        vwSignUp.isHidden = true
        if btnClickSocial == "NORMAL" {
        let rootVC : SignUpVC = SignUpVC(nibName: "SignUpVC", bundle: nil)
        rootVC.str_provider = btnClickSocial
        rootVC.strUserType = self.str_userType
        self.navigationController?.pushViewController(rootVC, animated: true)
        }
        else if btnClickSocial == "FACEBOOK" {
            loginButtonClicked()
        }
        else if btnClickSocial == "GOOGLEPLUS" {
            initiateGooglePlus()
        }
        else if btnClickSocial == "APPLE" {
            btnLoginWithAppleIDClicked()
        }
    }
}

//MARK: Custom Methods
extension SignInVC {
    //when login button clicked
    @objc func loginButtonClicked() {
        
            let loginManager = LoginManager()
            loginManager.logIn(
                permissions: [.publicProfile, .email],
                viewController: self
            ) { result in
                self.loginManagerDidComplete(result)
            }
    }
    
    func loginManagerDidComplete(_ result: LoginResult) {
        switch result {
        case .cancelled:
            print("cancelled")
        case .failed(let error):
  print(error)
        case .success(let grantedPermissions, let declinedPermissions, let accessToken):
            self.getFBUserData()
        }
    }
    
    //function is fetching the user data
    func getFBUserData(){
        //if((AccessToken.current()) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print("Result FB", result as Any)
                    self.check_for_login(result: result as! Dictionary< String, Any>,login_type: "FACEBOOK")
                }
            })
        //}
    }
    
    func initiateGooglePlus(){
        GIDSignIn.sharedInstance().clientID = "1082791936420-rchned9pnbr6a9hkg2o66pjs3gpm88jb.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
       // GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().presentingViewController = self;
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let userId:String = user.userID as String               // For client-side use only!
            let fullName = user.profile.name as String
            let email = user.profile.email as String
          
            //name email
            var dict_google = Dictionary<String, Any>()
            dict_google["id"] = userId
            dict_google["name"] = fullName
            dict_google["email"] = email
            
            check_for_login(result: dict_google,login_type: "GOOGLEPLUS")
            print(userId)
            
            // ...
        }
    }
   
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    func check_for_login(result:Dictionary<String,Any>,login_type:String)  {
        let dict_facebbok:Dictionary = result
     
        print(dict_facebbok)
        
        let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate

        // let strEmail  : String = dict_facebbok["email"] as! String
       // let strName  :String = dict_facebbok["name"] as! String
        let socialId  :String = dict_facebbok["id"] as! String
        let strDeviceToken:String = (objAppDelgate1.str_devicetoken)!
        let strDeviceType:String = "IOS"
        let strLat:String = objAppDelgate1.str_lattitude
        let strLong:String = objAppDelgate1.str_longitude
        //let strUserId:String = ""
        
        
        let postParam = ["social_id":socialId,"device_type":strDeviceType,"device_token":strDeviceToken,"lat":strLat, "lon":strLong, "provider":login_type]
        print("Login Post Parameter : \(postParam)")
        //let imageTest:UIImage = UIImage(named: "back_image")!
        
        
        //GIDSignIn.sharedInstance().signOut()
        ServerCommunication.getDataWithPost(url: ConstantFiles.socialLoginUrl, parameter: postParam, viewController: self, success: { (successDict) in
            //   SynapseUserDefault.shared.setGenId(userName: self.txtGenId.text!)
            // SynapseUserDefault.shared.setPassword(password: self.txtFieldPassword.text!)
            // SynapseUserDefault.shared.setUserId(userId: successDict.value(forKey: "userId") as! Int)
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.appdelegate().objUserInfo = UserInfo(dictUserInfo: successDict["response"] as! NSDictionary)
                DataUtil.appdelegate().objUserInfo?.str_accessToken = successDict["access_token"] as! String
                let responseDict:NSDictionary = successDict["response"] as! NSDictionary
                print(responseDict)
                UserDefaults.standard.set(DataUtil.appdelegate().objUserInfo?.userId, forKey: "LogedInUserID")

                 UserDefaults.standard.set( DataUtil.appdelegate().objUserInfo?.str_accessToken, forKey: "LogedInUserAccessToken")
                UserDefaults.standard.set(true, forKey: "LogedIn")
                UserDefaults.standard.synchronize()
                let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
                USER_DEFAULT.set(true, forKey: IS_LOGGED_IN)
                USER_DEFAULT.synchronize()
                
//                if DataUtil.appdelegate().objUserInfo?.str_userType == "2" {
                   // self.navigationController?.navigationBar.isHidden = true
                    self.navigationController?.pushViewController(self.tabBarObj, animated: true)
//                }
//                else {
//                  //  self.navigationController?.navigationBar.isHidden = false
//                    let obj_trainerHomeVC:TrainerHomeVC = TrainerHomeVC(nibName: "TrainerHomeVC", bundle: nil)
//                    self.navigationController?.pushViewController(obj_trainerHomeVC, animated: true)
//
//                }

//                objAppDelgate.loadSidePanelWithScan(objVC: self.navigationController!)
              
            }else{
                
                let rootVC : SignUpVC = SignUpVC(nibName: "SignUpVC", bundle: nil)
                rootVC.dict_facebook = dict_facebbok
                rootVC.str_provider = self.btnClickSocial
                rootVC.strUserType = self.str_userType
                self.navigationController?.pushViewController(rootVC, animated: true)
            }
            // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
              //  DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
                //
               // let obj_socialsignupVc : SocialSignupVC = SocialSignupVC(nibName: "SocialSignupVC", bundle: nil)
               // obj_socialsignupVc.social_dict = dict_facebbok
                //obj_socialsignupVc.strLogintype = strLogintype
                //self.navigationController?.pushViewController(obj_socialsignupVc, animated: true)
                //STATUS
                if (dictFailure.value(forKey: "STATUS") as? String) == "false"{
                    let rootVC : SignUpVC = SignUpVC(nibName: "SignUpVC", bundle: nil)
                    rootVC.dict_facebook = dict_facebbok
                    rootVC.str_provider = login_type
                    rootVC.strUserType = self.str_userType
                    self.navigationController?.pushViewController(rootVC, animated: true)
                }
                
            }
        }
        
    }
    
    func loginValidation() -> Bool {

        if (logincell.textfield_email.text?.isEmpty)!{
            DataUtil.alertMessage("Email Id is Empty", viewController: self)
            return false
        }
        let isValidEmail = ValidateUtils.isValidEmail(logincell.textfield_email.text!)
        if isValidEmail == false {
            DataUtil.alertMessage("Please provide the valid Email.", viewController: self)
            return false
        }
        
       
        if (logincell.textfield_password.text?.isEmpty)!{
            DataUtil.alertMessage("Password is Empty", viewController: self)
            return false
        }
        return true
    }
}

//MARK: Logincell delegate methods
extension SignInVC {
//    func login_btn_clicked(cell: LoginCell, btn: UIButton) {
//
//    }
//
//    func signup_btn_clicked(cell: LoginCell, btn: UIButton) {
//
//    }
//
//    func btnShowPassword(cell: LoginCell, btn: UIButton) {
//        if(iconClick == true) {
//            logincell.textfield_password.isSecureTextEntry = false
//        } else {
//            logincell.textfield_password.isSecureTextEntry = true
//        }
//
//        iconClick = !iconClick
//    }
//
//    func sociallogin_btn_clicked(cell: LoginCell, btn: UIButton) {
//
//    }
//
//    func forgetpassword_btn_clicked(cell: LoginCell, btn: UIButton) {
//
//    }
}

//MARK: Tableview delegate & datasource methods
extension SignInVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "LoginCell"
        logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? LoginCell
        if logincell == nil {
            tableView.register(UINib(nibName: "LoginCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? LoginCell
        }
        logincell.cellIndex = indexPath.row
        logincell.configureWithOwner(controller: self)
        logincell.delegate = self
       // self.setupProviderLoginView()
        return logincell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 616
    }
}

extension SignInVC {
    func initateAppleLogin(){
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
}

extension SignInVC: ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    /// - Tag: did_complete_authorization
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            // Create an account in your system.
            let userIdentifier = appleIDCredential.user
            
            let fullName = appleIDCredential.fullName?.givenName
            let email = appleIDCredential.email
            
            var dict_google = Dictionary<String, Any>()
            dict_google["id"] = userIdentifier
            dict_google["name"] = fullName
            dict_google["email"] = email
            check_for_login(result: dict_google,login_type: "APPLE")
            
        default:
            break
        }
    }
}

