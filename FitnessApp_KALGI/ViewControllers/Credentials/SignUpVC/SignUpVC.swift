//
//  SignUpVC.swift
//  FitnessApp_KALGI
//
//  Created by Yogesh on 2020-09-29.
//

import UIKit
import ObjectMapper
import Firebase
import FirebaseFirestore
import Alamofire


class SignUpVC: UIViewController, UITextFieldDelegate,SignupCellDelegate,IQActionSheetPickerViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    
    var dictSideMenu:NSArray = []
    var signupcell: SIgnupCell!
    var strCountryCode:String = "+91"
    var str_gender:String = ""
    var str_height:String = "5.5"
    var str_weight:String = "75"
    var str_dob:String = ""
    var strUserType: String = "2"
    var strIdtype:String = "1"
    var selectedImage:UIImage?
    let array_geneder:[String] = ["Male", "Female"]
    @IBOutlet weak var tableview_signup: UITableView!
    var str_category:String = ""
    var str_provider:String = ""
    var dict_facebook:Dictionary<String,Any>? = nil
    var social_id : String = ""
    @IBOutlet var tabBarObj: UITabBarController!

}


//MARK: VIEW LIFE CYCLE METHODS

extension SignUpVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        tableview_signup.delegate = self
        tableview_signup.dataSource = self
        dictSideMenu = DataUtil.readCountryJson(fileName: "CountryCodes") as NSArray
        tabBarObj.tabBar.isTranslucent = false
        UITabBar.appearance().barTintColor = .black

        
    }
    override func viewWillAppear(_ animated: Bool) {

    }
    
    
    
}

//MARK: BUTTON ACTION METHODS

extension SignUpVC {
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK: TABLEVIEW DELEGATE & DATASOURCE METHODS
extension SignUpVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "SIgnupCell"
        signupcell = tableView.dequeueReusableCell(withIdentifier: identifier) as? SIgnupCell
        
        if signupcell == nil {
            tableView.register(UINib(nibName: "SIgnupCell", bundle: nil), forCellReuseIdentifier: identifier)
            signupcell = tableView.dequeueReusableCell(withIdentifier: identifier) as? SIgnupCell
            signupcell.selectionStyle = UITableViewCell.SelectionStyle.none
            
        }
        
        
        
        signupcell.cellIndex = indexPath.row
        signupcell.configureWithOwner(controller: self)
        signupcell.delegate = self
        signupcell.textfield_mobile.delegate = self
        if str_provider == "FACEBOOK" || str_provider == "GOOGLEPLUS" || str_provider == "APPLE" {
            signupcell.constPwdHeight.constant = 0
        }
        else {
            signupcell.constPwdHeight.constant = 50
        }
        if str_provider == "FACEBOOK" {
            if (dict_facebook?["name"] != nil){
                signupcell.textfield_username.text = dict_facebook?["name"] as? String
            }
            if (dict_facebook?["first_name"] != nil) {
                signupcell.textfield_firstname.text = dict_facebook?["first_name"] as? String
            }
            if (dict_facebook?["last_name"] != nil) {
                signupcell.textfield_lastname.text = dict_facebook?["last_name"] as? String
            }
            if (dict_facebook?["email"] != nil) {
                signupcell.textfield_email.text = dict_facebook?["email"] as? String
            }
            if (dict_facebook?["id"] != nil) {
                social_id = (dict_facebook?["id"] as? String)!
            }
        }
        else if str_provider == "GOOGLEPLUS" {
            if (dict_facebook?["name"] != nil){
                signupcell.textfield_username.text = dict_facebook?["name"] as? String
            }
            if (dict_facebook?["first_name"] != nil) {
                signupcell.textfield_firstname.text = dict_facebook?["first_name"] as? String
            }
            if (dict_facebook?["last_name"] != nil) {
                signupcell.textfield_lastname.text = dict_facebook?["last_name"] as? String
            }
            if (dict_facebook?["email"] != nil) {
                signupcell.textfield_email.text = dict_facebook?["email"] as? String
            }
            if (dict_facebook?["id"] != nil) {
                social_id = (dict_facebook?["id"] as? String)!
            }
        }
        else if str_provider == "APPLE" {
            if (dict_facebook?["id"] != nil) {
                social_id = (dict_facebook?["id"] as? String)!
            }
            if (dict_facebook?["email"] != nil) {
                signupcell.textfield_email.text = dict_facebook?["email"] as? String
            }
            if (dict_facebook?["fullName"] != nil){
                signupcell.textfield_username.text = dict_facebook?["name"] as? String
            }
        }
       
        
        
        if strUserType == "2"{
            
            signupcell.view_about.isHidden = true
            signupcell.view_height.isHidden = false
            signupcell.view_weight.isHidden = false
            
        }else{
            
            signupcell.view_about.isHidden = false
            signupcell.view_height.isHidden = true
            signupcell.view_weight.isHidden = true
//            signupcell.constraint_height.constant = 80
        }
        
        //  cell.textLabel?.text = monthName
        return signupcell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 830
    }
}

extension SignUpVC {
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Hello delegate")
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //var selectedImage: UIImage?
        if let editedImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")]   as? UIImage {
            selectedImage = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerOriginalImage")] as? UIImage {
            selectedImage = originalImage
        }
        if let selectedImage = selectedImage {
            signupcell.imageview_profile.image = selectedImage;
            signupcell.imageview_profile.layer.cornerRadius = signupcell.imageview_profile.frame.size.height/2
            signupcell.imageview_profile.layer.masksToBounds = true
        }
        self.dismiss(animated: true, completion: nil)
        
        
    }
    func countrycode_btn_clicked(cell:SIgnupCell, btn:UIButton){
        let array: NSArray = (dictSideMenu.value(forKey: "dial_code") as! NSArray)
        let array1: NSArray = (dictSideMenu.value(forKey: "name") as! NSArray)
        var modifiedarray = [String]()
        //let modifiedarray:NSArray = []
        for dict in dictSideMenu {
            
            let name:String = (dict as AnyObject).value(forKey: "name") as! String
            let dialcode:String = (dict as AnyObject).value(forKey: "dial_code") as! String
            
            let finalstring = String(format:"(%@) %@", name, dialcode)
            modifiedarray.append(finalstring)
            
        }
        
        
        
        // var array1:Array = [String]
        
        var groups = [[String]]()
        if let swiftArray = modifiedarray as NSArray as? [String] {
            groups.append(swiftArray)
        }
        
        // print(array)
        
        // groups.append(array as! [String])
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()
        
    }
    
    func signupValidation() -> Bool {
        
        if selectedImage == nil{
            // DataUtil.alertMessage("Please choose profile picture", viewController: self)
            // return false
        }
        
        if (signupcell.textfield_firstname.text?.isEmpty)!{
            DataUtil.alertMessage("First name is Empty", viewController: self)
            return false
        }
        
        if (signupcell.textfield_lastname.text?.isEmpty)!{
            DataUtil.alertMessage("Last name is Empty", viewController: self)
            return false
        }
        
        if (signupcell.textfield_username.text?.isEmpty)!{
            
            DataUtil.alertMessage("User name is Empty", viewController: self)
            return false
        }
        
        if ((signupcell.textfield_username.text?.count)!)<5{
            DataUtil.alertMessage("User name shold be at least 5 caharcters", viewController: self)
            return false
        }
        

        
        
//        if (signupcell.textfield_name.text?.isEmpty)!{
//
//            DataUtil.alertMessage("Name is Empty", viewController: self)
//            return false
//        }
        
        
        if (signupcell.textfield_email.text?.isEmpty)!{
            DataUtil.alertMessage("Email Id is Empty", viewController: self)
            return false
        }
        let isValidEmail = ValidateUtils.isValidEmail(signupcell.textfield_email.text!)
        if isValidEmail == false {
            DataUtil.alertMessage("Please provide the valid Email.", viewController: self)
            return false
        }
        
        if (signupcell.textfield_mobile.text?.isEmpty)!{
            DataUtil.alertMessage("Phone is Empty", viewController: self)
            return false
        }
        
        let isValidPhone = ValidateUtils.isValidPhoneNumber(signupcell.textfield_mobile.text!)
        
        if isValidPhone == false {
            DataUtil.alertMessage("Please provide the valid Phone.", viewController: self)
            return false
        }
        
        if str_provider == "FACEBOOK" || str_provider == "GOOGLEPLUS" || str_provider == "APPLE" {
            
        }
        else {
            if (signupcell.textfield_password.text?.isEmpty)!{
                DataUtil.alertMessage("Password is Empty", viewController: self)
                return false
            }
        }
        
        if (signupcell.textfield_countrycode.text?.isEmpty)!{
            DataUtil.alertMessage("Countrycode is Empty", viewController: self)
            return false
        }
        
        if (signupcell.textfield_gender.text?.isEmpty)!{
            DataUtil.alertMessage("Gender is Empty", viewController: self)
            return false
        }
        
        if (signupcell.textfield_dob.text?.isEmpty)!{
            DataUtil.alertMessage("Date of birth is Empty", viewController: self)
            return false
        }
        
        if strUserType == "2"{
            if (signupcell.textfield_height.text?.isEmpty)!{
                DataUtil.alertMessage("Height is Empty", viewController: self)
                return false
            }
            
            if (signupcell.textfield_weight.text?.isEmpty)!{
                DataUtil.alertMessage("Weight is Empty", viewController: self)
                return false
            }
        }
        
        if signupcell.btn_checkBox.isSelected == false{
            DataUtil.alertMessage("Please accept terms & conditions", viewController: self)
            return false
        }
        
        return true
    }
    
    func signup_btn_clicked(cell: SIgnupCell, btn: UIButton) {
        if !signupValidation() {
           return
        }
        else {
            signupCall()
        }
    }
    
    func signin_btn_clicked(cell: SIgnupCell, btn: UIButton) {
        // signin button
    }
    
    func check_box_clicked(cell: SIgnupCell, btn: UIButton) {
        
        if btn.isSelected {
            btn.isSelected = false
        }else{
            btn.isSelected = true
        }
    }
    
    func gender_btn_clicked(cell: SIgnupCell, btn: UIButton) {
        var groups = [[String]]()
        groups.append(array_geneder)
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()
    }
    
    func trainer_btn_clicked(cell: SIgnupCell, btn: UIButton) {
        var groups = [[String]]()
        groups.append(DataUtil.appdelegate().array_category)
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = 110
        picker.show()
    }
    
    func dob_btn_clicked(cell: SIgnupCell, btn: UIButton) {
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.actionSheetPickerStyle = .datePicker
        picker.delegate = self
        picker.tag = 103
        let date:Date =  Date()
        //  let calendar = Calendar.current
        // let date_today:Date = calendar.date(byAdding: .day, value: 7, to: date)!
        picker.maximumDate = date
        picker.show()
    }
    
    func height_btn_clicked(cell: SIgnupCell, btn: UIButton) {
        var groups = [[String]]()
        groups.append(DataUtil.appdelegate().array_height)
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()
    }
    
    func weight_btn_clicked(cell: SIgnupCell, btn: UIButton) {
        var groups = [[String]]()
        groups.append(DataUtil.appdelegate().array_weight)
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()
    }
    
    func terms_btn_clicked(type: String) {
        
    }
    func opneGallery(){
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openCamera(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    func camera_btn_clicked(cell: SIgnupCell, btn: UIButton) {
        let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert) in
            self.openCamera()
            
        }))
        
        alert.addAction(UIAlertAction(title:"Photo Library", style: .default, handler: { (alert) in
            self.opneGallery()
        }))
        
        alert.addAction(UIAlertAction(title:"Cancel", style: .default, handler: { (alert) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView, didSelectTitlesAtIndexes indexes: [NSNumber]) {
        
        if pickerView.tag == 101{
            let index = indexes[0] as! NSInteger
            let array: NSArray = (dictSideMenu.value(forKey: "dial_code") as! NSArray)
            let title:String = array[index] as! String
            strCountryCode = title
            signupcell?.textfield_countrycode?.text = strCountryCode
            
        }else if pickerView.tag == 102{
            let index = indexes[0] as! NSInteger
            let title:String = array_geneder[index]
            str_gender = title
            signupcell?.textfield_gender?.text = str_gender
        }else if pickerView.tag == 104{
            let index = indexes[0] as! NSInteger
            let title:String = DataUtil.appdelegate().array_height[index]
            str_height = DataUtil.appdelegate().array_height_final[index]
            signupcell?.textfield_height?.text = title
        }else if pickerView.tag == 105{
            let index = indexes[0] as! NSInteger
            let title:String = DataUtil.appdelegate().array_weight[index]
            str_weight = title
            signupcell?.textfield_weight?.text = String(format: "%@ lbs", title)
        }else if pickerView.tag == 110{
            let index = indexes[0] as! NSInteger
            let title:String = DataUtil.appdelegate().array_category[index]
            str_category = title
            signupcell?.textfield_about?.text = title
        }
        
    }
    
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView, didSelect date: Date) {
        
        //DataUtil.stringFromDate(date: date_today, DateFormate: "yyyy-MM-dd")
        str_dob = DataUtil.stringFromDate(date: date, DateFormate: "yyyy-MM-dd")
        signupcell?.textfield_dob.text = str_dob
        // self.lbl_time.text = str_time
    }
}

//MARK: Webservice call methods
extension SignUpVC {
    func signupCall() {
        let str_name  : String = (signupcell.textfield_name.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate
        let strUserName  : String = (signupcell.textfield_username.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strFirstName  : String = (signupcell.textfield_firstname.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strLastName  : String = (signupcell.textfield_lastname.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        
        let strEmail  : String = (signupcell.textfield_email.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strMobile  : String = (signupcell.textfield_mobile.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        var strPassword  : String = (signupcell.textfield_password.text)!
        if str_provider == "FACEBOOK" || str_provider == "GOOGLEPLUS" || str_provider == "APPLE" {
            strPassword = ""
        }

        let strDeviceToken:String = (objAppDelgate1.str_devicetoken)!
        let strDeviceType:String = "IOS"
        let strAboutType : String = (signupcell.txtAbout.text)!
        let strLat:String = (objAppDelgate1.str_lattitude)
        let strLong:String = (objAppDelgate1.str_longitude)
        var headers: [String : String]  = ["":""]
        var category_index : Int = 0
        if strUserType == "3" {
        category_index = DataUtil.appdelegate().array_category.firstIndex(of: str_category)!
        }
        
        if DataUtil.appdelegate().objUserInfo != nil{
            headers = [
                "Authorization": String(format: "Bearer %@",  (DataUtil.appdelegate().objUserInfo?.str_accessToken)!),
                "Accept": "application/json"
            ]
        }else{
            headers = [
                "Accept": "application/json"
            ]
        }
        let params = ["user_name":strUserName,"email":strEmail,"password":strPassword,"user_type":strUserType, "country_code":strCountryCode, "phone_number":strMobile,"gender":str_gender, "dob":str_dob, "confirm_password":strPassword,"profile_picture" : "", "device_type":strDeviceType,"device_token":strDeviceToken,"lat":strLat, "lon":strLong, "height":str_height, "weight":str_weight, "about":strAboutType,"address":"ahmedaad", "provider":str_provider,"trainer_category" : "\(category_index+1)","social_id" : social_id,"firstname" : strFirstName,"lastname" : strLastName] as [String : Any]
        
        var isImage:Bool = true
        if selectedImage == nil{
            isImage = false
        }
        
        // DataUtil.alertMessage("You have been registered succesfully", viewController: self)
        
        ServerCommunication.getDataWithPostImage(url: ConstantFiles.register, image: selectedImage as Any,parameter: params, viewController: self, isImage:isImage,success: { (successDict) in
            
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.appdelegate().objUserInfo = UserInfo(dictUserInfo: successDict["response"] as! NSDictionary)
                DataUtil.appdelegate().objUserInfo?.str_accessToken = successDict["access_token"] as! String
                let responseDict:NSDictionary = successDict["response"] as! NSDictionary
                print(responseDict)
                self.registernewuser()
                UserDefaults.standard.set(DataUtil.appdelegate().objUserInfo?.userId, forKey: "LogedInUserID")
                UserDefaults.standard.set( DataUtil.appdelegate().objUserInfo?.str_accessToken, forKey: "LogedInUserAccessToken")
                UserDefaults.standard.set(DataUtil.appdelegate().objUserInfo?.email, forKey: "Email")
                UserDefaults.standard.set(true, forKey: "LogedIn")
                UserDefaults.standard.synchronize()
                let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
                objAppDelgate.isFromSignup = true
                self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
//                if DataUtil.appdelegate().objUserInfo?.str_userType == "2" {
                    self.navigationController?.pushViewController(self.tabBarObj, animated: true)
//                }
//                else {
//                    let obj_trainerHomeVC:TrainerHomeVC = TrainerHomeVC(nibName: "TrainerHomeVC", bundle: nil)
//                    self.navigationController?.pushViewController(obj_trainerHomeVC, animated: true)
//                }
            }else{
                let dict = successDict.value(forKey: "data") as! NSDictionary
                DataUtil.alertMessage((dict.value(forKey: "errors") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func registernewuser(){
        
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let str_name:String = String(format:(DataUtil.appdelegate().objUserInfo?.name)!)
        let str_profile:String = String(format:(DataUtil.appdelegate().objUserInfo?.profilePic)!)
        let str_token:String = String(format:(DataUtil.appdelegate().objUserInfo?.devicetoken)!)
        
        let db = Firestore.firestore()
        let id = str_userid
        db.collection("users").document(id).setData([
                                                        "id":id,"name":str_name,"profileImage":str_profile,"registrationTokens":str_token])
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == signupcell.textfield_mobile {
        let currentText = signupcell.textfield_mobile.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }

            // add their new text to the existing text
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

            // make sure the result is under 16 characters
            return updatedText.count <= 10
        }
        else {
            return true
        }
    }
}

