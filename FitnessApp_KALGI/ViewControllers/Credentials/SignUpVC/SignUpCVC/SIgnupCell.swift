//
//  SIgnupCell.swift
//  IslandTransfer
//
//  Created by Ashish Mishra on 13/03/18.
//  Copyright © 2018 Ashish Mishra. All rights reserved.
//

import UIKit
//countrycode_btn_clicked
protocol SignupCellDelegate:class {
    func signup_btn_clicked(cell: SIgnupCell, btn: UIButton)
    func countrycode_btn_clicked(cell: SIgnupCell, btn: UIButton)
    func camera_btn_clicked(cell: SIgnupCell, btn: UIButton)
    func check_box_clicked(cell: SIgnupCell, btn: UIButton)
    func signin_btn_clicked(cell: SIgnupCell, btn: UIButton)
    func gender_btn_clicked(cell: SIgnupCell, btn: UIButton)
    func dob_btn_clicked(cell: SIgnupCell, btn: UIButton)
    func height_btn_clicked(cell: SIgnupCell, btn: UIButton)
    func weight_btn_clicked(cell: SIgnupCell, btn: UIButton)
    func trainer_btn_clicked(cell: SIgnupCell, btn: UIButton)

    func terms_btn_clicked(type: String)
}

class SIgnupCell: UITableViewCell,UITextFieldDelegate {

    weak var delegate: SignupCellDelegate?
    var cellIndex:NSInteger = 0
    
    @IBOutlet weak var btn_checkBox: UIButton!
    @IBOutlet weak var textfield_name: UITextField!
    @IBOutlet weak var textfield_username: UITextField!
    @IBOutlet weak var textfield_countrycode: UITextField!
    @IBOutlet weak var textfield_email: UITextField!
    @IBOutlet weak var textfield_password: UITextField!
    @IBOutlet weak var textfield_mobile: UITextField!
    @IBOutlet weak var imageview_profile: CircularImageView!
    @IBOutlet var constPwdHeight: NSLayoutConstraint!
    
    @IBOutlet var txtAbout: UITextView!
    
    @IBOutlet weak var textfield_gender: UITextField!
    
    @IBOutlet var textfield_lastname: UITextField!
    
    @IBOutlet weak var textfield_dob: UITextField!

    @IBOutlet weak var textfield_weight: UITextField!
    @IBOutlet weak var textfield_height: UITextField!
    
    @IBOutlet var textfield_firstname: UITextField!
    @IBOutlet weak var view_height: UIView!
    @IBOutlet weak var view_weight: UIView!
    
    @IBOutlet weak var view_about: UIView!
    @IBOutlet weak var constraint_height: NSLayoutConstraint!
    
    @IBOutlet weak var textfield_about: UITextField!
    @IBOutlet weak var lbl_terms: UILabel!
    
    
    @IBOutlet var btnSignIn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureWithOwner(controller:SignUpVC){
        self.textfield_username.delegate = controller;
        self.textfield_name.delegate = controller;
        self.textfield_email.delegate = controller;
        self.textfield_mobile.delegate = controller;
        self.textfield_password.delegate = controller;
        self.txtAbout.placeholder = "About yourself"
        //self.textfield_about.delegate = controller;
        var main_string = "I agree to Terms & Conditions"
        var string_to_color = "Terms & Conditions"
        var range = (main_string as NSString).range(of: string_to_color)

        var attributedString = NSMutableAttributedString(string:main_string)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
        
        lbl_terms.attributedText = attributedString
        
    }
    
    func updateFields(social_dict:Dictionary<String,Any>) {
        if (social_dict["name"] != nil){
            self.textfield_name?.text = social_dict["name"] as? String
        }
        if (social_dict["email"] != nil){
            self.textfield_email?.text = social_dict["email"] as? String
        }
        
    }
    

    
    @IBAction func trainer_btn_clicked(_ sender: UIButton) {
        self.delegate?.trainer_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func country_btn_clicked(_ sender: UIButton) {
        self.delegate?.countrycode_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func signup_btn_clicked(_ sender: UIButton) {
        self.delegate?.signup_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func camera_btn_clicked(_ sender: UIButton) {
        self.delegate?.camera_btn_clicked(cell: self, btn: sender)
    }
    
    
    @IBAction func check_box_clicked(_ sender: UIButton) {
    self.delegate?.check_box_clicked(cell: self, btn: sender)
    }
    
    @IBAction func signin_btn_clicked(_ sender: UIButton) {
    self.delegate?.check_box_clicked(cell: self, btn: sender)
    }
    
    @IBAction func gender_btn_clicked(_ sender: UIButton) {
    self.delegate?.gender_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func dob_btn_clicked(_ sender: UIButton) {
        self.delegate?.dob_btn_clicked(cell: self, btn: sender)
    }
    
    
    @IBAction func height_btn_clicked(_ sender: UIButton) {
        self.delegate?.height_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func weight_btn_clicked(_ sender: UIButton) {
        self.delegate?.weight_btn_clicked(cell: self, btn: sender)
    }
    
    
}
