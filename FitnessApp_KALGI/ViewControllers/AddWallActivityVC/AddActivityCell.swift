//
//  AddActivityCell.swift
//  FitnessApp
//
//  Created by Ashish on 05/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

protocol AddWallActivityCellDelegate:class {
    func camera_btn_clicked(cell: AddActivityCell, btn: UIButton)
    func video_btn_clicked(cell: AddActivityCell, btn: UIButton)
    func image_btn_clicked(cell: AddActivityCell, btn: UIButton)
    func post_btn_clicked(cell: AddActivityCell, btn: UIButton)
    func close_video_btn_clicked(cell: AddActivityCell, btn: UIButton)
    func close_image_btn_clicked(cell: AddActivityCell, btn: UIButton)
    func play_btn_clicked(cell: AddActivityCell, btn: UIButton)
}

class AddActivityCell: UITableViewCell {

    
    weak var delegate:AddWallActivityCellDelegate?
    @IBOutlet weak var textview_message: UITextView!
    
     @IBOutlet weak var view1: UIView!
     @IBOutlet weak var view2: UIView!
     @IBOutlet weak var view3: UIView!
    
    
    @IBOutlet weak var view_video: UIView!
    @IBOutlet weak var view_image: UIView!
    @IBOutlet weak var constraint_top: NSLayoutConstraint!
    
    @IBOutlet weak var imageview_thumb: UIImageView!
    @IBOutlet weak var video_thumb: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textview_message.placeholder = "Message"
        self.textview_message.placeholderColor = UIColor.white
        // Initialization code
    }

    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
     @IBAction func camera_btn_clicked(_ sender: UIButton) {
        self.delegate?.camera_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func video_btn_clicked(_ sender: UIButton) {
         self.delegate?.video_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func image_btn_clicked(_ sender: UIButton) {
         self.delegate?.image_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func post_btn_clicked(_ sender: UIButton) {
        self.delegate?.post_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func close_video_btn_clicked(_ sender: UIButton) {
       self.delegate?.close_video_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func close_image_btn_clicked(_ sender: UIButton) {
        self.delegate?.close_image_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func play_btn_clicked(_ sender: UIButton) {
        self.delegate?.play_btn_clicked(cell: self, btn: sender)
    }
    
    
    
}
