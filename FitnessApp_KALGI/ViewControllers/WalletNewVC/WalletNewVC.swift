//
//  WalletNewVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 09/03/21.
//

import UIKit

class WalletNewVC: UIViewController {

    @IBOutlet var vwRequestPayment: UIView!
    @IBOutlet var tblWallet: UITableView!
    @IBOutlet var lblTotalBalance: UILabel!
    fileprivate var array_history:NSMutableArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.vwRequestPayment.isHidden = true
        self.apiWalletCall()
    }
}

//MARK: BUTTON ACTION METHODS
extension WalletNewVC {
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnRequestPayment(_ sender: UIButton) {
        self.vwRequestPayment.isHidden = false
    }
    @IBAction func btnRequestYes(_ sender: UIButton) {
        self.vwRequestPayment.isHidden = true
        self.apiRequestPayment()
    }
    @IBAction func btnRequestNo(_ sender: UIButton) {
        self.vwRequestPayment.isHidden = true
    }


}

//MARK: CUSTOM METHODS & API CALL
extension WalletNewVC {
    func apiRequestPayment() {
        if self.lblTotalBalance.text == "0"{
            DataUtil.alertMessage("There is no any amount in wallet to transfer", viewController: self)
            return
        }
        
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""

        
        let postParam = ["user_id":str_userid]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers  = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        //WalletModel
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.request_to_admin, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)

                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }

    func apiWalletCall() {
        //ANGNQSENSPRZ
        //let postParam: Parameters = ["user_id":"TOD2DR3TMJQ0"]
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let postParam = ["user_id":str_userid]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        //WalletModel
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.trainer_wallet_history, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    let dict = array[0] as! NSDictionary
                    let wallet_bal : String = dict["wallet_balance"] as! String
                    self.lblTotalBalance.text = wallet_bal
                    let array_transaction = dict["transactions"] as! NSArray
                    for item in array_transaction {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:WalletModel = WalletModel(dictUserInfo: dict)
                        self.array_history.add(obj_bookingmodal)
                    }
                    self.tblWallet.delegate = self
                    self.tblWallet.dataSource = self
                    self.tblWallet.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        // }
    }
    
}

//MARK: TABLEVIEW DELEGATE & DATASOURCE METHODS
extension WalletNewVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_history.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "WalletCVC"
        var logincell: WalletCVC? = tableView.dequeueReusableCell(withIdentifier: identifier) as? WalletCVC
        if logincell == nil {
            tableView.register(UINib(nibName: "WalletCVC", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? WalletCVC)!
        }
        logincell?.selectionStyle = .none
        let obj_requestmodal:WalletModel = array_history[indexPath.row] as! WalletModel
        let url = URL(string: obj_requestmodal.str_profile_pic)
        logincell!.imgProfilePic.setImageFrom(url)
        logincell!.lblTitle.text = obj_requestmodal.str_product_title
        logincell!.lblSubtitle.text = obj_requestmodal.str_sub_title
        logincell!.lblAmount.text = obj_requestmodal.str_amount
        logincell!.lblDate.text = obj_requestmodal.str_transaction_date
        return logincell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
}
