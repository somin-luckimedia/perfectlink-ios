//
//  FriendsListCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 23/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//




import UIKit
protocol FriendListCellDelegate:class {
    func reload_list()
}
class FriendsListCell: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var imageView_thumb: UIImageView!
    @IBOutlet weak var view_accept: UIView!
    @IBOutlet weak var btn_cancel: UIButton!

    var controller:FriendListVC! = FriendListVC()
    var obj_friendDetail:FriendsListModel! = FriendsListModel()
    
    weak var delegate:FriendListCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_bookingmodal:FriendsListModel){
        self.obj_friendDetail = obj_bookingmodal

        lbl_title.text = String(format: "%@", obj_bookingmodal.name)
//        lbl_description.text = String(format: "%@", obj_bookingmodal.is_about)
        var age : Int = 0
        if obj_bookingmodal.dob != "" {
            age = getAgeFromDOF(date: obj_bookingmodal.dob)
            print(age)
        }
//        imageView_thumb.setImageFromURL(stringImageUrl: (URL(string: (obj_bookingmodal.str_profile_picture))), placeholderImage: UIImage(named: "appLogo"))
        let url = URL(string: obj_bookingmodal.profile_picture)
        imageView_thumb.setImageFrom(url)
//        imageView_thumb.setImageFromURL(stringImageUrl:obj_bookingmodal.profile_picture)
//        lbl_price.text = String(format: "%@", obj_bookingmodal.email)
        lbl_description.text = "\(obj_bookingmodal.gender) \(age) years"
        if obj_bookingmodal.str_request_status == "0"{
            self.btn_cancel.setTitle("Send Request", for: .normal)
            self.view_accept.isHidden = true
        }
        if obj_bookingmodal.str_request_status == "1"{
            self.view_accept.isHidden = true
            self.btn_cancel.isHidden = true
        }
        if obj_bookingmodal.str_request_status == "2"{
            self.view_accept.isHidden = true
            self.btn_cancel.setTitle("Cancel Request", for: .normal)
        }
        if obj_bookingmodal.str_request_status == "3"{
            self.btn_cancel.isHidden = true
        }
    }
    
    @IBAction func cancel_btn_clicked(_ sender: Any) {
        if self.obj_friendDetail.str_request_status == "0"{
            self.sendRequest()
        }else{
            self.cancelRequest()
        }
    }
    
        func sendRequest(){
            let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
            let postParam = ["user_id":str_userid, "email":self.obj_friendDetail.str_email!]
            print("Login Post Parameter : \(postParam)")
            let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
            let headers = [
                "Authorization": String(format: "Bearer %@",str_accessToken),
                "Accept": "application/json"
            ]

            
            ServerCommunication.getDataWithPostHeader(url: ConstantFiles.send_friend_request, parameter: postParam, viewController: controller!, headers: headers, success: {  (successDict) in
                print(successDict)
                if (successDict.value(forKey: "STATUS") as? String) == "true"{
                    DispatchQueue.main.async {
                        self.view_accept.isHidden = true
                        self.btn_cancel.setTitle("Cancel Request", for: .normal)
                    }
                    self.delegate?.reload_list()
                    if let arr = successDict.value(forKey:"response"){
                        
                    }else{
                        DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
                    }
                    
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
                }
            }) { (dictFailure) in
                if (dictFailure.value(forKey: "message") as? String) != nil {
                    DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self.controller!)
                }
            }
        }
        func cancelRequest(){
            let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
            let postParam = ["user_id":str_userid, "id":self.obj_friendDetail.str_request_id!]
            print("Login Post Parameter : \(postParam)")
            let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

            let headers = [
                "Authorization": String(format: "Bearer %@", str_accessToken),
                "Accept": "application/json"
            ]
            
            ServerCommunication.getDataWithPostHeader(url: ConstantFiles.cancel_friend_request, parameter: postParam, viewController: controller!, headers: headers, success: {  (successDict) in
                print(successDict)
                if (successDict.value(forKey: "STATUS") as? String) == "true"{
                    DispatchQueue.main.async {
                        self.btn_cancel.setTitle("Send Request", for: .normal)
                        self.view_accept.isHidden = true
                    }
                    self.delegate?.reload_list()
                    if let arr = successDict.value(forKey:"response"){
                        
                    }else{
                        DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
                    }
                    
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
                }
            }) { (dictFailure) in
                if (dictFailure.value(forKey: "message") as? String) != nil {
                    DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self.controller!)
                }
            }
            
            
        }
    func getAgeFromDOF(date: String) -> (Int) {

        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "YYYY-MM-dd"
        let dateOfBirth = dateFormater.date(from: date)

        let calender = Calendar.current

        let dateComponent = calender.dateComponents([.year], from:dateOfBirth!, to: Date())

        return (dateComponent.year!)
    }
//    func ageCalculate() {
//        let ageComponents = Calendar.component(<#T##self: Calendar##Calendar#>)(.CalendarUnitYear,
//                                      fromDate: birthday,
//                                        toDate: now,
//                                       options: nil)
//        let age = ageComponents.year
//
//    }

    @IBAction func accept_btn_clicked(_ sender: Any) {
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam = ["user_id":str_userid, "id":self.obj_friendDetail.str_request_id!]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.accept_friend_request, parameter: postParam, viewController: controller!, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                
                DispatchQueue.main.async {
                    self.view_accept.isHidden = true
//                    self.btn_cancel.isHidden = true
                }
                self.delegate?.reload_list()
                if let arr = successDict.value(forKey:"response"){
                    
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self.controller!)
            }
        }
    }
    
    @IBAction func reject_btn_clicked(_ sender: Any) {
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam = ["user_id":str_userid, "id":self.obj_friendDetail.str_request_id!]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]

        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.reject_friend_request, parameter: postParam, viewController: controller!, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DispatchQueue.main.async {
                    self.view_accept.isHidden = true
                }
                self.delegate?.reload_list()
                if let arr = successDict.value(forKey:"response"){
                    
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self.controller!)
            }
        }
    }
}




