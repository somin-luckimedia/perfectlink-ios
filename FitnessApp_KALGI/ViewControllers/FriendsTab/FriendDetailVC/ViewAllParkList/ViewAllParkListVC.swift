
import UIKit
import AVFoundation
import Photos
import AVKit

class ViewAllParkListVC: UIViewController {

    @IBOutlet weak var collectionview_parklist: UICollectionView!
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    var isForPark = false
    var arr_parklist = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isForPark{
            self.title = "Visited Park"
            
        }else{
           self.title = "Gallery"
        }
        
         self.collectionview_parklist.register(UINib(nibName: "ParkListInfoCell", bundle: nil), forCellWithReuseIdentifier: "ParkListInfoCell")
        self.collectionview_parklist.reloadData()
        self.addLeftMenuButtonWithImage()
    }

    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension ViewAllParkListVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return self.arr_parklist.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:ParkListInfoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParkListInfoCell", for: indexPath) as! ParkListInfoCell
          if isForPark{
            
            let dictemp = self.arr_parklist[indexPath.row] as? NSDictionary ?? NSDictionary()
            let imgURl = dictemp["park_image"] as? String ?? ""
            let strName = dictemp["park_name"] as? String ?? ""
            let url = URL(string: imgURl)
            cell.img_park.setImageFrom(url)
            cell.btnDeleteIcon.isHidden = true
            cell.lbl_Name.text = strName
            cell.img_isVideo.isHidden = true
            
        }else {
           
            
            let dictemp = self.arr_parklist[indexPath.row] as? GymDetailModel
            let imgURl = (dictemp?.str_cover_image)!
            let strName = dictemp?.str_gym_title
            let url = URL(string: imgURl)
            cell.img_park.setImageFrom(url)
            cell.btnDeleteIcon.isHidden = true
            cell.lbl_Name.text = strName
            cell.img_isVideo.isHidden = true
//            let filetype = dictemp["file_type"] as? String ?? ""
//            if filetype == "IMAGE"{
//                let imgURl = dictemp["file"] as? String ?? ""
//                let strName = dictemp["text"] as? String ?? ""
//                cell.img_park.setImageFromURL(stringImageUrl: imgURl)
//                cell.lbl_Name.text = strName
//                cell.img_isVideo.isHidden = true
//
//            }else if filetype == "VIDEO"{
//               let imgURl = dictemp["thumbnail"] as? String ?? ""
//                let strName = dictemp["text"] as? String ?? ""
//                cell.img_park.setImageFromURL(stringImageUrl: imgURl)
//                cell.lbl_Name.text = strName
//                cell.img_isVideo.isHidden = false
//
//            }
            
            
        }
      return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
//        
//         if isForPark{
//            let dictemp = self.arr_parklist[indexPath.row] as? NSDictionary ?? NSDictionary()
//            let obj_DetailParkvc:DetailParkLinkVC = DetailParkLinkVC(nibName: "DetailParkLinkVC", bundle: nil)
//                obj_DetailParkvc.str_parkID = dictemp["park_id"] as? String ?? ""
//            self.navigationController?.pushViewController(obj_DetailParkvc, animated: true)
//            
//            
//         }else{
//            let dictemp = self.arr_parklist[indexPath.row] as? NSDictionary ?? NSDictionary()
//            let filetype = dictemp["file_type"] as? String ?? ""
//            if filetype == "IMAGE"{
//                let imgURl = dictemp["file"] as? String ?? ""
//                let obj_parkeAddressVC:ImageViewerVC = ImageViewerVC(nibName: "ImageViewerVC", bundle: nil)
//                       let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
//                       obj_parkeAddressVC.str_url = imgURl
//                       self.navigationController?.present(nav, animated: true, completion: nil)
//                
//            }else if filetype == "VIDEO"{
//               let imgURl = dictemp["file"] as? String ?? ""
//                let player = AVPlayer(url: URL(string: imgURl)!)
//                let playerController = AVPlayerViewController()
//                playerController.player = player
//                self.present(playerController, animated: true) {
//                    player.play()
//                }
//                
//            }
//            
//        }
//    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenwidth:CGFloat = (UIScreen.main.bounds.size.width/2)-25
        return CGSize(width: screenwidth, height: 160)
        
        
    }


}
