//
//  ParkListInfoCell.swift
//  FitnessApp
//
//  Created by Ashish tripathi on 19/06/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class ParkListInfoCell: UICollectionViewCell {

    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var img_park: UIImageView!
    @IBOutlet weak var img_isVideo: UIImageView!
    @IBOutlet var btnDeleteIcon: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
