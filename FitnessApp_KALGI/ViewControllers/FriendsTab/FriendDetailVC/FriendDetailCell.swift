//
//  FriendDetailCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 15/03/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//


import UIKit
import Alamofire

protocol FriendDetailCellDelegate: class {
    func chat_btn_clicked()
    func actbtn_ViewAllParkbtnClicked()
    func actbtn_ViewAllGalleryBtnClicked()
    func selectedParkAtIndex(index: Int)
    func selectedGymAtIndex(index: Int)
    func selectedGalleryTapped(index: Int)
    func selectedGalleryAtIndex(index: Int)
}

class FriendDetailCell: UITableViewCell {
    
    
    //    @IBOutlet weak var lbl_height_Weight: UILabel!
    
    @IBOutlet weak var btn_Chat: UIButton!
    @IBOutlet weak var heightVistedPark: NSLayoutConstraint!
    @IBOutlet weak var heightGallery: NSLayoutConstraint!
    @IBOutlet weak var heightGymlist: NSLayoutConstraint!
    @IBOutlet var lblCamera: UILabel!
    var strType = "camera"
    
    @IBOutlet var btnCamera: UIButton!
    @IBOutlet var btnVideo: UIButton!
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblVideo: UILabel!
    @IBOutlet weak var btnPark_viewAll: UIButton!
    
    @IBOutlet weak var collectionview_gallery: UICollectionView!
    @IBOutlet weak var collectionview_VistedPark: UICollectionView!
    @IBOutlet weak var collectionview_gym: UICollectionView!
    //    @IBOutlet weak var imageView_cover: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_gender: UILabel!
    @IBOutlet weak var lbl_age: UILabel!
    @IBOutlet weak var lbl_height: UILabel!
    @IBOutlet weak var lbl_weight: UILabel!
    @IBOutlet weak var lbl_Desc: UILabel!
    
    @IBOutlet var vwImagIcon: UIView!
    @IBOutlet weak var view_accept: UIView!
    @IBOutlet weak var btn_cancel: UIButton!
    
    @IBOutlet weak var view_gyms: UIView!
    
    weak var delegate:FriendDetailCellDelegate?
    
    var controller:FriendDetailVC! = FriendDetailVC()
    var obj_friendDetail:FriendDetailModel! = FriendDetailModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.lblCamera.isHidden = true
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func chat_btn_clicked(_ sender: UIButton) {
        self.delegate?.chat_btn_clicked()
    }
    
    @IBAction func actbtn_parkViewAll(_ sender: Any) {
        self.delegate?.actbtn_ViewAllParkbtnClicked()
    }
    
    func updateCellwithModal(obj_gymDetail:FriendDetailModel){
        self.obj_friendDetail = obj_gymDetail
        if self.obj_friendDetail.array_gyms.count == 0{
            self.view_gyms.isHidden = true
            self.heightGymlist.constant = 0
        }else{
            self.view_gyms.isHidden = false
            self.heightGymlist.constant = 180
        }
        
        if obj_gymDetail.str_request_status == "0"{
            self.btn_cancel.setTitle("Send Request", for: .normal)
            self.view_accept.isHidden = true
        }
        if obj_gymDetail.str_request_status == "1"{
            self.view_accept.isHidden = true
            self.btn_cancel.isHidden = true
        }
        if obj_gymDetail.str_request_status == "2"{
            self.view_accept.isHidden = true
            self.btn_cancel.setTitle("Cancel Request", for: .normal)
        }
        if obj_gymDetail.str_request_status == "3"{
            self.btn_cancel.isHidden = true
        }
        
        if obj_friendDetail.array_vistedpark.count >= 3{
            self.heightVistedPark.constant = 200
            self.btnPark_viewAll.isHidden = false
            
        }else if obj_friendDetail.array_vistedpark.count <= 2 && obj_friendDetail.array_vistedpark.count > 0 {
            self.heightVistedPark.constant = 200
            self.btnPark_viewAll.isHidden = false
        }else{
            self.heightVistedPark.constant = 60
            self.btnPark_viewAll.isHidden = true
        }
        
        if obj_friendDetail.array_gallery.count >= 3{
            self.heightGallery.constant = 360
        }else if obj_friendDetail.array_gallery.count >= 1{
            self.heightGallery.constant = 200
        }else{
            self.heightGallery.constant = 60
        }
        let url = URL(string: obj_friendDetail.str_profile_picture)
        imgIcon.setImageFrom(url)

        lbl_name.text = obj_gymDetail.str_name
        lbl_gender.text = obj_gymDetail.str_gender
        lbl_age.text = "\(obj_gymDetail.str_age ?? "")yrs"
        
        lbl_height.text = "\(obj_gymDetail.str_height ?? "")ft"
        lbl_weight.text = "\(obj_gymDetail.str_weight ?? "")lbs"
        lbl_Desc.text = obj_gymDetail.str_about
        
        self.collectionview_gym.register(UINib(nibName: "ParkListInfoCell", bundle: nil), forCellWithReuseIdentifier: "ParkListInfoCell")
        
        self.collectionview_VistedPark.register(UINib(nibName: "ParkListInfoCell", bundle: nil), forCellWithReuseIdentifier: "ParkListInfoCell")
        self.collectionview_gallery.register(UINib(nibName: "ParkListInfoCell", bundle: nil), forCellWithReuseIdentifier: "ParkListInfoCell")
        self.collectionview_gallery.register(UINib(nibName: "userInfoVideoCell", bundle: nil), forCellWithReuseIdentifier: "userInfoVideoCell")
        
        self.collectionview_VistedPark.reloadData()
        self.collectionview_gallery.reloadData()
        self.collectionview_gym.reloadData()
    }
    
    @IBAction func actbtn_GalleryViewAll(_ sender: Any) {
        self.delegate?.actbtn_ViewAllGalleryBtnClicked()
    }
    
    @IBAction func accept_btn_clicked(_ sender: Any) {
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam: Parameters = ["user_id":str_userid, "id":self.obj_friendDetail.str_request_id!]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]

        
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.accept_friend_request, parameter: postParam, viewController: controller!, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                
                DispatchQueue.main.async {
                    self.view_accept.isHidden = true
                    self.btn_cancel.isHidden = true
                }
                if let arr = successDict.value(forKey:"response"){
                    
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self.controller!)
            }
        }
        
        
    }
    
    @IBAction func btn_camera_clicked(_ sender: Any) {
        self.strType = "camera"
        self.lblVideo.isHidden = false
        self.lblCamera.backgroundColor = UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        self.lblVideo.backgroundColor = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 9.0/255.0, alpha: 1.0)
        self.btnVideo.setImage(UIImage(named: "video_offImage"), for: .normal)
        self.btnCamera.setImage(UIImage(named: "camera_imgUser"), for: .normal)
       // self.lblCamera.isHidden = true
        collectionview_gallery.reloadData()
    }
    @IBAction func btn_video_clicked(_ sender: Any) {
        self.strType = "video"
        self.lblCamera.isHidden = false
      //  self.lblVideo.isHidden = true
        self.lblVideo.backgroundColor = UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        self.lblCamera.backgroundColor = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 9.0/255.0, alpha: 1.0)
        self.btnVideo.setImage(UIImage(named: "camera_videoUser"), for: .normal)
        self.btnCamera.setImage(UIImage(named: "camera_offImage"), for: .normal)
        collectionview_gallery.reloadData()
    }
    
    
    @IBAction func reject_btn_clicked(_ sender: Any) {
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam: Parameters = ["user_id":str_userid, "id":self.obj_friendDetail.str_request_id!]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]

        
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.reject_friend_request, parameter: postParam, viewController: controller!, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DispatchQueue.main.async {
                    self.view_accept.isHidden = true
                    self.btn_cancel.isHidden = true
                }
                if let arr = successDict.value(forKey:"response"){
                    
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self.controller!)
            }
        }
        
        
    }
    
    @IBAction func cancel_btn_clicked(_ sender: Any) {
        if self.obj_friendDetail.str_request_status == "0"{
            self.sendRequest()
        }else{
            self.cancelRequest()
        }
    }
    
    func sendRequest(){
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam: Parameters = ["user_id":str_userid, "email":self.obj_friendDetail.str_email!]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]

        
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.send_friend_request, parameter: postParam, viewController: controller!, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DispatchQueue.main.async {
                    self.view_accept.isHidden = true
                    self.btn_cancel.setTitle("Cancel Request", for: .normal)
                }
                if let arr = successDict.value(forKey:"response"){
                    
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self.controller!)
            }
        }
        
        
    }
    
    func cancelRequest(){
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam: Parameters = ["user_id":str_userid, "id":self.obj_friendDetail.str_request_id!]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]

        
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.cancel_friend_request, parameter: postParam, viewController: controller!, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DispatchQueue.main.async {
                    self.btn_cancel.setTitle("Send Request", for: .normal)
                    self.view_accept.isHidden = true
                }
                if let arr = successDict.value(forKey:"response"){
                    
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self.controller!)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self.controller!)
            }
        }
        
        
    }
    
    
    @IBAction func send_request_btn_clicked(_ sender: Any) {
        
        
    }
    
}


extension FriendDetailCell:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if  collectionView == self.collectionview_VistedPark{
            return self.obj_friendDetail.array_vistedpark.count
        }else if collectionView == self.collectionview_gallery {
            if strType == "camera" {
                return self.obj_friendDetail.array_gallery.count
            }
            else {
                return self.obj_friendDetail.array_videos.count
            }
        }
        else{
            return self.obj_friendDetail.array_gyms.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if  collectionView == self.collectionview_VistedPark{
            let cell:ParkListInfoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParkListInfoCell", for: indexPath) as! ParkListInfoCell
            let dictemp = self.obj_friendDetail.array_vistedpark[indexPath.row] as? NSDictionary ?? NSDictionary()
            let imgURl = dictemp["park_image"] as? String ?? ""
            let strName = dictemp["park_name"] as? String ?? ""
            let url = URL(string: imgURl)
            cell.img_park.setImageFrom(url)
            cell.lbl_Name.text = strName
            cell.img_isVideo.isHidden = true
            if flagPark {
            cell.btnDeleteIcon.tag = indexPath.row
            cell.btnDeleteIcon.addTarget(self, action: #selector(self.unsubscribePark(sender:)), for: .touchUpInside)
            }
            else {
                cell.btnDeleteIcon.isHidden = true
//                flagPark = true
            }
            return cell
            
        }else if collectionView == self.collectionview_gallery{
            let cell_video:userInfoVideoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "userInfoVideoCell", for: indexPath) as! userInfoVideoCell
            if strType == "camera" {
                let cell:ParkListInfoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParkListInfoCell", for: indexPath) as! ParkListInfoCell
                let dictemp = self.obj_friendDetail.array_gallery[indexPath.row] as? NSDictionary ?? NSDictionary()
                let imgURl = dictemp["file"] as? String ?? ""
                let strName = dictemp["text"] as? String ?? ""
                let url = URL(string: imgURl)
                cell.img_park.setImageFrom(url)
//
                cell.lbl_Name.text = strName
                cell.img_isVideo.isHidden = true
                if flagGallery {
                cell.btnDeleteIcon.tag = indexPath.row
                cell.btnDeleteIcon.addTarget(self, action: #selector(self.deleteGalleryPost(sender:)), for: .touchUpInside)
                }
                else {
                    cell.btnDeleteIcon.isHidden = true
    //                flagPark = true
                }
                return cell
            }
            else {
                let dictemp = self.obj_friendDetail.array_videos[indexPath.row] as? NSDictionary ?? NSDictionary()
                let imgURl = dictemp["thumbnail"] as? String ?? ""
                let url = URL(string: imgURl)
                cell_video.imgVideo.setImageFrom(url)
//                cell_video.btnDeleteIcon.isHidden = true
                if flagGallery {
                    cell_video.btnDeleteIcon.tag = indexPath.row
                    cell_video.btnDeleteIcon.addTarget(self, action: #selector(self.deleteGalleryPost(sender:)), for: .touchUpInside)
                }
                else {
                    cell_video.btnDeleteIcon.isHidden = true
    //                flagPark = true
                }
                return cell_video
            }
            return cell_video
        }
        else{
            
            let cell:ParkListInfoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParkListInfoCell", for: indexPath) as! ParkListInfoCell
            //let dict_equipement:NSDictionary = self.obj_gymdetail.array_subscription[indexPath.row] as!NSDictionary
            let obj_gymDetail:GymDetailModel = self.obj_friendDetail.array_gyms[indexPath.row] as! GymDetailModel
            let imgURl = obj_gymDetail.str_cover_image!
            let strName = obj_gymDetail.str_gym_title!
            let url = URL(string: imgURl)
            cell.img_park.setImageFrom(url)
            cell.lbl_Name.text = strName
            cell.img_isVideo.isHidden = true
            if flagGym {
            cell.btnDeleteIcon.tag = indexPath.row
            cell.btnDeleteIcon.addTarget(self, action: #selector(self.unSubscribegym(sender:)), for: .touchUpInside)
            }
            else {
                cell.btnDeleteIcon.isHidden = true
//                flagGym = true
            }

//            cell.updateCellwithModal(obj_gymDetail: obj_gymDetail)
            return cell
        }
        
    }
    
    
    @objc func unsubscribePark(sender : UIButton){
        print(sender.tag)
        flagParkLink = false

        self.delegate?.selectedParkAtIndex(index: sender.tag)

        //                    }
    }
    @objc func unSubscribegym(sender : UIButton){
        print(sender.tag)
        flagGymLink = false
        self.delegate?.selectedGymAtIndex(index: sender.tag)

    }
    
    @objc func deleteGalleryPost(sender : UIButton){
        print(sender.tag)
        flagGalleryLink = false
        self.delegate?.selectedGalleryAtIndex(index: sender.tag)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView == self.collectionview_gym {
            flagGymLink = true
            self.delegate?.selectedGymAtIndex(index: indexPath.row)
        }
        if  collectionView == self.collectionview_VistedPark{
            flagParkLink = true
            self.delegate?.selectedParkAtIndex(index: indexPath.row)
        }
        else if collectionView == self.collectionview_gallery{
            strType_camera = strType
            flagGalleryLink = true
            self.delegate?.selectedGalleryAtIndex(index: indexPath.row)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if  collectionView == self.collectionview_VistedPark{
            let screenwidth:CGFloat = (UIScreen.main.bounds.size.width/2)-25
            return CGSize(width: screenwidth, height: 160)
        }
        else if collectionView == self.collectionview_gallery{
            if strType == "camera" {
            let numberOfItemsPerRow:CGFloat = 3
            let spacingBetweenCells:CGFloat = 0
            
            let totalSpacing = (2 * 5) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
            
            // if let collection = self.collectionData{
            let width = (collectionview_gallery.bounds.width - totalSpacing)/numberOfItemsPerRow
            return CGSize(width: width, height: width)
            }
            else {
                let width = collectionview_gallery.bounds.width
                return CGSize(width: width, height: 200.0)
            }
            
            //            let screenwidth:CGFloat = (UIScreen.main.bounds.size.width/2)-25
            //            return CGSize(width: screenwidth, height: 160)
        }
        else{
            let screenwidth:CGFloat = (UIScreen.main.bounds.size.width/2)-25
            return CGSize(width: screenwidth, height: 160)
        }
    }
}


