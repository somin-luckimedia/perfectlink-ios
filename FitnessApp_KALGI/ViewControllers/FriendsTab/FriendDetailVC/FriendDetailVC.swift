//
//  FriendDetailVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 15/03/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import Photos
import AVKit
import Firebase


class FriendDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, FriendDetailCellDelegate  {
    func chat_btn_clicked() {
         let objFriendsChatVC:FriendsChatVC = FriendsChatVC(nibName: "FriendsChatVC", bundle: nil)
         let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        objFriendsChatVC.myUserID = str_userid
        objFriendsChatVC.myFriendID = obj_friendDetail.str_user_id
        objFriendsChatVC.friendName = self.obj_friendDetail.str_name
        self.navigationController?.pushViewController(objFriendsChatVC, animated: true)
    }
    
    @IBAction func btnChatTapped(_ sender: UIButton) {
        let objFriendsChatVC:FriendsChatVC = FriendsChatVC(nibName: "FriendsChatVC", bundle: nil)
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        objFriendsChatVC.myUserID = str_userid
        objFriendsChatVC.myFriendID = obj_friendDetail.str_user_id
        objFriendsChatVC.friendName = self.obj_friendDetail.str_name
        self.navigationController?.pushViewController(objFriendsChatVC, animated: true)
    }
    
    
    var obj_friendDetail:FriendDetailModel! = FriendDetailModel()
    var str_userType:String = "2"
    var logincell: FriendDetailCell!
    var str_gymId:String!
    var productCount:Int = 1
    var totalCost:Double = 0.00
    var str_user_id:String = ""
    @IBOutlet var tblOnline: UITableView!
    var arrOnlineFriends : NSMutableArray = []
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")
    
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet weak var tableview_detail: UITableView!
    var obj_susbcription_dict:NSDictionary? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "FRIEND DETAIL"
        self.btnAdd.isHidden = true
        tableview_detail.delegate  = self as UITableViewDelegate
        tableview_detail.dataSource = self as UITableViewDataSource
        self.tableview_detail.isHidden = true
        tableview_detail.reloadData()
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }

                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
        }
        self.addLeftMenuButtonWithImage()
    }
        
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @IBAction func btn_AddFriend(_ sender: UIButton) {
        let refreshAlert = UIAlertController(title: "", message: "Are you sure you want to add this uer as your friend", preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.addFriendList()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func addFriendList(){
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam = ["user_id": str_userid,"email" : obj_friendDetail.str_email!] as [String : Any]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@",  (str_accessToken)),
            "Accept": "application/json"
        ]
//        self.array_mybookings.removeAllObjects()
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.add_friendsList, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                showAlertWithTitleFromVC(vc: self, title: "", andMessage: (successDict.value(forKey: "message") as? String)!, buttons: ["OK"]) { (index) in
                    if index == 0 {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }else{
                showAlertWithTitleFromVC(vc: self, andMessage: ((successDict.value(forKey: "message") as? String)!))
                //                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
            
            
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getMyBookings()

        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func membership_cell_clicked(obj_gymDetail: NSDictionary) {
        self.obj_susbcription_dict = obj_gymDetail
    }
    
    func join_btn_clicked() {
        //OrderGymSummaryVC
        //obj_susbcription_dict
        
        if obj_susbcription_dict != nil{
            var month:Int = 1
            var amount:Double = 0.00
            if let result_number = obj_susbcription_dict?.value(forKey: "subscription_month") as? NSNumber
            {
                month = Int("\(result_number)") ?? 0
            }
            
            if let result_number = obj_susbcription_dict?.value(forKey: "subscription_amount") as? String
            {
                amount = Double(result_number) ?? 0.0
            }
            self.totalCost = Double(month)*amount
        }else{
            DataUtil.alertMessage("Please select membership plan", viewController: self)
        }
    }
    
    
    func getMyBookings(){
        let postParam: Parameters = ["user_id":self.str_user_id]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.get_friend_info, parameter: postParam, viewController: self, headers: headers, success: { [self]  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.tableview_detail.isHidden = false
                if let arr = successDict.value(forKey:"response"){
                    self.obj_friendDetail = FriendDetailModel(dictUserInfo: arr as! NSDictionary)
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""

                    if self.obj_friendDetail.str_user_id != str_userid {
                        flagPark = false
                        flagGym = false
                        flagGallery = false
                    }
                    else {
                        flagPark = true
                        flagGym = true
                        flagGallery = true
                    }
                    if self.obj_friendDetail.str_is_friend == "0" {
                        self.btnAdd.isHidden = false
                    }
                    else {
                        self.btnAdd.isHidden = true
                    }
                    self.tableview_detail.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if tableView == tblOnline {
            return arrOnlineFriends.count
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblOnline {
            let identifier = "onlineTVCTableViewCell"
            var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
            
            if logincell == nil {
                tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
            }
            logincell?.selectionStyle = .none
            //        logincell?.layer.cornerRadius = 25
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            let url = URL(string: dict["pic"] as! String)
            logincell?.imgOnlineFriends.setImageFrom(url)
            logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
            
            return logincell!
        }
        else {
            let identifier = "FriendDetailCell"
            logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FriendDetailCell
            if logincell == nil {
                tableView.register(UINib(nibName: "FriendDetailCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FriendDetailCell
            }
            logincell.controller = self
            logincell.delegate = self
            logincell.updateCellwithModal(obj_gymDetail:self.obj_friendDetail)
            return logincell
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == tblOnline {
            return 60
        }
        else {
            return 1050 //550
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblOnline {
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            flagPark = false
            flagGym = false
            flagGallery = false
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            obj_phonelistvc.str_user_id = dict["userId"] as! String
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Hello delegate")
    }
    
    func selectedParkAtIndex(index: Int){
//        let dictemp = self.obj_friendDetail.array_vistedpark[index] as? NSDictionary ?? NSDictionary()
//        let obj_DetailParkvc:DetailParkLinkVC = DetailParkLinkVC(nibName: "DetailParkLinkVC", bundle: nil)
//            obj_DetailParkvc.str_parkID = dictemp["park_id"] as? String ?? ""
//        self.navigationController?.pushViewController(obj_DetailParkvc, animated: true)
        let dictemp = self.obj_friendDetail.array_vistedpark[index] as? NSDictionary ?? NSDictionary()
        let obj_DetailParkvc:ParkLinkDetailVC = ParkLinkDetailVC(nibName: "ParkLinkDetailVC", bundle: nil)
        obj_DetailParkvc.str_parkID = dictemp["park_id"] as? String ?? ""
        self.navigationController?.pushViewController(obj_DetailParkvc, animated: true)

        
    }
    func selectedGymAtIndex(index: Int) {
        let obj_requestmodal:GymListModel!//
        let obj_GymDetailVC:GymPersonalDetailVC = GymPersonalDetailVC(nibName: "GymPersonalDetailVC", bundle: nil)
        let dictemp = self.obj_friendDetail.array_gyms_id[index] as? NSDictionary ?? NSDictionary()
        obj_GymDetailVC.str_gymId = dictemp["gym_id"] as? String ?? ""
        self.navigationController?.pushViewController(obj_GymDetailVC, animated: true)
    }
    func selectedGalleryTapped(index: Int) {
        
    }
    func selectedGalleryAtIndex(index: Int){
        
        let dictemp = self.obj_friendDetail.array_gallery[index] as? NSDictionary ?? NSDictionary()
        let filetype = dictemp["file_type"] as? String ?? ""
        if strType_camera == "camera" {
            
            let imgURl = dictemp["file"] as? String ?? ""
            let obj_parkeAddressVC:ImageViewerVC = ImageViewerVC(nibName: "ImageViewerVC", bundle: nil)
//            let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
            //            obj_parkeAddressVC.modalPresentationStyle = .fullScreen
            //            obj_parkeAddressVC.transitioningDelegate = self
            obj_parkeAddressVC.dictTemp = dictemp
            obj_parkeAddressVC.name =  self.obj_friendDetail.str_name
            obj_parkeAddressVC.str_url = imgURl
                        self.navigationController?.pushViewController(obj_parkeAddressVC, animated: true)
//            self.navigationController?.present(nav, animated: true, completion: nil)
            
        }else {
            let imgURl = dictemp["thumbnail"] as? String ?? ""
            let obj_parkeAddressVC:ImageViewerVC = ImageViewerVC(nibName: "ImageViewerVC", bundle: nil)
//            let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
            //            obj_parkeAddressVC.modalPresentationStyle = .fullScreen
            //            obj_parkeAddressVC.transitioningDelegate = self
            obj_parkeAddressVC.dictTemp = dictemp
            obj_parkeAddressVC.name =  self.obj_friendDetail.str_name
            obj_parkeAddressVC.str_url = imgURl
            self.navigationController?.pushViewController(obj_parkeAddressVC, animated: true)

//            self.navigationController?.present(nav, animated: true, completion: nil)
            
            //            self.navigationController?.pushViewController(obj_parkeAddressVC, animated: true)
            //            let player = AVPlayer(url: URL(string: imgURl)!)
            //            let playerController = AVPlayerViewController()
            //            playerController.player = player
            //            self.present(playerController, animated: true) {
            //                player.play()
            //            }
            
            //        }
        }
        //        let dictemp = self.obj_friendDetail.array_gallery[index] as? NSDictionary ?? NSDictionary()
        //        let filetype = dictemp["file_type"] as? String ?? ""
        //        if filetype == "IMAGE"{
        //            let imgURl = dictemp["file"] as? String ?? ""
        //            let obj_parkeAddressVC:ImageViewerVC = ImageViewerVC(nibName: "ImageViewerVC", bundle: nil)
        //                   let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
        //                   obj_parkeAddressVC.str_url = imgURl
        //                   self.navigationController?.present(nav, animated: true, completion: nil)
        //
        //        }else if filetype == "VIDEO"{
        //           let imgURl = dictemp["file"] as? String ?? ""
        //            let player = AVPlayer(url: URL(string: imgURl)!)
        //            let playerController = AVPlayerViewController()
        //            playerController.player = player
        //            self.present(playerController, animated: true) {
        //                player.play()
        //            }
        //
        //        }
        
    }
    
    func actbtn_ViewAllParkbtnClicked(){
       
      let objViewAllParkListVC:ViewAllParkListVC = ViewAllParkListVC(nibName: "ViewAllParkListVC", bundle: nil)
        objViewAllParkListVC.isForPark = true
        objViewAllParkListVC.arr_parklist = self.obj_friendDetail.array_vistedpark
        self.navigationController?.pushViewController(objViewAllParkListVC, animated: true)
        
        
    }
    func actbtn_ViewAllGalleryBtnClicked(){
        
        let objViewAllParkListVC:ViewAllParkListVC = ViewAllParkListVC(nibName: "ViewAllParkListVC", bundle: nil)
        objViewAllParkListVC.isForPark = false
        objViewAllParkListVC.arr_parklist = self.obj_friendDetail.array_gyms
        self.navigationController?.pushViewController(objViewAllParkListVC, animated: true)
    }
    
    
    
    
    
}
