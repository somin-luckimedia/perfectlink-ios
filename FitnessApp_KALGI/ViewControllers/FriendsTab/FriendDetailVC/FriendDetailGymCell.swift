//
//  FriendDetailGymCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 15/03/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class FriendDetailGymCell: UICollectionViewCell {

    @IBOutlet weak var imageView_cover: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    var obj_gymdetail:GymDetailModel! = GymDetailModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateCellwithModal(obj_gymDetail:GymDetailModel){
        self.obj_gymdetail = obj_gymDetail
        let url = URL(string: obj_gymDetail.str_cover_image)
        imageView_cover.setImageFrom(url)
        lbl_title.text = obj_gymDetail.str_gym_title
        lbl_distance.text = String(format: "%@ Miles", obj_gymDetail.str_distance)
        let str_openclosetime:String = obj_gymDetail.str_open_close.replacingOccurrences(of: ",", with: "\n")
        lbl_time.text = str_openclosetime
        lbl_description.text = obj_gymDetail.str_full_description
    }

}
