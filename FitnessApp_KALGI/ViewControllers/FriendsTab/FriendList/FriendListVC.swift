//
//  FriendListVC.swift
//  FitnessApp_KALGI
//
//  Created by Yogesh on 2020-10-03.
//

import UIKit
import Firebase

class FriendListVC: UIViewController,FriendListCellDelegate {
    func reload_list() {
        self.getMyBookings()
    }
    
    
    //    @IBOutlet var tblOnline: UITableView!
    @IBOutlet var tblFriendList: UITableView!
    fileprivate var array_mybookings:NSMutableArray = NSMutableArray()
    var array_emails:[String] = [String]()
    let transiton = SlideInTransition()
    var topView: UIView?
    let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate
    @IBOutlet var tblOnline: UITableView!
    var arrOnlineFriends : NSMutableArray = []
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")
    @IBOutlet var lblEmpty: UILabel!
    
    
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet var vwInnerEmail: UIView!
    @IBOutlet var vwNewFriend: UIView!
    @IBOutlet var txtEmailAddress: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        self.tblFriendList.register(UINib(nibName: "FriendsListCell", bundle: nil), forCellReuseIdentifier: "FriendsListCell")
        lblEmpty.isHidden = true
        vwNewFriend.isHidden = true
        vwInnerEmail.layer.borderWidth = 1.0
        vwInnerEmail.layer.borderColor = #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1)
        vwInnerEmail.layer.masksToBounds = true
        self.tblFriendList.delegate =  self
        self.tblFriendList.dataSource = self
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
//                if snapshot.childrenCount == 1 {
//                    self.constWidthTbl.constant = 0
////                    self.constLeadingColl.constant = 20
//                    self.lblOnline.isHidden = true
//                }
//                else {
//                    self.constWidthTbl.constant = 60
////                    self.constLeadingColl.constant = 0
//                    self.lblOnline.isHidden = false
//                }
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }

                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
//            else {
//                self.constWidthTbl.constant = 0
//                self.constLeadingColl.constant = 10
//
//                self.lblOnline.isHidden = true
//            }
        }
//        self.tblFriendList.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        objAppDelgate1.sideMenuIndex = 0
        tabBarItem.title = ""
        self.getMyBookings()
    }
}

extension FriendListVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        if tableView == tblOnline {
        //            return 5
        //        }
        //        else {
        if tableView == tblOnline {
            return arrOnlineFriends.count
        }
        else {
            return array_mybookings.count
        }
        //        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //if tableView == tblOnline {
        //        let cell = tblOnline.dequeueReusableCell(withIdentifier: "onlineTVCTableViewCell") as! onlineTVCTableViewCell
        //        // Set up cell.button
        //        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        //        return cell
        //        }
        //        else {
        if tableView == tblOnline {
            let identifier = "onlineTVCTableViewCell"
            var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
            
            if logincell == nil {
                tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
            }
            logincell?.selectionStyle = .none
            //        logincell?.layer.cornerRadius = 25
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            let url = URL(string: dict["pic"] as! String)
            logincell?.imgOnlineFriends.setImageFrom(url)
            logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
            
            return logincell!
        }
        else {
            let cell = tblFriendList.dequeueReusableCell(withIdentifier: "FriendsListCell") as! FriendsListCell
            // Set up cell.button
            let obj_requestmodal:FriendsListModel = array_mybookings[indexPath.row] as! FriendsListModel
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.updateCellwithModal(obj_bookingmodal: obj_requestmodal)
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblOnline {
            return 60.0
        }
        else {
            return 140.0
        }
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblOnline {
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            flagPark = false
            flagGym = false
            flagGallery = false
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            obj_phonelistvc.str_user_id = dict["userId"] as! String
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
        else {
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            flagPark = false
            flagGym = false
            flagGallery = false
            let obj_requestmodal:FriendsListModel = array_mybookings[indexPath.row] as! FriendsListModel
            obj_phonelistvc.str_user_id = obj_requestmodal.str_user_id
            //        self.present(obj_phonelistvc, animated: true, completion: nil)
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
    }
}

//MARK : Button action methods

extension FriendListVC {
    func transitionToNew(_ menuType: MenuType) {
        switch menuType {
        case .FitnessWall:
            let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Challenges:
            let rootVC : OrderHistoryVC = OrderHistoryVC(nibName: "OrderHistoryVC", bundle: nil)
            self.present(rootVC, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.dismissControllerAnimated()
                
            }
        case .Chat:
            let rootVC : FriendListVC = FriendListVC(nibName: "FriendListVC", bundle: nil)
//            self.present(rootVC, animated: true, completion: nil)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                self.dismissControllerAnimated()
//            }
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.dismissControllerAnimated()
            }
        default:
            break
        }
    }
    
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        if (txtEmailAddress.text?.isEmpty)! {
            //            DataUtil.alertMessage("Enter friend's email Id or username", viewController: self)
            showAlertWithTitleFromVC(vc: self, andMessage: "Enter friend's email Id or username")
            return
        }
        else {
            self.addFriendList()
        }
    }
    @IBAction func btnNewFriend(_ sender: UIButton) {
        vwNewFriend.isHidden = false
    }
    
}

extension FriendListVC: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = true
        return transiton
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = false
        return transiton
    }
}

//MARK: WEBSERVICE CALL
extension FriendListVC {
    
    
    func getMyBookings(){
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam = ["user_id": str_userid]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@",  (str_accessToken)),
            "Accept": "application/json"
        ]
        self.array_mybookings.removeAllObjects()
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.get_friendsList, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            self.lblEmpty.isHidden = true

            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.array_mybookings.removeAllObjects()
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:FriendsListModel = FriendsListModel(dictUserInfo: dict)
                        self.array_mybookings.add(obj_bookingmodal)
                    }
                    self.tblFriendList.reloadData()
                }else{
                    self.lblEmpty.isHidden = false
                    self.lblEmpty.text = successDict.value(forKey: "message") as? String
                   // showAlertWithTitleFromVC(vc: self, andMessage: ((successDict.value(forKey: "message") as? String)!))
                    //                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func addFriendList(){
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam = ["user_id": str_userid,"email" : txtEmailAddress.text!] as [String : Any]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@",  (str_accessToken)),
            "Accept": "application/json"
        ]
        self.array_mybookings.removeAllObjects()
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.add_friendsList, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.vwNewFriend.isHidden = true
                self.getMyBookings()
                //                self.array_mybookings.removeAllObjects()
                //                if let arr = successDict.value(forKey:"response"){
                //                    let array:NSArray = arr as! NSArray
                //                    for item in array {
                //                        let dict:NSDictionary =  item as! NSDictionary
                //                        let obj_bookingmodal:FriendsListModel = FriendsListModel(dictUserInfo: dict)
                //                        self.array_mybookings.add(obj_bookingmodal)
                //                    }
                //                    self.tblFriendList.reloadData()
            }else{
                showAlertWithTitleFromVC(vc: self, andMessage: ((successDict.value(forKey: "message") as? String)!))
                //                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
            
            
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    
    func syncContacts(){
        //friends_email
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam = ["user_id":str_userid, "friends_email":array_emails.joined(separator: ",")]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@",  (str_accessToken)),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.contact_sync, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.getMyBookings()
                /* if let arr = successDict.value(forKey:"response"){
                 let array:NSArray = arr as! NSArray
                 for item in array {
                 let dict:NSDictionary =  item as! NSDictionary
                 let obj_bookingmodal:FriendsListModel = FriendsListModel(dictUserInfo: dict)
                 self.array_mybookings.add(obj_bookingmodal)
                 }
                 self.tableview_bookings.reloadData()
                 }else{
                 DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                 }*/
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
}
