//
//  SideMenuCell.swift
//  IslandTransfer
//
//  Created by Ashish Mishra on 08/03/18.
//  Copyright © 2018 Ashish Mishra. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet var vwTitke: UIView!
    @IBOutlet weak var imageview_icon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
