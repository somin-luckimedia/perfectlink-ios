//
//  SideMenuController.swift
//  IslandTransfer
//
//  Created by Ashish Mishra on 08/03/18.
//  Copyright © 2018 Ashish Mishra. All rights reserved.
//

import UIKit
import SideMenuController_Swift4
import Alamofire
//import SJSwiftSideMenuController

enum MenuType: Int {
    case FitnessWall
    case Challenges
    case Chat
}

enum MenuTypeTrainer: Int {
    case FitnessWall
    case MyOrders
    case Friends
    case MyTips
    case Achievements
    case Wallet
}



enum GradientDirection {
    case leftToRight
    case rightToLeft
    case topToBottom
    case bottomToTop
}

class SideMenuController1: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var menuTableView : UITableView!
    var didTapMenuType: ((MenuType) -> Void)?
    var didTapMenuType1: ((MenuTypeTrainer) -> Void)?

    let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate

    
    @IBOutlet weak var imageView_header: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_weight: UILabel!
    @IBOutlet var view_footer: UIView!
    var menuItems : NSArray = NSArray()
    var menuItemsTrainer : NSArray = NSArray()
    var selectedImageArray : NSArray = NSArray()
    var unselectedImageArray : NSArray = NSArray()
    var selectedImageTrainerArray : NSArray = NSArray()
    var unselectedImageTrainerArray : NSArray = NSArray()
    var userinfo:UserInfo? = nil
    //9540817888
    @IBOutlet weak var imageview_profile: UIImageView!
    
    @IBAction func btnClose(_ sender: UIButton) {
        flagMenu = false
        dismissControllerAnimated()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuTableView.bounces = true
        menuTableView?.tableHeaderView = view_footer
        
        menuItems = ["Fitness Wall", "My Orders" , "Friends" ]
        selectedImageArray = ["Fitness Wall", "My Orders", "Friends"]
        unselectedImageArray = ["Fitness Wall", "My Orders", "Friends"]
        
        menuItemsTrainer = ["Fitness Wall"  , "My Orders" ,"Friends" , "My Tips", "Achievements", "Wallet"]
        selectedImageTrainerArray = ["Fitness Wall", "My Orders", "Friends" ,"My Tips","Achievements", "Wallet"]
        unselectedImageTrainerArray = ["Fitness Wall", "My Orders", "Friends" ,"My Tips","Achievements", "Wallet"]
        
         self.imageView_header.backgroundColor = UIColor.black
    }
    
    func setGradientBackground(colors: [UIColor], startPoint: CAGradientLayer.Point = .topLeft, endPoint: CAGradientLayer.Point = .bottomLeft) -> UIImage {
        let updatedFrame = CGRect(x: 0, y: 0, width: imageView_header.frame.size.width, height: 155)
        //updatedFrame.size.height + = self.frame.origin.y
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: colors, startPoint: startPoint, endPoint: endPoint)
        return gradientLayer.createGradientImage()!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        menuTableView.allowsSelection = true
        menuTableView.isUserInteractionEnabled = true
        self.view.backgroundColor = UIColor.clear
//        dismissControllerAnimated()
        self.update_profileData()
//        self.tabBarController?.tabBar.isHidden = true
//        self.tabBarController?.tabBar.layer.zPosition = -1



//        if flagMenu {
//            dismissControllerAnimated()
//            flagMenu = false
//        }
        
    }
    
    func update_profileData(){
        if let userinfo = DataUtil.appdelegate().objUserInfo{
            let url = URL(string: userinfo.profilePic)
            imageview_profile.setImageFrom(url)
            lbl_name?.text = userinfo.name?.capitalizingFirstLetter()
            
            if DataUtil.appdelegate().objUserInfo?.str_userType == "2"{
                lbl_weight?.text = String(format: "%@ lbs", userinfo.str_weight)
            }else{
                lbl_weight?.text = ""
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (DataUtil.appdelegate().objUserInfo?.str_userType) == "2" {
        return menuItems.count
        }
        
         return menuItemsTrainer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "SideMenuCell"
        var cell: SideMenuCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? SideMenuCell
        if cell == nil {
            tableView.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? SideMenuCell
        }
        
        var str_title:String = ""
        var str_image:String = ""

        if (DataUtil.appdelegate().objUserInfo?.str_userType) == "2" {
            str_title = menuItems.object(at: indexPath.row) as? String ?? ""
            str_image = unselectedImageArray.object(at: indexPath.row) as? String ?? ""
            
        }else{
            str_title = menuItemsTrainer.object(at: indexPath.row) as? String ?? ""
            str_image = unselectedImageTrainerArray.object(at: indexPath.row) as? String ?? ""
        }
        cell.imageview_icon.image = UIImage.init(named: str_image)
        cell.imageview_icon.setImageColor(color: UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1.0))
        cell.lbl_title?.text = str_title
        cell.lbl_title.textColor = UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1.0)
        if indexPath.row == objAppDelgate1.sideMenuIndex {
            cell.vwTitke.gradientBackground(from: #colorLiteral(red: 1, green: 0.2196078431, blue: 0.2235294118, alpha: 1), to: #colorLiteral(red: 1, green: 0.2196078431, blue: 1, alpha: 0), direction: .leftToRight)
            str_image = selectedImageArray.object(at: indexPath.row) as? String ?? ""
            cell.imageview_icon.image = UIImage.init(named: str_image)
            cell.lbl_title.textColor = .white

//            cell.imageview_icon.setImageColor(color: UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1.0))
//            theImageView.image = theImageView.image?.withRenderingMode(.alwaysTemplate)
//            theImageView.tintColor = UIColor.red
//            str_image = unselectedImageArray.object(at: objAppDelgate1.sideMenuIndex) as? String ?? ""
//            cell.imageview_icon.image = UIImage.init(named: str_image)
//            cell.imageview_icon.image = cell.imageview_icon.image?.withRenderingMode(.alwaysTemplate)
//            cell.imageview_icon.tintColor = .white
//            cell.lbl_title.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        objAppDelgate1.sideMenuIndex = indexPath.row
        if (DataUtil.appdelegate().objUserInfo?.str_userType) == "2" {
            guard let menuType = MenuType(rawValue: indexPath.row) else { return }
            //dismiss(animated: true) { [weak self] in
            print("Dismissing: \(menuType)")
            self.didTapMenuType?(menuType)
        }
        else {
            guard let menuTypeTrainer = MenuTypeTrainer(rawValue: indexPath.row) else { return }
            //dismiss(animated: true) { [weak self] in
            print("Dismissing: \(menuTypeTrainer)")
            self.didTapMenuType1?(menuTypeTrainer)
        }

        //}
    }
    //let middleColor:UIColor = UIColor(red: 252/255, green: 238/255, blue: 209/255, alpha: 1.0)
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.menuTableView.reloadData()
//        let cell:SideMenuCell = tableView.cellForRow(at: indexPath) as! SideMenuCell
//        cell.lbl_title.textColor = UIColor(red: 209/255, green: 9/255, blue: 4/255, alpha: 1.0)
//        let str_image = selectedImageArray.object(at: indexPath.row) as? String
//        cell.imageview_icon.image = UIImage.init(named: str_image!)
//
//        if (DataUtil.appdelegate().objUserInfo?.str_userType) == "2" {
//
//            switch indexPath.row {
//            case 0:
//
//                if (DataUtil.appdelegate().objUserInfo?.str_userType) == "2" {
//                    SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu")
//                    let rootVC : SettingVC = SettingVC(nibName: "SettingVC", bundle: nil)
//                    let nav = UINavigationController(rootViewController: rootVC)
//                    sideMenuController?.embed(centerViewController: nav)
//
//                }else{
//                    SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu")
//                    let rootVC : SettingVC = SettingVC(nibName: "SettingVC", bundle: nil)
//                    let nav = UINavigationController(rootViewController: rootVC)
//                    sideMenuController?.embed(centerViewController: nav)
//                }
//
//
//
//                break
//
//            case 1:
//                if (DataUtil.appdelegate().objUserInfo?.str_userType) == "2" {
//                    SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu")
//                    let rootVC : SettingVC = SettingVC(nibName: "SettingVC", bundle: nil)
//                    let nav = UINavigationController(rootViewController: rootVC)
//                    sideMenuController?.embed(centerViewController: nav)
//
//                }else{
//                    SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu")
//                    let rootVC : SettingVC = SettingVC(nibName: "SettingVC", bundle: nil)
//                    let nav = UINavigationController(rootViewController: rootVC)
//                    sideMenuController?.embed(centerViewController: nav)
//                }
//                break
//
//            case 2:
//                SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu_icon")
//                let rootVC : SettingVC = SettingVC(nibName: "SettingVC", bundle: nil)
//                let nav = UINavigationController(rootViewController: rootVC)
//                sideMenuController?.embed(centerViewController: nav)
//                break
//
//
//            case 3:
//                //InboxVC
//
//                SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu_icon")
//                let rootVC : SettingVC = SettingVC(nibName: "SettingVC", bundle: nil)
//                let nav = UINavigationController(rootViewController: rootVC)
//                sideMenuController?.embed(centerViewController: nav)
//
//                break
//
//            case 4:
//
//                if (DataUtil.appdelegate().objUserInfo?.str_userType) == "2" {
//                    SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu_icon")
//                    let rootVC : SettingVC = SettingVC(nibName: "SettingVC", bundle: nil)
//                    let nav = UINavigationController(rootViewController: rootVC)
//                    sideMenuController?.embed(centerViewController: nav)
//                }else{
//                    SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu_icon")
//                    let rootVC : SettingVC = SettingVC(nibName: "SettingVC", bundle: nil)
//                    let nav = UINavigationController(rootViewController: rootVC)
//                    sideMenuController?.embed(centerViewController: nav)
//                }
//
//                break
//                //SupportVC
//            default:
//                break
//            }
//        }
//
//
//    }
    
//    func logout_btn_clicked(){
//        let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: "Are you sure to logout from the app?", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
//           // self.logoutServiceCalled()
//            UserDefaults.standard.removeObject(forKey: "LogedInUserID")
//            UserDefaults.standard.removeObject(forKey: "LogedIn")
//                UserDefaults.standard.removeObject(forKey: "LogedInUserAccessToken")
//                UserDefaults.standard.synchronize()
//            let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
//            objAppDelgate.load_login_view()
//
//        }))
//        alert.addAction(UIAlertAction(title:"Cancel", style: .default, handler: { (alert) in
//        }))
//        self.present(alert, animated: true, completion: nil)
//    }
    
    
//    func logoutServiceCalled(){
//        let str_userId:String = (DataUtil.appdelegate().objUserInfo?.userId)!
//        let str_token:String = ""
//        //devicetoken
//        let postParam: Parameters = ["user_id":str_userId,"token":str_token]
//        print("Login Post Parameter : \(postParam)")
//        ServerCommunication.getDataWithPost(url: ConstantFiles.logOut_user, parameter: postParam, viewController: self, success: { (successDict) in
//            print(successDict)
//            if (successDict.value(forKey: "status") as? String) == "true"{
//                // DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
//                let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
//               // objAppDelgate.load_login_view()
//            }else{
//                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
//            }
//
//            // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
//        }) { (dictFailure) in
//            if (dictFailure.value(forKey: "message") as? String) != nil {
//                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
//            }
//        }
//    }
   
    
}
extension UIImageView {
  func setImageColor(color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
}
extension CGFloat {
    public static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
extension UIColor {
    public static func randomColor() -> UIColor {
        // If you wanted a random alpha, just create another
        // random number for that too.
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

extension UIView {
    func gradientBackground(from color1: UIColor, to color2: UIColor, direction: GradientDirection) {
        let gradient = CAGradientLayer()
        let screenRect: CGRect = UIScreen.main.bounds

        gradient.frame = CGRect(x: 0, y: 10, width: screenRect.width - 60, height: 44)
        gradient.colors = [color1.cgColor, color2.cgColor]
    
        switch direction {
        case .leftToRight:
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        case .rightToLeft:
            gradient.startPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 0.0, y: 0.5)
        case .bottomToTop:
            gradient.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradient.endPoint = CGPoint(x: 0.5, y: 0.0)
        default:
            break
        }
        gradient.cornerRadius = self.layer.cornerRadius
        self.layer.insertSublayer(gradient, at: 0)
    }
}
