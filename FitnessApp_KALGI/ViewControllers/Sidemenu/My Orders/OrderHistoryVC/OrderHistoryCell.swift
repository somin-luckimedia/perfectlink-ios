//
//  OrderHistoryCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 20/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit

class OrderHistoryCell: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var imageView_thumb: UIImageView!
    @IBOutlet var lblOrderDate: UILabel!
    @IBOutlet var lblOrderId: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_bookingmodal:OrderHistoryModel){
        lbl_title.text = String(format: "%@", obj_bookingmodal.str_product_title)
        lbl_description.text = String(format: "%@", obj_bookingmodal.str_brief_description)
        let url = URL(string: obj_bookingmodal.str_cover_image)
        imageView_thumb.setImageFrom(url)
        
        if obj_bookingmodal.str_product_type == "ACCESSORIES" || obj_bookingmodal.str_product_type == "PRODUCT"{
            lblOrderId.text = obj_bookingmodal.str_order_id
            lblOrderDate.text = obj_bookingmodal.str_order_date
//        lbl_price.text = String(format: "%@%@/%@", obj_bookingmodal.str_currency,obj_bookingmodal.str_amount,obj_bookingmodal.str_per)
        }else{
//            lbl_price.text = String(format: "Membership Valid Upto: %@", obj_bookingmodal.str_valid_upto)
        }
    }
    
    
}

