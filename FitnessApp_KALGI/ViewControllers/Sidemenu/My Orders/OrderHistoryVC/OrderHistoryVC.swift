//
//  OrderHistoryVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 17/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit
import Alamofire
//OrderHistoryCell
class OrderHistoryVC: UIViewController , UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate  {
    
    fileprivate var array_mybookings:NSMutableArray = NSMutableArray()
    @IBOutlet weak var imageView_BG: UIImageView!
    
    @IBOutlet weak var tableview_bookings: UITableView!
    var dict_item:NSDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.title = "MY ORDERS"
        tableview_bookings.delegate  = self as UITableViewDelegate
        tableview_bookings.dataSource = self as UITableViewDataSource
        //self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        if (DataUtil.appdelegate().objUserInfo?.str_userType) == "2" {
            self.getMyBookings()
            
        }else{
            self.getTrainerOrders()
        }
        
        
        //self.addLeftMenuButtonWithImage()
        //    self.addRightMenuButtonWithImage()
        
        
    }
    @IBAction func btnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "filter_icon"), for: UIControl.State.normal)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getMyBookings(){
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        let str_userId:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let postParam: Parameters = ["user_id":str_userId]
        print("Login Post Parameter : \(postParam)")

        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.get_my_orders, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:OrderHistoryModel = OrderHistoryModel(dictUserInfo: dict)
                        self.array_mybookings.add(obj_bookingmodal)
                    }
                    self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    func getTrainerOrders(){
        let postParam: Parameters = ["user_id":(DataUtil.appdelegate().objUserInfo?.userId)!]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]

        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.my_order_trainer, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:OrderHistoryModel = OrderHistoryModel(dictUserInfo: dict)
                        self.array_mybookings.add(obj_bookingmodal)
                    }
                    self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 140
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.array_mybookings.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "OrderHistoryCell"
        var logincell: OrderHistoryCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderHistoryCell
        if logincell == nil {
            tableView.register(UINib(nibName: "OrderHistoryCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderHistoryCell)!
        }
        //logincell!.delegate = self
        let obj_requestmodal:OrderHistoryModel = array_mybookings[indexPath.row] as! OrderHistoryModel
        logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal)
        return logincell!
    }
    
//    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//       let obj_requestmodal:OrderHistoryModel = array_mybookings[indexPath.row] as! OrderHistoryModel
//        
//         if obj_requestmodal.str_product_type == "ACCESSORIES" || obj_requestmodal.str_product_type == "PRODUCT"{
//        let rootVC : OrderConfirmationVC = OrderConfirmationVC(nibName: "OrderConfirmationVC", bundle: nil)
//        rootVC.str_transactionId = String(format: "%@", obj_requestmodal.str_transaction_id)
//        rootVC.isFromHistory = true
//        self.navigationController?.pushViewController(rootVC, animated: true)
//         }else{
//            let rootVC : SubscriptionConfirmationVC = SubscriptionConfirmationVC(nibName: "SubscriptionConfirmationVC", bundle: nil)
//            rootVC.str_transactionId = String(format: "%@", obj_requestmodal.str_transaction_id)
//            rootVC.isFromHistory = true
//            rootVC.str_subscription_type = obj_requestmodal.str_product_type
//            self.navigationController?.pushViewController(rootVC, animated: true)
//        }
//    }
    
}


