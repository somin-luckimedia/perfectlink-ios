//
//  WallPostListingVC.swift
//  FitnessApp
//
//  Created by Ashish on 01/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit
import Alamofire
import AVFoundation
import Photos
import AVKit
import Firebase


class WallPostListingVC: UIViewController , UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, WallPostCellDelegate, ImageAddDelegate  {
    func btn_user(cell: WallPostCell, btn: UIButton) {
        let obj_requestmodal:WallListModel = array_mybookings[cell.rowIndex] as! WallListModel
        wallPostIndex = cell.rowIndex
        wallPostId = obj_requestmodal.str_user_id
        let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
        obj_phonelistvc.str_user_id = wallPostId
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)

    }
    
    func wall_post_delete(cell: WallPostCell, btn: UIButton) {
    vwDeletepost.isHidden = false
        let obj_requestmodal:WallListModel = array_mybookings[cell.rowIndex] as! WallListModel
        wallPostIndex = cell.rowIndex
        wallPostId = obj_requestmodal.str_id
    }
    

    
    func comment_btn_clicked(cell: WallPostCell, btn: UIButton) {
        let obj_requestmodal:WallListModel = array_mybookings[cell.rowIndex] as! WallListModel
        let obj_parkeAddressVC:CommentVC = CommentVC(nibName: "CommentVC", bundle: nil)
        // let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
//        if #available(iOS 13.0, *) {
//            obj_parkeAddressVC.modalPresentationStyle = .fullScreen
//        } else {
//            // Fallback on earlier versions
//        }
//        obj_parkeAddressVC.obj_wallpostModel = obj_requestmodal
        obj_parkeAddressVC.wallPost_id = Int(obj_requestmodal.str_id)!
        self.navigationController?.pushViewController(obj_parkeAddressVC, animated: true)
       // self.present(obj_parkeAddressVC, animated: true, completion: nil)
        //        self.navigationController?.present(nav, animated: true, completion: nil)
        
    }
    
    func share_btn_clicked(cell: WallPostCell, btn: UIButton) {
        
        let obj_requestmodal:WallListModel = array_mybookings[cell.rowIndex] as! WallListModel
        let shareNumber  : Int = NumberFormatter().number(from: obj_requestmodal.str_share_count) as! Int
        cell.lblShareCount.text = "\(shareNumber + 1)"
        shareBtnClick(wall_id: obj_requestmodal.str_id!,shareCount: shareNumber + 1)
        let str_message:String = String(format: "Please find the wall item posted by %@:", obj_requestmodal.str_name)
        let name = NSURL(string: obj_requestmodal.str_file)
        let objectsToShare = [str_message, name!] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)
    }
    
    func shareBtnClick(wall_id : String,shareCount : Int) {
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam  = ["wall_id":wall_id]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.add_wall_share, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{

            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    func replace_array(obj: WallListModel, index: Int) {
        self.array_mybookings.replaceObject(at: index, with: obj)
    }
    
    func Imageadded_clicked() {
        
    }
    
    @IBAction func btnAddWallTapped(_ sender: UIButton) {
        let obj_phonelistvc:AddWallActivityVC = AddWallActivityVC(nibName: "AddWallActivityVC", bundle: nil)
        self.present(obj_phonelistvc, animated: true, completion: nil)
    }

    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func like_btn_clicked(cell: WallPostCell, btn: UIButton) {
        let obj_requestmodal:WallListModel = array_mybookings[cell.rowIndex] as! WallListModel
        var like_count:Int = Int(obj_requestmodal.str_likes)!
        var dislike_count:Int = Int(obj_requestmodal.str_unlikes)!
        if obj_requestmodal.str_is_liked == "1"{
            obj_requestmodal.str_is_liked = "0"
            like_count = like_count - 1
        }else{
            obj_requestmodal.str_is_liked = "1"
            like_count = like_count + 1
            if obj_requestmodal.str_is_unlike == "1"{
                dislike_count = dislike_count - 1
                obj_requestmodal.str_is_unlike = "0"
            }
        }
        obj_requestmodal.str_likes = String(format: "%ld", like_count)
        obj_requestmodal.str_unlikes = String(format: "%ld", dislike_count)
        cell.lbl_like_count.text = String(format: "%@", obj_requestmodal.str_likes!)
        //cell.lbl_dislike_count.text = String(format: "%@", obj_requestmodal.str_unlikes!)
        self.array_mybookings.replaceObject(at: cell.rowIndex, with: obj_requestmodal)
        
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam: Parameters = ["user_id":str_userid, "wall_id":obj_requestmodal.str_id!, "like_unlike":"1"]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.like_post, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.getMyBookings()
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    func dislike_btn_clicked(cell: WallPostCell, btn: UIButton) {
        let obj_requestmodal:WallListModel = array_mybookings[cell.rowIndex] as! WallListModel
        var like_count:Int = Int(obj_requestmodal.str_likes)!
        var dislike_count:Int = Int(obj_requestmodal.str_unlikes)!
        
        if obj_requestmodal.str_is_unlike == "1"{
            obj_requestmodal.str_is_unlike = "0"
            dislike_count = dislike_count - 1
        }else{
            obj_requestmodal.str_is_unlike = "1"
            dislike_count = dislike_count + 1
            if obj_requestmodal.str_is_liked == "1"{
                like_count = like_count - 1
                obj_requestmodal.str_is_liked = "0"
            }
        }
        obj_requestmodal.str_likes = String(format: "%ld", like_count)
        obj_requestmodal.str_unlikes = String(format: "%ld", dislike_count)
        cell.lbl_like_count.text = String(format: "%@", obj_requestmodal.str_likes!)
        //cell.lbl_dislike_count.text = String(format: "%@", obj_requestmodal.str_unlikes!)
        self.array_mybookings.replaceObject(at: cell.rowIndex, with: obj_requestmodal)
        
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam: Parameters = ["user_id":str_userid, "wall_id":obj_requestmodal.str_id!, "like_unlike":"0"]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]

        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.dislike_post, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{

            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
        
    }
    
    
    
    func play_btn_clicked(cell: WallPostCell, btn: UIButton) {
        let obj_requestmodal:WallListModel = array_mybookings[cell.rowIndex] as! WallListModel
        let url = URL(string: obj_requestmodal.str_file)
        let player = AVPlayer(url: url!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.present(playerController, animated: true) {
            player.play()
        }
    }
    
    func preview_btn_clicked(cell: WallPostCell, btn: UIButton){
        let obj_requestmodal:WallListModel = array_mybookings[cell.rowIndex] as! WallListModel
        let obj_parkeAddressVC:ImageViewerVC = ImageViewerVC(nibName: "ImageViewerVC", bundle: nil)
//        let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
        obj_parkeAddressVC.delegate = self
        strType_camera = "camera"
        flagChallengeImageviewer = false
        obj_parkeAddressVC.str_url = obj_requestmodal.str_file
//        self.navigationController?.present(obj_parkeAddressVC, animated: true, completion: nil)
        self.present(obj_parkeAddressVC, animated: true, completion: nil)
    }
    
    @IBAction func btnNoDeletePostTapped(_ sender: UIButton) {
        self.vwDeletepost.isHidden = true
    }
    @IBAction func btnYesDeletePostTapped(_ sender: UIButton) {
        self.vwDeletepost.isHidden = true
        self.deletePost()
    }
    
    @IBOutlet var vwDeletepost: UIView!
    fileprivate var array_mybookings:NSMutableArray = NSMutableArray()
    @IBOutlet weak var imageView_BG: UIImageView!
    @IBOutlet weak var tableview_bookings: UITableView!
    
    var wallPostId : String = ""
    var dict_item:NSDictionary!
    var isFromSignup:Bool = false
    var wallPostIndex : Int = 0
    
    @IBOutlet var tblOnline: UITableView!
    var arrOnlineFriends : NSMutableArray = []
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vwDeletepost.isHidden = true

        self.title = "FITNESS WALL"
        tableview_bookings.delegate  = self as UITableViewDelegate
        tableview_bookings.dataSource = self as UITableViewDataSource
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
       // self.addLeftMenuButtonWithImage()
            self.addRightMenuButtonWithImage()
        let notificationCenter = NotificationCenter.default
               notificationCenter.addObserver(self,
                                              selector: #selector(dismissWall),
                                              name: NSNotification.Name(rawValue: "WallPost"),
                                              object: nil)
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
//                if snapshot.childrenCount == 1 {
//                    self.constWidthTbl.constant = 0
////                    self.constLeadingColl.constant = 20
//                    self.lblOnline.isHidden = true
//                }
//                else {
//                    self.constWidthTbl.constant = 60
////                    self.constLeadingColl.constant = 0
//                    self.lblOnline.isHidden = false
//                }
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }

                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
//            else {
//                self.constWidthTbl.constant = 0
//                self.constLeadingColl.constant = 10
//
//                self.lblOnline.isHidden = true
//            }
        }
        self.getMyBookings()
        
    }
    
    @objc func dismissWall(){
//        self.dismissControllerAnimated()
        print("----Dismiss wall ----")
        self.getMyBookings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        print("----viewwillAppear----")
    }
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "add_icon"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.add_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func add_btn_clicked(){
//                let obj_phonelistvc:AddWallActivityVC = AddWallActivityVC(nibName: "AddWallActivityVC", bundle: nil)
//                //let obj_requestmodal:ProteinListModel = array_mybookings[indexPath.row] as! ProteinListModel
//                //obj_phonelistvc.str_productId = obj_requestmodal.str_product_id
//                self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
    
    func deletePost(){
//user_id
        let postParam: Parameters = ["wallpost_id":wallPostId]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.delete_Wall_post, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                self.getMyBookings()
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func getMyBookings(){
//user_id
        self.array_mybookings.removeAllObjects()
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam: Parameters = ["user_id":str_userid, "page_number":"1"]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.get_wall_list, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:WallListModel = WallListModel(dictUserInfo: dict)
                        self.array_mybookings.add(obj_bookingmodal)
                    }
                   self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == tblOnline {
            return 60
        }
        else {
            return 360
        }
    }

        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if tableView == tblOnline {
                return arrOnlineFriends.count
            }
            else {
                return self.array_mybookings.count
            }
        }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblOnline {
            let identifier = "onlineTVCTableViewCell"
            var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
            
            if logincell == nil {
                tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
            }
            logincell?.selectionStyle = .none
            //        logincell?.layer.cornerRadius = 25
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            let url = URL(string: dict["pic"] as! String)
            logincell?.imgOnlineFriends.setImageFrom(url)
            logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
            return logincell!
        }
        else {
            let identifier = "WallPostCell"
            var logincell: WallPostCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? WallPostCell
            if logincell == nil {
                tableView.register(UINib(nibName: "WallPostCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? WallPostCell)!
            }
            logincell!.delegate = self
            print("----array_mybooking.count----",array_mybookings.count)
            print("----indexPath----",indexPath.row)
            let obj_requestmodal:WallListModel = array_mybookings[indexPath.row] as! WallListModel
            logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal, rowIndex: indexPath.row)
            return logincell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblOnline {
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            flagPark = false
            flagGym = false
            flagGallery = false
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            obj_phonelistvc.str_user_id = dict["userId"] as! String
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
    }
// 
//    
//    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
////        let obj_phonelistvc:ProtienDetailVC = ProtienDetailVC(nibName: "ProtienDetailVC", bundle: nil)
////        let obj_requestmodal:ProteinListModel = array_mybookings[indexPath.row] as! ProteinListModel
////        obj_phonelistvc.str_productId = obj_requestmodal.str_product_id
////        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
//        
    }
    
    
    
    /*
     
     
     
     */
    
    
    
    
    
    
    
