//
//  WallPostCell.swift
//  FitnessApp
//
//  Created by Ashish on 01/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import AVKit


protocol WallPostCellDelegate:class {
    func like_btn_clicked(cell: WallPostCell, btn: UIButton)
    func dislike_btn_clicked(cell: WallPostCell, btn: UIButton)
    func play_btn_clicked(cell: WallPostCell, btn: UIButton)
    func wall_post_delete(cell: WallPostCell, btn: UIButton)

    func preview_btn_clicked(cell: WallPostCell, btn: UIButton)
    func btn_user(cell: WallPostCell, btn: UIButton)

    func replace_array(obj: WallListModel, index: Int)
    
    func comment_btn_clicked(cell: WallPostCell, btn: UIButton)
    func share_btn_clicked(cell: WallPostCell, btn: UIButton)
}


class WallPostCell: UITableViewCell {
    @IBOutlet var lbl_commenCount: UILabel!
    
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_min_ago: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_like_count: UILabel!
    @IBOutlet weak var lbl_dislike_count: UILabel!
    @IBOutlet weak var imageView_profile_thumb: UIImageView!
    @IBOutlet weak var imageView_file: UIImageView!
    @IBOutlet weak var imageView_like: UIImageView!
    @IBOutlet weak var imageView_dislike: UIImageView!
    
    @IBOutlet var btnWallPostDeleteOutlt: UIButton!
    @IBOutlet weak var view_video: UIView!
    @IBOutlet weak var imageview_thumb: UIImageView!
    
    @IBOutlet var lblShareCount: UILabel!
    var rowIndex:Int = 0
    weak var delegate:WallPostCellDelegate?
    var obj_wallpostModel:WallListModel? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btnDeletepost(_ sender: UIButton) {
        self.delegate?.wall_post_delete(cell: self, btn: sender)
        

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_bookingmodal:WallListModel, rowIndex:Int){
        self.rowIndex = rowIndex
        self.obj_wallpostModel = obj_bookingmodal
        lbl_name.text = obj_bookingmodal.str_name
        lbl_description.text =  String(format: "%@", obj_bookingmodal.str_text!)
        lbl_like_count.text = String(format: "%@", obj_bookingmodal.str_likes!)
        lbl_commenCount.text = String(format: "%@", obj_bookingmodal.str_comment_count!)
        lblShareCount.text = String(format: "%@", obj_bookingmodal.str_share_count!)
        if obj_bookingmodal.str_is_user == "1" {
            
        }
        //lbl_dislike_count.text = String(format: "%@", obj_bookingmodal.str_unlikes!)
//        imageView_profile_thumb.s//(stringImageUrl: obj_bookingmodal.str_profile_picture)
        var url = URL(string: obj_bookingmodal.str_profile_picture)
        imageView_profile_thumb.setImageFrom(url)
//        imageView_file.setImageFromURL(stringImageUrl: obj_bookingmodal.str_file)
        url = URL(string: obj_bookingmodal.str_file)
        imageView_file.setImageFrom(url)
        if obj_bookingmodal.str_is_liked == "1"{
            imageView_like.image = UIImage(named: "likeD_icon")
        }else{
            imageView_like.image = UIImage(named: "likeS_icon")
        }
        
        
        if obj_bookingmodal.str_is_user == "1"{
            btnWallPostDeleteOutlt.isHidden = false
        }else{
            btnWallPostDeleteOutlt.isHidden = true
        }
        
        if obj_bookingmodal.str_file_type == "VIDEO"{
            self.view_video.isHidden = false
            self.imageView_file.isHidden = true
            let url = URL(string: obj_bookingmodal.str_thumb_image)
            imageview_thumb.setImageFrom(url)
//            imageview_thumb.setImageFromURL(stringImageUrl: obj_bookingmodal.str_thumb_image)
          /*  if self.obj_wallpostModel?.image_video != nil{
                self.imageview_thumb.image = self.obj_wallpostModel?.image_video
            }else{
            self.getThumbnailImageFromVideoUrl(url: url!) { (thumbImage) in
                self.imageview_thumb.image = thumbImage
                if thumbImage != nil{
                    if self.obj_wallpostModel?.image_video == nil{
                        self.obj_wallpostModel?.image_video = thumbImage
                        self.delegate?.replace_array(obj: self.obj_wallpostModel!, index: self.rowIndex)
                    }
                }
            }
            }*/
        }else{
            self.view_video.isHidden = true
            self.imageView_file.isHidden = false
        }
        
        if let timeResult = Double(obj_bookingmodal.str_posted_at) {
            let date = Date(timeIntervalSince1970: timeResult)
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.short //Set time style
            dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
            dateFormatter.timeZone = NSTimeZone() as TimeZone
            let localDate = dateFormatter.string(from: date as Date)
            lbl_min_ago.text = localDate
        }
        
        
    }
    
    @IBAction func btn_user(_ sender: UIButton) {
        self.delegate?.btn_user(cell: self, btn: sender)
    }
    
    @IBAction func like_btn_clicked(_ sender: UIButton) {
//        if self.obj_wallpostModel?.str_is_liked == "1"{
//            //likeD_icon
//            imageView_like.image = UIImage(named: "likeD_icon")
//        }else{
//            //likeS_icon
//            imageView_like.image = UIImage(named: "likeS_icon")
//        }
       // imageView_dislike.image = UIImage(named: "dislikeD_icon")
        self.delegate?.like_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func dislike_btn_clicked(_ sender: UIButton) {
//        if self.obj_wallpostModel?.str_is_unlike == "1"{
//           // imageView_dislike.image = UIImage(named: "dislikeD_icon")
//        }else{
//           // imageView_dislike.image = UIImage(named: "dislikeS_icon")
//        }
//        imageView_like.image = UIImage(named: "likeD_icon")
//        self.delegate?.dislike_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func play_btn_clicked(_ sender: UIButton) {
        self.delegate?.play_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func preview_btn_clicked(_ sender: UIButton) {
        self.delegate?.preview_btn_clicked(cell: self, btn: sender)
    }
    
     @IBAction func comment_btn_clicked(_ sender: UIButton) {
        self.delegate?.comment_btn_clicked(cell: self, btn: sender)
    }
    
     @IBAction func share_btn_clicked(_ sender: UIButton) {
        self.delegate?.share_btn_clicked(cell: self, btn: sender)
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
    
}
//preview
