//
//  PhoneCollectionCell.swift
//  Spolu
//
//  Created by Ashish Mishra on 31/03/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit

class PhoneCollectionCell: UICollectionViewCell {

    
    @IBOutlet weak var imageview_phone: UIImageView!
    @IBOutlet weak var view_inner: UIView!
    @IBOutlet weak var lbl_phonemodel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        view_inner.layer.shadowColor = UIColor.black.cgColor
        view_inner.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        view_inner.layer.shadowOpacity = 0.4
        view_inner.layer.shadowRadius = 6.0
        layer.masksToBounds = true
       // view_inner.dropShadow()
        // Initialization code
    }

}

extension UIView {
    
    func dropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
    }
}
