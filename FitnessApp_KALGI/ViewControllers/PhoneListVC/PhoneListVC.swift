//
//  PhoneListVC.swift
//  Spolu
//
//  Created by Ashish Mishra on 31/03/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit

class PhoneListVC: UIViewController {
    
    @IBOutlet weak var imageView_BG: UIImageView!
    @IBOutlet weak var collectioneview_phone: UICollectionView!
    var array_phonemodal:NSMutableArray = NSMutableArray(objects: "Apple", "Samsung", "Sony", "Google Pixel", "Xiomi", "Oppo", "Vivo", "Auxes", "LG", "HTC", "Motorola", "Redmi", "Karbonn", "Realme")
    var array_phone:NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Select Phone Model"
        self.addLeftMenuButtonWithImage()
        self.imageView_BG.image = DataUtil.setGradientBackgroundImage()
        self.collectioneview_phone.register(UINib(nibName: "PhoneCollectionCell", bundle: nil), forCellWithReuseIdentifier: "PhoneCollectionCell")
        self.get_mobilelisting()
        // Do any additional setup after loading the view.
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }

    
    func get_mobilelisting() {
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//http://spolu.in/carbon/public/brand-images/1557661662.png
extension PhoneListVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_phone.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:PhoneCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhoneCollectionCell", for: indexPath) as! PhoneCollectionCell
        let dict:NSDictionary = array_phone[indexPath.row] as! NSDictionary
        cell.lbl_phonemodel.text = dict["name"] as? String
        let str_url:String = String(format:"https://www.%@", DataUtil.nullToBlank(value: dict.value(forKey: "image") as AnyObject))
        print("image url  ", str_url)
        let url = URL(string: str_url)!
         cell.imageview_phone.setImageFrom(url)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenwidth:CGFloat = UIScreen.main.bounds.size.width/2
        return CGSize(width: screenwidth, height: 100)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let obj_phonelistvc:PhoneDetailVC = PhoneDetailVC(nibName: "PhoneDetailVC", bundle: nil)
        obj_phonelistvc.dict_phone = array_phone[indexPath.row] as? NSDictionary
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        
    }
    
}
