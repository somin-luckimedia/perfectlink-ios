//
//  TrainerHomeCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 25/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//


import UIKit
import Foundation
import Photos
import AVFoundation
import AVKit
import Alamofire


protocol TrainerHomeCellDelegate:class {
    func profile_edit_clicked(cell: TrainerHomeCell, btn: UIButton)
    func gallery_button_clicked(cell: TrainerHomeCell, btn: UIButton)
}


class TrainerHomeCell: UITableViewCell {
    
    

    @IBOutlet weak var imageView_cover: UIImageView!
    @IBOutlet weak var rateView: DYRateView!
    @IBOutlet weak var lbl_rate: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_gender: UILabel!
    weak var delegate: TrainerHomeCellDelegate?
    
    
    @IBOutlet weak var lbl_imagecount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateCellwithModal(obj_gymDetail:UserInfo){
        let url = URL(string: obj_gymDetail.str_coverImage)!

        imageView_cover.setImageFrom(url)
        lbl_rate.text = obj_gymDetail.str_rating
        lbl_title.text = obj_gymDetail.name
        lbl_gender.text = String(format: "%@, %@ years", obj_gymDetail.gender!, obj_gymDetail.str_age)
        lbl_imagecount.text = String(format: "%ld", obj_gymDetail.array_gallery_image.count)
    }
    
    @IBAction func edit_btn_clicked(_ sender: UIButton) {
        self.delegate?.profile_edit_clicked(cell: self, btn: sender)
    }
    
    
    @IBAction func gallery_btn_clicked(_ sender: UIButton) {
        self.delegate?.gallery_button_clicked(cell: self, btn: sender)
    }
    
    
}




extension TrainerHomeCell:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return 10
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
            let cell:EquipmentCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "EquipmentCollectionCell", for: indexPath) as! EquipmentCollectionCell
             cell.lbl_title.text = "Testing"
        cell.imageView_equipment.image = UIImage(named: "appLogo")
           // let dict_equipement:NSDictionary = self.obj_gymdetail.array_equipments[indexPath.row] as!NSDictionary
           // cell.updateCellwithModal(obj_gymDetail: dict_equipement)
            return cell
            
        
        
       
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    
  
    
    
    
}

