//
//  TrainerHomeLowerCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 12/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import Foundation
import Photos
import AVFoundation
import AVKit
import Alamofire




protocol TrainerLowerHomeCellDelegate:class {
    func about_edit_clicked(cell: TrainerHomeLowerCell, btn: UIButton)
    func video_edit_clicked(cell: TrainerHomeLowerCell, btn: UIButton)
    func achievements_edit_clicked(cell: TrainerHomeLowerCell, btn: UIButton)
    func paypal_edit_clicked(cell: TrainerHomeLowerCell, btn: UIButton)
    func membership_edit_clicked(cell: TrainerHomeLowerCell, btn: UIButton)
}

class TrainerHomeLowerCell: UITableViewCell {
    
    
    
    @IBOutlet weak var collectionview_equipments: UICollectionView!
    @IBOutlet weak var lbl_aboutTitle: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_achievement: UILabel!
    var obj_homecontroller:TrainerHomeVC? = nil
    @IBOutlet weak var lbl_singleRate: UITextField!
    @IBOutlet weak var lbl_double_rate: UITextField!
    @IBOutlet weak var lbl_paypal: UILabel!
    
    weak var delegate: TrainerLowerHomeCellDelegate?
    var obj_gymdetail:UserInfo!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateCellwithModal(obj_gymDetail:UserInfo){
        self.obj_gymdetail = obj_gymDetail
        self.collectionview_equipments.register(UINib(nibName: "HomeVideoThumbCell", bundle: nil), forCellWithReuseIdentifier: "HomeVideoThumbCell")
self.lbl_paypal.text = DataUtil.appdelegate().objUserInfo?.str_paypal_id
        lbl_aboutTitle.text = String(format:"About %@", obj_gymDetail.name!)
        //let str_about : String = "Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, "
        
        
     //   let str_about1 : String = " \u{2022} Lorem ipsum dolor sit amet.\n \u{2022} Lorem ipsum dolor sit amet.\n \u{2022} Lorem ipsum dolor sit amet.\n \u{2022} Lorem ipsum dolor sit amet.\n \u{2022} Lorem ipsum dolor sit amet.\n \u{2022} Lorem ipsum dolor sit amet.\n \u{2022} Lorem ipsum dolor sit amet.\n \u{2022} Lorem ipsum dolor sit amet.\n \u{2022} Lorem ipsum dolor sit amet.\n \u{2022} Lorem ipsum dolor sit amet.\n "
        
        //\u{2022}
        lbl_description.text = obj_gymdetail.str_about
        var str_achievement:String = ""
        for obj_model in obj_gymDetail.array_achievements{
            let obj_achievments:AchievementsModel = obj_model as! AchievementsModel
            str_achievement.append(String(format: " \u{2022} %@ \n\n", obj_achievments.str_achievement))
        }
        /*
         
         str_gruop_rate
         str_single_rate
         */
        
        lbl_achievement.text = str_achievement
        self.lbl_singleRate.text = String(format: "$%@", obj_gymDetail.str_single_rate)
        self.lbl_double_rate.text = String(format: "$%@", obj_gymDetail.str_gruop_rate)
        self.collectionview_equipments.reloadData()
        
        
    }
    

    
    @IBAction func about_edit_clicked(_ sender: UIButton) {
        self.delegate?.about_edit_clicked(cell: self, btn: sender)
    }
    
    @IBAction func video_edit_clicked(_ sender: UIButton) {
        self.delegate?.video_edit_clicked(cell: self, btn: sender)
    }
    
    @IBAction func achievements_edit_clicked(_ sender: UIButton) {
        self.delegate?.achievements_edit_clicked(cell: self, btn: sender)
    }
    
    @IBAction func paypal_edit_clicked(_ sender: UIButton) {
        self.delegate?.paypal_edit_clicked(cell: self, btn: sender)
    }
    
    @IBAction func membership_edit_clicked(_ sender: UIButton) {
        self.delegate?.membership_edit_clicked(cell: self, btn: sender)
    }
    
}




extension TrainerHomeLowerCell:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.obj_gymdetail.array_videos.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
    
        let cell:HomeVideoThumbCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeVideoThumbCell", for: indexPath) as! HomeVideoThumbCell
        let obj_videoModel:VideosModel = DataUtil.appdelegate().objUserInfo?.array_videos[indexPath.row] as! VideosModel
       cell.updateCellwithModal(model: obj_videoModel)
        
      //  cell.lbl_title.text = obj_videoModel.str_title
      //  cell.imageView_equipment.image = UIImage(named: "appLogo")
        // let dict_equipement:NSDictionary = self.obj_gymdetail.array_equipments[indexPath.row] as!NSDictionary
        // cell.updateCellwithModal(obj_gymDetail: dict_equipement)
        return cell
        
        
        
        
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if obj_homecontroller != nil{
            let obj_videoModel:VideosModel = DataUtil.appdelegate().objUserInfo?.array_videos[indexPath.row] as! VideosModel
            let fileUrl = URL(string: obj_videoModel.str_video)
            let player = AVPlayer(url: fileUrl!)
            let playerController = AVPlayerViewController()
            playerController.player = player
            obj_homecontroller!.present(playerController, animated: true) {
                player.play()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    
    
}

