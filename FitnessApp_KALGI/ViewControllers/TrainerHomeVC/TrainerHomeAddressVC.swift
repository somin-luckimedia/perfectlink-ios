//
//  TrainerHomeAddressVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 30/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//


import UIKit
import Alamofire
//TrainerHomeEditAddressCell
class TrainerHomeAddressVC: UIViewController , UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, EditAddressCellDelegate, ParkAddDelegate  {
    
    @IBOutlet weak var imageView_BG: UIImageView!
    @IBOutlet weak var tableview_locations: UITableView!
    var dict_item:NSDictionary!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Edit Address"
        tableview_locations.delegate  = self as UITableViewDelegate
        tableview_locations.dataSource = self as UITableViewDataSource
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        //self.getMyBookings()
        self.addLeftMenuButtonWithImage()
        self.tableview_locations.reloadData()
        //    self.addRightMenuButtonWithImage()
    }
    
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "filter_icon"), for: UIControl.State.normal)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getMyBookings(){
        
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getData(url: ConstantFiles.accessories_listing, viewController: self,headers:headers   ,success: { (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    
                    
                }else{

                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func delete_btn_clicked(cell: TrainerHomeEditAddressCell, btn: UIButton){
     
        let obj_locationModel:LocationServiceModel = DataUtil.appdelegate().objUserInfo!.array_locations[cell.rowIndex] as! LocationServiceModel
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        
        let postParam: Parameters = ["park_id":(obj_locationModel.str_park_id)!, "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!]
        print("Login Post Parameter : \(postParam)")
        
        
        
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.remove_park, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                //obj_locationModel
                DataUtil.appdelegate().objUserInfo!.array_locations.remove(obj_locationModel)
                self.tableview_locations.reloadData()
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func parkadded_clicked(){
        self.tableview_locations.reloadData()
    }
    
    @IBAction func add_btn_clicked(_ sender: Any) {
        let obj_parkeAddressVC:ParkAddressVC = ParkAddressVC(nibName: "ParkAddressVC", bundle: nil)
        obj_parkeAddressVC.delegate = self
        let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 80
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return (DataUtil.appdelegate().objUserInfo?.array_locations.count)!
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "TrainerHomeEditAddressCell"
        
        var logincell: TrainerHomeEditAddressCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerHomeEditAddressCell
        if logincell == nil {
            tableView.register(UINib(nibName: "TrainerHomeEditAddressCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerHomeEditAddressCell)!
        }
        let obj_locationModel:LocationServiceModel = DataUtil.appdelegate().objUserInfo!.array_locations[indexPath.row] as! LocationServiceModel
        logincell?.rowIndex = indexPath.row
        logincell?.delegate = self
        logincell!.updateCellwithModal(obj_location_model:obj_locationModel)
        return logincell!
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
}
