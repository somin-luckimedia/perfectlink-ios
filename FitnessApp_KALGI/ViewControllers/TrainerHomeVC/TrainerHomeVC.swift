//
//  TrainerHomeVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 25/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//


import UIKit
import Alamofire
import LocationPicker

class TrainerHomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, TrainerLowerHomeCellDelegate, TrainerHomeCellDelegate, TrainerAddressHeaderDelegate  {
    
    @IBOutlet weak var tableview_trainerHome: UITableView!
    var obj_gymDetail:GymDetailModel! = GymDetailModel()
    var str_userType:String = "2"
    var logincell: TrainerHomeCell!
    var str_gymId:String!
    @IBOutlet var lblTrainerName: UILabel!
    let transiton = SlideInTransition()

    
    var location: Location? {
        didSet {
            print(location.flatMap({ $0.title }) ?? "No location selected")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview_trainerHome.delegate  = self as UITableViewDelegate
        tableview_trainerHome.dataSource = self as UITableViewDataSource
        tableview_trainerHome.rowHeight = 525
        tableview_trainerHome.estimatedRowHeight = UITableView.automaticDimension
        lblTrainerName.text = DataUtil.appdelegate().objUserInfo?.name
//        self.title = String(format:"Hey!, %@", (DataUtil.appdelegate().objUserInfo?.name)!)
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        //TrainerProfileVC
        //self.addLeftMenuButtonWithImage()
        //self.getMyBookings()
        
        
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.dismissControllerAnimated()
        tableview_trainerHome.reloadData()
        // self.navigationController?.navigationBar.isHidden = true
        //   self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getMyBookings(){
        let postParam: Parameters = ["gym_id":str_gymId!]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.gym_detail, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    /* let array:NSArray = arr as! NSArray
                     for item in array {
                     let dict:NSDictionary =  item as! NSDictionary
                     let obj_bookingmodal:GymListModel = GymListModel(dictUserInfo: dict)
                     self.array_mybookings.add(obj_bookingmodal)
                     }*/
                    //GymDetailModel
                    self.obj_gymDetail = GymDetailModel(dictUserInfo: arr as! NSDictionary)
                    self.tableview_trainerHome.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1{
        return 35
        }
        return 0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header:TrainerHomeSectionHeader =  UINib(nibName: "TrainerHomeSectionHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TrainerHomeSectionHeader
        header.btn_edit.isHidden = false
        header.lbl_edit.text = "Address"
        header.delegate = self
        return header
    }
    
        public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
            if indexPath.section == 0{
                return 331
            }else if indexPath.section == 1{
                return 55
            }else{
                return UITableView.automaticDimension
            }
    
            
           // return 710
    
        }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 1{
            if DataUtil.appdelegate().objUserInfo?.str_trainer_category == "2"{
                 return DataUtil.appdelegate().objUserInfo!.array_home_locations.count
            }
            
            return DataUtil.appdelegate().objUserInfo!.array_locations.count
        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        ////TrainerHomeAddressCell
        if indexPath.section == 0{
        let identifier = "TrainerHomeCell"
        logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerHomeCell
        if logincell == nil {
            tableView.register(UINib(nibName: "TrainerHomeCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerHomeCell
        }
             logincell.delegate = self
            logincell.updateCellwithModal(obj_gymDetail:DataUtil.appdelegate().objUserInfo!)
       
        return logincell
        }else if indexPath.section == 1{
            let identifier = "TrainerHomeAddressCell"
            var homeCell: TrainerHomeAddressCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerHomeAddressCell
            if homeCell == nil {
                tableView.register(UINib(nibName: "TrainerHomeAddressCell", bundle: nil), forCellReuseIdentifier: identifier)
                homeCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerHomeAddressCell
            }
            
            
            if DataUtil.appdelegate().objUserInfo?.str_trainer_category == "2"{
                
                let obj_locationModel:HomeServiceLocation = DataUtil.appdelegate().objUserInfo!.array_home_locations[indexPath.row] as! HomeServiceLocation
                homeCell.updateCellwithModalHome(obj_location_model:obj_locationModel)
            }else{
                let obj_locationModel:LocationServiceModel = DataUtil.appdelegate().objUserInfo!.array_locations[indexPath.row] as! LocationServiceModel
                homeCell.updateCellwithModal(obj_location_model:obj_locationModel)
            }
            
            
           
          //  homeCell.delegate = self
            return homeCell
        }else{
            let identifier = "TrainerHomeLowerCell"
            var homeCell: TrainerHomeLowerCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerHomeLowerCell
            if homeCell == nil {
                tableView.register(UINib(nibName: "TrainerHomeLowerCell", bundle: nil), forCellReuseIdentifier: identifier)
                homeCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerHomeLowerCell
            }
            homeCell.obj_homecontroller = self
            homeCell.updateCellwithModal(obj_gymDetail:DataUtil.appdelegate().objUserInfo!)
            homeCell.delegate = self
            return homeCell
        }
    }
    
//    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
//        return UITableViewAutomaticDimension
//    }
//    @IBAction func btnMenuTapped(_ sender: UIButton) {
//        let rootVC : SideMenuController1 = SideMenuController1(nibName: "SideMenuController", bundle: nil)
//        rootVC.didTapMenuType1 = { menuType in
//            self.transitionToNew(menuType)
//        }
//        rootVC.modalPresentationStyle = .overCurrentContext
//        rootVC.transitioningDelegate = self
//        present(rootVC, animated: true)
//    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Hello delegate")
    }
    
    func open_location_picker(){
        
        
        let locationPicker = LocationPickerViewController()
        locationPicker.location = location
        locationPicker.showCurrentLocationButton = true
        locationPicker.useCurrentLocationAsHint = true
        locationPicker.selectCurrentLocationInitially = true
        locationPicker.mapType = .standard
        locationPicker.completion = {
            print("location completed")
            self.location = $0
            let obj_phonelistvc:PhoneListVC = PhoneListVC(nibName: "PhoneListVC", bundle: nil)
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
        let nav:UINavigationController = UINavigationController(rootViewController: locationPicker)
        navigationController?.present(nav, animated: true, completion: nil)
        //navigationController?.pushViewController(locationPicker, animated: true)
        
        
    }
    
    
    func address_edit_clicked() {
        //HomeVC
        //TrainerHomeServiceAddressVC
        if DataUtil.appdelegate().objUserInfo?.str_trainer_category == "2"{
           // self.open_location_picker()
            let obj_phonelistvc:TrainerHomeServiceAddressVC = TrainerHomeServiceAddressVC(nibName: "TrainerHomeServiceAddressVC", bundle: nil)
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }else{
            let obj_phonelistvc:TrainerHomeAddressVC = TrainerHomeAddressVC(nibName: "TrainerHomeAddressVC", bundle: nil)
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
        
        
        
        
        //TrainerHomeAddressVC
    }
    
    func about_edit_clicked(cell: TrainerHomeLowerCell, btn: UIButton) {
        //TrainerHomeAboutUs
        let obj_phonelistvc:TrainerHomeAboutUs = TrainerHomeAboutUs(nibName: "TrainerHomeAboutUs", bundle: nil)
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        
    }
    
    func video_edit_clicked(cell: TrainerHomeLowerCell, btn: UIButton) {
        let obj_phonelistvc:TrainerVideoVC = TrainerVideoVC(nibName: "TrainerVideoVC", bundle: nil)
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
    
    func achievements_edit_clicked(cell: TrainerHomeLowerCell, btn: UIButton) {
        //AchievementListVC
//        let obj_phonelistvc:AchievementListVC = AchievementListVC(nibName: "AchievementListVC", bundle: nil)
//        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
    
    func membership_edit_clicked(cell: TrainerHomeLowerCell, btn: UIButton) {
        //TrainerHomeMembershiprate
        let obj_phonelistvc:TrainerHomeMembershiprate = TrainerHomeMembershiprate(nibName: "TrainerHomeMembershiprate", bundle: nil)
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
    
    func paypal_edit_clicked(cell: TrainerHomeLowerCell, btn: UIButton) {
        let obj_phonelistvc:TrainerHomePaypalVC = TrainerHomePaypalVC(nibName: "TrainerHomePaypalVC", bundle: nil)
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
   
    func profile_edit_clicked(cell: TrainerHomeCell, btn: UIButton) {
//        let obj_phonelistvc:TrainerProfileVC = TrainerProfileVC(nibName: "TrainerProfileVC", bundle: nil)
//        obj_phonelistvc.isFromHome = true
//        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
    
    func gallery_button_clicked(cell: TrainerHomeCell, btn: UIButton){
        let obj_phonelistvc:TrainerGalleryVC = TrainerGalleryVC(nibName: "TrainerGalleryVC", bundle: nil)
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
}

////MARK : Button action methods
//
//extension TrainerHomeVC
//{
//    func transitionToNew(_ menuType: MenuTypeTrainer) {
//        switch menuType {
//        case .Home:
//            self.dismissControllerAnimated()
//        case .FitnessWall:
//            let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
//            self.navigationController?.pushViewController(rootVC, animated: true)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                flagMenu = false
//                self.dismissControllerAnimated()
//            }
//
//        case .Wallet:
//            let rootVC : WalletVC = WalletVC(nibName: "WalletVC", bundle: nil)
////            self.present(rootVC, animated: true, completion: nil)
//            self.navigationController?.pushViewController(rootVC, animated: true)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//                self.dismissControllerAnimated()
//            }
//        case .Profile:
//            let rootVC : UserProfileVC = UserProfileVC(nibName: "UserProfileVC", bundle: nil)
//            self.navigationController?.pushViewController(rootVC, animated: true)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//                self.dismissControllerAnimated()
//            }
//        case .Setting:
//            let rootVC : SettingVC = SettingVC(nibName: "SettingVC", bundle: nil)
//            self.navigationController?.pushViewController(rootVC, animated: true)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//                self.dismissControllerAnimated()
//            }
//        default:
//            break
//        }
//    }
//}

extension TrainerHomeVC: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = true
        return transiton
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = false
        return transiton
    }
}
