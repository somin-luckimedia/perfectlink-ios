//
//  TrainerHomeServiceAddressVC.swift
//  FitnessApp
//
//  Created by Ashish on 10/05/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire
import LocationPicker
import CoreLocation

class TrainerHomeServiceAddressVC: UIViewController {
    
    @IBOutlet weak var textview_address: UITextView!
    var str_lattitude:String = ""
    var str_longitude:String = ""
    var str_address:String? = ""
    var str_name:String? = ""
    var obj_home_location:HomeServiceLocation? = nil
    // @IBOutlet weak var textfield_subject: UITextField!
    
    
    var location: Location? {
        didSet {
            print(location.flatMap({ $0.title }) ?? "No location selected")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textview_address.placeholder = "Address"
        self.textview_address.placeholderColor = UIColor.white
        self.title = "HOME SERVICE ADDRESS"
        
        if DataUtil.appdelegate().objUserInfo!.array_home_locations.count > 0{
            self.obj_home_location = (DataUtil.appdelegate().objUserInfo!.array_home_locations[0] as! HomeServiceLocation)
            self.str_lattitude = self.obj_home_location?.str_lat ?? "0"
            self.str_longitude = self.obj_home_location?.str_lon ?? "0"
            self.str_name = self.obj_home_location?.str_name
            self.str_address = self.obj_home_location?.str_address
            self.textview_address.text = self.str_address
        }
        
        
        self.addLeftMenuButtonWithImage()
        self.addRightMenuButtonWithImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.barTintColor = UIColor.black
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "location_pick"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.map_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func map_btn_clicked(){
        self.open_location_picker()
    }
    
    
    func open_location_picker(){
        
        
        let locationPicker = LocationPickerViewController()
        locationPicker.location = location
        locationPicker.showCurrentLocationButton = true
        locationPicker.useCurrentLocationAsHint = true
        locationPicker.selectCurrentLocationInitially = true
        locationPicker.mapType = .standard
        locationPicker.completion = {
            print("location completed")
            self.location = $0
            
            
            if let locValue: CLLocationCoordinate2D = self.location?.coordinate{
                self.str_lattitude = "\(locValue.latitude)"
                self.str_longitude = "\(locValue.longitude)"
                self.str_address = "\(self.location?.address ?? "")"
                self.str_name = "\(self.location?.name ?? "")"
                self.textview_address.text = self.str_address!
//                print("locations = \(locValue.latitude, self.str_address) \(locValue.longitude)")
                
            }
        }
        //navigationController?.present(nav, animated: true, completion: nil)
        navigationController?.navigationBar.tintColor = UIColor.black
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.pushViewController(locationPicker, animated: true)
        
        
    }
    
    
    
    @objc func back1_btn_clicked(){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submit_btn_clicked(_ sender: Any) {
        print("delegate called")
        let str_userId:String = (DataUtil.appdelegate().objUserInfo?.userId)!
        
        
        var postParam: Parameters
        var str_url:String = ""
        if self.obj_home_location != nil{
        postParam = ["location_name":self.str_name!, "address":self.str_address!,"user_id":str_userId, "lat":self.str_lattitude, "lon":self.str_longitude, "id":self.obj_home_location?.str_id! ?? "0"]
            str_url = ConstantFiles.update_trainer_home_service
        }else{
            
            postParam = ["location_name":self.str_name!, "address":self.str_address!,"user_id":str_userId, "lat":self.str_lattitude, "lon":self.str_longitude]
            str_url = ConstantFiles.add_trainer_home_service
        }
        
        if !changepasswordValidation() {
            return
        }
        //update_trainer_home_service
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        
        
        
        ServerCommunication.getDataWithPostHeader(url: str_url, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.appdelegate().objUserInfo!.array_home_locations.removeAllObjects()
                if let array:NSArray = successDict["response"] as? NSArray{
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_model:HomeServiceLocation = HomeServiceLocation.init(dictUserInfo: dict)
                        DataUtil.appdelegate().objUserInfo!.array_home_locations.add(obj_model)
                    }
                }
                self.back_btn_clicked()
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                
                
                
                
                
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func changepasswordValidation() -> Bool {
        if (textview_address.text?.isEmpty)!{
            DataUtil.alertMessage("Message field is Empty", viewController: self)
            return false
        }
        
        return true
    }
    
}
