//
//  TrainerHomeAboutUs.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 31/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire

class TrainerHomeAboutUs: UIViewController {
    
    @IBOutlet weak var textview_message: UITextView!
    // @IBOutlet weak var textfield_subject: UITextField!
    var isFromHome:Bool = false
    
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.textview_message.placeholder = "About Us"
        self.textview_message.placeholderColor = UIColor.white
      //  self.title = "About Us"
        //self.addLeftMenuButtonWithImage()
        
        
       
        
        // if isFromHome == true{
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }

    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    

    @IBAction func submit_btn_clicked(_ sender: Any) {
        print("delegate called")
        
        let str_message  : String = (textview_message.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let str_userId:String = (DataUtil.appdelegate().objUserInfo?.userId)!
        
        let postParam: Parameters = ["subject":"About Us", "message":str_message,"user_id":str_userId]
        
        if !changepasswordValidation() {
            return
        }
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.helpRequest, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                
                
                
                
                
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func changepasswordValidation() -> Bool {
        if (textview_message.text?.isEmpty)!{
            DataUtil.alertMessage("Message field is Empty", viewController: self)
            return false
        }
        
        return true
    }
    
}
