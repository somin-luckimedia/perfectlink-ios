//
//  TrainerGalleryVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 19/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//


import UIKit
import Foundation
import Photos
import AVFoundation
import AVKit
import Alamofire

class TrainerGalleryVC: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate, ImageAddDelegate {
    func Imageadded_clicked() {
        self.collectioneview_video.reloadData()
    }
    
    
    
    @IBOutlet weak var collectioneview_video: UICollectionView!
    var selectedImage:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.title = "Photo Gallery"
        self.collectioneview_video.register(UINib(nibName: "TrainerGalleryCell", bundle: nil), forCellWithReuseIdentifier: "TrainerGalleryCell")
        self.collectioneview_video.reloadData()
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//        self.addLeftMenuButtonWithImage()
    }
    
//    func addLeftMenuButtonWithImage(){
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//        let button = UIButton.init(type: .custom)
//        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
//        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
//        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
//        let barButton = UIBarButtonItem(customView: button)
//        self.navigationItem.leftBarButtonItem = barButton
//    }
    
    
    @IBAction func back_btn_clicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func add_image_btn_clicked(_ sender: UIButton) {
        
        
        let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert) in
            self.openCamera()
            
        }))
        
        alert.addAction(UIAlertAction(title:"Photo Library", style: .default, handler: { (alert) in
            self.opneGallery()
        }))
        
        alert.addAction(UIAlertAction(title:"Cancel", style: .default, handler: { (alert) in
        }))
        self.present(alert, animated: true, completion: nil)
        
        
        
        
    }
    

    
    
    func opneGallery(){
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //var selectedImage: UIImage?
        if let editedImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")]   as? UIImage {
            selectedImage = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerOriginalImage")] as? UIImage {
            selectedImage = originalImage
        }
        self.dismiss(animated: true) {
            let obj_parkeAddressVC:ImageViewerVC = ImageViewerVC(nibName: "ImageViewerVC", bundle: nil)
            let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
            obj_parkeAddressVC.delegate = self
            flagChallengeImageviewer = false
            obj_parkeAddressVC.selectedImage = self.selectedImage
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
    }
    
    
    
    
    
    
}

extension TrainerGalleryVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataUtil.appdelegate().objUserInfo?.array_gallery_image.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:TrainerGalleryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrainerGalleryCell", for: indexPath) as! TrainerGalleryCell
        let obj_videoModel:GalleryImageModel = DataUtil.appdelegate().objUserInfo?.array_gallery_image[indexPath.row] as! GalleryImageModel
        cell.updateCell(model: obj_videoModel)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenwidth:CGFloat = UIScreen.main.bounds.size.width/2
        return CGSize(width: screenwidth, height: screenwidth)
    }
    
    
    //MealsVC
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let obj_videoModel:GalleryImageModel = DataUtil.appdelegate().objUserInfo?.array_gallery_image[indexPath.row] as! GalleryImageModel
        let obj_parkeAddressVC:ImageViewerVC = ImageViewerVC(nibName: "ImageViewerVC", bundle: nil)
        let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
        obj_parkeAddressVC.delegate = self
        obj_parkeAddressVC.obj_locationModel = obj_videoModel
        obj_parkeAddressVC.isViewer = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    
    
}

