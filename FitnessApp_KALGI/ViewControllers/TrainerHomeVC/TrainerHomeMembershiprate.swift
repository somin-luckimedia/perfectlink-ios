//
//  TrainerHomeMembershiprate.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 31/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire

class TrainerHomeMembershiprate: UIViewController {

    @IBOutlet weak var textfield_single: UITextField!
    @IBOutlet weak var textfield_gruop: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "MEMBERSHIP RATE"
        self.textfield_single.text = DataUtil.appdelegate().objUserInfo?.str_single_rate
        self.textfield_gruop.text = DataUtil.appdelegate().objUserInfo?.str_gruop_rate
        self.addLeftMenuButtonWithImage()
    }
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @IBAction func submit_btn_clicked(_ sender: Any) {
        
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        
        let postParam: Parameters = ["single_membership_rate":textfield_single.text!, "user_id":(DataUtil.appdelegate().objUserInfo?.userId)!, "group_membership_rate":textfield_gruop.text!]
        print("Login Post Parameter : \(postParam)")
        if !rateValidation() {
            return
        }
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.updat_membershiprate, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.appdelegate().objUserInfo?.str_single_rate = self.textfield_single.text!
                DataUtil.appdelegate().objUserInfo?.str_gruop_rate = self.textfield_gruop.text!
                
                let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                  //  self.delegate?.parkadded_clicked()
                    self.back_btn_clicked()
                }))
                self.present(alert, animated: true, completion: nil)
                
                // DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                //self.back_btn_clicked()
                
                /*   if let arr = successDict.value(forKey:"response"){
                 
                 
                 }else{
                 
                 }*/
            }else{
                
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                self.back_btn_clicked()
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
        
        
    }
    
    
    func rateValidation() -> Bool {
        if (textfield_single.text?.isEmpty)!{
            DataUtil.alertMessage("Single rate is Empty", viewController: self)
            return false
        }
        if (textfield_gruop.text?.isEmpty)!{
            DataUtil.alertMessage("Group rate is Empty", viewController: self)
            return false
        }
        
        return true
    }
    
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }

}
