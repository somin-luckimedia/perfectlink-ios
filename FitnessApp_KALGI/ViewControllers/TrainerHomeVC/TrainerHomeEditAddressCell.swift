//
//  TrainerHomeEditAddressCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 12/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit


protocol EditAddressCellDelegate:class {
    func delete_btn_clicked(cell: TrainerHomeEditAddressCell, btn: UIButton)
}
class TrainerHomeEditAddressCell: UITableViewCell {
    
    @IBOutlet weak var lbl_parkname: UILabel!
    @IBOutlet weak var lbl_parkAddress: UILabel!
    var rowIndex:Int = 0
    weak var delegate: EditAddressCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_location_model:LocationServiceModel){
        lbl_parkname.text = obj_location_model.str_park_name
        lbl_parkAddress.text = obj_location_model.str_park_address
    }
    
    @IBAction func delete_btn_clicked(_ sender: UIButton) {
        self.delegate?.delete_btn_clicked(cell: self, btn: sender)
    }
    
    
    
    
}
