//
//  TrainerVideoVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 29/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit
import Foundation
import Photos
import AVFoundation
import AVKit
import Alamofire

class TrainerVideoVC: UIViewController, ImageVideoPickerDelegate ,UIImagePickerControllerDelegate,UINavigationControllerDelegate, VideoAddDelegate, TrainerVideoCellDelegate {
    
    
   
    func videoadded_clicked() {
        self.collectioneview_video.reloadData()
    }
    
    
    
    @IBOutlet weak var collectioneview_video: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Video Tips"
        self.collectioneview_video.register(UINib(nibName: "TrainerVideoCell", bundle: nil), forCellWithReuseIdentifier: "TrainerVideoCell")
        self.collectioneview_video.reloadData()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.addLeftMenuButtonWithImage()
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func add_video_btn_clicked(_ sender: UIButton) {
      /* let storyboard = UIStoryboard(name: "MediaPicker", bundle: nil)
        let controller:ImageVideoPicker = storyboard.instantiateViewController(withIdentifier: "ImageVideoPicker") as! ImageVideoPicker
        controller.delegate = self
        let nav:UINavigationController = UINavigationController(rootViewController: controller)
        self.navigationController?.present(nav, animated: true, completion: nil)*/
        
        self.opneGallery()
        
        
    }
    
    func opneGallery(){
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.mediaTypes = ["public.movie"]
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
  /*  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let videoURL:URL = (info[UIImagePickerControllerMediaURL] as? URL)!
        print("videoURL:\(String(describing: videoURL))")
        self.dismiss(animated: true, completion: nil)
    }*/
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
        
    {
        
        
        if let videoUrl = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.mediaURL.rawValue)] as? URL
            
        {
            //self.dismiss(animated: true, completion: nil)
             //DataUtil.loadIndicatorView()
            self.dismiss(animated: true) {

                let obj_parkeAddressVC:VideoViewerVC = VideoViewerVC(nibName: "VideoViewerVC", bundle: nil)
                let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
                obj_parkeAddressVC.video_url = videoUrl
                obj_parkeAddressVC.delegate = self
                self.navigationController?.present(nav, animated: true, completion: nil)
                
                
            }
            /*
            
            
            
         //   self.uploadMedia(url_video: videoUrl)
           
            let postParam: Parameters = ["title":"Test iOS12", "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!]
            Alamofire.upload(multipartFormData:{ multipartFormData in
                multipartFormData.append(videoUrl, withName: "video")
                for (key, value) in postParam {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
                
            },
                             usingThreshold:UInt64.init(),
                             to:"http://perfectlinkfitness.com/api/trainer_add_video",
                             method:.post,
                             headers:[
                                "Authorization": String(format: "Bearer %@",  (DataUtil.appdelegate().objUserInfo?.str_accessToken)!),
                                "Accept": "application/json"
                ],
                             encodingCompletion: { encodingResult in
                                switch encodingResult {
                                case .success(let upload, _, _):
                                     DataUtil.removeIndicatorView()
                                    upload.responseJSON { response in
                                        guard let successDict = response.result.value as? NSDictionary else
                                        {
                                            return
                                        }
                                        if let status = successDict["STATUS"] as? String{
                                            if status == "true"{
                                                if (successDict.value(forKey: "STATUS") as? String) == "true"{
                                                    
                                                    if let array:NSArray = successDict["response"] as? NSArray{
                                                        DataUtil.appdelegate().objUserInfo?.array_videos.removeAllObjects()
                                                        for item in array {
                                                            let dict:NSDictionary =  item as! NSDictionary
                                                            let obj_model:VideosModel = VideosModel.init(dictUserInfo: dict)
                                                            DataUtil.appdelegate().objUserInfo?.array_videos.add(obj_model)
                                                        }
                                                        self.collectioneview_video.reloadData()
                                                    }
                                                    
                                                    
                                                    
                                                    let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertControllerStyle.alert)
                                                    
                                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                                                        //self.delegate?.parkadded_clicked()
                                                        self.back_btn_clicked()
                                                    }))
                                                    self.present(alert, animated: true, completion: nil)
                                                    
                                                    // DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                                                    //self.back_btn_clicked()
                                                    
                                                    /*   if let arr = successDict.value(forKey:"response"){
                                                     
                                                     
                                                     }else{
                                                     
                                                     }*/
                                                }else{
                                                    
                                                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                                                    self.back_btn_clicked()
                                                }
                                                
                                            }
                                        }
                                        debugPrint(response)
                                    }
                                case .failure(let encodingError):
                                     DataUtil.removeIndicatorView()
                                    print(encodingError)
                                }
            })
            
            */

        }
        
        
        
    }
    
    func onCancel() {
        
    }
    
    func uploadMedia(url_video:URL){
        
        
        guard let url = URL(string: "http://perfectlinkfitness.com/api/trainer_add_video") else {
            return
        }
        var request = URLRequest(url: url)
        let boundary = "------------------------your_boundary"
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        request.httpMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue(String(format: "Bearer %@", str_accessToken), forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        var movieData: Data?
        do {
            movieData = try Data(contentsOf: url_video, options: Data.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        var body = Data()
        
        // change file name whatever you want
        let filename = "video.mov"
        let mimetype = "video/mov"
        //"user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"video\"; user_id=\"DPYZC68VR5WS\";title=\"test ios\";filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(movieData!)
        
        let dict = ["title":"Test iOS", "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: [])
        body.append(jsonData!)
        
        
        
        
        
        request.httpBody = body
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, reponse: URLResponse?, error: Error?) in
            if let `error` = error {
                print(error)
                return
            }
            if let `data` = data {
                print(String(data: data, encoding: String.Encoding.utf8))
            }
        }
        
        task.resume()
    }
    
    func onDoneSelection(assets: [PHAsset]) {
        if assets.count > 0{
            ImageVideoPicker.getDataFrom(asset: assets[0]) { data in
                let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
                let headers: HTTPHeaders = [
                        "Authorization": String(format: "Bearer %@", str_accessToken),
                        "Accept": "application/json"
                    ]
                    
                    let postParam: Parameters = ["title":"Test iOS", "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!]
                    print("Login Post Parameter : \(postParam)")

                ServerCommunication.getDataWithPostVideo(url: ConstantFiles.add_video, image: data!  ,parameter: postParam,headers:headers, viewController: self, isImage:true,success: { (successDict) in
                        print(successDict)
                    if (successDict.value(forKey: "STATUS") as? String) == "true"{
                        
                        
                        
                    }else{
                            
                            DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                            self.back_btn_clicked()
                        }
                    }) { (dictFailure) in
                        if (dictFailure.value(forKey: "message") as? String) != nil {
                            DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
                        }
                    }
                    
                    
                    
            }
            
        }
    }
    
    func delete_btn_clicked(cell: TrainerVideoCell, btn: UIButton) {
        
        let obj_locationModel:VideosModel = DataUtil.appdelegate().objUserInfo!.array_videos[cell.rowIndex] as! VideosModel
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]

        
        let postParam: Parameters = ["id":(obj_locationModel.str_id)!, "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!]
        print("Login Post Parameter : \(postParam)")
        
        
        
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.remove_video, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                //obj_locationModel
                DataUtil.appdelegate().objUserInfo!.array_videos.remove(obj_locationModel)
                self.collectioneview_video.reloadData()
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
        
        
    }
    
    
    
}

extension TrainerVideoVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataUtil.appdelegate().objUserInfo?.array_videos.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:TrainerVideoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrainerVideoCell", for: indexPath) as! TrainerVideoCell
        let obj_videoModel:VideosModel = DataUtil.appdelegate().objUserInfo?.array_videos[indexPath.row] as! VideosModel
        cell.lbl_title.text = obj_videoModel.str_title
        cell.delegate = self
        cell.rowIndex = indexPath.row
        cell.updateCell(model: obj_videoModel)
        //cell.lbl_quantity.text = array_quantity[indexPath.row]
        //cell.imageview_icon.image = UIImage(named: array_icons[indexPath.row])
        //    cell.imageview_phone.sd_setImage(with: url, placeholderImage: UIImage(named: "phone"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenwidth:CGFloat = UIScreen.main.bounds.size.width/2
        return CGSize(width: screenwidth, height: 150)
    }
    
    
    //MealsVC
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let obj_videoModel:VideosModel = DataUtil.appdelegate().objUserInfo?.array_videos[indexPath.row] as! VideosModel
        let fileUrl = URL(string: obj_videoModel.str_video)
        let player = AVPlayer(url: fileUrl!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.present(playerController, animated: true) {
            player.play()
        }
    }
    
    
    
}
