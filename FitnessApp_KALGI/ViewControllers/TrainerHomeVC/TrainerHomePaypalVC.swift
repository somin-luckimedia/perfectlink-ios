//
//  TrainerHomePaypalVC.swift
//  FitnessApp
//
//  Created by Ashish on 10/05/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire

class TrainerHomePaypalVC: UIViewController {
    
    @IBOutlet weak var textfield_paypal: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "ACCOUNT DETAIL"
        self.textfield_paypal.text = DataUtil.appdelegate().objUserInfo?.str_paypal_id
        self.addLeftMenuButtonWithImage()
    }
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @IBAction func submit_btn_clicked(_ sender: Any) {
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]

        
        let postParam: Parameters = ["paypal_id":textfield_paypal.text!, "user_id":(DataUtil.appdelegate().objUserInfo?.userId)!]
        print("Login Post Parameter : \(postParam)")
        if !rateValidation() {
            return
        }
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.update_trainer_account_details, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.appdelegate().objUserInfo?.str_paypal_id = self.textfield_paypal.text!
                let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    self.back_btn_clicked()
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                self.back_btn_clicked()
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
        
        
    }
    
    
    func rateValidation() -> Bool {
        if (textfield_paypal.text?.isEmpty)!{
            DataUtil.alertMessage("Single rate is Empty", viewController: self)
            return false
        }
        let isValidEmail = ValidateUtils.isValidEmail(textfield_paypal.text!)
        if isValidEmail == false {
            DataUtil.alertMessage("Please provide the valid Email.", viewController: self)
            return false
        }
        return true
    }
    
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
}

