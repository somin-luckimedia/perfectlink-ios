//
//  TrainerVideoCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 29/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit
import AVFoundation
protocol TrainerVideoCellDelegate:class {
    func delete_btn_clicked(cell: TrainerVideoCell, btn: UIButton)
}


class TrainerVideoCell: UICollectionViewCell {
    
    @IBOutlet weak var view_inner: UIView!
    @IBOutlet weak var lbl_title: UILabel!
    weak var delegate: TrainerVideoCellDelegate?
    var rowIndex:Int = 0
    @IBOutlet weak var imageview_thumb: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //view_inner.layer.shadowColor = UIColor.black.cgColor
        //view_inner.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        //view_inner.layer.shadowOpacity = 0.4
        //view_inner.layer.shadowRadius = 6.0
        //layer.masksToBounds = true
        // view_inner.dropShadow()
        // Initialization code
    }
    
    @IBAction func delete_btn_clicked(_ sender: UIButton) {
self.delegate?.delete_btn_clicked(cell: self, btn: sender)
    }
    
    
    func updateCell(model:VideosModel){
        let url = URL(string: model.str_video)
       
//        if let thumbnailImage = getThumbnailImage(forUrl: url!) {
//            imageview_thumb.image = thumbnailImage
//
//    }
        
        self.getThumbnailImageFromVideoUrl(url: url!) { (thumbImage) in
            self.imageview_thumb.image = thumbImage
        }
        
        
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
    
    
    
}
