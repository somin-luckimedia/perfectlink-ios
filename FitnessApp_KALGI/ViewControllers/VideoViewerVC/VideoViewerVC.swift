//
//  VideoViewerVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 16/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import Photos
import AVKit


protocol VideoAddDelegate:class {
    func videoadded_clicked()
}

class VideoViewerVC: UIViewController {

    @IBOutlet weak var textfield_title: UITextField!
    @IBOutlet weak var imageview_thumb: UIImageView!
    var video_url:URL! = nil
    weak var delegate: VideoAddDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLeftMenuButtonWithImage()
        self.addRightMenuButtonWithImage()
        self.getThumbnailImageFromVideoUrl(url: video_url!) { (thumbImage) in
            self.imageview_thumb.image = thumbImage
        }
        // Do any additional setup after loading the view.
    }
    
     @IBAction func play_btn_clicked(_ sender: Any) {
        let player = AVPlayer(url: self.video_url!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.present(playerController, animated: true) {
            player.play()
        }
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "close"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        //button.setImage(UIImage(named: "filter_icon"), for: UIControlState.normal)
        button.setTitle("UPLOAD", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(self.upload_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func upload_btn_clicked(){
        
        
        //   self.uploadMedia(url_video: videoUrl)
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
//        let headers: HTTPHeaders = [
//            "Authorization": String(format: "Bearer %@",str_accessToken),
//            "Accept": "application/json"
//        ]

        DataUtil.loadIndicatorView()
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""

        let postParam: Parameters = ["title":self.textfield_title.text, "user_id":str_userid]
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(self.video_url, withName: "video")
            for (key, value) in postParam {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
            
        },
                         usingThreshold:UInt64.init(),
                         to:"http://admin.perfectlinkfitness.com/api/trainer_add_video",
                         method:.post,
                         headers:[
                            "Authorization": String(format: "Bearer %@",str_accessToken),
                            "Accept": "application/json"
            ],
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    guard let successDict = response.result.value as? NSDictionary else
                                    {
                                        DataUtil.removeIndicatorView()
                                        return
                                    }
                                    DataUtil.removeIndicatorView()
                                    if let status = successDict["STATUS"] as? String{
                                        if status == "true"{
                                            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                                                
                                                if let array:NSArray = successDict["response"] as? NSArray{
                                                    DataUtil.appdelegate().objUserInfo?.array_videos.removeAllObjects()
                                                    for item in array {
                                                        let dict:NSDictionary =  item as! NSDictionary
                                                        let obj_model:VideosModel = VideosModel.init(dictUserInfo: dict)
                                                        DataUtil.appdelegate().objUserInfo?.array_videos.add(obj_model)
                                                    }
                                                    self.delegate?.videoadded_clicked()
                                                    self.dismiss(animated: true, completion: nil)
                                                }
                                                
                                                
                                                
                                                let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                                                
                                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                                                    //self.delegate?.parkadded_clicked()
                                                    self.back_btn_clicked()
                                                }))
                                              //  self.present(alert, animated: true, completion: nil)
                                                
                                                // DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                                                //self.back_btn_clicked()
                                                
                                                /*   if let arr = successDict.value(forKey:"response"){
                                                 
                                                 
                                                 }else{
                                                 
                                                 }*/
                                            }else{
                                                
                                                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                                                self.back_btn_clicked()
                                            }
                                            
                                        }
                                    }
                                    debugPrint(response)
                                }
                            case .failure(let encodingError):
                                DataUtil.removeIndicatorView()
                                print(encodingError)
                            }
        })
        
        
    }


    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }

}
