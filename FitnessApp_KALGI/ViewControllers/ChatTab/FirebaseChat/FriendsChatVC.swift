//
//  FriendsChatVC.swift
//  FirebaseChat
//
//  Created by Ashish tripathi on 26/04/20.
//  Copyright © 2020 Octatrades Technologies. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import IQKeyboardManagerSwift



class FriendsChatVC: UIViewController,UITableViewDelegate,UITableViewDataSource, HPGrowingTextViewDelegate {
    
    

    let db = Firestore.firestore()
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!

     var textView: HPGrowingTextView!
    
    
    @IBOutlet weak var growingTextView: UIView!
    var arrChat = [[String:Any]]()
    var myUserID = ""
    var myFriendID = ""
    var NodeID = ""
    var friendName = ""
    var FriendDeviceToken = ""
    var myUserName = ""
    var myProfileImage = ""
    var myDevicetoken = ""
    @IBOutlet weak var constraint_view: NSLayoutConstraint!

    /*
     _textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(LEFT_BUTTON_SIZE,
     5,
     size.width - LEFT_BUTTON_SIZE - RIGHT_BUTTON_SIZE,
     size.height)];
     _textView.isScrollable = NO;
     _textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
     
     _textView.minNumberOfLines = 1;
     _textView.maxNumberOfLines = 6;
     // you can also set the maximum height in points with maxHeight
     // textView.maxHeight = 200.0f;
     _textView.returnKeyType = UIReturnKeyGo; //just as an example
     _textView.font = [UIFont systemFontOfSize:15.0f];
     _textView.delegate = self;
     _textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
     _textView.backgroundColor = [UIColor whiteColor];
     _textView.placeholder = _placeholder;
     
     //textView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
     _textView.keyboardType = UIKeyboardTypeDefault;
     _textView.returnKeyType = UIReturnKeyDefault;
     _textView.enablesReturnKeyAutomatically = YES;
     //textView.scrollIndicatorInsets = UIEdgeInsetsMake(0.0, -1.0, 0.0, 1.0);
     //textView.textContainerInset = UIEdgeInsetsMake(8.0, 4.0, 8.0, 0.0);
     _textView.layer.cornerRadius = 5.0;
     _textView.layer.borderWidth = 0.5;
     _textView.layer.borderColor =  [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:205.0/255.0 alpha:1.0].CGColor;
     
     _textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
     
     */
    
    
    
    @IBOutlet weak var txtmessage: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //self.tblView.estimatedRowHeight = 70.0
        //self.tblView.rowHeight = UITableView.automaticDimension
        self.title = self.friendName
        self.addLeftMenuButtonWithImage()
        self.LoadUserdetails()
        self.LoadPreviousChatData()
        lblTitle.text = self.friendName
        self.textView = HPGrowingTextView(frame: CGRect(x: 5, y: 5, width: self.growingTextView.bounds.size.width-10, height: self.growingTextView.bounds.size.height-10) )
        self.growingTextView.addSubview( self.textView)
        self.textView.delegate = self
        self.textView.minNumberOfLines = 1
        self.textView.maxNumberOfLines = 3
        self.navigationController?.navigationBar.isHidden = true
        self.textView.isScrollable = false;
        self.textView.contentInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);
        
        
        
        self.textView.font = UIFont(name: "Helvetica", size: 15.0)
        self.textView.internalTextView.scrollIndicatorInsets = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0);
        self.textView.backgroundColor = UIColor.clear
        self.textView.placeholder = ""
        self.textView.textColor = UIColor.white
        //textView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        self.textView.keyboardType = .default
        self.textView.returnKeyType = .default
        self.textView.enablesReturnKeyAutomatically = true
        //textView.scrollIndicatorInsets = UIEdgeInsetsMake(0.0, -1.0, 0.0, 1.0);
        //textView.textContainerInset = UIEdgeInsetsMake(8.0, 4.0, 8.0, 0.0);
       // self.textView.layer.cornerRadius = 5.0;
        //self.textView.layer.borderWidth = 0.5;
        //self.textView.layer.borderColor =  [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:205.0/255.0 alpha:1.0].CGColor;
        
        self.textView.autoresizingMask = .flexibleWidth;
        
    }

    
    func growingTextView(_ growingTextView: HPGrowingTextView!, willChangeHeight height: Float) {
        
        let diff:Float = Float(growingTextView.frame.size.height) - height
        
        var r:CGRect = self.growingTextView.frame
        r.size.height =  r.size.height - CGFloat(diff)
        r.origin.y =  r.origin.y + CGFloat(diff)
        self.constraint_view.constant = self.constraint_view.constant - CGFloat(diff)
        
        //        float diff = (growingTextView.frame.size.height - height);
        //        CGRect r = self.frame;
        //        r.size.height -= diff;
        //        r.origin.y += diff;
        //        self.frame = r;
        
        
    }
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func btnNotificationTapped(_ sender: UIButton) {
        let obj_NotificationVC:NotificationVC = NotificationVC(nibName: "NotificationVC", bundle: nil)
        self.navigationController?.pushViewController(obj_NotificationVC, animated: true)

    }
    
    func growingTextViewDidChange(_ growingTextView: HPGrowingTextView!) {
        
        //let diff:Float = growingTextView.frame.size.height - height
//        float diff = (growingTextView.frame.size.height - height);
//        CGRect r = self.frame;
//        r.size.height -= diff;
//        r.origin.y += diff;
//        self.frame = r;
    }
    
    func growingTextViewDidEndEditing(_ growingTextView: HPGrowingTextView!) {
        
    }
    
    func growingTextViewDidChangeSelection(_ growingTextView: HPGrowingTextView!) {
        
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func LoadUserdetails() {
        self.db.collection("users").document(self.myUserID).getDocument { (docSnap, error) in
            if (error != nil) {
            }else{
                if docSnap?.exists ?? false{
                    let value = docSnap?.data()
                    let name = value?["name"] as? String ?? ""
                    let image = value?["profileImage"] as? String ?? ""
                    let token = value?["registrationTokens"] as? String ?? ""
                    self.myUserName = name
                    self.myDevicetoken = token
                    self.myProfileImage = image
                }
                
            }
        }
        
        
        self.db.collection("users").document(self.myFriendID).getDocument { (docSnap, error) in
            if (error != nil) {
            }else{
                if docSnap?.exists ?? false{
                    let value = docSnap?.data()
                    let devicetoken = value?["registrationTokens"] as? String ?? ""
                    let name = value?["name"] as? String ?? ""
                    self.friendName = name
                    self.FriendDeviceToken = devicetoken
                }
                
            }
        }
        
        
    }
    
    
    func LoadPreviousChatData()  {
        //        self.myUserID = "VRHLT81XDW8U"
        //        self.myFriendID = "AEFFB9PAH1ZC"
        //        self.NodeID = ""
        
        
        
        
        
        if NodeID != ""{
            self.db.collection("chatChannels").document(self.NodeID).collection("messages").order(by: "time").addSnapshotListener { (docSnap, error) in
                if error != nil{
                    
                }else{
                    self.arrChat.removeAll()
                    if !(docSnap?.isEmpty ?? false){
                        let documents = docSnap?.documents ?? [QueryDocumentSnapshot]()
                        for doc in documents{
                            let value = doc.data()
                            self.arrChat.append(value)
                        }
                        self.tblView.reloadData()
                        self.scrollToBottom()
                    }
                }
            }
        }else{
            self.db.collection("chatChannels").whereField("userIds",arrayContains: self.myUserID).getDocuments { (documentSnap, error) in
                if error != nil{
                    
                }else{
                    if !(documentSnap?.isEmpty ?? false){
                        
                        let documents = documentSnap?.documents ?? [QueryDocumentSnapshot]()
                        
                        for doctemp in documents{
                            let value = doctemp.data()
                            let temapp = value["userIds"] as? [String] ?? [String]()
                            if temapp.contains(self.myFriendID){
                                self.NodeID = doctemp.documentID
                                self.db.collection("chatChannels").document(self.NodeID).collection("messages").order(by: "time").addSnapshotListener { (docSnap, error) in
                                    if error != nil{
                                        
                                    }else{
                                        self.arrChat.removeAll()
                                        if !(docSnap?.isEmpty ?? false){
                                            let documents = docSnap?.documents ?? [QueryDocumentSnapshot]()
                                            for doc in documents{
                                                let value = doc.data()
                                                self.arrChat.append(value)
                                            }
                                            self.tblView.reloadData()
                                            self.scrollToBottom()
                                        }
                                    }
                                }
                            }
                            
                        }
                        
                    }else{
                        self.NodeID = ""
                    }
                    
                    
                }
            }
        }
    }
    
    

    @IBAction func actbtn_sendbtnClicked(_ sender: Any) {
        
        if textView.text == "" {
            
        }else{
            let userinfo = ["name":self.myUserName,"profileImage": self.myProfileImage,"registrationTokens":self.myDevicetoken,"userId": self.myUserID]
            
            let dict = ["text":textView.text ?? "","time":FieldValue.serverTimestamp(),"type":"TEXT","senderId":self.myUserID,"isSeen":false] as [String : Any]
            
            let Notificationdict = ["user_info":userinfo,"body":textView.text ?? "","type":"TEXT","sender_id":self.myUserID,"flag":"111","tittle": self.myUserName] as [String : Any]
            if self.NodeID != ""{
                
                self.db.collection("chatChannels").document(self.NodeID).collection("messages").addDocument(data: dict)
                self.sendPushOfMessage(userdict: Notificationdict, FriendName: self.friendName, Friendtoken: self.FriendDeviceToken, Message: self.textView.text ?? "")
                
            }else{
                self.db.collection("chatChannels").whereField("userIds",arrayContains: self.myUserID).getDocuments { (documentSnap, error) in
                    if error != nil{
                        
                    }else{
                        if !(documentSnap?.isEmpty ?? false){
                            let documents = documentSnap?.documents ?? [QueryDocumentSnapshot]()
                            for doctemp in documents{
                                let value = doctemp.data()
                                let temapp = value["userIds"] as? [String] ?? [String]()
                                if temapp.contains(self.myFriendID){
                                    self.NodeID = doctemp.documentID
                                }
                            }
                            if self.NodeID != ""{
                                self.db.collection("chatChannels").document(self.NodeID).collection("messages").addDocument(data: dict)
                                self.sendPushOfMessage(userdict: Notificationdict, FriendName: self.friendName, Friendtoken: self.FriendDeviceToken, Message: self.textView.text ?? "")
                                
                            }else{
                                let documentID = self.db.collection("chatChannels").document().documentID
                                self.db.collection("chatChannels").document(documentID).setData( ["userIds":[self.myUserID,self.myFriendID]])
                                self.NodeID = documentID
                                self.db.collection("chatChannels").document(self.NodeID).collection("messages").addDocument(data: dict)
                                self.sendPushOfMessage(userdict: Notificationdict, FriendName: self.friendName, Friendtoken: self.FriendDeviceToken, Message: self.textView.text ?? "")
                                self.LoadPreviousChatData()
                            }
                        }else{
                            let documentID = self.db.collection("chatChannels").document().documentID
                            self.db.collection("chatChannels").document(documentID).setData( ["userIds":[self.myUserID,self.myFriendID]])
                            self.NodeID = documentID
                            self.db.collection("chatChannels").document(self.NodeID).collection("messages").addDocument(data: dict)
                            self.sendPushOfMessage(userdict: Notificationdict, FriendName: self.friendName, Friendtoken: self.FriendDeviceToken, Message: self.textView.text ?? "")
                            self.LoadPreviousChatData()
                        }
                    }
                }
            }
        }
         
    }
    
    func sendPushOfMessage(userdict:[String: Any],FriendName: String, Friendtoken:String,Message: String) {
        textView.text = ""
        SendNotificationManager.share.sendPushNotification(userToken: Friendtoken, userinfo: userdict, type: "TEXT", message: Message, tittle: FriendName)
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrChat.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
let dictmessage = self.arrChat[indexPath.row]
let str_message:String = dictmessage["text"] as? String ?? ""
let font = UIFont(name: "Helvetica", size: 12.0)
let height = heightForView(text:str_message, font: font!, width: UIScreen.main.bounds.width-168)
return height + 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dictmessage = self.arrChat[indexPath.row]
        let senderID =  dictmessage["senderId"] as? String ?? ""
         if senderID == self.myUserID{
            
            let identifier = "MyChatCell"
            var logincell: MyChatCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? MyChatCell
            if logincell == nil {
                tableView.register(UINib(nibName: "MyChatCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? MyChatCell)!
            }
            
           
                logincell!.lbl_message.text = dictmessage["text"] as? String ?? ""
                //cell.imageView?.image = UIImage(named: "")
                let time = dictmessage["time"] as? Timestamp ?? Timestamp()
                let sentDate = self.getdate(date: time.dateValue())
                logincell!.lbl_time.text = sentDate
                logincell!.backgroundColor = UIColor.clear
                logincell!.selectionStyle = .none
                
                return logincell!
            
            
            
        }else{
            
            let identifier = "FriendCell"
            var logincell: FriendCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? FriendCell
            if logincell == nil {
                tableView.register(UINib(nibName: "FriendCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? FriendCell)!
            }
            
                logincell!.lbl_message.text = dictmessage["text"] as? String ?? ""
                //cell.imageView?.image = UIImage(named: "")
                let time = dictmessage["time"] as? Timestamp ?? Timestamp()
                let sentDate = self.getdate(date: time.dateValue())
                logincell!.lbl_time.text = sentDate
                logincell!.backgroundColor = UIColor.clear
                logincell!.selectionStyle = .none
                
                return logincell!
            
            
        }
    }
    func getdate(date: Date)-> String{
        let date = date
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = .current //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.arrChat.count-1, section: 0)
            self.tblView.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }

}
