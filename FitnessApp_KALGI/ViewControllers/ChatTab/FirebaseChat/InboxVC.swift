//
//  InboxVC.swift
//  FirebaseChat
//
//  Created by Ashish tripathi on 26/04/20.
//  Copyright © 2020 Octatrades Technologies. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore



class InboxVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tblInbox: UITableView!
    var arrChatList = [[String: Any]]()
    var userID = ""
    let dbMain = Firestore.firestore()
    let transiton = SlideInTransition()
    var topView: UIView?
    @IBOutlet var tblOnline: UITableView!
    var arrOnlineFriends : NSMutableArray = []
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //self.tblInbox.estimatedRowHeight = 70.0
        //self.tblInbox.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        //        self.title = "Chat"
        //        self.LoadRecentFriendChat()
        self.navigationController?.navigationBar.isHidden = true
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
                //                if snapshot.childrenCount == 1 {
                //                    self.constWidthTbl.constant = 0
                ////                    self.constLeadingColl.constant = 20
                //                    self.lblOnline.isHidden = true
                //                }
                //                else {
                //                    self.constWidthTbl.constant = 60
                ////                    self.constLeadingColl.constant = 0
                //                    self.lblOnline.isHidden = false
                //                }
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }
                
                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
        }
        
        //        if userID == ""{
        //            return
        //        }else{
        //        self.LoadRecentFriendChat()
        //        }
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(dismissChat),
                                       name: NSNotification.Name(rawValue: "DismissChat"),
                                       object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.LoadRecentFriendChat()
    }
    
    @objc func dismissChat(){
        //        flagCChat = false
        self.dismissControllerAnimated()
    }
    
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        flagCChat = true
        let rootVC : SideMenuController1 = SideMenuController1(nibName: "SideMenuController", bundle: nil)
        if DataUtil.appdelegate().objUserInfo?.str_userType == "2" {
            rootVC.didTapMenuType = { menuType in
                self.transitionToNew(menuType)
            }
        }
        else {
            rootVC.didTapMenuType1 = {menuTypeTrainer in
                self.transitionTrainer(menuTypeTrainer)
            }
        }
        rootVC.modalPresentationStyle = .overCurrentContext
        rootVC.transitioningDelegate = self
        present(rootVC, animated: true)
    }
    
    func transitionToNew(_ menuType: MenuType) {
        switch menuType {
        case .FitnessWall:
            let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Challenges:
            let rootVC : OrderHistoryVC = OrderHistoryVC(nibName: "OrderHistoryVC", bundle: nil)
            self.present(rootVC, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.dismissControllerAnimated()
                
            }
        case .Chat:
            let rootVC : FriendListVC = FriendListVC(nibName: "FriendListVC", bundle: nil)
            //            self.present(rootVC, animated: true, completion: nil)
            //            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            //                self.dismissControllerAnimated()
            //            }
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.dismissControllerAnimated()
            }
        default:
            break
        }
    }
    
    func transitionTrainer(_ menuType : MenuTypeTrainer) {
        switch menuType {
        case .FitnessWall:
            let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
            
        case .MyOrders:
            let rootVC : OrderHistoryVC = OrderHistoryVC(nibName: "OrderHistoryVC", bundle: nil)
            self.present(rootVC, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Friends:
            let rootVC : FriendListVC = FriendListVC(nibName: "FriendListVC", bundle: nil)
            //            self.present(rootVC, animated: true, completion: nil)
            //            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            //                self.dismissControllerAnimated()
            //            }
            // self.present(rootVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Achievements:
            let rootVC : AchievementsVC = AchievementsVC(nibName: "AchievementsVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .MyTips:
            let rootVC : FreeTipsVCViewController = FreeTipsVCViewController(nibName: "FreeTipsVCViewController", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
            
        case .Wallet:
            let rootVC : WalletNewVC = WalletNewVC(nibName: "WalletNewVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
            
        default:
            break
        }
    }
    
    func LoadRecentFriendChat() {
        DataUtil.loadIndicatorView()
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        self.dbMain.collection("chatChannels").whereField("userIds",arrayContains:str_userid).getDocuments { (documentSnap, error) in
            DataUtil.removeIndicatorView()
            if error != nil{
                
            }else{
                
                if !(documentSnap?.isEmpty ?? false){
                    let document = documentSnap?.documents ?? [QueryDocumentSnapshot]()
                    self.arrChatList = []
                    for value in document{
                        let data = value.data()
                        let arrID = data["userIds"] as? [String] ?? [String]()
                        let docID = value.documentID
                        //                        self.arrChatList = []
                        if arrID[0] == str_userid{
                            let dict = ["documentID": docID, "friendID":arrID[1] ]
                            self.arrChatList.append(dict)
                        }else{
                            let dict = ["documentID": docID, "friendID":arrID[0] ]
                            self.arrChatList.append(dict)
                        }
                    }
                    self.tblInbox.reloadData()
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblOnline {
            return arrOnlineFriends.count
        }
        else {
            return self.arrChatList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblOnline {
            let identifier = "onlineTVCTableViewCell"
            var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
            
            if logincell == nil {
                tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
            }
            logincell?.selectionStyle = .none
            //        logincell?.layer.cornerRadius = 25
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            let url = URL(string: dict["pic"] as! String)
            logincell?.imgOnlineFriends.setImageFrom(url)
            logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
            
            return logincell!
        }
        else {
            let identifier = "InboxCell"
            var logincell: InboxCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? InboxCell
            if logincell == nil {
                tableView.register(UINib(nibName: "InboxCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? InboxCell)!
            }
            
            let dicttemp = self.arrChatList[indexPath.row]
            logincell!.loadLastMessage(friendNode: dicttemp)
            logincell!.backgroundColor = UIColor.clear
            logincell!.selectionStyle = .none
            
            return logincell!
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblOnline {
            return 60
        }
        else {
            return 86
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 86
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if tableView == tblOnline {
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            flagPark = false
            flagGym = false
            flagGallery = false
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            obj_phonelistvc.str_user_id = dict["userId"] as! String
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
        else {
            let cell:InboxCell = tableView.cellForRow(at: indexPath) as! InboxCell
            
            let dicttemp = self.arrChatList[indexPath.row]
            let objFriendsChatVC:FriendsChatVC = FriendsChatVC(nibName: "FriendsChatVC", bundle: nil)
            //let objFriendsChatVC = self.storyboard?.instantiateViewController(withIdentifier: "FriendsChatVC") as? FriendsChatVC
            
            let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
            //        objFriendsChatVC.myUserID = str_userid
            //        objFriendsChatVC.myFriendID = obj_friendDetail.str_user_id
            
            objFriendsChatVC.myUserID = str_userid
            objFriendsChatVC.myFriendID = dicttemp["friendID"] as? String ?? ""
            objFriendsChatVC.NodeID = dicttemp["documentID"] as? String ?? ""
            objFriendsChatVC.friendName = cell.lblFriendName.text!
            self.navigationController?.pushViewController(objFriendsChatVC, animated: true)
        }
    }
    
}

extension InboxVC: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = true
        return transiton
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = false
        return transiton
    }
}
