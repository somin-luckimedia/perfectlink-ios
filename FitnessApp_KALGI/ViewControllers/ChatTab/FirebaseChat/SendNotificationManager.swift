//
//  SendNotificationManager.swift
//  BigBanksTakesLittle
//
//  Created by Ashish tripathi on 25/01/20.
//  Copyright © 2020 Marie's. All rights reserved.
//

import UIKit

class SendNotificationManager: NSObject {
    static let share = SendNotificationManager.init()
    
    // Private initalliser..
    private override init(){
        super.init()
        
    }
    
    func sendPushNotification(userToken:String,userinfo:[String : Any], type: String, message : String, tittle: String) {
    
        let payloadDict: [String: Any] = ["to": userToken,"notification": ["title":tittle,"body":message,"badge":1,"priority":"high", "sound":"default"],"data":userinfo,"type":type]

       let url = URL(string: "https://fcm.googleapis.com/fcm/send")!
       var request = URLRequest(url: url)
       request.setValue("application/json", forHTTPHeaderField: "Content-Type")
       // get your **server key** from your Firebase project console under **Cloud Messaging** tab
       request.setValue("key=AAAA_BttwaQ:APA91bHHFCxSdPw-nr5_JTCCkHwGLq3cNX4Ys32E66b3ljOCC4bzhRDVpQvUtsbrFwmNRWph9QtKYGFSufcOHGh2ZhGCAFH8Z6p1UMs4wFnUdRurUHzWWiFQG5Kfp_0umIpB9McI8IaA", forHTTPHeaderField: "Authorization")
       request.httpMethod = "POST"
       request.httpBody = try? JSONSerialization.data(withJSONObject: payloadDict, options: [])
       let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data, error == nil else {
            print(error ?? "")
            return
          }
          if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            print("statusCode should be 200, but is \(httpStatus.statusCode)")
            print(response ?? "")
          }
          print("Notfication sent successfully.")
          let responseString = String(data: data, encoding: .utf8)
          print(responseString ?? "")
       }
       task.resume()
    }
    
    
    
    

}
