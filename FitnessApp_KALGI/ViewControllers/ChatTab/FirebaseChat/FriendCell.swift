//
//  FriendCell.swift
//  FitnessApp
//
//  Created by Ashish on 01/05/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class FriendCell: UITableViewCell {

    @IBOutlet weak var lbl_message: UILabel!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var constraint_view_message: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
