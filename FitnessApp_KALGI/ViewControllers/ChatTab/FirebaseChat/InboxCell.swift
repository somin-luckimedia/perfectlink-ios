//
//  InboxCell.swift
//  FitnessApp
//
//  Created by Ashish on 01/05/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class InboxCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    let db = Firestore.firestore()
    @IBOutlet weak var imgFriend: UIImageView!
    @IBOutlet weak var lblFriendName: UILabel!
    @IBOutlet weak var lbl_message: UILabel!
    
    func loadLastMessage (friendNode: [String: Any]){
        let friendID = friendNode["friendID"] as? String ?? ""
        let NodeID = friendNode["documentID"] as? String ?? ""
        self.db.collection("users").document(friendID).getDocument { (docSnap, error) in
            if (error != nil) {
            }else{
                if docSnap?.exists ?? false{
                    let value = docSnap?.data()
                    let imgUrl = value?["profileImage"] as? String ?? ""
                    let url = URL(string: imgUrl)
                    self.imgFriend.setImageFrom(url)
                    let name = value?["name"] as? String ?? ""
                    self.lblFriendName.text = name
                    self.imageView?.image = UIImage(named: "")
                }
                
            }
        }
        
        self.db.collection("chatChannels").document(NodeID).collection("messages").order(by: "time").getDocuments { (docSnap, error) in
            if error != nil{
                
            }else{
                var  arrmessgae  = [[String : Any]]()
                if !(docSnap?.isEmpty ?? false){
                    
                    let documents = docSnap?.documents ?? [QueryDocumentSnapshot]()
                    
                    for doc in documents{
                        let value = doc.data()
                        arrmessgae.append(value)
                    }
                    
                    if arrmessgae.count > 0{
                        
                        let dictemp = arrmessgae.last
                        let message = dictemp?["text"]
                        self.lbl_message.text = message as? String ?? ""
                    }else{
                        self.lbl_message.text = ""
                    }
                    
                    
                }
                
            }
        }
        
        
    }
    
}


