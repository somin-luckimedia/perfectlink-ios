//
//  ProtienVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 20/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//


import UIKit
import Alamofire
import PopItUp

class ProtienVC: UIViewController , UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, ProtienFilterdelegate  {
    fileprivate var array_mybookings:NSMutableArray = NSMutableArray()
    @IBOutlet weak var imageView_BG: UIImageView!
    
    @IBOutlet weak var tableview_bookings: UITableView!
    var dict_item:NSDictionary!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "SUPPLEMENTS"
        tableview_bookings.delegate  = self as UITableViewDelegate
        tableview_bookings.dataSource = self as UITableViewDataSource
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
         self.getMyBookings()
        self.addLeftMenuButtonWithImage()
        self.addRightMenuButtonWithImage()
        
        
    }
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "filter_icon"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.filter_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func filter_btn_clicked(){
        //let obj_requestmodal:FerryBoatModal = array_ferrylist[cell.index_row!] as! FerryBoatModal
        let obj_ferrylistingVC : SupplementFilterVC = SupplementFilterVC(nibName: "SupplementFilterVC", bundle: nil)
        obj_ferrylistingVC.delegate = self
        let color_obj:UIColor = UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 0.6)
        presentPopup(obj_ferrylistingVC,
                     animated: true,
                     backgroundStyle: .color(color_obj), // present the popup with a blur effect has background
            constraints: [.width(300), .height(500)], // fix leading edge and the width
            transitioning: .zoom, // the popup come and goes from the left side of the screen
            autoDismiss: true, // when touching outside the popup bound it is not dismissed
            completion: nil)
    }
    
    func filter_button_clicked(value: String) {
        print("Slider value ", value)
    }
    
    
    
    func getMyBookings(){
            let postParam: Parameters = ["type":"0"]
            let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
            print("Login Post Parameter : \(postParam)")
            let headers: HTTPHeaders = [
                "Authorization": String(format: "Bearer %@",str_accessToken),
                "Accept": "application/json"
            ]
            ServerCommunication.getDataWithPostHeader(url: ConstantFiles.protein_listing, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
                print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:ProteinListModel = ProteinListModel(dictUserInfo: dict)
                        self.array_mybookings.add(obj_bookingmodal)
                    }
                    self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 120
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.array_mybookings.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "ProtienCell"
        var logincell: ProtienCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? ProtienCell
        if logincell == nil {
            tableView.register(UINib(nibName: "ProtienCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? ProtienCell)!
        }
        //logincell!.delegate = self
           let obj_requestmodal:ProteinListModel = array_mybookings[indexPath.row] as! ProteinListModel
        
         logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal)
        return logincell!
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj_phonelistvc:ProtienDetailVC = ProtienDetailVC(nibName: "ProtienDetailVC", bundle: nil)
        let obj_requestmodal:ProteinListModel = array_mybookings[indexPath.row] as! ProteinListModel
        obj_phonelistvc.str_productId = obj_requestmodal.str_product_id
        obj_phonelistvc.str_title = obj_requestmodal.str_product_title
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
}
