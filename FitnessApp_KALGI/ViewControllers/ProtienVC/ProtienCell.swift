//
//  ProtienCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 20/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//



import UIKit

class ProtienCell: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var imageView_thumb: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
     func updateCellwithModal(obj_bookingmodal:ProteinListModel){
     
   
     
     lbl_title.text = String(format: "%@", obj_bookingmodal.str_product_title!)
     lbl_description.text = String(format: "%@", obj_bookingmodal.str_brief_description!)
     let url = URL(string: obj_bookingmodal.str_cover_image)
     imageView_thumb.setImageFrom(url)
     lbl_price.text = String(format: "%@%@/%@", obj_bookingmodal.str_currency!,obj_bookingmodal.str_amount!,obj_bookingmodal.str_per!)
     
     }
     
     
    
}

