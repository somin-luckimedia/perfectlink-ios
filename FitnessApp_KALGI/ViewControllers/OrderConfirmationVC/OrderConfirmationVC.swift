//
//  OrderConfirmationVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 16/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire

class OrderConfirmationVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var str_transactionId:String = ""
    @IBOutlet weak var tableview_detail: UITableView!
    var obj_orderSummaryCell: OrdarSummaryCell!
    var obj_orderShippingCell: OrderShippingCell!
    var obj_orderTrackCell: OrderTrackCell!
    var obj_orderDetailModel:OrderDetailModel = OrderDetailModel()
    var isFromHistory:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview_detail.delegate  = self as UITableViewDelegate
        tableview_detail.dataSource = self as UITableViewDataSource
        self.title = "ORDER CONFIRMATION"
        self.addLeftMenuButtonWithImage()
        self.tableview_detail.reloadData()
        self.getMyBookings()
        // Do any additional setup after loading the view.
    }

    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        if isFromHistory == true{
            button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
            button.addTarget(self, action: #selector(self.back_summary_btn_clicked), for: UIControl.Event.touchUpInside)
        }else{
            button.setImage(UIImage(named: "homeIcon"), for: UIControl.State.normal)
            button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        }
       
        button.frame = CGRect(x: 0, y: 0, width: 25 , height: 25)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }

    func getMyBookings(){
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        let postParam: Parameters = ["transaction_id":str_transactionId]
        print("Login Post Parameter : \(postParam)")
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.get_orderInfo, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    self.obj_orderDetailModel = OrderDetailModel(dictUserInfo: arr as! NSDictionary)
                    self.tableview_detail.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    @objc func back_btn_clicked(){
        let rootVC : CitySelectionVC = CitySelectionVC(nibName: "CitySelectionVC", bundle: nil)
        let nav = UINavigationController(rootViewController: rootVC)
        sideMenuController?.embed(centerViewController: nav)
        /*
         let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
         objAppDelgate.loadSidePanel(objVC: self.navigationController!)
         */
    }

    @objc func back_summary_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 70
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header:OrderSummaryHeaderView =  UINib(nibName: "OrderSummaryHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! OrderSummaryHeaderView
        if section == 1{
            header.lbl_title.text = "Shippping Detail"
        }else if section == 2{
            header.lbl_title.text = "Price Detail"
        }
        
        return header
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0{
            if indexPath.row == 0{
                return 55
            }else if indexPath.row == 1{
                return 120
            }else{
               return 110.5
            }
        }else if indexPath.section == 1{
            return UITableView.automaticDimension
        }else{
            return 55
        }
        
        
        // return 710
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0{
            return 3
        }else if section == 1{
            return 1
        }else{
            return 2
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        ////TrainerHomeAddressCell
        if indexPath.section == 0{
            if indexPath.row == 0{
                let identifier = "OrderAmountCell"
                var obj_amountcell: OrderAmountCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderAmountCell
                if obj_amountcell == nil {
                    tableView.register(UINib(nibName: "OrderAmountCell", bundle: nil), forCellReuseIdentifier: identifier)
                    obj_amountcell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderAmountCell)!
                }
                obj_amountcell?.lbl_title.text = String(format: "Order Id: %@ ", self.obj_orderDetailModel.str_order_id)
                obj_amountcell?.lbl_value.text = ""
                return obj_amountcell!
            }
            else if indexPath.row == 1{
                let identifier = "OrdarSummaryCell"
                obj_orderSummaryCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrdarSummaryCell
                if obj_orderSummaryCell == nil {
                    tableView.register(UINib(nibName: "OrdarSummaryCell", bundle: nil), forCellReuseIdentifier: identifier)
                    obj_orderSummaryCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrdarSummaryCell
                }
                obj_orderSummaryCell.updateCellwithModal(obj_accessoryiesModel: self.obj_orderDetailModel.obj_accessories_detail, str_qty: String(format: "%@", obj_orderDetailModel.str_quantity))
                return obj_orderSummaryCell
            }else{
                //obj_orderTrackCell
                let identifier = "OrderTrackCell"
                obj_orderTrackCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderTrackCell
                if obj_orderTrackCell == nil {
                    tableView.register(UINib(nibName: "OrderTrackCell", bundle: nil), forCellReuseIdentifier: identifier)
                    obj_orderTrackCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderTrackCell
                }
                // obj_orderSummaryCell.updateCellwithModal(obj_accessoryiesModel: self.obj_accessoriesModel, str_qty: String(format: "%ld", productCount))
                return obj_orderTrackCell
                
            }
        }else if indexPath.section == 1{
            let identifier = "OrderShippingCell"
            obj_orderShippingCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderShippingCell
            if obj_orderShippingCell == nil {
                tableView.register(UINib(nibName: "OrderShippingCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_orderShippingCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderShippingCell
            }
            
            //let obj_locationModel:LocationServiceModel = obj_trainer_detail!.array_locations[indexPath.row] as! LocationServiceModel
            //homeCell.updateCellwithModal(obj_location_model:obj_locationModel)
            //  homeCell.delegate = self
            obj_orderShippingCell.updateCellwithModal(obj_shippingAddress: self.obj_orderDetailModel.obj_address)
            return obj_orderShippingCell
        }else{
            let identifier = "OrderAmountCell"
            var obj_amountcell: OrderAmountCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderAmountCell
            if obj_amountcell == nil {
                tableView.register(UINib(nibName: "OrderAmountCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_amountcell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderAmountCell)!
            }
            if indexPath.row == 0{
            obj_amountcell?.lbl_title.text = String(format: "Total Amount")
            obj_amountcell?.lbl_value.text = String(format: "%@%@", obj_orderDetailModel.obj_accessories_detail.str_currency, obj_orderDetailModel.str_total_amount_paid)
            }else{
                obj_amountcell?.lbl_title.text = String(format: "Payment Method")
                obj_amountcell?.lbl_value.text = String(format: "%@", obj_orderDetailModel.str_payment_method)
            }
            return obj_amountcell!
        }
    }
    
    

}
