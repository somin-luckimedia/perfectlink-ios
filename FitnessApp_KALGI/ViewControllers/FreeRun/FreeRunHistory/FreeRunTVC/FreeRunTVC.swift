//
//  FreeRunTVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 13/01/21.
//

import UIKit

class FreeRunTVC: UITableViewCell {

    @IBOutlet var btnTime: UIButton!
    @IBOutlet var lblCalories: UILabel!
    @IBOutlet var lblSteps: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var btnMiles: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
