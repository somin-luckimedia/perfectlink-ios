//
//  FreeRunHistoryVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 13/01/21.
//

import UIKit

class FreeRunHistoryVC: UIViewController {
    @IBOutlet var tblFreeRunhistory: UITableView!
    fileprivate var array_categories: NSMutableArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        tblFreeRunHistory.register(UINib(nibName: "FreeRunTVC", bundle: nil), forCellReuseIdentifier: "FreeRunTVC")
        getMyBookings()
    }
    @IBAction func onBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet var tblFreeRunHistory: UITableView!
}

//MARK : WEBSERVICE CALL METHODS
extension FreeRunHistoryVC {
    func getMyBookings(){
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        
        ServerCommunication.getData(url: ConstantFiles.freeRun_get, viewController: self,headers:headers   ,success: { (successDict) in
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                let array: NSArray = successDict["response"] as! NSArray
                for i in 0..<array.count{
                    self.array_categories.add(array[i] as! NSDictionary)
                }
                self.tblFreeRunHistory.delegate = self
                self.tblFreeRunHistory.dataSource = self
                self.tblFreeRunHistory.reloadData()
                print(self.array_categories)
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
            print(successDict)
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
}

extension FreeRunHistoryVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : FreeRunTVC = tblFreeRunHistory.dequeueReusableCell(withIdentifier: "FreeRunTVC") as! FreeRunTVC
        cell.selectionStyle = .none
        let dict = array_categories[indexPath.row] as! NSDictionary
        cell.lblSteps.text = "\(String(describing: dict["steps"] as! String)) Steps"
        cell.lblCalories.text = "\(String(describing: dict["calories"] as! String)) Calories"
        cell.btnTime.setTitle("\(dict["duration"] as! String)", for: .normal)
        cell.btnMiles.setTitle("\(dict["distance"] as! String)", for: .normal)
        cell.lblDate.text = "\(String(describing: dict["created_at"] as! String))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = array_categories[indexPath.row] as! NSDictionary
        let obj_phonelistvc:FreeRunChallengeVC = FreeRunChallengeVC(nibName: "FreeRunChallengeVC", bundle: nil)
        obj_phonelistvc.dict_challenge = dict
        obj_phonelistvc.FreeRunId = dict["id"] as! String
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
}
