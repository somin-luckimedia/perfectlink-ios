//
//  FreeRunChallengeVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 18/01/21.
//

import UIKit

class FreeRunChallengeVC: UIViewController {

    @IBOutlet var imgMap: UIImageView!
    @IBOutlet var lblSteps: UILabel!
    @IBOutlet var lblDistance: UILabel!
    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var lblCalories: UILabel!
    var dict_challenge : NSDictionary!
    var FreeRunId : String = ""
    
    @IBAction func btnSendthisRun(_ sender: UIButton) {
        let obj_phonelistvc:ChallengeFriendsVC = ChallengeFriendsVC(nibName: "ChallengeFriendsVC", bundle: nil)
        let url = URL(string: dict_challenge["image"] as! String)!
        let imageData = try! Data(contentsOf: url)
        let image = UIImage(data: imageData)
        obj_phonelistvc.selectedImage = image
//        obj_phonelistvc.selectedImage = UIImage(named: "map_img")
        obj_phonelistvc.str_fileType = "IMAGE"
        obj_phonelistvc.freeRunId = FreeRunId
        obj_phonelistvc.str_message = "I have finished my \(lblSteps.text!) steps in \(lblDuration.text!) time, Will you accept this challenge?"
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        lblSteps.text = dict_challenge["steps"] as? String
        lblDistance.text = dict_challenge["distance"] as? String
        lblDuration.text = dict_challenge["duration"] as? String
        lblCalories.text = dict_challenge["calories"] as? String

        // Do any additional setup after loading the view.
    }


    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
