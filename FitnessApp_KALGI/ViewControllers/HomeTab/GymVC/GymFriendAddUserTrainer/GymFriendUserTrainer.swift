//
//  GymFriendUserTrainer.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 07/11/20.
//

import UIKit

class GymFriendUserTrainer: UIViewController {

    

    var str_gymId = ""
    @IBOutlet var tblFriendUserTrainer: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblFriendUserTrainer.register(UINib(nibName: "UserTrainerCVC", bundle: nil), forCellReuseIdentifier: "UserTrainerCVC")
        getUserTrainer()
    }
    var arrUSer : NSArray = []
    var arrTrainer : NSArray = []
    var arrIndexUser : NSMutableArray = []
    var arrIndexTrainer : NSMutableArray = []
    var strJoineEmails : String = ""
    var strType = ""

}


//MARK : BUTTON ACTION EVENTS
extension GymFriendUserTrainer {
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func btnAddFriend(_ sender: UIButton) {
//        let total = arrIndexTrainer.count + arrIndexUser.count
        var arrEmails : NSMutableArray = []

//        arrEmails.removeAllObjects()
        for i in 0..<arrIndexTrainer.count {
            let dict = arrTrainer[i] as! NSDictionary
            arrEmails.add(dict["email"] as! String)

        }
        for i in 0..<arrIndexUser.count {
            let dict = arrUSer[i] as! NSDictionary
            arrEmails.add(dict["email"] as! String)
        }
        
        let joined = arrEmails.componentsJoined(by: ",")
        print(joined)
        strJoineEmails = joined
        self.addFriendList()
    }
}

//MARK : BUTTON ACTION EVENTS
extension GymFriendUserTrainer : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrUSer.count
        }
        else {
            return arrTrainer.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UserTrainerCVC = tblFriendUserTrainer.dequeueReusableCell(withIdentifier: "UserTrainerCVC") as! UserTrainerCVC
        cell.selectionStyle = .none

        if indexPath.section == 0 {
            let dict = arrUSer[indexPath.row] as! NSDictionary
            let url = URL(string: dict["profile_picture"] as! String)
            cell.imgProfile.setImageFrom(url)
            cell.lblGym.text = "\(dict["firstname"] as! String) \(dict["lastname"] as! String)"
            cell.lblDetail.text = "\(dict["gender"] as! String) \(dict["age"] as! Int)yrs"
            if arrIndexUser.contains(indexPath.row) {
                cell.imgSelectUnselect.image = UIImage(named: "icn_select")
            }
            else {
                cell.imgSelectUnselect.image = UIImage(named: "default_shipping")

            }
        }
        else {
            let dict = arrTrainer[indexPath.row] as! NSDictionary
            let url = URL(string: dict["profile_picture"] as! String)
            cell.imgProfile.setImageFrom(url)
//            cell.imgProfile.setImageFromURL(stringImageUrl: dict["profile_picture"] as! String)
            cell.lblGym.text = "\(dict["firstname"] as! String) \(dict["lastname"] as! String)"
            cell.lblDetail.text = "\(dict["gender"] as! String) \(dict["age"] as! Int)yrs"
            if arrIndexTrainer.contains(indexPath.row) {
                cell.imgSelectUnselect.image = UIImage(named: "icn_select")
            }
            else {
                cell.imgSelectUnselect.image = UIImage(named: "default_shipping")
            }
        }
         
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: tblFriendUserTrainer.frame.size.width, height: 44))
        viewHeader.backgroundColor = UIColor.clear
        
        let lblTitle = UILabel(frame: CGRect(x: 10, y: 0, width: viewHeader.frame.size.width, height: viewHeader.frame.size.height))
        
        if section == 0 {
            lblTitle.text = "User"
        }
        else {
            lblTitle.text = "Trainer"
        }
        lblTitle.font = UIFont(name: "Lato-Bold", size: 20.0)
        lblTitle.textColor = UIColor(red: 96.0/255.0, green: 99.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        lblTitle.backgroundColor = .clear
        lblTitle.textAlignment = .left
//        viewHeader.addSubview(viewHeader1)
//        viewHeader.addSubview(viewHeader2)
        viewHeader.addSubview(lblTitle)
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if arrIndexUser.contains(indexPath.row) {
                arrIndexUser.remove(indexPath.row)
            }
            else {
                arrIndexUser.add(indexPath.row)
            }
        }
        else {
            if arrIndexTrainer.contains(indexPath.row) {
                arrIndexTrainer.remove(indexPath.row)
            }
            else {
                arrIndexTrainer.add(indexPath.row)
            }
        }
        tblFriendUserTrainer.reloadData()
    }
}

//MARK: WEBSERVICE CALL
extension GymFriendUserTrainer {
    func getUserTrainer()
    {
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
 
        let postParam = ["id":str_gymId,"type" : strType]//"GYM"]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.user_trainers, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let array:NSDictionary = arr as! NSDictionary
                        self.arrUSer = array["users"] as? NSArray ?? []
                        self.arrTrainer = array["trainers"] as? NSArray ?? []
                    
                    self.tblFriendUserTrainer.delegate = self
                    self.tblFriendUserTrainer.dataSource = self
                    self.tblFriendUserTrainer.reloadData()
                }
//                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func addFriendList(){
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam = ["user_id": str_userid,"email" : strJoineEmails] as [String : Any]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@",  (str_accessToken)),
            "Accept": "application/json"
        ]
      //  self.array_mybookings.removeAllObjects()
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.send_friend_request, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
//                showAlertWithTitleFromVC(vc: self, andMessage: ((successDict.value(forKey: "message") as? String)!))
                let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    self.navigationController?.popToRootViewController(animated: true)
                }))
            }else{
                showAlertWithTitleFromVC(vc: self, andMessage: ((successDict.value(forKey: "message") as? String)!))
            }
            
            
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
}
