//
//  UserTrainerCVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 07/11/20.
//

import UIKit

class UserTrainerCVC: UITableViewCell {

    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblGym: UILabel!
    @IBOutlet var lblDetail: UILabel!
    @IBOutlet var imgSelectUnselect: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
