//
//  GymDetailTVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 19/10/20.
//

import UIKit

class GymDetailTVC: UITableViewCell {

    @IBOutlet var lblOpen: UILabel!
    @IBOutlet var lblGymname: UILabel!
    @IBOutlet var lblGymDescr: UILabel!
    @IBOutlet var imgGym: UIImageView!
    @IBOutlet var lblMiles: UILabel!
    @IBOutlet var btnNavigation: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
