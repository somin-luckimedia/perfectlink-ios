//
//  GymDetailVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 17/10/20.
//

import UIKit
import PopItUp

class GymDetailVC: UIViewController,SliderFilterdelegate {
    let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate


    func filter_button_clicked(value: String) {
        print("Slider value ", value)
        str_radius = value
        self.getMyBookings(str_radius: value)
    }
    

    @IBOutlet var tblGymDetail: UITableView!
    var dict_item:NSDictionary!
    var str_radius:String! = "50"

    fileprivate var array_mybookings:NSMutableArray = NSMutableArray()
    @IBOutlet weak var lbl_message: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!

    var searchEnabled = false
    var searchText = ""

    @IBOutlet var vwSearchBar: UIView!
    @IBAction func btnCloseIcnTapped(_ sender: UIButton) {
        vwSearchBar.isHidden = true
    }
//    @IBAction func btnSearchTapped(_ sender: Any) {
//    }
    var arrFavSearchHeader : NSMutableArray = []
    var dictFavSearchData:NSMutableDictionary = [:]
    @IBOutlet var searchBar: UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        tblGymDetail.register(UINib(nibName: "GymDetailTVC", bundle: nil), forCellReuseIdentifier: "GymDetailTVC")
        tblGymDetail.delegate = self
        tblGymDetail.dataSource = self
        lbl_Title.text = "\(dict_item["category_name"] as! String) Gym"
        self.getMyBookings(str_radius: "50")
        searchBar.layer.cornerRadius = 5.0
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont(name: "Lato-Regular", size: 16.0)
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        searchBar.setShowsCancelButton(false, animated: true)
        textFieldInsideSearchBar?.leftViewMode = .never
        self.searchBar.searchTextPositionAdjustment = UIOffset(horizontal: -19, vertical: 0)
        textFieldInsideSearchBar?.placeholder = "Search"
        textFieldInsideSearchBar?.borderStyle = .none
        textFieldInsideSearchBar?.textColor = .white
        vwSearchBar.isHidden = true

        // Do any additional setup after loading the view.
    }
}

//MARK: SearchBar Delegate Methods
extension GymDetailVC: UISearchBarDelegate {
    override func touchesBegan(_ touches: Set<UITouch>,with event: UIEvent?){
        searchBar.resignFirstResponder()
    }
    //    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    //        searchBar.setShowsCancelButton(true, animated: true)
    //    }
    //
    //    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    //        searchBar.setShowsCancelButton(false, animated: true)
    //    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchBar.text?.count == 0 {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            searchEnabled = false
            tblGymDetail.reloadData()
            tblGymDetail.layoutIfNeeded()
            tblGymDetail.setNeedsLayout()
            
        }
        else {
            searchEnabled = true
            self.searchText = searchBar.text!
            filterContentforSearchText(searchText: searchBar.text!)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
//        Analytics.logEvent("recipe_submit_search", parameters: nil)
        searchBar.resignFirstResponder()
        if searchBar.text == "" {
        }
        else
        {
            searchEnabled = true
            filterContentforSearchText(searchText: searchBar.text!)//khushbu_new
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchEnabled = false
        tblGymDetail.reloadData()
        tblGymDetail.layoutIfNeeded()
        tblGymDetail.setNeedsLayout()
        
        
    }
}

extension GymDetailVC {
    //MARK: Filter Methods
    func filterContentforSearchText(searchText: String) {
        
        //        let strFavBtnStatus = StaticClass.retrieve(fromUserDefaults: "isBtnFavRec")
        //        if strFavBtnStatus == "YES"
        //        {
        arrFavSearchHeader = []
        dictFavSearchData = [:]
        
        for i in 0..<array_mybookings.count {
            let dict : GymListModel = array_mybookings[i] as! GymListModel
            let strTitle = dict.str_gym_title
            if strTitle!.lowercased().range(of:searchText.lowercased()) != nil {
                arrFavSearchHeader.add(dict)
            }
        }
        
        //        if (self.arrFavSearchHeader.count > 0){
        //                for i in 0..<arrFavHeaderData.count{
        //                    let strHeader = arrFavSearchHeader[i]
        //                    let arrData = dictFavSearchData[strHeader] as! NSArray
        //
        //                    let arrDictAdd:NSMutableArray = []
        //
        ////                    for j in 0..<array_categories.count{
        //////                        var objFlatTummyInfo : SquatInfo = SquatInfo()
        ////                        objFlatTummyInfo = arrData.object(at: j) as! SquatInfo
        ////                        let strTitle = objFlatTummyInfo.Recipe_title
        ////
        ////                        if strTitle.lowercased().range(of:searchText.lowercased()) != nil {
        ////
        ////                            if !arrFavSearchHeader.contains(strHeader) {
        ////                                arrFavSearchHeader.add(strHeader)
        ////                            }
        ////                            arrDictAdd.add(arrData[j])
        ////                            dictFavSearchData[strHeader] = arrDictAdd
        ////                        }
        ////                    }
        //                }
        //            }
        tblGymDetail.reloadData()
        tblGymDetail.setNeedsLayout()
        tblGymDetail.layoutIfNeeded()
        
    }
}

//MARK: WEB SERVICE CALL METHODS
extension GymDetailVC {
    func getMyBookings(str_radius:String){
        let str_userid:String = String(format: "%ld", (dict_item["id"] as! Int) )
        let strLat:String = DataUtil.appdelegate().str_lattitude
        let strLong:String = DataUtil.appdelegate().str_longitude
        let postParam = ["category_id":str_userid, "lat":strLat, "lon":strLong, "radius":str_radius]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        self.array_mybookings.removeAllObjects()
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.gym_listing, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                    if let arr = successDict.value(forKey:"response"){
                        let array:NSArray = arr as! NSArray
                        for item in array {
                            let dict:NSDictionary =  item as! NSDictionary
                            let obj_bookingmodal:GymListModel = GymListModel(dictUserInfo: dict)
                            self.array_mybookings.add(obj_bookingmodal)
                        }
                        if self.array_mybookings.count == 0{
                            self.lbl_message.isHidden = false
                        }else{
                            self.lbl_message.isHidden = true
                        }
                        self.tblGymDetail.reloadData()
                    }else{
                        self.lbl_message.isHidden = false
                    }
            }else{
                self.lbl_message.isHidden = false

            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
}

//MARK : TABLEVIEW DELEGATE & DATA SOURCE METHODS
extension GymDetailVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchEnabled
        {
            return arrFavSearchHeader.count
        }
        else {
            return array_mybookings.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : GymDetailTVC = tblGymDetail.dequeueReusableCell(withIdentifier: "GymDetailTVC") as! GymDetailTVC
        cell.selectionStyle = .none
        var obj_requestmodal : GymListModel!
        if searchEnabled {
            obj_requestmodal = arrFavSearchHeader[indexPath.row] as? GymListModel
        }
        else {
            obj_requestmodal = array_mybookings[indexPath.row] as? GymListModel
        }
//        let obj_requestmodal:GymListModel = array_mybookings[indexPath.row] as! GymListModel
        let url = URL(string: obj_requestmodal.str_cover_image)
        cell.imgGym.setImageFrom(url)
        cell.lblGymname.text = obj_requestmodal.str_gym_title
        cell.lblGymDescr.text = obj_requestmodal.str_brief_description
        cell.lblOpen.text = obj_requestmodal.str_open_close
        cell.lblMiles.text = String(format: "%@ Miles", obj_requestmodal.str_distance)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj_requestmodal:GymListModel!//
        let obj_GymDetailVC:GymPersonalDetailVC = GymPersonalDetailVC(nibName: "GymPersonalDetailVC", bundle: nil)
       // obj_GymDetailVC.str_gymId = obj_requestmodal.str_gym_id
        if searchEnabled {
            obj_requestmodal = arrFavSearchHeader[indexPath.row] as! GymListModel
            obj_GymDetailVC.str_gymId = obj_requestmodal.str_gym_id
        }
        else {
            obj_requestmodal = array_mybookings[indexPath.row] as! GymListModel
            obj_GymDetailVC.str_gymId = obj_requestmodal.str_gym_id
        }
        self.navigationController?.pushViewController(obj_GymDetailVC, animated: true)
    }
}

//MARK : BUTTON ACTION METHODS
extension GymDetailVC {
    @IBAction func btnSearchTapped(_ sender: UIButton) {
        vwSearchBar.isHidden = false

    }
    @IBAction func btnFilterTapped(_ sender: UIButton) {
        //let obj_requestmodal:FerryBoatModal = array_ferrylist[cell.index_row!] as! FerryBoatModal
        let obj_ferrylistingVC : SliderVC = SliderVC(nibName: "SliderVC", bundle: nil)
        obj_ferrylistingVC.delegate = self
        obj_ferrylistingVC.str_title = "Gym Filter"
//        obj_ferrylistingVC.str_current = str_radius
        //obj_ferrylistingVC.obj_boatmodal = obj_requestmodal
        //self.navigationController?.pushViewController(obj_ferrylistingVC, animated: true)
        // Comment by Ashish
        /*let color_obj:UIColor = UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 0.6)
       presentPopup(obj_ferrylistingVC,
                           animated: true,
                           backgroundStyle: .color(color_obj),
                           constraints: [.width(300), .height(225)],
                           transitioning: .zoom,
                           autoDismiss: true,
                           completion: nil)
 */
        
        let color_obj:UIColor = UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 0.2)
        
        presentPopup(obj_ferrylistingVC,
                     animated: true,
                     backgroundStyle: .color(color_obj), // present the popup with a blur effect has background
            constraints: [.width(300), .height(225)], // fix leading edge and the width
            transitioning: .zoom, // the popup come and goes from the left side of the screen
            autoDismiss: true, // when touching outside the popup bound it is not dismissed
            completion: nil)
    }
    
    @IBAction func btnBackTaped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}


