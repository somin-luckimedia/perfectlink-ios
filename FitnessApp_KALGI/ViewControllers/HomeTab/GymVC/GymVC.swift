//
//  GymVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 16/10/20.
//

import UIKit
import Firebase

class GymVC: UIViewController {


    @IBOutlet var tblGym: UITableView!
    fileprivate var array_categories: NSMutableArray = NSMutableArray()
    var searchEnabled = false
    var searchText = ""

    @IBOutlet var vwSearchBar: UIView!
    @IBAction func btnCloseIcnTapped(_ sender: UIButton) {
        vwSearchBar.isHidden = true
    }
    @IBAction func btnSearchTapped(_ sender: Any) {
        vwSearchBar.isHidden = false
    }
    var arrFavSearchHeader : NSMutableArray = []
    var dictFavSearchData:NSMutableDictionary = [:]
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tblOnline: UITableView!
    var arrOnlineFriends : NSMutableArray = []
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblGym.register(UINib(nibName: "GymTVC", bundle: nil), forCellReuseIdentifier: "GymTVC")
        searchBar.layer.cornerRadius = 5.0
//        searchBar.barTintColor = .clear
//        searchBar.backgroundColor = .clear
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont(name: "Lato-Regular", size: 16.0)
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        searchBar.setShowsCancelButton(false, animated: true)
        textFieldInsideSearchBar?.leftViewMode = .never
        self.searchBar.searchTextPositionAdjustment = UIOffset(horizontal: -19, vertical: 0)
        textFieldInsideSearchBar?.placeholder = "Search"
        textFieldInsideSearchBar?.borderStyle = .none
        textFieldInsideSearchBar?.textColor = .white

        vwSearchBar.isHidden = true
        tblGym.delegate = self
        tblGym.dataSource = self
        self.get_categories_list()
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
//                if snapshot.childrenCount == 1 {
//                    self.constWidthTbl.constant = 0
////                    self.constLeadingColl.constant = 20
//                    self.lblOnline.isHidden = true
//                }
//                else {
//                    self.constWidthTbl.constant = 60
////                    self.constLeadingColl.constant = 0
//                    self.lblOnline.isHidden = false
//                }
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }

                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
//            else {
//                self.constWidthTbl.constant = 0
//                self.constLeadingColl.constant = 10
//
//                self.lblOnline.isHidden = true
//            }
        }
        DataUtil.appdelegate().isAuthorizedtoGetUserLocation()
        
//        tblGym.reloadData()


        // Do any additional setup after loading the view.
    }
}

//MARK: SearchBar Delegate Methods
extension GymVC: UISearchBarDelegate {
    override func touchesBegan(_ touches: Set<UITouch>,with event: UIEvent?){
        searchBar.resignFirstResponder()
    }
    //    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    //        searchBar.setShowsCancelButton(true, animated: true)
    //    }
    //
    //    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    //        searchBar.setShowsCancelButton(false, animated: true)
    //    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchBar.text?.count == 0 {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            searchEnabled = false
                tblGym.reloadData()
                tblGym.layoutIfNeeded()
                tblGym.setNeedsLayout()

        }
        else {
            searchEnabled = true
            self.searchText = searchBar.text!
            filterContentforSearchText(searchText: searchBar.text!)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
//        Analytics.logEvent("recipe_submit_search", parameters: nil)
        searchBar.resignFirstResponder()
        if searchBar.text == "" {
        }
        else
        {
            searchEnabled = true
            filterContentforSearchText(searchText: searchBar.text!)//khushbu_new
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchEnabled = false
        tblGym.reloadData()
        tblGym.layoutIfNeeded()
        tblGym.setNeedsLayout()
        
        
    }
}

extension GymVC {
    //MARK: Filter Methods
    func filterContentforSearchText(searchText: String) {
        
//        let strFavBtnStatus = StaticClass.retrieve(fromUserDefaults: "isBtnFavRec")
//        if strFavBtnStatus == "YES"
//        {
            arrFavSearchHeader = []
            dictFavSearchData = [:]
        
        for i in 0..<array_categories.count {
            let dict = array_categories[i] as! NSDictionary
            let strTitle = dict["category_name"] as? String
            
            if strTitle!.lowercased().range(of:searchText.lowercased()) != nil {
                arrFavSearchHeader.add(dict)
            }
        }
            
//        if (self.arrFavSearchHeader.count > 0){
//                for i in 0..<arrFavHeaderData.count{
//                    let strHeader = arrFavSearchHeader[i]
//                    let arrData = dictFavSearchData[strHeader] as! NSArray
//
//                    let arrDictAdd:NSMutableArray = []
//
////                    for j in 0..<array_categories.count{
//////                        var objFlatTummyInfo : SquatInfo = SquatInfo()
////                        objFlatTummyInfo = arrData.object(at: j) as! SquatInfo
////                        let strTitle = objFlatTummyInfo.Recipe_title
////
////                        if strTitle.lowercased().range(of:searchText.lowercased()) != nil {
////
////                            if !arrFavSearchHeader.contains(strHeader) {
////                                arrFavSearchHeader.add(strHeader)
////                            }
////                            arrDictAdd.add(arrData[j])
////                            dictFavSearchData[strHeader] = arrDictAdd
////                        }
////                    }
//                }
//            }
            tblGym.reloadData()
            tblGym.setNeedsLayout()
            tblGym.layoutIfNeeded()

    }
}
//MARK: Tableview delegate & datasource methods
extension GymVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblOnline {
            return arrOnlineFriends.count
        }
        else {
            if searchEnabled
            {
                return arrFavSearchHeader.count
            }
            else {
                return array_categories.count
            }
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblOnline {
            let identifier = "onlineTVCTableViewCell"
            var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
            
            if logincell == nil {
                tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
            }
            logincell?.selectionStyle = .none
            //        logincell?.layer.cornerRadius = 25
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            let url = URL(string: dict["pic"] as! String)
            logincell?.imgOnlineFriends.setImageFrom(url)
            logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
            
            return logincell!
        }
        else {
            let cell : GymTVC = tblGym.dequeueReusableCell(withIdentifier: "GymTVC") as! GymTVC
            cell.selectionStyle = .none
            var dict_item : NSDictionary = [:]
            if searchEnabled {
                dict_item = arrFavSearchHeader[indexPath.row] as! NSDictionary
            }
            else {
                dict_item = array_categories[indexPath.row] as! NSDictionary
            }
            cell.lblTitle.text = dict_item["category_name"] as? String
            cell.lblSubTitle.text = String(format: "%ld options", dict_item["options"] as! Int)
            let url = URL(string: (dict_item["category_image"] as? String)!)
            cell.imgGym.setImageFrom(url)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblOnline {
            return 60
        }
        else {
            return 80.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblOnline {
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            flagPark = false
            flagGym = false
            flagGallery = false
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            obj_phonelistvc.str_user_id = dict["userId"] as! String
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
        else {
            let obj_GymDetailVC:GymDetailVC = GymDetailVC(nibName: "GymDetailVC", bundle: nil)
            var dict_item : NSDictionary = [:]
            if searchEnabled {
                dict_item = arrFavSearchHeader[indexPath.row] as! NSDictionary
            }
            else {
                dict_item = array_categories[indexPath.row] as! NSDictionary
            }
            obj_GymDetailVC.dict_item = dict_item
            self.navigationController?.pushViewController(obj_GymDetailVC, animated: true)
        }
    }
}

//MARK: Button action methods
extension GymVC {
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: Web service call
extension GymVC {
    func get_categories_list(){
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        print(headers)
        
        ServerCommunication.getData(url: ConstantFiles.gym_categories, viewController: self,headers:headers   ,success: { (successDict) in
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                let array: NSArray = successDict["response"] as! NSArray
                for item in array{
                    let dict_item:NSDictionary = (item as? NSDictionary)!
                    if dict_item != nil {
                        self.array_categories.add(dict_item)
                    }
                }
                self.tblGym.reloadData()
                print(self.array_categories)
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
            
            
            print(successDict)
            // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
}

