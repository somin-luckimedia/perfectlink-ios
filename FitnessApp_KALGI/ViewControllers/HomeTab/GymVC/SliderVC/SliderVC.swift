//
//  SliderVC.swift
//  FitnessApp
//
//  Created by Ashish on 26/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

protocol SliderFilterdelegate:class {
    func filter_button_clicked(value:String)
}

class SliderVC: UIViewController {

    @IBOutlet weak var lbl_miles: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var obj_slider: UISlider!
    weak var delegate: SliderFilterdelegate?
    var str_title:String! = ""
    var str_current:String! = "1"
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.lbl_title.text = self.str_title
        self.obj_slider.setValue(Float(str_current)!, animated: false)
        self.lbl_miles.text = String(format:"%@ Miles", str_current)
        // Do any additional setup after loading the view.
    }

    @IBAction func filter_btn_clicked(_ sender: Any) {
        let currentValue = Int(obj_slider.value)
        self.delegate?.filter_button_clicked(value: "\(currentValue)")
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func close_btn_clickrd(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func slider_changed(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        DispatchQueue.main.async {
            self.lbl_miles.text = "\(currentValue) Miles"
        }
    }
    
    /*
     
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
