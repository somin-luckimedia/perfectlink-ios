

import UIKit

class joinedUserCell: UICollectionViewCell {

    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_user: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func updateCellwithModal(obj_userModel:NSDictionary){
        
        if let result_number = obj_userModel.value(forKey: "profile_picture") as? String
        {
            img_user.setImageFromURL(stringImageUrl: result_number)
        }
        
        if let result_number = obj_userModel.value(forKey: "name") as? String
        {
           lbl_name.text = result_number
        }
    }
    
}
