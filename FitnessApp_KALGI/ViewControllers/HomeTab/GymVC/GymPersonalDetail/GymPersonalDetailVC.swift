//
//  GymPersonalDetailVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 28/10/20.
//

import UIKit

class GymPersonalDetailVC: UIViewController {

    var obj_gymDetail:GymDetailModel! = GymDetailModel()
    @IBOutlet var imgCover: UIImageView!
    @IBOutlet var lblGymTitle: UILabel!
    @IBOutlet var rateView: DYRateView!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblMiles: UILabel!
    @IBOutlet var collEquipment: UICollectionView!
    @IBOutlet var lblAboutGym: UILabel!
    @IBOutlet var txtGymDetail: UITextView!
    var strRate = ""
    var array_equipments: NSMutableArray  = NSMutableArray()
    private let spacing:CGFloat = 5.0
    
    @IBAction func btnNavigation(_ sender: UIButton) {
        let obj_GymDetailVC:ParkLinkDetailMapVC = ParkLinkDetailMapVC(nibName: "ParkLinkDetailMapVC", bundle: nil)
        obj_GymDetailVC.strLat = obj_gymDetail.str_lat
        obj_GymDetailVC.strLon = obj_gymDetail.str_long
        obj_GymDetailVC.strTitle = "Gym Location"
        self.navigationController?.pushViewController(obj_GymDetailVC, animated: true)
    }
    @IBAction func btnStarTapped(_ sender: UIButton) {
        self.vwRateView.isHidden = false
        rateView_view.alignment = RateViewAlignmentCenter
        rateView_view.editable = true
        self.rateView_view.rate = 0
    }
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        strRate =  String(format: "%.f", rateView.rate)
        print(strRate)
        self.getAddRate()
    }
    
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        self.vwRateView.isHidden = true
    }
    
    @IBOutlet var rateView_view: DYRateView!
    @IBOutlet var vwRateView: UIView!
    
    @IBAction func btnAddTapped(_ sender: UIButton) {
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Add gym to profile", style: .default) { action -> Void in
            self.getAddGymProfile()
        }
        let secondAction: UIAlertAction = UIAlertAction(title: "Add users/trainers from gym", style: .default) { action -> Void in
            let obj_GymDetailVC:GymFriendUserTrainer = GymFriendUserTrainer(nibName: "GymFriendUserTrainer", bundle: nil)
            obj_GymDetailVC.strType = "GYM"
            obj_GymDetailVC.str_gymId = self.str_gymId
            self.navigationController?.pushViewController(obj_GymDetailVC, animated: true)
        }
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        
        // add actions
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(cancelAction)
        
        //khushbu_new
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad )
        {
//            if let currentPopoverpresentioncontroller = actionSheetController.popoverPresentationController{
//                currentPopoverpresentioncontroller.sourceView = btnTYpeOutlt
//                currentPopoverpresentioncontroller.sourceRect = btnTYpeOutlt.bounds;
//                //
//
//                currentPopoverpresentioncontroller.permittedArrowDirections = UIPopoverArrowDirection.up;
//                self.present(actionSheetController, animated: true, completion: nil)
//            }
        }else{
            self.present(actionSheetController, animated: true, completion: nil)
        }
    }
    
    @IBOutlet var constCollUserHeight: NSLayoutConstraint!
    @IBOutlet var constHeightUser: NSLayoutConstraint!
    @IBOutlet var lblUser: UILabel!
    @IBOutlet var collUsers: UICollectionView!
    @IBOutlet var constheightTextView: NSLayoutConstraint!
    var str_gymId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collEquipment.register(UINib(nibName: "EquipmentCVC", bundle: nil), forCellWithReuseIdentifier: "EquipmentCVC")
        self.collUsers.register(UINib(nibName: "joinedUserCell", bundle: nil), forCellWithReuseIdentifier: "joinedUserCell")
        self.collEquipment.delegate = self
        self.collEquipment.dataSource = self
        self.collUsers.delegate = self
        self.collUsers.dataSource = self
        self.vwRateView.isHidden = true

        getMyBookings()
    }
}


//MARK : WEB SERVICE METHODS
extension GymPersonalDetailVC {
    func getMyBookings(){
        let strLat:String = DataUtil.appdelegate().str_lattitude
        let strLong:String = DataUtil.appdelegate().str_longitude

        let postParam = ["gym_id":str_gymId,"lat":strLat, "lon":strLong]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.gym_detail, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){

                    self.obj_gymDetail = GymDetailModel(dictUserInfo: arr as! NSDictionary)
                  //  self.tableview_trainerHome.reloadData()
                    let url = URL(string: self.obj_gymDetail.str_cover_image)
                    self.imgCover.setImageFrom(url)
                    self.lblGymTitle.text = self.obj_gymDetail.str_gym_title
                    self.rateView.rate = 4//CGFloat(Float(self.obj_gymDetail.str_star_rating)!)
                    self.lblAddress.text = self.obj_gymDetail.str_address
                    self.lblTime.text = self.obj_gymDetail.str_open_close
                    self.lblMiles.text = "\(self.obj_gymDetail.str_distance ?? "") Miles"
                    self.txtGymDetail.isScrollEnabled = false
                    self.txtGymDetail.text = self.obj_gymDetail.str_full_description
                    self.constheightTextView.constant = self.txtGymDetail.contentSize.height + 20
                    self.collEquipment.reloadData()
                    if self.obj_gymDetail.array_userList.count == 0 {
                        self.constHeightUser.constant = 0
                    }
                    else {
                    self.collUsers.reloadData()
                    let height = self.collUsers.collectionViewLayout.collectionViewContentSize.height
                    self.constCollUserHeight.constant = height
                    self.constHeightUser.constant = height + 70
                    self.view.setNeedsLayout()
                    self.collUsers.reloadData()
                    }
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    func getAddGymProfile()
    {
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
 
        let postParam = ["id":str_gymId,"user_id" : str_userid,"type" : "GYM"]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.add_to_Profile, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
//                if let arr = successDict.value(forKey:"response"){
//
//                    self.obj_gymDetail = GymDetailModel(dictUserInfo: arr as! NSDictionary)
//                  //  self.tableview_trainerHome.reloadData()
//                    self.imgCover.setImageFromURL(stringImageUrl: self.obj_gymDetail.str_cover_image)
//                    self.lblGymTitle.text = self.obj_gymDetail.str_gym_title
//                    self.rateView.rate = CGFloat(Float(self.obj_gymDetail.str_star_rating)!)
//                    self.lblAddress.text = self.obj_gymDetail.str_address
//                    self.lblTime.text = self.obj_gymDetail.str_open_close
//                    self.txtGymDetail.isScrollEnabled = false
//                    self.txtGymDetail.text = self.obj_gymDetail.str_full_description
//                    self.constheightTextView.constant = self.txtGymDetail.contentSize.height + 20
//                    self.collEquipment.reloadData()
//                    if self.obj_gymDetail.array_userList.count == 0 {
//                        self.constHeightUser.constant = 0
//                    }
//                    else {
//                    self.collUsers.reloadData()
//                    let height = self.collUsers.collectionViewLayout.collectionViewContentSize.height
//                    self.constCollUserHeight.constant = height
//                    self.constHeightUser.constant = height + 70
//                    self.view.setNeedsLayout()
//                    self.collUsers.reloadData()
//                    }
//                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
//                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    func getAddRate()
    {
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
 
        let postParam = ["gym_id":str_gymId,"type" : "GYM","rating" : strRate]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.add_star_rating, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            self.vwRateView.isHidden = true
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                }))
//                if let arr = successDict.value(forKey:"response"){
//
//                    self.obj_gymDetail = GymDetailModel(dictUserInfo: arr as! NSDictionary)
//                  //  self.tableview_trainerHome.reloadData()
//                    self.imgCover.setImageFromURL(stringImageUrl: self.obj_gymDetail.str_cover_image)
//                    self.lblGymTitle.text = self.obj_gymDetail.str_gym_title
//                    self.rateView.rate = CGFloat(Float(self.obj_gymDetail.str_star_rating)!)
//                    self.lblAddress.text = self.obj_gymDetail.str_address
//                    self.lblTime.text = self.obj_gymDetail.str_open_close
//                    self.txtGymDetail.isScrollEnabled = false
//                    self.txtGymDetail.text = self.obj_gymDetail.str_full_description
//                    self.constheightTextView.constant = self.txtGymDetail.contentSize.height + 20
//                    self.collEquipment.reloadData()
//                    if self.obj_gymDetail.array_userList.count == 0 {
//                        self.constHeightUser.constant = 0
//                    }
//                    else {
//                    self.collUsers.reloadData()
//                    let height = self.collUsers.collectionViewLayout.collectionViewContentSize.height
//                    self.constCollUserHeight.constant = height
//                    self.constHeightUser.constant = height + 70
//                    self.view.setNeedsLayout()
//                    self.collUsers.reloadData()
//                    }
//                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
//                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func getUserTrainer()
    {
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
 
        let postParam = ["gym_id":str_gymId]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.user_trainers, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{

                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
}

//MARK : BUTTON ACTION METHODS
extension GymPersonalDetailVC {
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK : CUSTOM METHODS
extension GymPersonalDetailVC {
    
}

//MARK : COLLECTION VIEW DELEGATE & DATASOURCE METHODS
extension GymPersonalDetailVC : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collEquipment {
            return obj_gymDetail.array_equipments.count
        }
        else {
            return obj_gymDetail.array_userList.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collEquipment {
            return CGSize(width: 70.0, height: 100.0)
        }
        else {
        let numberOfItemsPerRow:CGFloat = 3
        let spacingBetweenCells:CGFloat = 0
        
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        // if let collection = self.collectionData{
        let width = (collUsers.bounds.width - totalSpacing)/numberOfItemsPerRow
        return CGSize(width: width, height: width)
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collEquipment {
        let cell:EquipmentCVC = collectionView.dequeueReusableCell(withReuseIdentifier: "EquipmentCVC", for: indexPath) as! EquipmentCVC
        
        let dict_equipement:NSDictionary = self.obj_gymDetail.array_equipments[indexPath.row] as!NSDictionary
        cell.updateCellwithModal(obj_gymDetail: dict_equipement)
        return cell
        }
        else {
            let cell:joinedUserCell = collectionView.dequeueReusableCell(withReuseIdentifier: "joinedUserCell", for: indexPath) as! joinedUserCell
            
            let dict_equipement:NSDictionary = self.obj_gymDetail.array_userList[indexPath.row] as!NSDictionary
            cell.updateCellwithModal(obj_userModel: dict_equipement)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict_equipement:NSDictionary = self.obj_gymDetail.array_userList[indexPath.row] as!NSDictionary
        let user_id = dict_equipement["user_id"] as! String
//        wallPostIndex = cell.rowIndex
//        wallPostId = obj_requestmodal.str_user_id
        let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
        obj_phonelistvc.str_user_id = user_id
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)

    }
    
}


