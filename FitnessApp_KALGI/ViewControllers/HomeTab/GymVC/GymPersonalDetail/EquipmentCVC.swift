//
//  EquipmentCVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 28/10/20.
//

import UIKit

class EquipmentCVC: UICollectionViewCell {

    @IBOutlet var lblEquipmentName: UILabel!
    @IBOutlet var imgCover: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateCellwithModal(obj_gymDetail:NSDictionary){
        
        if let result_number = obj_gymDetail.value(forKey: "equipment_image") as? String
        {
            imgCover.setImageFromURL(stringImageUrl:  result_number)
        }
        
        if let result_number = obj_gymDetail.value(forKey: "equipment_name") as? String
        {
            lblEquipmentName.text = result_number
        }
        
    }

}
