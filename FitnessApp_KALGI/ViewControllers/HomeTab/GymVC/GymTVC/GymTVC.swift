//
//  GymTVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 16/10/20.
//

import UIKit

class GymTVC: UITableViewCell {

    @IBOutlet var imgGym: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
