//
//  SupplementTVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 20/10/20.
//

import UIKit

class SupplementTVC: UITableViewCell {

    
    @IBOutlet var lblPrize: UILabel!
    @IBOutlet var lblDescr: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var imgIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
