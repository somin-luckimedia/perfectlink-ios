//
//  ProtienDetailCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 22/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//




import UIKit

protocol ProtienDetailCellDelegate:class {
    func decrease_btn_clicked(cell: ProtienDetailCell, btn: UIButton)
    func increase_btn_clicked(cell: ProtienDetailCell, btn: UIButton)
    func book_btn_clicked(cell: ProtienDetailCell, btn: UIButton)
}

class ProtienDetailCell: UITableViewCell {
    
    
    
    @IBOutlet var lblPrice: UILabel!
//    @IBOutlet weak var imageView_cover: UIImageView!
    @IBOutlet weak var rateView: DYRateView!
    @IBOutlet weak var lbl_rate: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_subTitle: UILabel!
    @IBOutlet weak var lbl_aboutTitle: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    
    @IBOutlet var collImageCover: UICollectionView!
    @IBOutlet weak var btn_decrease: UIButton!
    @IBOutlet weak var btn_increase: UIButton!
    @IBOutlet weak var btn_bookNow: UIButton!
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet var pageControl: UIPageControl!
    
     weak var delegate:ProtienDetailCellDelegate?
    
    @IBOutlet weak var lbl_price: UILabel!
    var obj_gymdetail:ProtienDetailModel! = ProtienDetailModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateCellwithModal(obj_bookingmodal:ProtienDetailModel){
        self.obj_gymdetail = obj_bookingmodal
        lbl_title.text = String(format: "%@", obj_bookingmodal.str_product_title!)
//        lbl_subTitle.text = String(format: "%@", obj_bookingmodal.str_sub_title!)
        lbl_description.text = String(format: "%@", obj_bookingmodal.str_full_description!)
//        imageView_cover.setImageFromURL(stringImageUrl: obj_bookingmodal.str_cover_image)
        lbl_price.text = String(format: "%@%@/%@", obj_bookingmodal.str_currency!,obj_bookingmodal.str_amount!,obj_bookingmodal.str_per!)
        
        rateView.rate = CGFloat(Float(obj_bookingmodal.str_star_rating) ?? 0)
        lbl_rate.text = obj_bookingmodal.str_star_rating
        //lbl_aboutTitle.text = String(format:"About %@", obj_bookingmodal.str_product_title)
        print(obj_bookingmodal.array_galleryImage.count)
        pageControl.hidesForSinglePage = true
        pageControl.numberOfPages = self.obj_gymdetail.array_galleryImage.count

        self.collImageCover.register(UINib(nibName: "SupplementsImageCVC", bundle: nil), forCellWithReuseIdentifier: "SupplementsImageCVC")
        collImageCover.delegate = self
        collImageCover.dataSource = self
        collImageCover.reloadData()
    //        btn_increase.layer.masksToBounds = true
    //        btn_increase.layer.borderColor = UIColor.white.cgColor
    //        btn_increase.layer.borderWidth = 2.0
    //        btn_increase.layer.cornerRadius = 20.0
        
        btn_decrease.layer.masksToBounds = true
        btn_decrease.layer.borderColor = UIColor.white.cgColor
        btn_decrease.layer.borderWidth = 2.0
        btn_decrease.layer.cornerRadius = 20.0
        
    }
    
    @IBAction func decrease_btn_clicked(_ sender: UIButton) {
        self.delegate?.decrease_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func increase_btn_clicked(_ sender: UIButton) {
        self.delegate?.increase_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func book_btn_clicked(_ sender: UIButton) {
        self.delegate?.book_btn_clicked(cell: self, btn: sender)
    }
    
}

extension ProtienDetailCell:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.obj_gymdetail.array_galleryImage.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:SupplementsImageCVC = collImageCover.dequeueReusableCell(withReuseIdentifier: "SupplementsImageCVC", for: indexPath) as! SupplementsImageCVC
        let dict = obj_gymdetail.array_galleryImage[indexPath.row] as! NSDictionary
        let url = URL(string: dict.value(forKey: "image") as! String)
        cell.imgCover.setImageFrom(url)
//        cell.imgCover.setImageFromURL(stringImageUrl: (dict.value(forKey: "image")) as! String)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenwidth:CGFloat = UIScreen.main.bounds.size.width
        return CGSize(width: screenwidth, height: 500)
        
        //        return CGSize(width: 70, height: 97)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == collImageCover{
            self.pageControl.currentPage = indexPath.row
        }
    }
}


