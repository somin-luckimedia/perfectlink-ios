//
//  ProtienDetailVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 22/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire

class ProtienDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, ProtienDetailCellDelegate  {
    
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    //ProtienDetailModel
    var obj_gymDetail:ProtienDetailModel! = ProtienDetailModel()
    var str_userType:String = "2"
    var logincell: ProtienDetailCell!
    var str_productId:String!
    var str_title:String!
    var productCount:Int = 1
    var totalCost:Double = 0.00
    @IBOutlet weak var tableview_detail: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
//        self.title = self.str_title
        tableview_detail.delegate  = self as UITableViewDelegate
        tableview_detail.dataSource = self as UITableViewDataSource
        //tableview_detail.rowHeight = 700
      // tableview_detail.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableview_detail.isHidden = true
//        tableview_detail.reloadData()
//        self.addLeftMenuButtonWithImage()
        self.getMyBookings()
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // self.navigationController?.navigationBar.isHidden = true
        //   self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func decrease_btn_clicked(cell: ProtienDetailCell, btn: UIButton) {
        if productCount != 1{
            productCount = productCount - 1
            logincell.lbl_count.text = String(format: "%ld", productCount)
            self.calculate_total_price()
        }
    }
    
    func increase_btn_clicked(cell: ProtienDetailCell, btn: UIButton) {
        if productCount != 25{
            productCount = productCount + 1
            logincell.lbl_count.text = String(format: "%ld", productCount)
            self.calculate_total_price()
        }
    }
    
    func book_btn_clicked(cell: ProtienDetailCell, btn: UIButton) {
//        ShippingAddressVC
        let obj_ShippingAdressVC:ShippingAdressVC = ShippingAdressVC(nibName: "ShippingAdressVC", bundle: nil)
        obj_ShippingAdressVC.obj_protienDetailModel = self.obj_gymDetail
        obj_ShippingAdressVC.isfromProtien = true
        obj_ShippingAdressVC.productCount = self.productCount
        obj_ShippingAdressVC.totalCost = self.totalCost
        self.navigationController?.pushViewController(obj_ShippingAdressVC, animated: true)
//        (nibName: "ProtienDetailVC", bundle: nil)
    }
    
    func calculate_total_price(){
        let productCost:Double = Double(self.obj_gymDetail.str_amount)!
        self.totalCost = Double(productCount)*productCost
        self.logincell.lblPrice.text = String(format: "$%.2f", self.totalCost)
//        self.logincell.btn_bookNow.setTitle(String(format: "BUY NOW \u{2022} $%.2f", self.totalCost), for: .normal)
    }

    
    func getMyBookings(){
        let postParam: Parameters = ["product_id":str_productId!]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.protien_detail, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.tableview_detail.isHidden = false
                if let arr = successDict.value(forKey:"response"){
                    /* let array:NSArray = arr as! NSArray
                     for item in array {
                     let dict:NSDictionary =  item as! NSDictionary
                     let obj_bookingmodal:GymListModel = GymListModel(dictUserInfo: dict)
                     self.array_mybookings.add(obj_bookingmodal)
                     }*/
                    self.obj_gymDetail = ProtienDetailModel(dictUserInfo: arr as! NSDictionary)
                    self.tableview_detail.reloadData()
                    self.calculate_total_price()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
    //
    //
    //        return UITableViewAutomaticDimension
    //       // return 710
    //
    //    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "ProtienDetailCell"
        logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ProtienDetailCell
        if logincell == nil {
            tableView.register(UINib(nibName: "ProtienDetailCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ProtienDetailCell
        }
        logincell.delegate = self
        logincell.updateCellwithModal(obj_bookingmodal:self.obj_gymDetail)
        // logincell.cellIndex = indexPath.row
        // logincell.configureWithOwner(controller: self)
        // logincell.delegate = self
        //  cell.textLabel?.text = monthName
        return logincell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        let font = UIFont(name: "Helvetica", size: 14.0)
        let height = heightForView(text: String(format: "%@", self.obj_gymDetail.str_full_description!), font: font!, width: UIScreen.main.bounds.width-40)
        if obj_gymDetail.array_galleryImage.count > 0 {
            return  height + 1050
        }
        else {
            return height + 550
        }
       
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Hello delegate")
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
   

    /*
     func login_btn_clicked(cell: LoginCell, btn: UIButton){
     print("delegate called")
     //  let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
     //  objAppDelgate.loadSidePanel(objVC: self.navigationController!)
     //  return
     let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate
     let strEmail  : String = (logincell.textfield_email.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
     let strPassword  : String = (logincell.textfield_password.text)!
     let strDeviceToken:String = (objAppDelgate1.str_devicetoken)!
     let strDeviceType:String = "IOS"
     let strLat:String = DataUtil.appdelegate().str_lattitude
     let strLong:String = DataUtil.appdelegate().str_longitude
     
     //email, password, user_id, login_type, device_token, device_type, lat, lng
     //let strCountrycode1:String = "+91"
     
     let postParam: Parameters = ["email":strEmail,  "password":strPassword,"device_type":strDeviceType,"device_token":"342423442344","lat":"342423442344", "lon":"342423442344"]
     print("Login Post Parameter : \(postParam)")
     
     
     //let imageTest:UIImage = UIImage(named: "back_image")!
     
     if !loginValidation() {
     return
     }
     
     ServerCommunication.getDataWithPost(url: ConstantFiles.loginUrl, parameter: postParam, viewController: self, success: { (successDict) in
     //   SynapseUserDefault.shared.setGenId(userName: self.txtGenId.text!)
     // SynapseUserDefault.shared.setPassword(password: self.txtFieldPassword.text!)
     // SynapseUserDefault.shared.setUserId(userId: successDict.value(forKey: "userId") as! Int)
     print(successDict)
     if (successDict.value(forKey: "STATUS") as? String) == "true"{
     DataUtil.appdelegate().objUserInfo = UserInfo(dictUserInfo: successDict["response"] as! NSDictionary)
     DataUtil.appdelegate().objUserInfo?.str_accessToken = successDict["access_token"] as! String
     let responseDict:NSDictionary = successDict["response"] as! NSDictionary
     print(responseDict)
     
     let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
     objAppDelgate.loadSidePanelWithScan(objVC: self.navigationController!)
     
     if (responseDict.value(forKey: "verify_status") as? String) == "0" {
     
     //let rootVC : VerifyOTPVC = VerifyOTPVC(nibName: "VerifyOTPVC", bundle: nil)
     //self.navigationController?.pushViewController(rootVC, animated: true)
     
     } else{
     //let rootVC : HomeVC = HomeVC(nibName: "HomeVC", bundle: nil)
     //self.navigationController?.pushViewController(rootVC, animated: true)
     }
     }else{
     DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
     }
     
     
     
     
     
     // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
     }) { (dictFailure) in
     if (dictFailure.value(forKey: "message") as? String) != nil {
     DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
     }
     }
     
     
     
     
     }
     
     
     
     
     */
    
    
    
    
    
    
}
