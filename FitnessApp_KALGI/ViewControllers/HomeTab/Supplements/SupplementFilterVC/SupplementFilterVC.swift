//
//  SupplementFilterVC.swift
//  FitnessApp
//
//  Created by Ashish on 28/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//




import UIKit
import Alamofire

protocol ProtienFilterdelegate:class {
    func filter_button_clicked(value:String)
}



class SupplementFilterVC: UIViewController , UITableViewDelegate, UITableViewDataSource  {
    
    /*
     "1"Protein
     "2"Amino acids
     "3"Weight control
     "4"Vitamins
     "5"Performance
     "6"Total life changes
     "7"Other
     
     */
    fileprivate var array_title:[String] = ["All", "Protein", "Amino acids", "Weight control", "Vitamins", "Performance", "Total life changes", "Other"]
    fileprivate var array_Value:[String] = ["8", "1", "2", "3", "4", "5", "6", "7"]
    @IBOutlet weak var tableview_parks: UITableView!
    var productCount:Int = 0
    var totalCost:Double = 0.00
    var obj_selected_address:ShippingAddressModel? = nil
    //var obj_accessoriesModel:AccessoriesDetailModel! = AccessoriesDetailModel()
    //var obj_protienDetailModel:ProtienDetailModel! = ProtienDetailModel()
    var isfromProtien:Bool = false
    weak var delegate: ProtienFilterdelegate?
    var dict_item:NSDictionary!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview_parks.delegate  = self as UITableViewDelegate
        tableview_parks.dataSource = self as UITableViewDataSource

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //   self.navigationController?.isNavigationBarHidden = true
    }
 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func filter_btn_clicked(_ sender: Any) {
        let str = array_Value[productCount]
        self.delegate?.filter_button_clicked(value: str)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func close_btn_clickrd(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 44
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.array_title.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "SupplementFilterCell"
        var logincell: SupplementFilterCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? SupplementFilterCell
        if logincell == nil {
            tableView.register(UINib(nibName: "SupplementFilterCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? SupplementFilterCell)!
        }
        logincell?.lbl_title.text = array_title[indexPath.row]
        logincell?.imageview_radio.image = UIImage(named: "radiodeselected_btn")
        return logincell!
    }
    // 209, 9, 4
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:SupplementFilterCell = tableView.cellForRow(at: indexPath) as! SupplementFilterCell
       // cell.view_container.backgroundColor = UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
       // obj_selected_address = (array_address[indexPath.row] as! ShippingAddressModel)
        productCount = indexPath.row
        cell.imageview_radio.image = UIImage(named: "radioselected_btn")
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell:SupplementFilterCell = tableView.cellForRow(at: indexPath) as! SupplementFilterCell
       // cell.view_container.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 0.25)
        cell.imageview_radio.image = UIImage(named: "radiodeselected_btn")
    }
}
