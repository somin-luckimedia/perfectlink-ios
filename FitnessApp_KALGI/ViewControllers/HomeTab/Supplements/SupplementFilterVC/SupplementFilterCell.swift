//
//  SupplementFilterCell.swift
//  FitnessApp
//
//  Created by Ashish on 28/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class SupplementFilterCell: UITableViewCell {

    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var imageview_radio: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
