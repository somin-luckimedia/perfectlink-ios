//
//  TrainerGalleryCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 19/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit
import AVFoundation


class TrainerGalleryCell: UICollectionViewCell {
    
    @IBOutlet weak var view_inner: UIView!
    @IBOutlet weak var imageview_thumb: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //view_inner.layer.shadowColor = UIColor.black.cgColor
        //view_inner.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        //view_inner.layer.shadowOpacity = 0.4
        //view_inner.layer.shadowRadius = 6.0
        //layer.masksToBounds = true
        // view_inner.dropShadow()
        // Initialization code
    }
    
   
    
    
    func updateCell(model:GalleryImageModel){
        let url = URL(string: model.str_image)
        imageview_thumb.setImageFrom(url)
    }
}
