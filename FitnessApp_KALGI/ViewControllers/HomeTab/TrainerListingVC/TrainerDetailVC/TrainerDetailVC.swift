//
//  TrainerDetailVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 21/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit
import Alamofire

class TrainerDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, TrainerDetailLowerCellDelegate, TrainerDetailCellDelegate, TrainerAddressHeaderDelegate, IQActionSheetPickerViewDelegate  {
    func follow_btn_clicked(cell: TrainerDetailCell, btn: UIButton)
    {
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        
        let postParam = ["trainer_id" : str_trainer_id ,"user_id" : str_userid] as [String : Any]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.trainerFollow, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
//            self.vwRate.isHidden = true
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                }))
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    @IBAction func btnRateSubmitTapped(_ sender: UIButton) {
        strRate =  String(format: "%.f", CGFloat(Float(obj_trainer_detail.str_rating)!))
        print(strRate)
        self.getAddRate()
    }

    @IBAction func btnRateCancelTappped(_ sender: UIButton) {
        self.vwRate.isHidden = true
    }
    func getAddRate()
    {
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
 
        let postParam = ["trainer_id":str_trainer_id,"type":"TRAINER","rating" : strRate] as [String : Any]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.add_star_rating, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            self.vwRate.isHidden = true
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                }))

                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }

    
    func about_edit_clicked(cell: TrainerDetailLowerCell, btn: UIButton) {
        
    }
    
    func video_edit_clicked(cell: TrainerDetailLowerCell, btn: UIButton) {
        
    }
    
    func achievements_edit_clicked(cell: TrainerDetailLowerCell, btn: UIButton) {
        
    }
    
    func membership_edit_clicked(cell: TrainerDetailLowerCell, btn: UIButton) {
        
    }
    
    func profile_edit_clicked(cell: TrainerDetailCell, btn: UIButton) {
        
    }
    
    func address_edit_clicked() {
        
    }
    

    
    
    @IBOutlet weak var tableview_trainerHome: UITableView!
    var obj_gymDetail:GymDetailModel! = GymDetailModel()
    var str_userType:String = "2"
    var logincell: TrainerDetailCell!
    var str_gymId:String!
    var obj_trainer_detail:TrainerDetailModel!
    var obj_lowercell: TrainerDetailLowerCell!
    var str_trainer_id:String = ""
    var memberCount:Int = 1
    var str_title:String = ""
    var str_start_date:String = DataUtil.stringFromDate(date: Date(), DateFormate: "yyyy-MM-dd")
    var str_start_time:String = ""
    var str_membership_type:String = "SINGLE"
    var totalcost:Int = 0
    var strRate = ""
    @IBOutlet var vwRate: UIView!
    @IBOutlet var rateSubmitView: DYRateView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.title = str_title
        tableview_trainerHome.delegate  = self as UITableViewDelegate
        tableview_trainerHome.dataSource = self as UITableViewDataSource
        tableview_trainerHome.rowHeight = 525
        tableview_trainerHome.estimatedRowHeight = UITableView.automaticDimension
        self.tableview_trainerHome.isHidden = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.vwRate.isHidden = true
        self.addLeftMenuButtonWithImage()
        self.getMyBookings()
        //TrainerProfileVC
        //self.addLeftMenuButtonWithImage()
        //self.getMyBookings()
        
        
    }
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnShareTapped(_ sender: UIButton) {
        
//        let obj_requestmodal:WallListModel = array_mybookings[cell.rowIndex] as! WallListModel
        let str_message:String = String(format: "Trainer detail")
        let name = NSURL(string: str_title)
        let objectsToShare = [str_message, name!] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @IBAction func btnRateTapped(_ sender: UIButton) {
        self.vwRate.isHidden = false
        rateSubmitView.alignment = RateViewAlignmentCenter
        rateSubmitView.editable = true
        self.rateSubmitView.rate = 0
    }
    
    func bookingValidation() -> Bool {
        if self.str_start_date == ""{
            DataUtil.alertMessage("Please choose a date", viewController: self)
            return false
        }
        
        if self.str_start_time == ""{
            DataUtil.alertMessage("Please choose a time", viewController: self)
            return false
        }
        
        if self.str_membership_type == ""{
            DataUtil.alertMessage("Please choose plan", viewController: self)
            return false
        }
        return true
    }
    
    
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getMyBookings(){
        
        
        let postParam: Parameters = ["trainer_user_id":str_trainer_id]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.trainer_detail, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"
            {
                self.tableview_trainerHome.isHidden = false
                self.obj_trainer_detail = TrainerDetailModel(dictUserInfo: successDict["response"] as! NSDictionary)
            }
            else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
            self.tableview_trainerHome.reloadData()
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // self.navigationController?.navigationBar.isHidden = true
        //   self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if section == 1{
//            return 35
//        }
        return 0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header:TrainerHomeSectionHeader =  UINib(nibName: "TrainerHomeSectionHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TrainerHomeSectionHeader
        header.btn_edit.isHidden = true
        header.delegate = self
        return header
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0{
            return 480
            
            
        }else{
            return UITableView.automaticDimension
        }
        
        
        // return 710
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if obj_trainer_detail == nil{
            return 0
        }
        
//        if section == 1{
//            return obj_trainer_detail!.array_locations.count
//        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        ////TrainerHomeAddressCell
        if indexPath.section == 0{
            let identifier = "TrainerDetailCell"
            logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerDetailCell
            if logincell == nil {
                tableView.register(UINib(nibName: "TrainerDetailCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerDetailCell
            }
            logincell.delegate = self
            logincell.updateCellwithModal(obj_gymDetail:obj_trainer_detail!)
            return logincell
//        }else if indexPath.section == 1{
//            let identifier = "TrainerHomeAddressCell"
//            var homeCell: TrainerHomeAddressCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerHomeAddressCell
//            if homeCell == nil {
//                tableView.register(UINib(nibName: "TrainerHomeAddressCell", bundle: nil), forCellReuseIdentifier: identifier)
//                homeCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerHomeAddressCell
//            }
//
//            let obj_locationModel:LocationServiceModel = obj_trainer_detail!.array_locations[indexPath.row] as! LocationServiceModel
//            homeCell.updateCellwithModal(obj_location_model:obj_locationModel)
//            //  homeCell.delegate = self
//            return homeCell
        }
            else{
            let identifier = "TrainerDetailLowerCell"
            obj_lowercell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerDetailLowerCell
            if obj_lowercell == nil {
                tableView.register(UINib(nibName: "TrainerDetailLowerCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_lowercell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerDetailLowerCell
            }
            obj_lowercell.obj_homecontroller = self
            obj_lowercell.updateCellwithModal(obj_gymDetail:obj_trainer_detail!)
            obj_lowercell.delegate = self
            obj_lowercell.lbl_count.text = String(format: "%ld", memberCount)
            return obj_lowercell
        }
    }
    
    //    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
    //        return UITableViewAutomaticDimension
    //    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Hello delegate")
    }
    
//    func address_edit_clicked() {
//        //HomeVC
//        let obj_phonelistvc:TrainerHomeAddressVC = TrainerHomeAddressVC(nibName: "TrainerHomeAddressVC", bundle: nil)
//        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
//        
//        //TrainerHomeAddressVC
//    }
//    
//    func about_edit_clicked(cell: TrainerDetailLowerCell, btn: UIButton) {
//        //TrainerHomeAboutUs
//        let obj_phonelistvc:TrainerHomeAboutUs = TrainerHomeAboutUs(nibName: "TrainerHomeAboutUs", bundle: nil)
//        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
//        
//    }
//    
//    func video_edit_clicked(cell: TrainerDetailLowerCell, btn: UIButton) {
//        let obj_phonelistvc:TrainerVideoVC = TrainerVideoVC(nibName: "TrainerVideoVC", bundle: nil)
//        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
//    }
//    
//    func achievements_edit_clicked(cell: TrainerDetailLowerCell, btn: UIButton) {
//        //AchievementListVC
//        let obj_phonelistvc:AchievementListVC = AchievementListVC(nibName: "AchievementListVC", bundle: nil)
//        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
//    }
//    
//    func membership_edit_clicked(cell: TrainerDetailLowerCell, btn: UIButton) {
//        //TrainerHomeMembershiprate
//        let obj_phonelistvc:TrainerHomeMembershiprate = TrainerHomeMembershiprate(nibName: "TrainerHomeMembershiprate", bundle: nil)
//        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
//    }
//    
//    func profile_edit_clicked(cell: TrainerDetailCell, btn: UIButton) {
//        let obj_phonelistvc:TrainerProfileVC = TrainerProfileVC(nibName: "TrainerProfileVC", bundle: nil)
//        obj_phonelistvc.isFromHome = true
//        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
//    }
    
    func gallery_button_clicked(cell: TrainerDetailCell, btn: UIButton){
        let obj_phonelistvc:TrainerDetailGalleryVC = TrainerDetailGalleryVC(nibName: "TrainerDetailGalleryVC", bundle: nil)
        obj_phonelistvc.obj_trainer_detail = self.obj_trainer_detail
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
    
    
    func date_btn_clicked(cell: TrainerDetailLowerCell, btn: UIButton) {
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.actionSheetPickerStyle = .datePicker
        picker.delegate = self
        picker.tag = 2
        let date:Date =  Date()
        let calendar = Calendar.current
        let date_today:Date = calendar.date(byAdding: .day, value: 7, to: date)!
        picker.minimumDate = date
        picker.show()
    }
    
    func time_btn_clicked(cell: TrainerDetailLowerCell, btn: UIButton) {
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        
        picker.actionSheetPickerStyle = .timePicker
        picker.delegate = self
        picker.tag = 1
        let date:Date =  Date()
        let calendar = Calendar.current
        let date_today:Date = calendar.date(byAdding: .minute, value: 15, to: date)!
        //picker.minimumDate = date_today
        picker.show()
    }
    
    func book_btn_clicked(cell: TrainerDetailLowerCell, btn: UIButton){
        //OrderGymSummaryVC
        //obj_susbcription_dict
        if !bookingValidation() {
            return
        }
        let obj_shippingAddressVC:OrderTrainerSummaryVC = OrderTrainerSummaryVC(nibName: "OrderTrainerSummaryVC", bundle: nil)
        obj_shippingAddressVC.obj_gymdetail = self.obj_trainer_detail
        obj_shippingAddressVC.str_member_count = self.memberCount
        obj_shippingAddressVC.str_start_date = self.str_start_date
        obj_shippingAddressVC.str_start_time = self.str_start_time
        obj_shippingAddressVC.str_membership_type = self.str_membership_type
        self.navigationController?.pushViewController(obj_shippingAddressVC, animated: true)
    }
    
    
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView, didSelect date: Date) {
        if pickerView.tag == 1{
            let  str_time:String = DataUtil.stringFromDate(date: date, DateFormate: "HH:mm")
            obj_lowercell.lbl_time.text =  DataUtil.stringFromDate(date: date, DateFormate: "hh:mm a")
            self.str_start_time = DataUtil.stringFromDate(date: date, DateFormate: "hh:mm a")
        }else{
            //DataUtil.stringFromDate(date: date_today, DateFormate: "yyyy-MM-dd")
            let str_date:String = DataUtil.stringFromDate(date: date, DateFormate: "yyyy-MM-dd")
            obj_lowercell.lbl_date.text = str_date
            self.str_start_date = str_date
            // self.lbl_time.text = str_time
        }
        
        
    }
    
    /*
     */
    
    func decrease_btn_clicked(cell: TrainerDetailLowerCell, btn: UIButton) {
        if memberCount != 1{
            memberCount = memberCount - 1
            obj_lowercell.lbl_count.text = String(format: "%ld", memberCount)
        }
        self.calculate_member_type()
    }
    
    func increase_btn_clicked(cell: TrainerDetailLowerCell, btn: UIButton) {
        if memberCount != 10{
            memberCount = memberCount + 1
            obj_lowercell.lbl_count.text = String(format: "%ld", memberCount)
        }
        self.calculate_member_type()
    }
    
    func calculate_member_type(){
        if self.memberCount > self.obj_trainer_detail.str_count{
            self.str_membership_type = "GROUP"
        }else{
            self.str_membership_type = "SINGLE"
        }
        
        
        
    }
    
    func single_btn_clicked(str_type: String, btn: UIButton){
       self.str_membership_type = str_type
    }
    
}
