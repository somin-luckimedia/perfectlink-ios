//
//  TrainerHomeAddressCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 12/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class TrainerHomeAddressCell: UITableViewCell {

    @IBOutlet weak var lbl_parkname: UILabel!
    @IBOutlet weak var lbl_parkAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
      func updateCellwithModal(obj_location_model:LocationServiceModel){
        lbl_parkname.text = obj_location_model.str_park_name
        lbl_parkAddress.text = obj_location_model.str_park_address
      }
    
    func updateCellwithModalHome(obj_location_model:HomeServiceLocation){
        lbl_parkname.text = obj_location_model.str_name
        lbl_parkAddress.text = obj_location_model.str_address
    }
    
}
