//
//  TrainerHomeCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 25/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//


import UIKit
import Foundation
import Photos
import AVFoundation
import AVKit
import Alamofire


protocol TrainerDetailCellDelegate:class {
    func profile_edit_clicked(cell: TrainerDetailCell, btn: UIButton)
    func gallery_button_clicked(cell: TrainerDetailCell, btn: UIButton)
    func follow_btn_clicked(cell: TrainerDetailCell, btn: UIButton)

}


class TrainerDetailCell: UITableViewCell {
    
    

    @IBOutlet weak var imageView_cover: UIImageView!
    @IBOutlet weak var rateView: DYRateView!
    @IBOutlet weak var lbl_rate: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_gender: UILabel!
    @IBOutlet weak var lbl_followers: UILabel!
    @IBOutlet weak var lbl_videos: UILabel!
    @IBOutlet weak var lbl_courses: UILabel!

    @IBOutlet weak var lbl_hourly_rate: UILabel!
    weak var delegate: TrainerDetailCellDelegate?
    
    @IBOutlet weak var lbl_imagecount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateCellwithModal(obj_gymDetail:TrainerDetailModel){
        let url = URL(string: obj_gymDetail.str_coverImage)
        imageView_cover.setImageFrom(url)
        rateView.rate = CGFloat(Float(obj_gymDetail.str_rating)!)
        lbl_rate.text = obj_gymDetail.str_rating
        lbl_title.text = obj_gymDetail.name
        lbl_followers.text = "\(obj_gymDetail.follower_count)"
        lbl_videos.text = "\(obj_gymDetail.video_count)"
        lbl_courses.text = "\(obj_gymDetail.courses_count)"

        
//        lbl_gender.text = String(format: "%@, %@ years", obj_gymDetail.gender!, obj_gymDetail.str_age)
//        lbl_imagecount.text = String(format: "%ld", obj_gymDetail.array_gallery_image.count)
       // lbl_hourly_rate.text = String(format: "%@ $/hr", obj_gymDetail.str_single_rate)
    }
    
    @IBAction func edit_btn_clicked(_ sender: UIButton) {
        self.delegate?.profile_edit_clicked(cell: self, btn: sender)
    }
    
    
    @IBAction func gallery_btn_clicked(_ sender: UIButton) {
        self.delegate?.gallery_button_clicked(cell: self, btn: sender)
    }
    @IBAction func follow_btn_clicked(_ sender: UIButton) {
        self.delegate?.follow_btn_clicked(cell: self, btn: sender)
    }
    
    
}






