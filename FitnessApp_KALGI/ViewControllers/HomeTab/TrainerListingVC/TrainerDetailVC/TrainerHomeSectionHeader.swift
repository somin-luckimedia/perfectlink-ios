//
//  TrainerHomeSectionHeader.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 12/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

protocol TrainerAddressHeaderDelegate:class {
    func address_edit_clicked()
}


class TrainerHomeSectionHeader: UIView {
 weak var delegate: TrainerAddressHeaderDelegate?
 @IBOutlet weak var btn_edit: UIButton!
    @IBOutlet weak var lbl_edit: UILabel!
    
    @IBAction func edit_btn_clicked(_ sender: Any) {
        self.delegate?.address_edit_clicked()
        
    }
    
    

}
