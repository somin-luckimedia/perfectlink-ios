//
//  TrainerHomeLowerCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 12/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import Foundation
import Photos
import AVFoundation
import AVKit
import Alamofire




protocol TrainerDetailLowerCellDelegate:class {
    func about_edit_clicked(cell: TrainerDetailLowerCell, btn: UIButton)
    func video_edit_clicked(cell: TrainerDetailLowerCell, btn: UIButton)
    func achievements_edit_clicked(cell: TrainerDetailLowerCell, btn: UIButton)
    func membership_edit_clicked(cell: TrainerDetailLowerCell, btn: UIButton)
    
    func date_btn_clicked(cell: TrainerDetailLowerCell, btn: UIButton)
    func time_btn_clicked(cell: TrainerDetailLowerCell, btn: UIButton)
    
    func decrease_btn_clicked(cell: TrainerDetailLowerCell, btn: UIButton)
    func increase_btn_clicked(cell: TrainerDetailLowerCell, btn: UIButton)
    
    func book_btn_clicked(cell: TrainerDetailLowerCell, btn: UIButton)
    
    func single_btn_clicked(str_type: String, btn: UIButton)
}

class TrainerDetailLowerCell: UITableViewCell {
    
    @IBOutlet var constHeightAchievement: NSLayoutConstraint!
    
    private let spacing:CGFloat = 5.0

    @IBOutlet var collAchievements: UICollectionView!
    @IBOutlet weak var collectionview_equipments: UICollectionView!
    @IBOutlet weak var lbl_aboutTitle: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_achievement: UILabel!
    var obj_homecontroller:TrainerDetailVC? = nil
    
    @IBOutlet weak var lbl_singleRate: UILabel!
    @IBOutlet weak var lbl_double_rate: UILabel!
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var view_count: UIView!
    
    @IBOutlet weak var lbl_disclaimer: UILabel!
    
    @IBOutlet weak var view_single: UIView!
    @IBOutlet weak var view_group: UIView!
    @IBOutlet weak var lbl_date: UITextField!
    @IBOutlet weak var lbl_time: UITextField!
    weak var delegate: TrainerDetailLowerCellDelegate?
    var obj_gymdetail:TrainerDetailModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateCellwithModal(obj_gymDetail:TrainerDetailModel){
        //self.view_count.isHidden = true
        let str_start_date:String = DataUtil.stringFromDate(date: Date(), DateFormate: "yyyy-MM-dd")
        lbl_date.text = str_start_date
        self.obj_gymdetail = obj_gymDetail
        self.view_group.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 0.25)
        self.view_single.backgroundColor = UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
        
        self.collAchievements.register(UINib(nibName: "AchievementCVC", bundle: nil), forCellWithReuseIdentifier: "AchievementCVC")
        
        self.collectionview_equipments.register(UINib(nibName: "HomeVideoThumbCell", bundle: nil), forCellWithReuseIdentifier: "HomeVideoThumbCell")

        self.lbl_disclaimer.text = String(format: "Upto %ld persons $%@/Person/hr price will applicable. If your planning for more than 3 person price will be reduce to $%@/person/hr.", obj_gymDetail.str_count, obj_gymDetail.str_single_rate, obj_gymDetail.str_gruop_rate)
        lbl_aboutTitle.text = String(format:"About %@", obj_gymDetail.name!)

        lbl_description.text = obj_gymdetail.str_about
        var str_achievement:String = ""
        for obj_model in obj_gymDetail.array_achievements{
            let obj_achievments:AchievementsModel = obj_model as! AchievementsModel
            str_achievement.append(String(format: " \u{2022} %@ \n\n", obj_achievments.str_achievement))
        }
        /*
         
         str_gruop_rate
         str_single_rate
         */
        
        lbl_achievement.text = str_achievement
        self.lbl_singleRate.text = String(format: "$%@", obj_gymDetail.str_single_rate)
        self.lbl_double_rate.text = String(format: "$%@", obj_gymDetail.str_gruop_rate)
        
        self.collectionview_equipments.reloadData()
        let height = self.collAchievements.collectionViewLayout.collectionViewContentSize.height
//        self.constHeightAchievement.constant = height
        self.constHeightAchievement.constant = height + 40
        self.collAchievements.setNeedsLayout()
        self.collAchievements.reloadData()
    }
    

    
    @IBAction func about_edit_clicked(_ sender: UIButton) {
        self.delegate?.about_edit_clicked(cell: self, btn: sender)
    }
    
    @IBAction func video_edit_clicked(_ sender: UIButton) {
        self.delegate?.video_edit_clicked(cell: self, btn: sender)
    }
    
    @IBAction func achievements_edit_clicked(_ sender: UIButton) {
        self.delegate?.achievements_edit_clicked(cell: self, btn: sender)
    }
    
    @IBAction func membership_edit_clicked(_ sender: UIButton) {
        self.delegate?.membership_edit_clicked(cell: self, btn: sender)
    }
    
     @IBAction func preffered_date_clicked(_ sender: UIButton) {
        self.delegate?.date_btn_clicked(cell: self, btn: sender)
    }
    
     @IBAction func preffered_time_clicked(_ sender: UIButton) {
        self.delegate?.time_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func single_rate_clicked(_ sender: UIButton) {
        self.view_group.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 0.25)
        self.view_single.backgroundColor = UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
        //self.view_count.isHidden = true
        //GROUP, SINGLE
        self.delegate?.single_btn_clicked(str_type: "SINGLE", btn: sender)
    }
    
    @IBAction func group_rate_clicked(_ sender: UIButton) {
        self.view_single.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 0.25)
        self.view_group.backgroundColor = UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
        //self.view_count.isHidden = false
        self.delegate?.single_btn_clicked(str_type: "GROUP", btn: sender)
    }
    
    
    @IBAction func decrease_clicked(_ sender: UIButton) {
        self.delegate?.decrease_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func increase_clicked(_ sender: UIButton) {
        self.delegate?.increase_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func book_btn_clicked(_ sender: UIButton) {
        self.delegate?.book_btn_clicked(cell: self, btn: sender)
    }
   
    
 
    
}




extension TrainerDetailLowerCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collAchievements {
            return self.obj_gymdetail.array_achievements.count
        }
        return self.obj_gymdetail.array_videos.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collAchievements {
            let cell:AchievementCVC = collectionView.dequeueReusableCell(withReuseIdentifier: "AchievementCVC", for: indexPath) as! AchievementCVC
            
//            cell.lblAchievements.text =
            let obj_videoModel:AchievementsModel = self.obj_gymdetail.array_achievements[indexPath.row] as! AchievementsModel
            cell.lblAchievements.text = obj_videoModel.str_achievement
//           cell.updateCellwithModal(model: obj_videoModel)
            return cell
        }
        else {
        let cell:HomeVideoThumbCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeVideoThumbCell", for: indexPath) as! HomeVideoThumbCell
        let obj_videoModel:VideosModel = self.obj_gymdetail.array_videos[indexPath.row] as! VideosModel
       cell.updateCellwithModal(model: obj_videoModel)
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collAchievements {
        let numberOfItemsPerRow:CGFloat = 2
        let spacingBetweenCells:CGFloat = 0
            let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        // if let collection = self.collectionData{
        let width = (collAchievements.bounds.width - totalSpacing)/numberOfItemsPerRow
        return CGSize(width: width, height: 60)
        }
        else {
            return CGSize(width: collectionview_equipments.bounds.width,height: 200)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if obj_homecontroller != nil{
            let obj_videoModel:VideosModel = self.obj_gymdetail.array_videos[indexPath.row] as! VideosModel
            let fileUrl = URL(string: obj_videoModel.str_video)
            let player = AVPlayer(url: fileUrl!)
            let playerController = AVPlayerViewController()
            playerController.player = player
            obj_homecontroller!.present(playerController, animated: true) {
                player.play()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    
    
}

