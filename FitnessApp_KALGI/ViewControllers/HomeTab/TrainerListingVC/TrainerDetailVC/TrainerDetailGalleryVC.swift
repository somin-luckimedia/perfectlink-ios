//
//  TrainerDetailGalleryVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 21/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//


import UIKit
import Foundation
import Photos
import AVFoundation
import AVKit
import Alamofire

class TrainerDetailGalleryVC: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate, ImageAddDelegate {
    func Imageadded_clicked() {
        self.collectioneview_video.reloadData()
    }
    
    
    
    @IBOutlet weak var collectioneview_video: UICollectionView!
    var selectedImage:UIImage?
    var obj_trainer_detail:TrainerDetailModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Photo Gallery"
        self.collectioneview_video.register(UINib(nibName: "TrainerGalleryCell", bundle: nil), forCellWithReuseIdentifier: "TrainerGalleryCell")
        self.collectioneview_video.reloadData()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.addLeftMenuButtonWithImage()
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

extension TrainerDetailGalleryVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return obj_trainer_detail!.array_gallery_image.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:TrainerGalleryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrainerGalleryCell", for: indexPath) as! TrainerGalleryCell
        let obj_videoModel:GalleryImageModel = obj_trainer_detail!.array_gallery_image[indexPath.row] as! GalleryImageModel
        cell.updateCell(model: obj_videoModel)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenwidth:CGFloat = UIScreen.main.bounds.size.width/2
        return CGSize(width: screenwidth, height: screenwidth)
    }
    
    
    //MealsVC
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let obj_videoModel:GalleryImageModel = obj_trainer_detail!.array_gallery_image[indexPath.row] as! GalleryImageModel
        let obj_parkeAddressVC:ImageViewerVC = ImageViewerVC(nibName: "ImageViewerVC", bundle: nil)
        let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
        obj_parkeAddressVC.delegate = self
        flagChallengeImageviewer = false
        obj_parkeAddressVC.obj_locationModel = obj_videoModel
        obj_parkeAddressVC.isViewer = true
        obj_parkeAddressVC.isDelete = false
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    
    
}


