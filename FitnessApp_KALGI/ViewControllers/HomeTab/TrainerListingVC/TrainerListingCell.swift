//
//  TrainerListingCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 21/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit

class TrainerListingCell: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var rateView: DYRateView!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var imageView_thumb: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_bookingmodal:TrainerListModel){
        lbl_title.text = String(format: "%@", obj_bookingmodal.str_name!)
        lbl_description.text = String(format: "%@", obj_bookingmodal.str_about!)
        lbl_distance.text = String(format: "%@ Miles", obj_bookingmodal.str_distance!)
//        imageView_thumb.sd_setImage(with: URL(string: (obj_bookingmodal.str_profile_image)), placeholderImage: UIImage(named: "appLogo"))
        let url = URL(string: obj_bookingmodal.str_profile_image)
        imageView_thumb.setImageFrom(url)
        rateView.rate = CGFloat(Float(obj_bookingmodal.str_star_rating)!)
        
//        rateView.rate = obj_bookingmodal.str_star_rating.floatValue()!
//        rateView.rate = CGFloat(obj_bookingmodal.str_star_rating)
    }
    
    
    
}

