//
//  TrainerListingVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 21/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//




import UIKit
import Alamofire
import Firebase

class TrainerListingVC: UIViewController , UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, SliderFilterdelegate, ParkAddressFilterDelegate  {
    @IBOutlet var collTrainers: UICollectionView!
    func done_btn_clicked(str_parks: String) {
        self.getMyBookings(str_radius: "30", str_park_id: str_parks)
    }
    
    @IBOutlet var btnFreeRun: UIButton!
    @IBOutlet var btnHomeService: UIButton!
    @IBOutlet var btnParkLinks: UIButton!
    
    fileprivate var array_mybookings:NSMutableArray = NSMutableArray()
    @IBOutlet weak var imageView_BG: UIImageView!
    @IBOutlet weak var tableview_bookings: UITableView!
    @IBOutlet weak var lbl_message: UILabel!
    var str_trainer_category:String = "1"
    var str_title:String = ""
    var is_homeService:Bool = false
    var str_radius:String! = "30"
    var selectedIndex:Int = 0
    @IBOutlet var tblOnline: UITableView!
    var arrOnlineFriends : NSMutableArray = []
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")


    @IBAction func btnFreeRunTapped(_ sender: UIButton) {
        str_trainer_category = "1"
        getMyBookings(str_radius: "5", str_park_id: "0")
    }
    @IBAction func btnHomeScreenTapped(_ sender: UIButton) {
        str_trainer_category = "2"
        getMyBookings(str_radius: "5", str_park_id: "0")

    }
    @IBAction func btnParkLinksTapped(_ sender: UIButton) {
        str_trainer_category = "3"
        getMyBookings(str_radius: "5", str_park_id: "0")

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.str_title
        tableview_bookings.delegate  = self as UITableViewDelegate
        tableview_bookings.dataSource = self as UITableViewDataSource
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        collTrainers.register(UINib(nibName: "TrainerCVC", bundle: nil), forCellWithReuseIdentifier: "TrainerCVC")
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
//                if snapshot.childrenCount == 1 {
//                    self.constWidthTbl.constant = 0
////                    self.constLeadingColl.constant = 20
//                    self.lblOnline.isHidden = true
//                }
//                else {
//                    self.constWidthTbl.constant = 60
////                    self.constLeadingColl.constant = 0
//                    self.lblOnline.isHidden = false
//                }
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }

                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
//            else {
//                self.constWidthTbl.constant = 0
//                self.constLeadingColl.constant = 10
//
//                self.lblOnline.isHidden = true
//            }
        }
        self.getMyBookings(str_radius:"5", str_park_id: "0")
        // self.imageView_BG.image = DataUtil.setGradientBackgroundImage()
    //self.addLeftMenuButtonWithImage()
    //    self.addRightMenuButtonWithImage()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.getMyBookings()
        super.viewWillAppear(true)
        if self.selectedIndex == 0{
            str_trainer_category = "1"
            getMyBookings(str_radius: "5", str_park_id: "0")
        }else if self.selectedIndex == 1{
            str_trainer_category = "2"
            getMyBookings(str_radius: "5", str_park_id: "0")
        }else{
            str_trainer_category = "3"
            getMyBookings(str_radius: "5", str_park_id: "0")
        }
    }
    
    @IBAction func header_btn_clicked(_ sender: UIButton) {
        let imageview1:UIImageView = self.view.viewWithTag(201) as! UIImageView
        let imageview2:UIImageView = self.view.viewWithTag(202) as! UIImageView
        let imageview3:UIImageView = self.view.viewWithTag(203) as! UIImageView
        let btn1:UIButton = self.view.viewWithTag(101) as! UIButton
        let btn2:UIButton = self.view.viewWithTag(102) as! UIButton
        let btn3:UIButton = self.view.viewWithTag(103) as! UIButton
        btn1.setTitleColor(.lightGray, for: .normal)
        btn2.setTitleColor(.lightGray, for: .normal)
        btn3.setTitleColor(.lightGray, for: .normal)
        sender.setTitleColor(.white, for: .normal)
        
        let imageview_selected:UIImageView = self.view.viewWithTag(sender.tag + 100) as! UIImageView
        imageview1.backgroundColor = UIColor.clear
        imageview2.backgroundColor = UIColor.clear
        imageview3.backgroundColor = UIColor.clear
        imageview_selected.backgroundColor = UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
        if sender.tag == 101{
            self.selectedIndex = 0
            str_trainer_category = "1"
            getMyBookings(str_radius: "5", str_park_id: "0")
        }else if sender.tag == 102{
            self.selectedIndex = 1
            str_trainer_category = "2"
            getMyBookings(str_radius: "5", str_park_id: "0")
        }else{
            self.selectedIndex = 2
            str_trainer_category = "3"
            getMyBookings(str_radius: "5", str_park_id: "0")
        }
        
    }
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "filter_icon"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.filter_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_filter_click(_ sender: UIButton) {
        //let obj_requestmodal:FerryBoatModal = array_ferrylist[cell.index_row!] as! FerryBoatModal
        let obj_ferrylistingVC : SliderVC = SliderVC(nibName: "SliderVC", bundle: nil)
        obj_ferrylistingVC.delegate = self
        obj_ferrylistingVC.str_title = "Trainer Filter"
//        obj_ferrylistingVC.str_current = str_radius
        //obj_ferrylistingVC.obj_boatmodal = obj_requestmodal
        //self.navigationController?.pushViewController(obj_ferrylistingVC, animated: true)
        // Comment by Ashish
        /*let color_obj:UIColor = UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 0.6)
       presentPopup(obj_ferrylistingVC,
                           animated: true,
                           backgroundStyle: .color(color_obj),
                           constraints: [.width(300), .height(225)],
                           transitioning: .zoom,
                           autoDismiss: true,
                           completion: nil)
 */
        
        let color_obj:UIColor = UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 0.2)
        
        presentPopup(obj_ferrylistingVC,
                     animated: true,
                     backgroundStyle: .color(color_obj), // present the popup with a blur effect has background
            constraints: [.width(300), .height(225)], // fix leading edge and the width
            transitioning: .zoom, // the popup come and goes from the left side of the screen
            autoDismiss: true, // when touching outside the popup bound it is not dismissed
            completion: nil)
    }
    @objc func filter_btn_clicked(){
        
        
        if str_trainer_category == "2"{
        //let obj_requestmodal:FerryBoatModal = array_ferrylist[cell.index_row!] as! FerryBoatModal
        let obj_ferrylistingVC : SliderVC = SliderVC(nibName: "SliderVC", bundle: nil)
        obj_ferrylistingVC.delegate = self
        obj_ferrylistingVC.str_title = "Trainer Filter"
        obj_ferrylistingVC.str_current = str_radius
        //obj_ferrylistingVC.obj_boatmodal = obj_requestmodal
        //self.navigationController?.pushViewController(obj_ferrylistingVC, animated: true)
//        let color_obj:UIColor = UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 0.6)
//        presentPopup(obj_ferrylistingVC,
//                     animated: true,
//                     backgroundStyle: .color(color_obj),
//                     constraints: [.width(300), .height(225)],
//                     transitioning: .zoom,
//                     autoDismiss: true,
//                     completion: nil)
            
            
            
            let color_obj:UIColor = UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 0.6)
            presentPopup(obj_ferrylistingVC,
                         animated: true,
                         backgroundStyle: .color(color_obj), // present the popup with a blur effect has background
                constraints: [.width(300), .height(225)], // fix leading edge and the width
                transitioning: .zoom, // the popup come and goes from the left side of the screen
                autoDismiss: true, // when touching outside the popup bound it is not dismissed
                completion: nil)
            
        }else{
            let obj_parkeAddressVC:ParkAddressFilterVC = ParkAddressFilterVC(nibName: "ParkAddressFilterVC", bundle: nil)
            obj_parkeAddressVC.str_park_type = str_trainer_category
            obj_parkeAddressVC.delegate = self
            let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
           // obj_parkeAddressVC.obj_wallpostModel = obj_requestmodal
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
        
    }
    
    func filter_button_clicked(value: String) {
        print("Slider value ", value)
        str_radius = value
        self.getMyBookings(str_radius: value, str_park_id: "0")
    }
    
    func getMyBookings(str_radius:String, str_park_id:String){
//        "25.2858922"
//        var str_longitude:String = "82.981733"
        let strLat:String = DataUtil.appdelegate().str_lattitude
        let strLong:String = DataUtil.appdelegate().str_longitude
//        let postParam: Parameters = ["trainer_category":str_trainer_category,"lat":strLat, "lon":strLong, "radius":str_radius, "park_id":str_park_id]
        let postParam = ["trainer_category":str_trainer_category,"lat":strLat, "lon":strLong, "radius":str_radius, "park_id":str_park_id]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        self.array_mybookings.removeAllObjects()
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.trainer_list, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let dict : NSDictionary = arr as! NSDictionary
                    let array:NSArray = dict["trainer_list"] as! NSArray
                    let array_trainerCount : NSArray = dict["trainer_count"] as! NSArray
                    let dict_trainerCount = array_trainerCount[0] as! NSDictionary
                    let freeRun  = dict_trainerCount["freerun"] as! Int
                    let homeservice = dict_trainerCount["home_service"] as! Int
                    let parklink = dict_trainerCount["parklink"] as! Int
                    
                    let freeRunLabel : String = "Free Run (\(freeRun))"
                    self.btnFreeRun.setTitle(freeRunLabel, for: .normal)
                    
                    let homeserviceLabel : String =  "Home Service (\(homeservice))"
                    self.btnHomeService.setTitle(homeserviceLabel, for: .normal)
                    
                    let parklinkLabel : String = "Park Links (\(parklink))"
                    self.btnParkLinks.setTitle(parklinkLabel, for: .normal)
                    
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:TrainerListModel = TrainerListModel(dictUserInfo: dict)
                        self.array_mybookings.add(obj_bookingmodal)
                    }
                
                    if self.array_mybookings.count == 0{
                        self.lbl_message.isHidden = false
                    }else{
                        self.lbl_message.isHidden = true
                    }
                    self.collTrainers.delegate = self
                    self.collTrainers.dataSource = self
                    self.collTrainers.reloadData()
                  //  self.tableview_bookings.reloadData()
                }else{
                        self.lbl_message.isHidden = false
                    //DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                self.lbl_message.isHidden = false
                //DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == tblOnline {
            return 60
        }
        else {
            return 150
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if tableView == tblOnline {
            return arrOnlineFriends.count
        }
        else {
            return array_mybookings.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblOnline {
            let identifier = "onlineTVCTableViewCell"
            var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
            
            if logincell == nil {
                tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
            }
            logincell?.selectionStyle = .none
            //        logincell?.layer.cornerRadius = 25
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            let url = URL(string: dict["pic"] as! String)
            logincell?.imgOnlineFriends.setImageFrom(url)
            logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
            
            return logincell!
        }
        else {
        let identifier = "TrainerListingCell"
        var logincell: TrainerListingCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerListingCell
        if logincell == nil {
            tableView.register(UINib(nibName: "TrainerListingCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? TrainerListingCell)!
        }
        //logincell!.delegate = self
        let obj_requestmodal:TrainerListModel = array_mybookings[indexPath.row] as! TrainerListModel
        logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal)
            return logincell!
            
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
        if tableView == tblOnline {
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            flagPark = false
            flagGym = false
            flagGallery = false
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            obj_phonelistvc.str_user_id = dict["userId"] as! String
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
//         let obj_requestmodal:TrainerListModel = array_mybookings[indexPath.row] as! TrainerListModel
//        let obj_phonelistvc:TrainerDetailVC = TrainerDetailVC(nibName: "TrainerDetailVC", bundle: nil)
//        obj_phonelistvc.str_trainer_id = obj_requestmodal.str_trainer_id
//        obj_phonelistvc.str_title = obj_requestmodal.str_name
//        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
    
    
    
    /*
     
     
     
     */
    
    
    
    
    
    
    
}

//MARK: Button action methods
extension TrainerListingVC {
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: COLLECTIONVIEW DELEGATE & DATASOURCE METHODS
extension TrainerListingVC : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_mybookings.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : TrainerCVC = collTrainers.dequeueReusableCell(withReuseIdentifier: "TrainerCVC", for: indexPath) as! TrainerCVC
        let obj_requestmodal:TrainerListModel = array_mybookings[indexPath.row] as! TrainerListModel
        let url = URL(string: obj_requestmodal.str_profile_image)
        cell.imgTrainer.setImageFrom(url)
        cell.lblTrainerName.text = obj_requestmodal.str_name
        cell.lblFollowers.text = obj_requestmodal.followers
        cell.lblVideos.text = obj_requestmodal.videos
        cell.lblCourses.text = obj_requestmodal.courses
       // cell.lblVideos.text = obj_requestmodal
        
        cell.rateView.rate = CGFloat(Float(obj_requestmodal.str_star_rating) ?? 0)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let numberOfItemsPerRow:CGFloat = 3
//        let spacingBetweenCells:CGFloat = 0
//
//        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
//
        // if let collection = self.collectionData{
        let height = collTrainers.bounds.height
        return CGSize(width: collTrainers.bounds.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj_requestmodal:TrainerListModel = array_mybookings[indexPath.row] as! TrainerListModel
        let obj_TrainerDetailVC:TrainerDetailVC = TrainerDetailVC(nibName: "TrainerDetailVC", bundle: nil)
        obj_TrainerDetailVC.str_trainer_id = obj_requestmodal.str_trainer_id

//        obj_TrainerDetailVC.dictTrainer = obj_requestmodal
        
        self.navigationController?.pushViewController(obj_TrainerDetailVC, animated: true)

    }
    
}
