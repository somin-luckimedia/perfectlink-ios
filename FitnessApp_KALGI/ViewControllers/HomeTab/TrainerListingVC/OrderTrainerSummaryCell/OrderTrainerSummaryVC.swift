//
//  OrderTrainerSummaryVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 23/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire
import SquareInAppPaymentsSDK


class OrderTrainerSummaryVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, PromocodeCellDelegate, DiscountControllerDelegate, OrderTrainerSummaryCellDelegate, IQActionSheetPickerViewDelegate, OrderViewControllerDelegate, UIViewControllerTransitioningDelegate, SQIPCardEntryViewControllerDelegate  {
    func didRequestPayWithApplyPay() {
        
    }
    

    
    func cardEntryViewController(_ cardEntryViewController: SQIPCardEntryViewController, didObtain cardDetails: SQIPCardDetails, completionHandler: @escaping (Error?) -> Void) {
        guard serverHostSet else {
            printCurlCommand(nonce: cardDetails.nonce)
            completionHandler(nil)
            return
        }
        //        getMyBookings(cardDetails.nonce)
        processPayment(cardDetails.nonce) { (transactionID, errorDescription) in
            guard let errorDescription = errorDescription else {
                // No error occured, we successfully charged
                completionHandler(nil)
                return
            }
            
            // Pass error description
            let error = NSError(domain: "com.example.supercookie", code: 0, userInfo:[NSLocalizedDescriptionKey : errorDescription])
            completionHandler(error)
        }
    }
    
    func processPayment(_ nonce: String, completion: @escaping (String?, String?) -> Void) {
//        self.getMyBookings(nonce)
        let str_id:String = self.obj_gymdetail.str_trainer_user_id
        let str_type:String = "TRAINER"
        
//        showOrderSheet()
        
        
      //  let string:String =    String(format:"user_id=%@&subscription_for=%@&trainer_id=%@&months=%ld&discount_id=%@&start_date=%@&subscription_amount=%.2f&preferred_time=%@&membership_type=%@&member_count=%ld",((DataUtil.appdelegate().objUserInfo?.userId)!),str_type,str_id, str_member_plan,self.str_couponId, self.str_start_date, self.totalCost, self.str_start_time, self.str_membership_type, self.str_member_count)
        let postParam = ["nonce": nonce,"user_id" : ((DataUtil.appdelegate().objUserInfo?.userId)!),"subscription_for" : str_type,"trainer_id" : str_id,"months" : str_member_plan,"discount_id" : self.str_couponId, "start_date" : self.str_start_date,"preferred_time" : self.str_start_time, "membership_type" : self.str_membership_type,"member_count" : self.str_member_count] as [String : Any]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Accept": "application/json"
        ]
        let url = URL(string: ConstantFiles.Square.CHARGE_URL_Trainer)!

        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.Square.CHARGE_URL_Trainer, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
//                self.tableview_detail.isHidden = false
                if let arr = successDict.value(forKey:"response"){
                    
//                    self.didChargeSuccessfully()
                    
                    DispatchQueue.main.async {
                        completion("success", nil)
                    }
                    /* let array:NSArray = arr as! NSArray
                     for item in array {
                     let dict:NSDictionary =  item as! NSDictionary
                     let obj_bookingmodal:GymListModel = GymListModel(dictUserInfo: dict)
                     self.array_mybookings.add(obj_bookingmodal)
                     }*/
                    //GymDetailModel
                    // self.obj_gymDetail = GymDetailModel(dictUserInfo: arr as! NSDictionary)
                    // self.tableview_detail.reloadData()
                }else{
//                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
//                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
//                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
//    func getMyBookings(_ nonce: String){
//        let str_id:String = self.obj_gymdetail.str_trainer_user_id
//        let str_type:String = "TRAINER"
//
////        showOrderSheet()
//
//
//      //  let string:String =    String(format:"user_id=%@&subscription_for=%@&trainer_id=%@&months=%ld&discount_id=%@&start_date=%@&subscription_amount=%.2f&preferred_time=%@&membership_type=%@&member_count=%ld",((DataUtil.appdelegate().objUserInfo?.userId)!),str_type,str_id, str_member_plan,self.str_couponId, self.str_start_date, self.totalCost, self.str_start_time, self.str_membership_type, self.str_member_count)
//        let postParam = ["nonce": nonce,"user_id" : ((DataUtil.appdelegate().objUserInfo?.userId)!),"subscription_for" : str_type,"trainer_id" : str_id,"months" : str_member_plan,"discount_id" : self.str_couponId, "start_date" : self.str_start_date,"preferred_time" : self.str_start_time, "membership_type" : self.str_membership_type,"member_count" : self.str_member_count] as [String : Any]
//        print("Login Post Parameter : \(postParam)")
//        let headers = [
//            "Accept": "application/json"
//        ]
//        let url = URL(string: ConstantFiles.Square.CHARGE_URL_Trainer)!
//
//        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.Square.CHARGE_URL_Trainer, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
//            print(successDict)
//            if (successDict.value(forKey: "STATUS") as? String) == "true"{
////                self.tableview_detail.isHidden = false
//                if let arr = successDict.value(forKey:"response"){
//
////                    self.didChargeSuccessfully()
//
//                    DispatchQueue.main.async {
//                        completion("success", nil)
//                    }
//                    /* let array:NSArray = arr as! NSArray
//                     for item in array {
//                     let dict:NSDictionary =  item as! NSDictionary
//                     let obj_bookingmodal:GymListModel = GymListModel(dictUserInfo: dict)
//                     self.array_mybookings.add(obj_bookingmodal)
//                     }*/
//                    //GymDetailModel
//                    // self.obj_gymDetail = GymDetailModel(dictUserInfo: arr as! NSDictionary)
//                    // self.tableview_detail.reloadData()
//                }else{
////                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
//                }
//
//            }else{
////                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
//            }
//        }) { (dictFailure) in
//            if (dictFailure.value(forKey: "message") as? String) != nil {
////                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
//            }
//        }
//    }
    
    func cardEntryViewController(_ cardEntryViewController: SQIPCardEntryViewController, didCompleteWith status: SQIPCardEntryCompletionStatus) {
        dismiss(animated: true) {
            switch status {
            case .canceled:
                self.showOrderSheet()
                break
            case .success:
                guard self.serverHostSet else {
                    self.showCurlInformation()
                    return
                }
                self.didChargeSuccessfully()
            }
        }
    }
    

    private func didChargeSuccessfully() {
        // Let user know that the charge was successful
//        let alert = UIAlertController(title: "Your order was successful",
//                                      message: "Go to your Square dashbord to see this order reflected in the sales tab.",
//                                      preferredStyle: UIAlertController.Style.alert)
//        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//        present(alert, animated: true, completion: nil)
//        showAlertWithTitleFromVC(vc: self, title: "Your order was successful", andMessage: "Go to your Square dashbord to see this order reflected in the sales tab.", buttons: ["OK"]) { (index) in
//            if index == 0 {
//                self.navigationController?.popToRootViewController(animated: true)
//            }
        imgOrderSuccessful.isHidden = false
    
    }

    
//    func didRequestPayWithApplyPay() {
//        dismiss(animated: true) {
//            self.requestApplePayAuthorization()
//        }
//    }
    
    func didRequestPayWithCard() {
        dismiss(animated: true) {
            let vc = self.makeCardEntryViewController()
            vc.delegate = self

            let nc = UINavigationController(rootViewController: vc)
            self.present(nc, animated: true, completion: nil)
        }
    }
    

    
    

    var obj_accessoriesModel:AccessoriesDetailModel! = AccessoriesDetailModel()
    var obj_protienDetailModel:ProtienDetailModel! = ProtienDetailModel()
    var obj_selected_address:ShippingAddressModel! =  ShippingAddressModel()
    var obj_gymdetail:TrainerDetailModel! = TrainerDetailModel()
    var productCount:Int = 1
    var totalCost:Double = 0.00
    var discount:Double = 0.00
    var str_promocode:String = ""
    var str_couponId:String = ""
    var str_userType:String = "2"
    var obj_orderSummaryCell: OrdarSummaryCell!
    var obj_orderShippingCell: OrderGymSummaryCell!
    var obj_applyCodeCell: ApplyCodeCell!
    var obj_trainerSummaryCell: OrderTrainerSummaryCell?
    var str_gymId:String!
    var isDiscountApplied:Bool = false
    var obj_susbcription_dict:NSDictionary? = nil
    @IBOutlet weak var tableview_detail: UITableView!
    var str_start_date:String = DataUtil.stringFromDate(date: Date(), DateFormate: "yyyy-MM-dd")
    var str_start_time:String = ""
    var str_membership_type:String = "SINGLE"
    var str_member_count:Int = 1
    var str_member_plan:Int =  1
    var str_gruop_count:Int = 1
    var str_membership_rate:String = "0"
    @IBOutlet var imgOrderSuccessful: UIImageView!
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //GROUP, SINGLE
    @IBOutlet weak var view_header: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        tableview_detail.delegate  = self as UITableViewDelegate
        tableview_detail.dataSource = self as UITableViewDataSource
        self.title = "SUBSCRIPTION SUMMARY"
        self.tableview_detail.tableFooterView = self.view_header
//        self.addLeftMenuButtonWithImage()
        //tableview_detail.rowHeight = 700
        //tableview_detail.estimatedRowHeight = UITableViewAutomaticDimension
        self.calculateTotalCost()
        imgOrderSuccessful.isHidden = true

    }
    
    func calculateTotalCost() {
        var g_amount:Double = 0
        if str_membership_type == "SINGLE"{
            g_amount = Double(self.obj_gymdetail.str_single_rate)!
        }else{
            g_amount = Double(self.obj_gymdetail.str_gruop_rate)!
        }
        
        
        self.totalCost = g_amount*Double(str_member_plan)*Double(self.str_member_count)
        tableview_detail.reloadData()
    }
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func discount_btn_clicked(obj_discount: DiscountModel) {
        if obj_discount.str_type == "percentage"{
            if let cost = Double(obj_discount.str_percentage!) {
                self.discount = self.calculatePercentage(value: Double(self.totalCost) , percentageVal: cost)
            } else {
                self.discount = 0
            }
            
        }else{
            self.discount = Double(obj_discount.str_amount)!
        }
        self.str_promocode = obj_discount.str_coupon_code
        self.str_couponId = obj_discount.str_couponId
        self.isDiscountApplied = true
        self.tableview_detail.reloadData()
    }
    
    private var serverHostSet: Bool {
        return ConstantFiles.Square.CHARGE_SERVER_HOST != "REPLACE_ME"
    }
    
//    private var appleMerchanIdSet: Bool {
//        return ConstantFiles.ApplePay.MERCHANT_IDENTIFIER != "REPLACE_ME"
//    }
    
    public func calculatePercentage(value:Double,percentageVal:Double)->Double{
        let val = value * percentageVal
        let valD:Double = val / 100.0
        return valD
    }
    private func showOrderSheet() {
        // Open the buy modal
        let orderViewController = OrderViewController()
        orderViewController.delegate = self
        let nc = OrderNavigationController(rootViewController: orderViewController)
        nc.modalPresentationStyle = .custom
        nc.transitioningDelegate = self
        present(nc, animated: true, completion: nil)
    }
    
    private func printCurlCommand(nonce : String) {
        let uuid = UUID().uuidString
        print("curl --request POST https://connect.squareupsandbox.com/v2/payments \\" +
            "--header \"Content-Type: application/json\" \\" +
            "--header \"Authorization: Bearer YOUR_ACCESS_TOKEN\" \\" +
            "--header \"Accept: application/json\" \\" +
            "--data \'{" +
            "\"idempotency_key\": \"\(uuid)\"," +
            "\"autocomplete\": true," +
            "\"amount_money\": {" +
            "\"amount\": 100," +
            "\"currency\": \"USD\"}," +
            "\"source_id\": \"\(nonce)\"" +
            "}\'");
    }
    @IBAction func continue_btn_clicked(_ sender: UIButton) {
        

        
        
        let str_id:String = self.obj_gymdetail.str_trainer_user_id
        let str_type:String = "TRAINER"
        
        showOrderSheet()
        
        
      //  let string:String =    String(format:"user_id=%@&subscription_for=%@&trainer_id=%@&months=%ld&discount_id=%@&start_date=%@&subscription_amount=%.2f&preferred_time=%@&membership_type=%@&member_count=%ld",((DataUtil.appdelegate().objUserInfo?.userId)!),str_type,str_id, str_member_plan,self.str_couponId, self.str_start_date, self.totalCost, self.str_start_time, self.str_membership_type, self.str_member_count)
 /*       let url = URL(string: "http://perfectlinkfitness.com/api/subscription-payment")
        print("URL: ", string)
        let data = string.data(using: .utf8)
        var request = URLRequest(url: url!)
        //AIzaSyB2AX5rToBHRxVO0HW4ROU-A1He5oGLUZE
        request.httpMethod = "POST"
        request.httpBody =  data
//        let rootVC : PaymentWebVC = PaymentWebVC(nibName: "PaymentWebVC", bundle: nil)
//        rootVC.str_type = str_type
//        rootVC.request = request
//        
//        self.navigationController?.pushViewController(rootVC, animated: true)*/
    }
    
    private func showCurlInformation() {
        let alert = UIAlertController(title: "Nonce generated but not charged",
                                      message: "Check your console for a CURL command to charge the nonce, or replace Constants.Square.CHARGE_SERVER_HOST with your server host.",
                                      preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView, didSelect date: Date) {
        let str_date:String = DataUtil.stringFromDate(date: date, DateFormate: "yyyy-MM-dd")
        obj_orderShippingCell.lbl_date.text = str_date
        str_start_date = str_date
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // self.navigationController?.navigationBar.isHidden = true
        //   self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*  func getMyBookings(){
     let postParam: Parameters = ["gym_id":str_gymId!]
     print("Login Post Parameter : \(postParam)")
     let headers: HTTPHeaders = [
     "Authorization": String(format: "Bearer %@",  (DataUtil.appdelegate().objUserInfo?.str_accessToken)!),
     "Accept": "application/json"
     ]
     ServerCommunication.getDataWithPostHeader(url: ConstantFiles.gym_detail, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
     print(successDict)
     if (successDict.value(forKey: "STATUS") as? String) == "true"{
     if let arr = successDict.value(forKey:"response"){
     /* let array:NSArray = arr as! NSArray
     for item in array {
     let dict:NSDictionary =  item as! NSDictionary
     let obj_bookingmodal:GymListModel = GymListModel(dictUserInfo: dict)
     self.array_mybookings.add(obj_bookingmodal)
     }*/
     //GymDetailModel
     self.obj_gymDetail = GymDetailModel(dictUserInfo: arr as! NSDictionary)
     self.tableview_detail.reloadData()
     }else{
     DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
     }
     
     }else{
     DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
     }
     }) { (dictFailure) in
     if (dictFailure.value(forKey: "message") as? String) != nil {
     DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
     }
     }
     }
     
     */
    
    
    func promocode_btn_clicked(cell: ApplyCodeCell, btn: UIButton) {
        if isDiscountApplied == true{
            self.discount = 0.00
            self.str_promocode = ""
            self.str_couponId = ""
            self.isDiscountApplied = false
            self.tableview_detail.reloadData()
        }else{
            let obj_parkeAddressVC:DiscountsVC = DiscountsVC(nibName: "DiscountsVC", bundle: nil)
            obj_parkeAddressVC.delegate = self
            obj_parkeAddressVC.forPromoCode = true
            
           
            obj_parkeAddressVC.str_type = "2"
            
            let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
        
    }
    
    func plan_btn_clicked(cell: OrderTrainerSummaryCell, btn: UIButton) {
        var groups = [[String]]()
        groups.append(DataUtil.appdelegate().array_member_plan)
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()
    }
    
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView, didSelectTitlesAtIndexes indexes: [NSNumber]) {
            let index = indexes[0] as! NSInteger
            let title:String = DataUtil.appdelegate().array_member_plan[index]
            obj_trainerSummaryCell?.lbl_date.text = title
            self.str_member_plan = index+1
            self.calculateTotalCost()
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 70
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header:OrderSummaryHeaderView =  UINib(nibName: "OrderSummaryHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! OrderSummaryHeaderView
        if section == 1{
            header.lbl_title.text = "Trainer Membership Detail"
        }else if section == 2{
            header.lbl_title.text = "Price Detail"
        }
        
        return header
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0{
            return 120
        }else{
            return 55
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
      
        if section == 1{
            return 4
        }else if section == 2{
            if isDiscountApplied == true{
                return 4
            }else{
                return 3
            }
        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        ////TrainerHomeAddressCell
        if indexPath.section == 0{
            let identifier = "OrdarSummaryCell"
            obj_orderSummaryCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrdarSummaryCell
            if obj_orderSummaryCell == nil {
                tableView.register(UINib(nibName: "OrdarSummaryCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_orderSummaryCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrdarSummaryCell
            }
            obj_orderSummaryCell.updateCellwithModalTrainer(obj_protienModel: self.obj_gymdetail)
            return obj_orderSummaryCell
        }else if indexPath.section == 1{
            if indexPath.row == 1{
            let identifier = "OrderTrainerSummaryCell"
            obj_trainerSummaryCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderTrainerSummaryCell
            if obj_trainerSummaryCell == nil {
                tableView.register(UINib(nibName: "OrderTrainerSummaryCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_trainerSummaryCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderTrainerSummaryCell
            }
                obj_trainerSummaryCell?.lbl_title.text = "Membership Plan"
                var str:String = ""
                if self.str_member_plan == 1{
                    str = String(format: "%ld Day", self.str_member_plan)
                }else{
                    str = String(format: "%ld Days", self.str_member_plan)
                }
                obj_trainerSummaryCell?.lbl_date.text =  str
                obj_trainerSummaryCell?.delegate = self
            return obj_trainerSummaryCell!
                
            }
            let identifier = "SubscriptionMembershipCell"
            var obj_amountcell: SubscriptionMembershipCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? SubscriptionMembershipCell
            if obj_amountcell == nil {
                tableView.register(UINib(nibName: "SubscriptionMembershipCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_amountcell = tableView.dequeueReusableCell(withIdentifier: identifier) as? SubscriptionMembershipCell
            }
            var str_title:String = ""
            var str_value:String = ""
            if indexPath.row == 0{
                str_title = "Start Date"
                str_value = self.str_start_date
            }else if indexPath.row == 2{
                str_title = "Membership Type"
                str_value = self.str_membership_type
            }else{
                str_title = "Member Count"
                str_value = String(format: "%ld", self.str_member_count)
            }
            obj_amountcell?.lbl_title.text = str_title
            obj_amountcell?.lbl_value.text = str_value
            return obj_amountcell!
        }else{
            if indexPath.row == 0{
                let identifier = "ApplyCodeCell"
                obj_applyCodeCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ApplyCodeCell
                if obj_applyCodeCell == nil {
                    tableView.register(UINib(nibName: "ApplyCodeCell", bundle: nil), forCellReuseIdentifier: identifier)
                    obj_applyCodeCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ApplyCodeCell
                }
                if isDiscountApplied == true{
                    obj_applyCodeCell.btn_title.setTitle("Remove Promo Code", for: .normal)
                    obj_applyCodeCell.lbl_promocode.text = self.str_promocode
                }else{
                    obj_applyCodeCell.btn_title.setTitle("Apply Promo Code", for: .normal)
                    obj_applyCodeCell.lbl_promocode.text = ""
                }
                obj_applyCodeCell.delegate = self
                //let obj_locationModel:LocationServiceModel = obj_trainer_detail!.array_locations[indexPath.row] as! LocationServiceModel
                //homeCell.updateCellwithModal(obj_location_model:obj_locationModel)
                //  homeCell.delegate = self
                return obj_applyCodeCell
            }
            
            let identifier = "OrderAmountCell"
            var obj_amountcell: OrderAmountCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderAmountCell
            if obj_amountcell == nil {
                tableView.register(UINib(nibName: "OrderAmountCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_amountcell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderAmountCell)!
            }
            
            if isDiscountApplied{
                if indexPath.row == 1{
                    obj_amountcell?.updateCellwithModal(str_title: "Total Amount", str_value: String(format: "%@%.2f", self.obj_gymdetail.str_currency ,self.totalCost), str_payable: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost))
                }else if indexPath.row == 2{
                    obj_amountcell?.updateCellwithModal(str_title: "Discount", str_value: String(format: "%@%.2f", self.obj_gymdetail.str_currency,self.discount), str_payable: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost))
                }else if indexPath.row == 3{
                    obj_amountcell?.updateCellwithModal(str_title: "Amount Payable", str_value: String(format: "%@%.2f", self.obj_gymdetail.str_currency,self.totalCost-self.discount), str_payable: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost))
                }
            }else{
                if indexPath.row == 1{
                    
                    obj_amountcell?.updateCellwithModal(str_title: "Total Amount", str_value: String(format: "%@%.2f", self.obj_gymdetail.str_currency,self.totalCost), str_payable: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost))
                    
                }else if indexPath.row == 2{
                    obj_amountcell?.updateCellwithModal(str_title: "Amount Payable", str_value: String(format: "%@%.2f", self.obj_gymdetail.str_currency,self.totalCost), str_payable: String(format: "%@%.2f", self.obj_protienDetailModel.str_currency ,self.totalCost))
                }
            }
            return obj_amountcell!
            
        }
    }
    
}

extension OrderTrainerSummaryVC {
    func makeCardEntryViewController() -> SQIPCardEntryViewController {
        // Customize the card payment form
        let theme = SQIPTheme()
        theme.errorColor = .red
        theme.tintColor = UIColor(red: 0.14, green: 0.6, blue: 0.55, alpha: 1)
        theme.keyboardAppearance = .light
        theme.messageColor = UIColor(red: 0.48, green: 0.48, blue: 0.48, alpha: 1)
        theme.saveButtonTitle = "Pay"

        return SQIPCardEntryViewController(theme: theme)
    }
}

/*extension OrderTrainerSummaryVC: PKPaymentAuthorizationViewControllerDelegate {
//    func requestApplePayAuthorization() {
//        guard SQIPInAppPaymentsSDK.canUseApplePay else {
//            return
//        }
//
////        guard appleMerchanIdSet else {
////            showMerchantIdNotSet()
////            return
////        }
//
////        let paymentRequest = PKPaymentRequest.squarePaymentRequest(
////            merchantIdentifier: Constants.ApplePay.MERCHANT_IDENTIFIER,
////            countryCode: Constants.ApplePay.COUNTRY_CODE,
////            currencyCode: Constants.ApplePay.CURRENCY_CODE
////        )
//
//        paymentRequest.paymentSummaryItems = [
//            PKPaymentSummaryItem(label: "Super Cookie", amount: 1.00)
//        ]
//
//        let paymentAuthorizationViewController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
//
//        paymentAuthorizationViewController!.delegate = self
//
//        present(paymentAuthorizationViewController!, animated: true, completion: nil)
//    }

    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController,
                                            didAuthorizePayment payment: PKPayment,
                                            handler completion: @escaping (PKPaymentAuthorizationResult) -> Void){

        // Turn the response into a nonce, if possible
        // Nonce is used to actually charge the card on the server-side
        let nonceRequest = SQIPApplePayNonceRequest(payment: payment)

        nonceRequest.perform { [weak self] cardDetails, error in
            guard let cardDetails = cardDetails else {
                let errors = [error].compactMap { $0 }
                completion(PKPaymentAuthorizationResult(status: .failure, errors: errors))
                return
            }
            
            guard let strongSelf = self else {
                completion(PKPaymentAuthorizationResult(status: .failure, errors: []))
                return
            }
            
            guard strongSelf.serverHostSet else {
                strongSelf.printCurlCommand(nonce: cardDetails.nonce)
                strongSelf.applePayResult = .success
                completion(PKPaymentAuthorizationResult(status: .failure, errors: []))
                return
            }
            
//            ChargeApi.processPayment(cardDetails.nonce) { (transactionId, error) in
//                if let error = error, !error.isEmpty {
//                    strongSelf.applePayResult = Result.failure(error)
//                } else {
//                    strongSelf.applePayResult = Result.success
//                }
//
//                completion(PKPaymentAuthorizationResult(status: .success, errors: nil))
//            }
        }
    }

    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true) {
            switch self.applePayResult {
            case .success:
                guard self.serverHostSet else {
                    self.showCurlInformation()
                    return
                }
                self.didChargeSuccessfully()
            case .failure(let description):
                self.didNotChargeApplePay(description)
                break
            case .canceled:
                self.showOrderSheet()
            }
        }
    }*/

