//
//  OrderTrainerSummaryCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 23/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//


import UIKit

protocol OrderTrainerSummaryCellDelegate:class {
    func plan_btn_clicked(cell: OrderTrainerSummaryCell, btn: UIButton)
}

class OrderTrainerSummaryCell: UITableViewCell {
    weak var delegate:OrderTrainerSummaryCellDelegate?
    @IBOutlet weak var lbl_date: UITextField!
    @IBOutlet weak var lbl_title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    @IBAction func date_btn_clicked(_ sender: UIButton) {
        self.delegate?.plan_btn_clicked(cell: self, btn: sender)
    }
    
}
