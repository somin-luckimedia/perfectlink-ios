//
//  TrainerCVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 21/11/20.
//

import UIKit

class TrainerCVC: UICollectionViewCell {

    @IBOutlet var imgTrainer: UIImageView!
    @IBOutlet var lblTrainerName: UILabel!
    @IBOutlet var rateView: DYRateView!
    @IBOutlet var lblFollowers: UILabel!
    @IBOutlet var lblVideos: UILabel!
    @IBOutlet var lblCourses: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
