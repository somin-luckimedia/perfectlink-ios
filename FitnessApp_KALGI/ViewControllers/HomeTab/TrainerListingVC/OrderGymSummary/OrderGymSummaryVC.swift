//
//  OrderGymSummaryVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 21/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit
import Alamofire

class OrderGymSummaryVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, PromocodeCellDelegate, DiscountControllerDelegate, OrderGymSummaryCellDelegate, IQActionSheetPickerViewDelegate  {
    
    

    var obj_accessoriesModel:AccessoriesDetailModel! = AccessoriesDetailModel()
    var obj_protienDetailModel:ProtienDetailModel! = ProtienDetailModel()
    var obj_selected_address:ShippingAddressModel! =  ShippingAddressModel()
    var obj_gymdetail:GymDetailModel! = GymDetailModel()
    var productCount:Int = 1
    var totalCost:Double = 0.00
    var discount:Double = 0.00
    var str_promocode:String = ""
    var str_couponId:String = ""
    var str_userType:String = "2"
    var obj_orderSummaryCell: OrdarSummaryCell!
    var obj_orderShippingCell: OrderGymSummaryCell!
    var obj_applyCodeCell: ApplyCodeCell!
    var str_gymId:String!
    var isDiscountApplied:Bool = false
    var isfromGym:Bool = false
    var obj_susbcription_dict:NSDictionary? = nil
    @IBOutlet weak var tableview_detail: UITableView!
    var str_start_date:String = DataUtil.stringFromDate(date: Date(), DateFormate: "yyyy-MM-dd")
    
    @IBOutlet weak var view_header: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview_detail.delegate  = self as UITableViewDelegate
        tableview_detail.dataSource = self as UITableViewDataSource
        self.title = "GYM SUBSCRIPTION SUMMARY"
        self.tableview_detail.tableFooterView = self.view_header
        self.addLeftMenuButtonWithImage()
        //tableview_detail.rowHeight = 700
        //tableview_detail.estimatedRowHeight = UITableViewAutomaticDimension
        tableview_detail.reloadData()
        
        
        
        
        //self.getMyBookings()
        
        
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func discount_btn_clicked(obj_discount: DiscountModel) {
        if obj_discount.str_type == "percentage"{
            if let cost = Double(obj_discount.str_percentage!) {
                self.discount = self.calculatePercentage(value: self.totalCost , percentageVal: cost)
            } else {
                self.discount = 0.00
            }
            
        }else{
            self.discount = Double(obj_discount.str_amount)!
        }
        self.str_promocode = obj_discount.str_coupon_code
        self.str_couponId = obj_discount.str_couponId
        self.isDiscountApplied = true
        self.tableview_detail.reloadData()
    }
    
    public func calculatePercentage(value:Double,percentageVal:Double)->Double{
        let val = value * percentageVal
        let valD:Double = val / 100.0
        return valD
    }
    @IBAction func continue_btn_clicked(_ sender: UIButton) {
        var str_id:String = ""
        var str_type:String = ""
        if isfromGym == true{
            str_id = self.obj_gymdetail.str_gym_id
            str_type = "GYM"
        }else{
            str_id = self.obj_accessoriesModel.str_accessorie_id
            str_type = "TRAINER"
        }
        var month:Int = 0
        if obj_susbcription_dict != nil{
            if let result_number = obj_susbcription_dict?.value(forKey: "subscription_month") as? NSNumber{
                month = Int("\(result_number)") ?? 0
            }
        }
            
        
        let string:String =    String(format:"user_id=%@&subscription_for=%@&gym_id=%@&months=%ld&discount_id=%@&start_date=%@&subscription_amount=%.2f",((DataUtil.appdelegate().objUserInfo?.userId)!),str_type,str_id, month,self.str_couponId, self.str_start_date, self.totalCost)

        let url = URL(string: "http://perfectlinkfitness.com/api/subscription-payment")
        print("URL: ", string)
        
        let data = string.data(using: .utf8)
        var request = URLRequest(url: url!)
        //AIzaSyB2AX5rToBHRxVO0HW4ROU-A1He5oGLUZE
        request.httpMethod = "POST"
        request.httpBody =  data
//        let rootVC : PaymentWebVC = PaymentWebVC(nibName: "PaymentWebVC", bundle: nil)
//        rootVC.str_type = str_type
//        rootVC.request = request
//
//        self.navigationController?.pushViewController(rootVC, animated: true)
    }
    
    func date_btn_clicked(cell: OrderGymSummaryCell, btn: UIButton) {
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.actionSheetPickerStyle = .datePicker
        picker.delegate = self
        picker.tag = 2
        let date:Date =  Date()
        let calendar = Calendar.current
        let date_today:Date = calendar.date(byAdding: .day, value: 7, to: date)!
        picker.minimumDate = date
        picker.show()
    }
    
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView, didSelect date: Date) {
            let str_date:String = DataUtil.stringFromDate(date: date, DateFormate: "yyyy-MM-dd")
            obj_orderShippingCell.lbl_date.text = str_date
            str_start_date = str_date
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // self.navigationController?.navigationBar.isHidden = true
        //   self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*  func getMyBookings(){
     let postParam: Parameters = ["gym_id":str_gymId!]
     print("Login Post Parameter : \(postParam)")
     let headers: HTTPHeaders = [
     "Authorization": String(format: "Bearer %@",  (DataUtil.appdelegate().objUserInfo?.str_accessToken)!),
     "Accept": "application/json"
     ]
     ServerCommunication.getDataWithPostHeader(url: ConstantFiles.gym_detail, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
     print(successDict)
     if (successDict.value(forKey: "STATUS") as? String) == "true"{
     if let arr = successDict.value(forKey:"response"){
     /* let array:NSArray = arr as! NSArray
     for item in array {
     let dict:NSDictionary =  item as! NSDictionary
     let obj_bookingmodal:GymListModel = GymListModel(dictUserInfo: dict)
     self.array_mybookings.add(obj_bookingmodal)
     }*/
     //GymDetailModel
     self.obj_gymDetail = GymDetailModel(dictUserInfo: arr as! NSDictionary)
     self.tableview_detail.reloadData()
     }else{
     DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
     }
     
     }else{
     DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
     }
     }) { (dictFailure) in
     if (dictFailure.value(forKey: "message") as? String) != nil {
     DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
     }
     }
     }
     
     */
    
    
    func promocode_btn_clicked(cell: ApplyCodeCell, btn: UIButton) {
        if isDiscountApplied == true{
            self.discount = 0
            self.str_promocode = ""
            self.str_couponId = ""
            self.isDiscountApplied = false
            self.tableview_detail.reloadData()
        }else{
            let obj_parkeAddressVC:DiscountsVC = DiscountsVC(nibName: "DiscountsVC", bundle: nil)
            obj_parkeAddressVC.delegate = self
            obj_parkeAddressVC.forPromoCode = true
            obj_parkeAddressVC.str_type = "1"
            let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
        
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 70
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header:OrderSummaryHeaderView =  UINib(nibName: "OrderSummaryHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! OrderSummaryHeaderView
        if section == 1{
            header.lbl_title.text = "Gym Membership Detail"
        }else if section == 2{
            header.lbl_title.text = "Price Detail"
        }
        
        return header
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0{
            return 120
        }else if indexPath.section == 1{
            return 90
        }else{
            return 55
        }
        
        
        // return 710
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 2{
            
            if isDiscountApplied == true{
                return 4
            }else{
                return 3
            }
        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        ////TrainerHomeAddressCell
        if indexPath.section == 0{
            let identifier = "OrdarSummaryCell"
            obj_orderSummaryCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrdarSummaryCell
            if obj_orderSummaryCell == nil {
                tableView.register(UINib(nibName: "OrdarSummaryCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_orderSummaryCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrdarSummaryCell
            }
           obj_orderSummaryCell.updateCellwithModalGym(obj_protienModel: self.obj_gymdetail)
            return obj_orderSummaryCell
        }else if indexPath.section == 1{
            let identifier = "OrderGymSummaryCell"
            obj_orderShippingCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderGymSummaryCell
            if obj_orderShippingCell == nil {
                tableView.register(UINib(nibName: "OrderGymSummaryCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_orderShippingCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderGymSummaryCell
            }
            let str_date:String = self.str_start_date
            obj_orderShippingCell.lbl_date.text = str_date
            obj_orderShippingCell.delegate = self
            
            if obj_susbcription_dict != nil{
                if let result_number = obj_susbcription_dict?.value(forKey: "subscription_title") as? String
                {
                    obj_orderShippingCell.lbl_membershipPlan.text = "\(result_number)"
                }
            }
            
            // obj_orderShippingCell.updateCellwithModal(obj_shippingAddress: self.obj_selected_address)
            return obj_orderShippingCell
        }else{
            if indexPath.row == 0{
                let identifier = "ApplyCodeCell"
                obj_applyCodeCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ApplyCodeCell
                if obj_applyCodeCell == nil {
                    tableView.register(UINib(nibName: "ApplyCodeCell", bundle: nil), forCellReuseIdentifier: identifier)
                    obj_applyCodeCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ApplyCodeCell
                }
                if isDiscountApplied == true{
                    obj_applyCodeCell.btn_title.setTitle("Remove Promo Code", for: .normal)
                    obj_applyCodeCell.lbl_promocode.text = self.str_promocode
                }else{
                    obj_applyCodeCell.btn_title.setTitle("Apply Promo Code", for: .normal)
                    obj_applyCodeCell.lbl_promocode.text = ""
                }
                obj_applyCodeCell.delegate = self
                //let obj_locationModel:LocationServiceModel = obj_trainer_detail!.array_locations[indexPath.row] as! LocationServiceModel
                //homeCell.updateCellwithModal(obj_location_model:obj_locationModel)
                //  homeCell.delegate = self
                return obj_applyCodeCell
            }
            
            let identifier = "OrderAmountCell"
            var obj_amountcell: OrderAmountCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderAmountCell
            if obj_amountcell == nil {
                tableView.register(UINib(nibName: "OrderAmountCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_amountcell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderAmountCell)!
            }
            
            if isDiscountApplied{
                if indexPath.row == 1{
                    
                    obj_amountcell?.updateCellwithModal(str_title: "Total Amount", str_value: String(format: "%@%.2f", self.obj_gymdetail.str_curency ,self.totalCost), str_payable: String(format: "%@%.2f", self.obj_gymdetail.str_curency ,self.totalCost))
                        
                    
                    
                }else if indexPath.row == 2{
                    
                   
                    obj_amountcell?.updateCellwithModal(str_title: "Discount", str_value: String(format: "%@%.2f", self.obj_gymdetail.str_curency,self.discount), str_payable: String(format: "%@%.2f", self.obj_gymdetail.str_curency ,self.totalCost))
                    
                    
                }else if indexPath.row == 3{
                    obj_amountcell?.updateCellwithModal(str_title: "Amount Payable", str_value: String(format: "%@%.2f", self.obj_gymdetail.str_curency,self.totalCost-self.discount), str_payable: String(format: "%@%.2f", self.obj_gymdetail.str_curency ,self.totalCost))
                }
            }else{
                if indexPath.row == 1{
                    
                    obj_amountcell?.updateCellwithModal(str_title: "Total Amount", str_value: String(format: "%@%.2f", self.obj_gymdetail.str_curency,self.totalCost), str_payable: String(format: "%@%.2f", self.obj_gymdetail.str_curency ,self.totalCost))

                }else if indexPath.row == 2{
                    obj_amountcell?.updateCellwithModal(str_title: "Amount Payable", str_value: String(format: "%@%.2f", self.obj_gymdetail.str_curency,self.totalCost), str_payable: String(format: "%@%.2f", self.obj_gymdetail.str_curency ,self.totalCost))
                }
            }
            return obj_amountcell!
            
        }
    }
    
}

