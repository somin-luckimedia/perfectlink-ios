//
//  OrderGymSummaryCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 21/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

protocol OrderGymSummaryCellDelegate:class {
    func date_btn_clicked(cell: OrderGymSummaryCell, btn: UIButton)
}

class OrderGymSummaryCell: UITableViewCell {
    weak var delegate:OrderGymSummaryCellDelegate?
    @IBOutlet weak var lbl_date: UITextField!
    @IBOutlet weak var lbl_membershipPlan: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
      @IBAction func date_btn_clicked(_ sender: UIButton) {
        self.delegate?.date_btn_clicked(cell: self, btn: sender)
    }
    
}
