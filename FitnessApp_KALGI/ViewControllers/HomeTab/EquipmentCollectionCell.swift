//
//  EquipmentCollectionCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 14/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit

class EquipmentCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imageView_equipment: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    
    func updateCellwithModal(obj_gymDetail:NSDictionary){
        
        if let result_number = obj_gymDetail.value(forKey: "equipment_image") as? String
        {
            imageView_equipment.setImageFromURL(stringImageUrl: result_number)//(with: URL(string: result_number), placeholderImage: UIImage(named: "default_profile"))
        }
        
        if let result_number = obj_gymDetail.value(forKey: "equipment_name") as? String
        {
           lbl_title.text = result_number
        }
        
    }

}
