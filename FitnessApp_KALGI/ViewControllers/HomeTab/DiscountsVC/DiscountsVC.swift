//
//  DiscountsVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 19/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

protocol DiscountControllerDelegate:class {
    func discount_btn_clicked(obj_discount: DiscountModel)
}

class DiscountsVC: UIViewController , UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, DiscountApplyCellDelegate  {
    
    
    var searchEnabled = false
    var searchText = ""

    @IBOutlet var vwSearchBar: UIView!
    @IBAction func btnCloseIcnTapped(_ sender: UIButton) {
        vwSearchBar.isHidden = true
    }
    @IBAction func btnSearchTapped(_ sender: Any) {
        vwSearchBar.isHidden = false
    }
    var arrFavSearchHeader : NSMutableArray = []
    var dictFavSearchData:NSMutableDictionary = [:]
    @IBOutlet var searchBar: UISearchBar!
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    fileprivate var array_mybookings:NSMutableArray = NSMutableArray()
    @IBOutlet weak var imageView_BG: UIImageView!
    
    @IBOutlet weak var tableview_bookings: UITableView!
    var dict_item:NSDictionary!
    var forPromoCode:Bool = false
    weak var delegate:DiscountControllerDelegate?
    var str_type:String = "0"
    @IBOutlet var tblOnline: UITableView!
    var arrOnlineFriends : NSMutableArray = []
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  self.title = "DISCOUNT"
        searchBar.layer.cornerRadius = 5.0
//        searchBar.barTintColor = .clear
//        searchBar.backgroundColor = .clear
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont(name: "Lato-Regular", size: 16.0)
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        searchBar.setShowsCancelButton(false, animated: true)
        textFieldInsideSearchBar?.leftViewMode = .never
        self.searchBar.searchTextPositionAdjustment = UIOffset(horizontal: -19, vertical: 0)
        textFieldInsideSearchBar?.placeholder = "Search"
        textFieldInsideSearchBar?.borderStyle = .none
        textFieldInsideSearchBar?.textColor = .white
//        searchBar.isHidden = true
        vwSearchBar.isHidden = true
        tableview_bookings.delegate  = self as UITableViewDelegate
        tableview_bookings.dataSource = self as UITableViewDataSource
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
//                if snapshot.childrenCount == 1 {
//                    self.constWidthTbl.constant = 0
////                    self.constLeadingColl.constant = 20
//                    self.lblOnline.isHidden = true
//                }
//                else {
//                    self.constWidthTbl.constant = 60
////                    self.constLeadingColl.constant = 0
//                    self.lblOnline.isHidden = false
//                }
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }

                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
//            else {
//                self.constWidthTbl.constant = 0
//                self.constLeadingColl.constant = 10
//
//                self.lblOnline.isHidden = true
//            }
        }
        ///self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.getMyBookings()
        // self.imageView_BG.image = DataUtil.setGradientBackgroundImage()
      //  self.title = dict_item["category_name"] as? String
        
        
        
        self.addLeftMenuButtonWithImage()
    //    self.addRightMenuButtonWithImage()
        
        
    }
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        if self.forPromoCode == true{
            button.setImage(UIImage(named: "close"), for: UIControl.State.normal)
            button.addTarget(self, action: #selector(self.dismisse_btn_clicked), for: UIControl.Event.touchUpInside)
        }else{
            button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
            button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        }
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "filter_icon"), for: UIControl.State.normal)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismisse_btn_clicked(){
        self.dismiss(animated: true, completion: nil)
    }
   
    
    
    /*
     
     func getMyBookings(){
     let headers: HTTPHeaders = [
     "Authorization": String(format: "Bearer %@",  (DataUtil.appdelegate().objUserInfo?.str_accessToken)!),
     "Accept": "application/json"
     ]
     ServerCommunication.getData(url: ConstantFiles.discount_listing, viewController: self,headers:headers   ,success: { (successDict) in
     print(successDict)
     if (successDict.value(forKey: "STATUS") as? String) == "true"{
     if let arr = successDict.value(forKey:"response"){
     let array:NSArray = arr as! NSArray
     for item in array {
     let dict:NSDictionary =  item as! NSDictionary
     let obj_bookingmodal:DiscountModel = DiscountModel(dictUserInfo: dict)
     self.array_mybookings.add(obj_bookingmodal)
     }
     self.tableview_bookings.reloadData()
     }else{
     DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
     }
     
     }else{
     DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
     }
     }) { (dictFailure) in
     if (dictFailure.value(forKey: "message") as? String) != nil {
     DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
     }
     }
     }
     
     0- All
     1- gym
     2- trainer
     3- supplements
     4-accessories..
     
     */
    
    func getMyBookings(){

        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        let postParam = ["":""]
        
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.discount_listing, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:DiscountModel = DiscountModel(dictUserInfo: dict)
                        self.array_mybookings.add(obj_bookingmodal)
                    }
                    self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func apply_btn_clicked(cell: DiscountsCell, btn: UIButton) {
        let obj_requestmodal:DiscountModel = array_mybookings[cell.rowIndex] as! DiscountModel
        self.delegate?.discount_btn_clicked(obj_discount: obj_requestmodal)
        self.dismiss(animated: true, completion: nil)
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == tblOnline {
            return 60
        }
        else {
            return 120
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if tableView == tblOnline {
            return arrOnlineFriends.count
        }
        else {
            if searchEnabled {
                return arrFavSearchHeader.count
            }
            else {
                return self.array_mybookings.count
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblOnline {
            let identifier = "onlineTVCTableViewCell"
            var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
            
            if logincell == nil {
                tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
            }
            logincell?.selectionStyle = .none
            //        logincell?.layer.cornerRadius = 25
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            let url = URL(string: dict["pic"] as! String)
            logincell?.imgOnlineFriends.setImageFrom(url)
            logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
            
            return logincell!
        }
        else {
            let identifier = "DiscountsCell"
            var logincell: DiscountsCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? DiscountsCell
            if logincell == nil {
                tableView.register(UINib(nibName: "DiscountsCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? DiscountsCell)!
            }
            //self.forPromoCode
            logincell?.rowIndex = indexPath.row
            logincell?.delegate = self
            if self.forPromoCode == true{
                logincell?.btn_apply.isHidden = false
            }
            let obj_requestmodal:DiscountModel!
            if searchEnabled {
                obj_requestmodal = arrFavSearchHeader[indexPath.row] as? DiscountModel
                
            }
            else {
                obj_requestmodal = array_mybookings[indexPath.row] as? DiscountModel
            }
            logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal)
            return logincell!
        }
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblOnline {
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            flagPark = false
            flagGym = false
            flagGallery = false
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            obj_phonelistvc.str_user_id = dict["userId"] as! String
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
    }
}

//MARK: SearchBar Delegate Methods
extension DiscountsVC: UISearchBarDelegate {
    override func touchesBegan(_ touches: Set<UITouch>,with event: UIEvent?){
        searchBar.resignFirstResponder()
    }
    //    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    //        searchBar.setShowsCancelButton(true, animated: true)
    //    }
    //
    //    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    //        searchBar.setShowsCancelButton(false, animated: true)
    //    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchBar.text?.count == 0 {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            searchEnabled = false
            tableview_bookings.reloadData()
            tableview_bookings.layoutIfNeeded()
            tableview_bookings.setNeedsLayout()

        }
        else {
            searchEnabled = true
            self.searchText = searchBar.text!
            filterContentforSearchText(searchText: searchBar.text!)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
//        Analytics.logEvent("recipe_submit_search", parameters: nil)
        searchBar.resignFirstResponder()
        if searchBar.text == "" {
        }
        else
        {
            searchEnabled = true
            filterContentforSearchText(searchText: searchBar.text!)//khushbu_new
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchEnabled = false
        tableview_bookings.reloadData()
        tableview_bookings.layoutIfNeeded()
        tableview_bookings.setNeedsLayout()
        
        
    }
}

extension DiscountsVC {
    //MARK: Filter Methods
    func filterContentforSearchText(searchText: String) {
        
//        let strFavBtnStatus = StaticClass.retrieve(fromUserDefaults: "isBtnFavRec")
//        if strFavBtnStatus == "YES"
//        {
            arrFavSearchHeader = []
            dictFavSearchData = [:]
        
        for i in 0..<array_mybookings.count {
            let dict = array_mybookings[i] as! NSDictionary
            let strTitle = dict["title"] as? String
            
            if strTitle!.lowercased().range(of:searchText.lowercased()) != nil {
                arrFavSearchHeader.add(dict)
            }
        }
            
//        if (self.arrFavSearchHeader.count > 0){
//                for i in 0..<arrFavHeaderData.count{
//                    let strHeader = arrFavSearchHeader[i]
//                    let arrData = dictFavSearchData[strHeader] as! NSArray
//
//                    let arrDictAdd:NSMutableArray = []
//
////                    for j in 0..<array_categories.count{
//////                        var objFlatTummyInfo : SquatInfo = SquatInfo()
////                        objFlatTummyInfo = arrData.object(at: j) as! SquatInfo
////                        let strTitle = objFlatTummyInfo.Recipe_title
////
////                        if strTitle.lowercased().range(of:searchText.lowercased()) != nil {
////
////                            if !arrFavSearchHeader.contains(strHeader) {
////                                arrFavSearchHeader.add(strHeader)
////                            }
////                            arrDictAdd.add(arrData[j])
////                            dictFavSearchData[strHeader] = arrDictAdd
////                        }
////                    }
//                }
//            }
        tableview_bookings.reloadData()
        tableview_bookings.setNeedsLayout()
        tableview_bookings.layoutIfNeeded()

    }
}
