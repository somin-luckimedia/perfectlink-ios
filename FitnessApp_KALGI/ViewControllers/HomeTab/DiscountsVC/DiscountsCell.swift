//
//  DiscountsCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 19/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//



import UIKit

protocol DiscountApplyCellDelegate:class {
    func apply_btn_clicked(cell: DiscountsCell, btn: UIButton)
}

class DiscountsCell: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_percentage: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_couponcode: UILabel!
     @IBOutlet weak var btn_apply: UIButton!
    var rowIndex:Int = 0
    weak var delegate:DiscountApplyCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_bookingmodal:DiscountModel){

        lbl_title.text = obj_bookingmodal.str_coupon_title!
        lbl_description.text = String(format: "%@", obj_bookingmodal.str_brief_description!)
        lbl_couponcode.text = String(format: "Coupon Code : %@", obj_bookingmodal.str_coupon_code!)

        var str_coupontype:String = ""
        if obj_bookingmodal.str_type == "percentage"{
            str_coupontype = String(format: "%@%%", obj_bookingmodal.str_percentage!)
        }else{
            str_coupontype = String(format: "%@%@", obj_bookingmodal.str_currency, obj_bookingmodal.str_amount!)
        }
        lbl_percentage.text = str_coupontype
        
    }
    
    @IBAction func apply_btn_clicked(_ sender: UIButton) {
        self.delegate?.apply_btn_clicked(cell: self, btn: sender)
    }
    
    
}
