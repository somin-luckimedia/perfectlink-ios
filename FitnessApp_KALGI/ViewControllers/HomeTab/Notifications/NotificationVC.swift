//
//  NotificationVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 20/10/20.
//

import UIKit

class NotificationVC: UIViewController {

    @IBOutlet var tblNotifications: UITableView!
    fileprivate var array_mybookings:NSMutableArray = NSMutableArray()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblNotifications.register(UINib(nibName: "NotificationTVC", bundle: nil), forCellReuseIdentifier: "NotificationTVC")
        tblNotifications.delegate = self
        tblNotifications.dataSource = self
        notification_btn_call()
        // Do any additional setup after loading the view.
    }
}

//MARK : TABLEVIEW DELEGATE & DATASOURCE METHODS
extension NotificationVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_mybookings.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : NotificationTVC = tblNotifications.dequeueReusableCell(withIdentifier: "NotificationTVC") as! NotificationTVC
        cell.selectionStyle = .none
        let dict  =  array_mybookings[indexPath.row] as! NotificationListModel
        cell.lblTitle.text = dict.str_message
        cell.lblSubTitle.text = dict.str_create_at
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
}

//MARK : BUTTON ACTION METHODS
extension NotificationVC {
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK : WEBCALL METHODS
extension NotificationVC {
    
    func notification_btn_call() {
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        let str_userId:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""

        let postParam = ["user_id":str_userId] as [String : Any]
        print("Login Post Parameter : \(postParam)")
        
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.strNotificationByUser, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                    if let arr = successDict.value(forKey:"response"){
                        let array:NSArray = arr as! NSArray
                        for item in array {
                            let dict:NSDictionary =  item as! NSDictionary
                            let obj_bookingmodal:NotificationListModel = NotificationListModel(dictUserInfo: dict)
                            self.array_mybookings.add(obj_bookingmodal)
                        }
                        print("Notification",self.array_mybookings)
                        self.tblNotifications.reloadData()
                   }
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
        
        
    }
    
}
