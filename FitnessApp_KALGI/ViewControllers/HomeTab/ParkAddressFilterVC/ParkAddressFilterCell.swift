//
//  ParkAddressCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 12/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.

import UIKit

class ParkAddressFilterCell: UITableViewCell {

    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var imageView_thumb: UIImageView!
    
    @IBOutlet weak var view_container: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_bookingmodal:LocationServiceModel){
        lbl_title.text = String(format: "%@", obj_bookingmodal.str_park_name!)
        lbl_address.text = String(format: "%@", obj_bookingmodal.str_park_address!)
//        imageView_thumb.sd_setImage(with: URL(string: (obj_bookingmodal.str_park_image)), placeholderImage: UIImage(named: "appLogo"))
        let url = URL(string: obj_bookingmodal.str_park_image)
        imageView_thumb.setImageFrom(url)
    }
    
    
    
}
