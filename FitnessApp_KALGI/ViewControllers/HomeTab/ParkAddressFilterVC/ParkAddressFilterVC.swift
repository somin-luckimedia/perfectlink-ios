
//
//  ParkAddressVC.swift
//  FitnessApp//
//  Created by Ashish Mishra on 12/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//


import UIKit
import Alamofire
//ParkAddressFilterVC
//ParkAddressFilterCell

protocol ParkAddressFilterDelegate: class {
    func done_btn_clicked(str_parks:String)
}

//array_selectedFriends.componentsJoined(by: ",")
class ParkAddressFilterVC: UIViewController , UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate  {
    
    fileprivate var array_parks:NSMutableArray = NSMutableArray()
    @IBOutlet weak var imageView_BG: UIImageView!
    @IBOutlet weak var tableview_parks: UITableView!
    @IBOutlet weak var lbl_message: UILabel!
    var obj_selected_park:LocationServiceModel? = nil
    var str_park_type:String = ""
    fileprivate var array_selectedPark:NSMutableArray = NSMutableArray()
    weak var delegate:ParkAddressFilterDelegate?
 
    var dict_item:NSDictionary!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Park Address"
        tableview_parks.delegate  = self as UITableViewDelegate
        tableview_parks.dataSource = self as UITableViewDataSource
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.getMyBookings()
        self.addLeftMenuButtonWithImage()
        self.addRightMenuButtonWithImage()
        
        
    }
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        //button.setImage(UIImage(named: "filter_icon"), for: UIControlState.normal)
        button.setTitle("Done", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(self.done_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func done_btn_clicked(){
        self.delegate?.done_btn_clicked(str_parks: array_selectedPark.componentsJoined(by: ","))
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func add_btn_clicked(){
        if obj_selected_park == nil{
            DataUtil.alertMessage("Please select a park", viewController: self)
            return
        }
        
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]

            
        let postParam: Parameters = ["park_id":(obj_selected_park?.str_id)!, "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!]
            print("Login Post Parameter : \(postParam)")
            
            
            
            ServerCommunication.getDataWithPostHeader(url: ConstantFiles.add_park, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
                print(successDict)
                if (successDict.value(forKey: "STATUS") as? String) == "true"{
                    
                    if let array:NSArray = successDict["response"] as? NSArray{
                        DataUtil.appdelegate().objUserInfo?.array_locations.removeAllObjects()
                        for item in array {
                            let dict:NSDictionary =  item as! NSDictionary
                            let obj_model:LocationServiceModel = LocationServiceModel.init(dictUserInfo: dict)
                            DataUtil.appdelegate().objUserInfo?.array_locations.add(obj_model)
                        }
                    }
                    
                    
                    
                    let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        //self.delegate?.parkadded_clicked()
                        //self.back_btn_clicked()
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                   // DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                    //self.back_btn_clicked()
                    
                 /*   if let arr = successDict.value(forKey:"response"){
                        
                        
                    }else{
                       
                    }*/
                }else{
                    
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                    self.back_btn_clicked()
                }
            }) { (dictFailure) in
                if (dictFailure.value(forKey: "message") as? String) != nil {
                    DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
                }
            }
            
        

    }
    
    func getMyBookings(){
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        let strLat:String = DataUtil.appdelegate().str_lattitude
        let strLong:String = DataUtil.appdelegate().str_longitude
        let postParam: Parameters = ["park_type":str_park_type, "lat":strLat, "lon":strLong, "radius":"30"]
        print("Login Post Parameter : \(postParam)")
        //park_type
        // park link
    // free run
        // both
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.park_list, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array{
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:LocationServiceModel = LocationServiceModel(dictUserInfo: dict)
                        self.array_parks.add(obj_bookingmodal)
                    }
                    if self.array_parks.count == 0{
                        self.lbl_message.isHidden = false
                    }else{
                        self.lbl_message.isHidden = true
                    }
                    self.tableview_parks.reloadData()
                }else{
                        self.lbl_message.isHidden = false
                    //DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                    self.lbl_message.isHidden = false
                //DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 94
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.array_parks.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "ParkAddressFilterCell"
        var logincell: ParkAddressFilterCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? ParkAddressFilterCell
        if logincell == nil {
            tableView.register(UINib(nibName: "ParkAddressFilterCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? ParkAddressFilterCell)!
        }
        //logincell!.delegate = self
        let obj_requestmodal:LocationServiceModel = array_parks[indexPath.row] as! LocationServiceModel
        //AccesoriesListModel
        
        
        logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal)
        
        if obj_requestmodal.is_selected == true{
            logincell!.view_container.backgroundColor = UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
        }else{
            logincell!.view_container.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 0.25)
        }
        
        
        return logincell!
    }
    // 209, 9, 4
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell:ParkAddressFilterCell = tableView.cellForRow(at: indexPath) as! ParkAddressFilterCell
        cell.view_container.backgroundColor = UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
        let obj_selected_park:LocationServiceModel = array_parks[indexPath.row] as! LocationServiceModel
        obj_selected_park.is_selected = true
        array_selectedPark.add(obj_selected_park.str_id)
        array_parks.replaceObject(at: indexPath.row, with: obj_selected_park)
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell:ParkAddressFilterCell = tableView.cellForRow(at: indexPath) as! ParkAddressFilterCell
        cell.view_container.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 0.25)
        let obj_selected_park:LocationServiceModel = array_parks[indexPath.row] as! LocationServiceModel
        obj_selected_park.is_selected = false
        array_selectedPark.remove(obj_selected_park.str_id)
        array_parks.replaceObject(at: indexPath.row, with: obj_selected_park)
    }
    
}
