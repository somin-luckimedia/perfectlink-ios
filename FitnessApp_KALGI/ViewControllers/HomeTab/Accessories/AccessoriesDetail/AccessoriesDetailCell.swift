//
//  AccessoriesDetailCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 23/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit

protocol AccessoriesDetailCellDelegate:class {
    func decrease_btn_clicked(cell: AccessoriesDetailCell, btn: UIButton)
    func increase_btn_clicked(cell: AccessoriesDetailCell, btn: UIButton)
    func book_btn_clicked(cell: AccessoriesDetailCell, btn: UIButton)
}

class AccessoriesDetailCell: UITableViewCell {
    
    
    
    @IBOutlet weak var imageView_cover: UIImageView!
    @IBOutlet weak var rateView: DYRateView!
    @IBOutlet weak var lbl_rate: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_subTitle: UILabel!
    @IBOutlet weak var lbl_aboutTitle: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var btn_decrease: UIButton!
    @IBOutlet weak var btn_increase: UIButton!
    @IBOutlet weak var btn_bookNow: UIButton!
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet var lblPrice: UILabel!
    
    @IBOutlet weak var lbl_price: UILabel!
    weak var delegate:AccessoriesDetailCellDelegate?
    var obj_gymdetail:AccessoriesDetailModel! = AccessoriesDetailModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateCellwithModal(obj_bookingmodal:AccessoriesDetailModel){
        
        lbl_title.text = String(format: "%@", obj_bookingmodal.str_accessorie_title!)
       // lbl_subTitle.text = String(format: "%@", obj_bookingmodal.str_sub_title!)
        lbl_description.text = String(format: "%@", obj_bookingmodal.str_full_description!)
        let url = URL(string: obj_bookingmodal.str_cover_image)
        imageView_cover.setImageFrom(url)
        lbl_price.text = String(format: "%@%@/%@", obj_bookingmodal.str_currency!,obj_bookingmodal.str_amount!,obj_bookingmodal.str_per!)
        
        rateView.rate = CGFloat(Float(obj_bookingmodal.str_star_rating) ?? 0)
        lbl_rate.text = obj_bookingmodal.str_star_rating
//        lbl_aboutTitle.text = String(format:"About %@", obj_bookingmodal.str_accessorie_title)
        
         btn_increase.layer.masksToBounds = true
         btn_increase.layer.borderColor = UIColor.white.cgColor
         btn_increase.layer.borderWidth = 2.0
         btn_increase.layer.cornerRadius = 20.0
        
        btn_decrease.layer.masksToBounds = true
        btn_decrease.layer.borderColor = UIColor.white.cgColor
        btn_decrease.layer.borderWidth = 2.0
        btn_decrease.layer.cornerRadius = 20.0
         
        
    }
    
    @IBAction func decrease_btn_clicked(_ sender: UIButton) {
        self.delegate?.decrease_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func increase_btn_clicked(_ sender: UIButton) {
        self.delegate?.increase_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func book_btn_clicked(_ sender: UIButton) {
        self.delegate?.book_btn_clicked(cell: self, btn: sender)
    }
    
}
