//
//  AccessoriesDetailVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 23/12/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire

class AccessoriesDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, AccessoriesDetailCellDelegate{

    
    
    
    //ProtienDetailModel
    var obj_gymDetail:AccessoriesDetailModel! = AccessoriesDetailModel()
    var str_userType:String = "2"
    var logincell: AccessoriesDetailCell!
    var str_accesoriesId:String!
    var productCount:Int = 1
    var totalCost:Double = 0.00
    var str_title:String!
    var str_productId:String!

   
    @IBOutlet weak var tableview_detail: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
//         self.title = self.str_title
        tableview_detail.delegate  = self as UITableViewDelegate
        tableview_detail.dataSource = self as UITableViewDataSource
        //tableview_detail.rowHeight = 700
        //tableview_detail.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableview_detail.isHidden = true
        tableview_detail.reloadData()
        
        
        
//        self.addLeftMenuButtonWithImage()
        self.getMyBookings()
        
        
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // self.navigationController?.navigationBar.isHidden = true
        //   self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getMyBookings(){
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam: Parameters = ["accessorie_id":str_accesoriesId!]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.accessories_detail, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.tableview_detail.isHidden = false
                if let arr = successDict.value(forKey:"response"){
                    /* let array:NSArray = arr as! NSArray
                     for item in array {
                     let dict:NSDictionary =  item as! NSDictionary
                     let obj_bookingmodal:GymListModel = GymListModel(dictUserInfo: dict)
                     self.array_mybookings.add(obj_bookingmodal)
                     }*/
                    self.obj_gymDetail = AccessoriesDetailModel(dictUserInfo: arr as! NSDictionary)
                    self.tableview_detail.reloadData()
                    self.calculate_total_price()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    
    func decrease_btn_clicked(cell: AccessoriesDetailCell, btn: UIButton) {
        if productCount != 1{
            productCount = productCount - 1
            logincell.lbl_count.text = String(format: "%ld", productCount)
            self.calculate_total_price()
        }
    }
    
    func increase_btn_clicked(cell: AccessoriesDetailCell, btn: UIButton) {
        if productCount != 25{
            productCount = productCount + 1
            logincell.lbl_count.text = String(format: "%ld", productCount)
            self.calculate_total_price()
        }
    }
    
    func book_btn_clicked(cell: AccessoriesDetailCell, btn: UIButton) {
        //ShippingAddressVC
        let obj_shippingAddressVC:ShippingAdressVC = ShippingAdressVC(nibName: "ShippingAdressVC", bundle: nil)
        obj_shippingAddressVC.obj_accessoriesModel = self.obj_gymDetail
        obj_shippingAddressVC.productCount = self.productCount
        obj_shippingAddressVC.totalCost = self.totalCost
        self.navigationController?.pushViewController(obj_shippingAddressVC, animated: true)
        //(nibName: "ProtienDetailVC", bundle: nil)
    }
    
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
    //
    //
    //        return UITableViewAutomaticDimension
    //       // return 710
    //
    //    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "AccessoriesDetailCell"
        logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? AccessoriesDetailCell
        if logincell == nil {
            tableView.register(UINib(nibName: "AccessoriesDetailCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? AccessoriesDetailCell
        }
        logincell.updateCellwithModal(obj_bookingmodal:self.obj_gymDetail)
        logincell.lbl_count.text = String(format: "%ld", productCount)
        self.calculate_total_price()
        logincell.delegate = self
        // logincell.cellIndex = indexPath.row
        // logincell.configureWithOwner(controller: self)
        // logincell.delegate = self
        //  cell.textLabel?.text = monthName
        return logincell
    }
    
    func calculate_total_price(){
        let productCost:Double = Double(self.obj_gymDetail.str_amount)!
        self.totalCost = Double(productCount)*productCost
//        self.logincell.btn_bookNow.setTitle(String(format: "BUY NOW"))
        self.logincell.lblPrice.text = String(format:"$%.2f", self.totalCost)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 1000
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Hello delegate")
    }
    
    
    
    
    
    
    
    
    
}
