//
//  AccessoriesVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 29/10/20.
//

import UIKit
import Firebase

class AccessoriesVC: UIViewController {

    @IBOutlet var tblAccessories: UITableView!
    fileprivate var array_mybookings:NSMutableArray = NSMutableArray()
    var searchEnabled = false
    var searchText = ""

    @IBOutlet var vwSearchBar: UIView!
    @IBAction func btnCloseIcnTapped(_ sender: UIButton) {
        vwSearchBar.isHidden = true
    }

    var arrFavSearchHeader : NSMutableArray = []
    var dictFavSearchData:NSMutableDictionary = [:]
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tblOnline: UITableView!
    var arrOnlineFriends : NSMutableArray = []
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        searchBar.layer.cornerRadius = 5.0
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont(name: "Lato-Regular", size: 16.0)
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        searchBar.setShowsCancelButton(false, animated: true)
        textFieldInsideSearchBar?.leftViewMode = .never
        self.searchBar.searchTextPositionAdjustment = UIOffset(horizontal: -19, vertical: 0)
        textFieldInsideSearchBar?.placeholder = "Search"
        textFieldInsideSearchBar?.borderStyle = .none
        textFieldInsideSearchBar?.textColor = .white
        vwSearchBar.isHidden = true
        tblAccessories.register(UINib(nibName: "SupplementTVC", bundle: nil), forCellReuseIdentifier: "SupplementTVC")
        tblAccessories.delegate = self
        tblAccessories.dataSource = self
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
//                if snapshot.childrenCount == 1 {
//                    self.constWidthTbl.constant = 0
////                    self.constLeadingColl.constant = 20
//                    self.lblOnline.isHidden = true
//                }
//                else {
//                    self.constWidthTbl.constant = 60
////                    self.constLeadingColl.constant = 0
//                    self.lblOnline.isHidden = false
//                }
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }

                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
//            else {
//                self.constWidthTbl.constant = 0
//                self.constLeadingColl.constant = 10
//
//                self.lblOnline.isHidden = true
//            }
        }
        self.getMyBookings()
        
    }

}
//MARK: BUTTON ACTION METHODS
extension AccessoriesVC {
//    @IBAction func btnFilterTapped(_ sender: UIButton) {
//        let obj_ferrylistingVC : SupplementFilterVC = SupplementFilterVC(nibName: "SupplementFilterVC", bundle: nil)
////        obj_ferrylistingVC.delegate = self
//        let color_obj:UIColor = UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 0.2)
//        presentPopup(obj_ferrylistingVC,
//                     animated: true,
//                     backgroundStyle: .color(color_obj), // present the popup with a blur effect has background
//            constraints: [.width(300), .height(500)], // fix leading edge and the width
//            transitioning: .zoom, // the popup come and goes from the left side of the screen
//            autoDismiss: true, // when touching outside the popup bound it is not dismissed
//            completion: nil)
//    }
    @IBAction func btnSearchTapped(_ sender: UIButton) {
        vwSearchBar.isHidden = false

    }
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: WEBSERVICE METHODS
extension AccessoriesVC {
    func getMyBookings(){
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getData(url: ConstantFiles.accessories_listing, viewController: self,headers:headers   ,success: { (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:AccesoriesListModel = AccesoriesListModel(dictUserInfo: dict)
                        self.array_mybookings.add(obj_bookingmodal)
                    }
                    self.tblAccessories.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
}

//MARK: TABLEVIEW DELEGATE & DATA SOURCE METHODS
extension AccessoriesVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblOnline {
            return arrOnlineFriends.count
        }
        else {
            if searchEnabled {
                return arrFavSearchHeader.count
            }
            else {
                return array_mybookings.count
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblOnline {
            let identifier = "onlineTVCTableViewCell"
            var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
            
            if logincell == nil {
                tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
            }
            logincell?.selectionStyle = .none
            //        logincell?.layer.cornerRadius = 25
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            let url = URL(string: dict["pic"] as! String)
            logincell?.imgOnlineFriends.setImageFrom(url)
            logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
            
            return logincell!
        }
        else {
            let cell : SupplementTVC = tblAccessories.dequeueReusableCell(withIdentifier: "SupplementTVC") as! SupplementTVC
            cell.selectionStyle = .none
            let obj_requestmodal:AccesoriesListModel!
            if searchEnabled {
                obj_requestmodal = arrFavSearchHeader[indexPath.row] as? AccesoriesListModel
            }
            else {
                obj_requestmodal = array_mybookings[indexPath.row] as? AccesoriesListModel
            }
            cell.lblTitle.text = String(format: "%@", obj_requestmodal.str_accessorie_title!)
            cell.lblDescr.text = String(format: "%@", obj_requestmodal.str_brief_description!)
            //        cell.imgIcon.sd_setImage(with: URL(string: (obj_requestmodal.str_cover_image)), placeholderImage: UIImage(named: "appLogo"))
            let url = URL(string: obj_requestmodal.str_cover_image)
            
            cell.imgIcon.setImageFrom(url)
            cell.lblPrize.text = String(format: "%@%@/%@", obj_requestmodal.str_currency!,obj_requestmodal.str_amount!,obj_requestmodal.str_per!)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblOnline {
            return 60.0
            
        }
        else {
            return 130.0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblOnline {
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            flagPark = false
            flagGym = false
            flagGallery = false
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            obj_phonelistvc.str_user_id = dict["userId"] as! String
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
        else {
            let obj_phonelistvc: AccessoriesDetailVC = AccessoriesDetailVC(nibName: "AccessoriesDetailVC", bundle: nil)
            let obj_requestmodal:AccesoriesListModel = array_mybookings[indexPath.row] as! AccesoriesListModel
            obj_phonelistvc.str_accesoriesId = obj_requestmodal.str_accessorie_id
            //        obj_phonelistvc.str_title = obj_requestmodal.str_product_title
            obj_phonelistvc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
    }
}

//MARK: SearchBar Delegate Methods
extension AccessoriesVC: UISearchBarDelegate {
    override func touchesBegan(_ touches: Set<UITouch>,with event: UIEvent?){
        searchBar.resignFirstResponder()
    }
    //    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    //        searchBar.setShowsCancelButton(true, animated: true)
    //    }
    //
    //    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    //        searchBar.setShowsCancelButton(false, animated: true)
    //    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchBar.text?.count == 0 {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            searchEnabled = false
            tblAccessories.reloadData()
            tblAccessories.layoutIfNeeded()
            tblAccessories.setNeedsLayout()
        }
        else {
            searchEnabled = true
            self.searchText = searchBar.text!
            filterContentforSearchText(searchText: searchBar.text!)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
//        Analytics.logEvent("recipe_submit_search", parameters: nil)
        searchBar.resignFirstResponder()
        if searchBar.text == "" {
        }
        else
        {
            searchEnabled = true
            filterContentforSearchText(searchText: searchBar.text!)//khushbu_new
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchEnabled = false
        tblAccessories.reloadData()
        tblAccessories.layoutIfNeeded()
        tblAccessories.setNeedsLayout()
        
        
    }
}

extension AccessoriesVC {
    //MARK: Filter Methods
    func filterContentforSearchText(searchText: String) {

        arrFavSearchHeader = []
        dictFavSearchData = [:]
        
        for i in 0..<array_mybookings.count {
            let dict = array_mybookings[i] as! AccesoriesListModel
            let strTitle = dict.str_accessorie_title
            
            if strTitle!.lowercased().range(of:searchText.lowercased()) != nil {
                arrFavSearchHeader.add(dict)
            }
        }
        tblAccessories.reloadData()
        tblAccessories.setNeedsLayout()
        tblAccessories.layoutIfNeeded()
        
    }
}
