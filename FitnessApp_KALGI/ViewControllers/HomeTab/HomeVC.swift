//
//  HomeVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 16/10/20.
//

import UIKit
import AppTrackingTransparency
import Firebase

class HomeVC: UIViewController {

    @IBOutlet var collHome: UICollectionView!
    @IBOutlet var lblName: UILabel!
    
    let array_title:[String] = ["GYMS", "TRAINERS","PARK LINKS","RUN", "DISCOUNTS", "MEALS", "SUPPLEMENTS", "ACCESSORIES"]
    let array_labels : [String] = ["740+ Options", "8+ Available","487+ Parks","Available", "2+ Options", "10+ Options", "16+ Options", "6+ Options"]
    private let spacing:CGFloat = 5.0
    @IBOutlet var constLeadingColl: NSLayoutConstraint!
    @IBOutlet var lblOnline: UILabel!
    @IBOutlet var constWidthTbl: NSLayoutConstraint!
    let transiton = SlideInTransition()
    var topView: UIView?
    let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate
    let tempToken: String? = nil //If you have a token, put it here.
    var userID: UInt = 0 //This tells Agora to generate an id for you. If you have unique user IDs already, you can use those.
    var channelName = "default"
    @IBOutlet var tblOnline: UITableView!
    var arrOnlineFriends : NSMutableArray = []
    
    var remoteUserIDs: [UInt] = []
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")
    


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.collHome.register(UINib(nibName: "HomeCVC", bundle: nil), forCellWithReuseIdentifier: "HomeCVC")
//        /self.tblOnline.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellWithReuseIdentifier: "onlineTVCTableViewCell")
        lblName.text = DataUtil.appdelegate().objUserInfo?.name
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
//                if snapshot.childrenCount == 1 {
//                    self.constWidthTbl.constant = 0
////                    self.constLeadingColl.constant = 20
//                    self.lblOnline.isHidden = true
//                }
//                else {
//                    self.constWidthTbl.constant = 60
////                    self.constLeadingColl.constant = 0
//                    self.lblOnline.isHidden = false
//                }
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }

                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
//            else {
//                self.constWidthTbl.constant = 0
//                self.constLeadingColl.constant = 10
//
//                self.lblOnline.isHidden = true
//            }
        }
        self.collHome.delegate = self
        self.collHome.dataSource = self
        self.collHome.reloadData()
        let notificationCenter = NotificationCenter.default
               notificationCenter.addObserver(self,
                                              selector: #selector(dismissHome),
                                              name: NSNotification.Name(rawValue: "DismissHome"),
                                              object: nil)
        
        let notificationCenter1 = NotificationCenter.default
               notificationCenter1.addObserver(self,
                                              selector: #selector(pushNotification),
                                              name: NSNotification.Name(rawValue: "pushNotification"),
                                              object: nil)
        
   
    }
    
    @objc func dismissHome(){
        self.dismissControllerAnimated()
    }
    
    @objc func pushNotification(){
        self.tabBarController?.selectedIndex = 0
        if flagNotification != 0 {
            if flagNotification == 1 {
                let rootVC : FriendListVC = FriendListVC(nibName: "FriendListVC", bundle: nil)
                self.navigationController?.pushViewController(rootVC, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    flagMenu = false
                    self.dismissControllerAnimated()
                }
            }
            else if flagNotification == 2 {
                self.tabBarController?.selectedIndex = 1
            }
            else if flagNotification == 3 {
                let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
                self.navigationController?.pushViewController(rootVC, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    flagMenu = false
                    self.dismissControllerAnimated()
                }
            }
            else if flagNotification == 4 {
                let rootVC : OrderHistoryVC = OrderHistoryVC(nibName: "OrderHistoryVC", bundle: nil)
                self.present(rootVC, animated: true, completion: nil)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    flagMenu = false
                    self.dismissControllerAnimated()
                }
            }
            flagNotification = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.dismissControllerAnimated()
        lblName.text = DataUtil.appdelegate().objUserInfo?.name

        flagMenu = true


        objAppDelgate1.sideMenuIndex = 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.dismissControllerAnimated()
    }
    
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        let rootVC : SideMenuController1 = SideMenuController1(nibName: "SideMenuController", bundle: nil)
        if DataUtil.appdelegate().objUserInfo?.str_userType == "2" {
            rootVC.didTapMenuType = { menuType in
                self.transitionToNew(menuType)
            }
        }
        else {
            rootVC.didTapMenuType1 = {menuTypeTrainer in
                self.transitionTrainer(menuTypeTrainer)
            }
        }
        
        rootVC.modalPresentationStyle = .overCurrentContext
        rootVC.transitioningDelegate = self
        flagHome = true
        present(rootVC, animated: true)
    }
    
    
    @IBAction func btnNotificationTapped(_ sender: UIButton) {
        let obj_NotificationVC:NotificationVC = NotificationVC(nibName: "NotificationVC", bundle: nil)
        self.navigationController?.pushViewController(obj_NotificationVC, animated: true)

    }
}

extension HomeVC: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = true
        return transiton
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = false
        return transiton
    }
}

extension HomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return array_title.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:HomeCVC = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCVC", for: indexPath) as! HomeCVC
        cell.contentView.layer.cornerRadius = 10.0
        cell.lblTitle.text = array_title[indexPath.row]
        
        cell.bgImage.image = UIImage(named: array_title[indexPath.row])
        cell.innerImage.image = UIImage(named: "\(array_title[indexPath.row])-inner")
        cell.lblOption.text = array_labels[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow:CGFloat = 2
        let spacingBetweenCells:CGFloat = 0
        
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        // if let collection = self.collectionData{
        let width = (collHome.bounds.width - totalSpacing)/numberOfItemsPerRow
        return CGSize(width: width, height: width)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let obj_GymVC:GymVC = GymVC(nibName: "GymVC", bundle: nil)
            self.navigationController?.pushViewController(obj_GymVC, animated: true)
        }
        else if indexPath.row == 1 {
            let obj_TrainerListingVC:TrainerListingVC = TrainerListingVC(nibName: "TrainerListingVC", bundle: nil)
            self.navigationController?.pushViewController(obj_TrainerListingVC, animated: true)
        }
        else if indexPath.row == 2 {
            let obj_ParkLinkListVC:ParkLinkListVC = ParkLinkListVC(nibName: "ParkLinkListVC", bundle: nil)
            self.navigationController?.pushViewController(obj_ParkLinkListVC, animated: true)
        }
        else if indexPath.row == 3 {
            let obj_FreeRunVC:FreeRunVC = FreeRunVC(nibName: "FreeRunVC", bundle: nil)
            self.navigationController?.pushViewController(obj_FreeRunVC, animated: true)
        }

        else if indexPath.row == 4 {
            let obj_DiscountsVC:DiscountsVC = DiscountsVC(nibName: "DiscountsVC", bundle: nil)
            self.navigationController?.pushViewController(obj_DiscountsVC, animated: true)
        }
        else if indexPath.row == 6 {
            let obj_SupplementsVC:SupplementsVC = SupplementsVC(nibName: "SupplementsVC", bundle: nil)
            self.navigationController?.pushViewController(obj_SupplementsVC, animated: true)
        }
        else if indexPath.row == 7 {
            let obj_AccessoriesVC:AccessoriesVC = AccessoriesVC(nibName: "AccessoriesVC", bundle: nil)
            self.navigationController?.pushViewController(obj_AccessoriesVC, animated: true)
        }
    }
    
    
}

//MARK: TABLEVIEW DELEGATE & DATAOURCE METHODS
extension HomeVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOnlineFriends.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell : onlineTVCTableViewCell = tblOnline.dequeueReusableCell(withReuseIdentifier: "onlineTVCTableViewCell", for: indexPath) as! onlineTVCTableViewCell
//        cell.selectionStyle = .none
////        cell.imgOnlineFriends.image
//        return cell
        let identifier = "onlineTVCTableViewCell"
        var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
        
        if logincell == nil {
            tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
        }
        logincell?.selectionStyle = .none
//        logincell?.layer.cornerRadius = 25
        let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
        let url = URL(string: dict["pic"] as! String)
        logincell?.imgOnlineFriends.setImageFrom(url)
        logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
        
        return logincell!

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
//        let objFriendsChatVC:FriendsChatVC = FriendsChatVC(nibName: "FriendsChatVC", bundle: nil)
//        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
//       objFriendsChatVC.myUserID = str_userid
//       objFriendsChatVC.myFriendID = dict["userId"] as! String
//       objFriendsChatVC.friendName = dict["name"] as! String
//        self.navigationController?.pushViewController(objFriendsChatVC, animated: true)
        
        let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
        flagPark = false
        flagGym = false
        flagGallery = false
        let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
        obj_phonelistvc.str_user_id = dict["userId"] as! String
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
}
//MARK : Button action methods

extension HomeVC
{
    func transitionToNew(_ menuType: MenuType) {
//        if DataUtil.appdelegate().objUserInfo?.str_userType == "2" {
            switch menuType {
            case .FitnessWall:
                let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
                self.navigationController?.pushViewController(rootVC, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    flagMenu = false
                    self.dismissControllerAnimated()
                }
                
            case .Challenges:
                let rootVC : OrderHistoryVC = OrderHistoryVC(nibName: "OrderHistoryVC", bundle: nil)
                self.present(rootVC, animated: true, completion: nil)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    flagMenu = false
                    self.dismissControllerAnimated()
                }
            case .Chat:
                let rootVC : FriendListVC = FriendListVC(nibName: "FriendListVC", bundle: nil)
                //            self.present(rootVC, animated: true, completion: nil)
                //            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                //                self.dismissControllerAnimated()
                //            }
                // self.present(rootVC, animated: true, completion: nil)
                self.navigationController?.pushViewController(rootVC, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    flagMenu = false
                    self.dismissControllerAnimated()
                }
                
            default:
                break
            }

    }
    
    func transitionTrainer(_ menuType : MenuTypeTrainer) {
        switch menuType {
        case .FitnessWall:
            let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
            
        case .MyOrders:
            let rootVC : OrderHistoryVC = OrderHistoryVC(nibName: "OrderHistoryVC", bundle: nil)
            self.present(rootVC, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Friends:
            let rootVC : FriendListVC = FriendListVC(nibName: "FriendListVC", bundle: nil)
            //            self.present(rootVC, animated: true, completion: nil)
            //            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            //                self.dismissControllerAnimated()
            //            }
            // self.present(rootVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Achievements:
            let rootVC : AchievementsVC = AchievementsVC(nibName: "AchievementsVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .MyTips:
            let rootVC : FreeTipsVCViewController = FreeTipsVCViewController(nibName: "FreeTipsVCViewController", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        
        case .Wallet:
            let rootVC : WalletNewVC = WalletNewVC(nibName: "WalletNewVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
            
        default:
            break
        }
    }
}

