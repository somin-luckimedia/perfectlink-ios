//
//  FreeRunVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 04/01/21.
//

import UIKit
import MapKit
import CoreLocation
import HealthKit
import CoreMotion
import Firebase


class FreeRunVC: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate {
    @IBOutlet var mapView: MKMapView!
    var locationManager = CLLocationManager()
    var StartLocation : CLLocation!
    var LastLocation : CLLocation!
    let newPin = MKPointAnnotation()
    var arrLatitude : NSMutableArray = []
    let pedoMeter = CMPedometer()
    @IBOutlet var lblDistanceinKm: UILabel!
    private var distance = Measurement(value: 0, unit: UnitLength.meters)
    var flagOnceLatLong : Bool = true
    var countdownDuration = Timer()
    var totalTime : Int = 0,startStop : Int = 0
    var totalStepPedometer : NSNumber = 0
    var flagPedometer : Bool = true,flagDistanceStpes = true
    @IBOutlet var btnStartOutlt: UIButton!
    @IBOutlet var lblCalories: UILabel!
    
    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var lblSteps: UILabel!
    var strTotalStep : String!
    private var locationList: [CLLocation] = []
    @IBOutlet var lblDurationText: UILabel!
    @IBOutlet var lblDistanceKm: UILabel!
    @IBOutlet var lblCaloriesText: UILabel!
    @IBOutlet var lblStepsText: UILabel!
    @IBOutlet var tblOnline: UITableView!
    var arrOnlineFriends : NSMutableArray = []
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")
    @IBAction func FreeRunHistory(_ sender: UIButton) {
        let rootVC : FreeRunHistoryVC = FreeRunHistoryVC(nibName: "FreeRunHistoryVC", bundle: nil)
        self.navigationController?.pushViewController(rootVC, animated: true)
    }
    
    
    @IBAction func onSongTapped(_ sender: UIButton) {
        UIApplication.shared.open((URL(string: "music://")!), options: [:], completionHandler: nil)
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"music://"] options:@{} completionHandler:nil];

    }
    @IBAction func onStartTapped(_ sender: UIButton) {
        if startStop == 0 {
            flagPedometer = true
            lblSteps.isHidden = false
            lblStepsText.isHidden = false
            lblCalories.isHidden = false
            lblCalories.text = "0.0"
            lblCaloriesText.isHidden = false
            lblDuration.isHidden = false
            lblDurationText.isHidden = false
            lblDistanceKm.isHidden = false
            lblDistanceinKm.isHidden = false
            flagOnceLatLong = true
            btnStartOutlt.setImage(UIImage(named: "Stop_run"), for: .normal)
            locationManager.startUpdatingLocation()
            getStepsFromPedometer()
            countdownDuration = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
            totalTime = 0
            startStop = 1
        }
        else {
            btnStartOutlt.setImage(UIImage(named: "Start_run"), for: .normal)
            locationManager.stopUpdatingLocation()
            flagOnceLatLong = false
            countdownDuration.invalidate()
            btnStartOutlt.isHidden = true
            StopFreeRun()
        }
    }
    @objc func updateTime() {
        totalTime += 1
//        lblDuration.text = timeString(time: TimeInterval(totalTime))
//        let formattedTime = FormatDisplay.time(totalTime)
//        lblDuration.text = "\(formattedTime)"


        updateDisplay()
    }
    private func updateDisplay() {
            let formattedDistance = FormatDisplay.distance(distance)
            let formattedTime = FormatDisplay.time(totalTime)
            let formattedPace = FormatDisplay.pace(distance: distance,
                                                   seconds: totalTime,
                                                   outputUnit: UnitSpeed.minutesPerMile)
            
            lblDistanceinKm.text = "\(formattedDistance)"
            lblDuration.text = "\(formattedTime)"
            calorieCalc()
    }
    func timeString(time: TimeInterval) -> String {
            let hour = Int(time) / 3600
            let minute = Int(time) / 60 % 60
            let second = Int(time) % 60
            // return formated string
            return String(format: "%02i:%02i:%02i", hour, minute, second)
        }
    func calorieCalc() {
        let str_weightLbs = Double(DataUtil.appdelegate().objUserInfo?.str_weight ?? "0.0")
        let strWeightLbstoKg = str_weightLbs! * 0.45359237
        let str_heightFt = Double(DataUtil.appdelegate().objUserInfo?.str_height ?? "0.0")
        let str_height = str_heightFt! * 30.48
        let walkingFactor = Double(0.57)
        let calorieBurnedPermile = walkingFactor * (strWeightLbstoKg * 2.2)
        let strip = Double (str_height * 0.415)
        let stepCountPerMile = Double( 160934.4 / strip)
        let conversationFactor = Double(calorieBurnedPermile / stepCountPerMile)
        let stepCount = (lblSteps.text! as NSString).doubleValue
        var CaloriesBurned = stepCount * conversationFactor
        if CaloriesBurned < 0 {
            CaloriesBurned = 0.00
        }
        lblCalories.text = String(format: "%.2f", CaloriesBurned)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblSteps.isHidden = true
        lblStepsText.isHidden = true
        lblCalories.isHidden = true
        lblCaloriesText.isHidden = true
        lblDuration.isHidden = true
        lblDurationText.isHidden = true
        lblDistanceKm.isHidden = true
        lblDistanceinKm.isHidden = true
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.startUpdatingLocation()
        }
        else if  CLLocationManager.authorizationStatus() != .authorizedAlways     {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.startUpdatingLocation()
        }
        
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = true
        mapView.delegate = self
        mapView.mapType = .standard
        if #available(iOS 13.0, *) {
            mapView.overrideUserInterfaceStyle = .dark
        }
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        
        if let coor = mapView.userLocation.location?.coordinate{
            mapView.setCenter(coor, animated: true)
        }
        
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
//                if snapshot.childrenCount == 1 {
//                    self.constWidthTbl.constant = 0
////                    self.constLeadingColl.constant = 20
//                    self.lblOnline.isHidden = true
//                }
//                else {
//                    self.constWidthTbl.constant = 60
////                    self.constLeadingColl.constant = 0
//                    self.lblOnline.isHidden = false
//                }
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }

                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
//            else {
//                self.constWidthTbl.constant = 0
//                self.constLeadingColl.constant = 10
//
//                self.lblOnline.isHidden = true
//            }
        }
    }

    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blue
            polylineRenderer.lineWidth = 5
            return polylineRenderer
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      for newLocation in locations {
        mapView.removeAnnotation(newPin)
//        newPin.coordinate = locations.last!.coordinate
//        mapView.addAnnotation(newPin)
        if flagOnceLatLong {
        if let userLocation = locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 200, longitudinalMeters: 200)
            mapView.setRegion(viewRegion, animated: true)
            newPin.coordinate = userLocation
                mapView.addAnnotation(newPin)
        }
        }

        let howRecent = newLocation.timestamp.timeIntervalSinceNow
        guard newLocation.horizontalAccuracy < 20 && abs(howRecent) < 10 else { continue }
        
        if let lastLocation = locationList.last {
            flagOnceLatLong = false
          let delta = newLocation.distance(from: lastLocation)
          distance = distance + Measurement(value: delta, unit: UnitLength.meters)
          let coordinates = [lastLocation.coordinate, newLocation.coordinate]
            mapView.addOverlay(MKPolyline(coordinates: coordinates, count: 2))
            let region = MKCoordinateRegion(center: newLocation.coordinate, latitudinalMeters: 200, longitudinalMeters: 200)
          mapView.setRegion(region, animated: true)
        }
        locationList.append(newLocation)
        newPin.coordinate = locations.last!.coordinate
        mapView.addAnnotation(newPin)
      }
    }
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        if let location = locations.last{
//            mapView.removeAnnotation(newPin)
//                LastLocation = StartLocation
//
//                var location = locations.last! as CLLocation
//                StartLocation = location
//                print("LastLocation",LastLocation)
//                print("NewLocation",StartLocation)
//            if LastLocation != nil {
//                let distance = LastLocation.distance(from: StartLocation)/1000
//                lblDistanceinKm.text = String(format: "%.01f", distance)
//            }
//
//                let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
//                let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
//            mapView.setRegion(region, animated: true)
//            arrLatitude.add(center)
//            if arrLatitude.count > 2 {
//                let myFirstLocation = self.arrLatitude[0] as! CLLocationCoordinate2D
//                let myLastLocation = self.arrLatitude[self.arrLatitude.count - 1] as! CLLocationCoordinate2D
//                let route:[CLLocationCoordinate2D] = [myFirstLocation ,myLastLocation]
//                print("Route-----",route)
//                let polyline = MKPolyline(coordinates: route, count: arrLatitude.count)
//                self.mapView.addOverlay(polyline)
//                arrLatitude.removeAllObjects()
//            }
//                newPin.coordinate = location.coordinate
//                mapView.addAnnotation(newPin)
//            }
//
//
//        //centerMap(locValue)
//    }
    
    func getStepsFromPedometer()
    {
        let now : Date = Date()
        var cal = Calendar.autoupdatingCurrent
        
        var comps: DateComponents? = cal.dateComponents([.year, .month, .day, .hour, .minute], from: now)
        
        
        comps?.hour = 0
        comps?.minute = 0
        comps?.second = 0
        let timeZone = NSTimeZone.system
        cal.timeZone = timeZone
        
        let midnightOfToday = cal.date(from: comps!)
        
        if(CMPedometer.isStepCountingAvailable()){
            let fromDate = Date()
            self.pedoMeter.queryPedometerData(from: fromDate as Date, to: NSDate() as Date) { (data : CMPedometerData!, error) -> Void in
                // print(data)
                  DispatchQueue.main.async
                    {
                        if(error == nil){
                        
                        self.lblSteps.text =  "\(data.numberOfSteps)"
                            print("steps--",self.lblSteps.text!)
                        }
                }
            }
            self.pedoMeter.startUpdates(from: midnightOfToday!) { (data: CMPedometerData!, error) -> Void in
                  DispatchQueue.main.async
                    {
                    if(error == nil){
                        if self.flagPedometer {
                            self.totalStepPedometer = data.numberOfSteps
                            self.flagPedometer = false
                        }
                        let steps = Int(data.numberOfSteps) - Int(self.totalStepPedometer)
                        self.lblSteps.text =  "\(steps)"
                        print("strSTep_Pedo===\(steps)")
                       // self.flagDistanceStpes = false
                    }
                }
            }
        }
    }
}
extension FreeRunVC {
    func StopFreeRun() {
//        self.calorieCalc()
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers = [
            "Authorization": String(format: "Bearer %@",  (str_accessToken)),
            "Accept": "application/json"
        ]
        //        let headers : [String : String] = ["Accept": "application/json"]
        let postParam = ["duration":lblDuration.text!,"distance":lblDistanceinKm.text!, "steps":lblSteps.text!,"calories" : lblCalories.text!,"image" : ""]
        
        var isImage:Bool = true
        var selectedImage:UIImage?
        selectedImage = UIImage(named: "map_img")
        if selectedImage == nil{
            isImage = false
        }
        
        // DataUtil.alertMessage("You have been registered succesfully", viewController: self)
        
        ServerCommunication.getDataWithPostImageForTrainer(url: ConstantFiles.stopFreeRun, image: selectedImage as Any,parameter: postParam, viewController: self, isImage:isImage,success: { (successDict) in
            
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
//                DataUtil.appdelegate().objUserInfo = UserInfo(dictUserInfo: successDict["response"] as! NSArray)
                //                DataUtil.appdelegate().objUserInfo?.str_accessToken = successDict["access_token"] as! String
                let responseDict:NSArray = successDict["response"] as! NSArray
//                print(responseDict)
//                if item
                showAlertWithTitleFromVC(vc: self, title: "", andMessage: (successDict.value(forKey: "message") as? String)!, buttons: ["OK"]) { (index) in
                    if index == 0 {
                        let rootVC : FreeRunHistoryVC = FreeRunHistoryVC(nibName: "FreeRunHistoryVC", bundle: nil)
                        self.navigationController?.pushViewController(rootVC, animated: true)

//                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "errors") as? String)!, viewController: self)
            }
            
            
            // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
    }
}

//MARK: TABLEVIEW DELEGATE & DATAOURCE METHODS
extension FreeRunVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOnlineFriends.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell : onlineTVCTableViewCell = tblOnline.dequeueReusableCell(withReuseIdentifier: "onlineTVCTableViewCell", for: indexPath) as! onlineTVCTableViewCell
//        cell.selectionStyle = .none
////        cell.imgOnlineFriends.image
//        return cell
        let identifier = "onlineTVCTableViewCell"
        var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
        
        if logincell == nil {
            tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
        }
        logincell?.selectionStyle = .none
//        logincell?.layer.cornerRadius = 25
        let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
        let url = URL(string: dict["pic"] as! String)
        logincell?.imgOnlineFriends.setImageFrom(url)
        logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
        
        return logincell!

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
        flagPark = false
        flagGym = false
        flagGallery = false
        let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
        obj_phonelistvc.str_user_id = dict["userId"] as! String
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
}
