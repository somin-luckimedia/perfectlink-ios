

import UIKit

class ParkLinkListCell: UITableViewCell {
    
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var img_Parkimage: UIImageView!
    @IBOutlet weak var lbl_parkName: UILabel!
    @IBOutlet weak var lbl_userCount: UILabel!
    @IBOutlet weak var lbl_trainerCount: UILabel!
    @IBOutlet weak var lbl_parkAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCellwithModal(obj_park:Park){
        
        self.lbl_distance.text = obj_park.distance
        self.lbl_parkName.text = obj_park.Name
        self.lbl_parkAddress.text = obj_park.Address
        self.lbl_userCount.text = obj_park.totalNumberOfUsers
        self.lbl_trainerCount.text = obj_park.totalNumberofTrainers
        let url = URL(string: obj_park.imageURL)
        img_Parkimage.setImageFrom(url)
//        img_Parkimage.setImageFromURL(stringImageUrl: obj_park.imageURL)
        
        
    }
    
}
