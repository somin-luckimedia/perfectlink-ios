

import UIKit
import Alamofire
import Firebase

struct Park {
    
    var Name = ""
    var Address = ""
    var distance = ""
    var imageURL = ""
    var totalNumberofTrainers = "0"
    var totalNumberOfUsers = "0"
    var id = "0"
    var parktype = "0"
    var lat = "0.0"
    var long = "0.0"
    
    
    init(objPark : NSDictionary) {
        let tempparkID = objPark["park_id"] as? NSNumber ?? NSNumber ()
        let temptotalTrainer = objPark["total_trainers"] as? NSNumber ?? NSNumber ()
        let tempdistance = objPark["distance"] as? NSNumber ?? NSNumber ()
        let tempTotalUser = objPark["total_users"] as? NSNumber ?? NSNumber ()
        self.Name = objPark["park_name"] as? String ?? ""
        self.Address = objPark["park_address"] as? String ?? ""
//        self.distance = String(format: "%.2f", tempdistance.doubleValue)  + " Miles"
        self.distance = String(format: "%.2f Miles", tempdistance.doubleValue)
        self.imageURL = objPark["park_image"] as? String ?? ""
        self.totalNumberofTrainers = temptotalTrainer.stringValue
        self.totalNumberOfUsers = tempTotalUser.stringValue
        self.id = tempparkID.stringValue
        self.parktype = objPark["park_type"] as? String ?? "0"
        self.lat = objPark["lat"] as? String ?? "0"
        self.long = objPark["lon"] as? String ?? "0"
    }
    
    init() {
        Name = ""
         Address = ""
         distance = ""
         imageURL = ""
         totalNumberofTrainers = "0"
         totalNumberOfUsers = "0"
         id = "0"
         parktype = "0"
         lat = "0.0"
         long = "0.0"
    }
}
//MARK: SearchBar Delegate Methods
extension ParkLinkListVC: UISearchBarDelegate {
    override func touchesBegan(_ touches: Set<UITouch>,with event: UIEvent?){
        searchBar.resignFirstResponder()
    }
    //    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    //        searchBar.setShowsCancelButton(true, animated: true)
    //    }
    //
    //    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    //        searchBar.setShowsCancelButton(false, animated: true)
    //    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchBar.text?.count == 0 {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            searchEnabled = false
                tbl_parklist.reloadData()
                tbl_parklist.layoutIfNeeded()
                tbl_parklist.setNeedsLayout()

        }
        else {
            searchEnabled = true
            self.searchText = searchBar.text!
            filterContentforSearchText(searchText: searchBar.text!)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
//        Analytics.logEvent("recipe_submit_search", parameters: nil)
        searchBar.resignFirstResponder()
        if searchBar.text == "" {
        }
        else
        {
            searchEnabled = true
            filterContentforSearchText(searchText: searchBar.text!)//khushbu_new
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchEnabled = false
        tbl_parklist.reloadData()
        tbl_parklist.layoutIfNeeded()
        tbl_parklist.setNeedsLayout()
        
        
    }
}

extension ParkLinkListVC {
    //MARK: Filter Methods
    func filterContentforSearchText(searchText: String) {
        
        //        let strFavBtnStatus = StaticClass.retrieve(fromUserDefaults: "isBtnFavRec")
        //        if strFavBtnStatus == "YES"
        //        {
        arrFavSearchHeader = []
        dictFavSearchData = [:]
        
        for i in 0..<arr_parklist.count {
            let dict = arr_parklist[i]
            let strTitle = dict.Name
            
            if strTitle.lowercased().range(of:searchText.lowercased()) != nil {
                arrFavSearchHeader.add(dict)
            }
        }
        
        //        if (self.arrFavSearchHeader.count > 0){
        //                for i in 0..<arrFavHeaderData.count{
        //                    let strHeader = arrFavSearchHeader[i]
        //                    let arrData = dictFavSearchData[strHeader] as! NSArray
        //
        //                    let arrDictAdd:NSMutableArray = []
        //
        ////                    for j in 0..<array_categories.count{
        //////                        var objFlatTummyInfo : SquatInfo = SquatInfo()
        ////                        objFlatTummyInfo = arrData.object(at: j) as! SquatInfo
        ////                        let strTitle = objFlatTummyInfo.Recipe_title
        ////
        ////                        if strTitle.lowercased().range(of:searchText.lowercased()) != nil {
        ////
        ////                            if !arrFavSearchHeader.contains(strHeader) {
        ////                                arrFavSearchHeader.add(strHeader)
        ////                            }
        ////                            arrDictAdd.add(arrData[j])
        ////                            dictFavSearchData[strHeader] = arrDictAdd
        ////                        }
        ////                    }
        //                }
        //            }
        tbl_parklist.reloadData()
        tbl_parklist.setNeedsLayout()
        tbl_parklist.layoutIfNeeded()
        
    }
}
class ParkLinkListVC: UIViewController, UITableViewDelegate,UITableViewDataSource, SliderFilterdelegate {
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    var searchEnabled = false
    var searchText = ""

    @IBOutlet var vwSearchBar: UIView!
    @IBAction func btnCloseIcnTapped(_ sender: UIButton) {
        vwSearchBar.isHidden = true
    }
    @IBAction func btnSearchTapped(_ sender: Any) {
        vwSearchBar.isHidden = false
    }
    var arrFavSearchHeader : NSMutableArray = []
    var dictFavSearchData:NSMutableDictionary = [:]
    @IBOutlet var searchBar: UISearchBar!
    
    var str_radius:String! = "50"
    @IBOutlet weak var tbl_parklist: UITableView!
    @IBOutlet weak var lbl_Noresult: UILabel!
    var arr_parklist = [Park]()
    @IBOutlet var tblOnline: UITableView!
    var arrOnlineFriends : NSMutableArray = []
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.layer.cornerRadius = 5.0
//        searchBar.barTintColor = .clear
//        searchBar.backgroundColor = .clear
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont(name: "Lato-Regular", size: 16.0)
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        searchBar.setShowsCancelButton(false, animated: true)
        textFieldInsideSearchBar?.leftViewMode = .never
        self.searchBar.searchTextPositionAdjustment = UIOffset(horizontal: -19, vertical: 0)
        textFieldInsideSearchBar?.placeholder = "Search"
        textFieldInsideSearchBar?.borderStyle = .none
        textFieldInsideSearchBar?.textColor = .white
//        searchBar.isHidden = true
        vwSearchBar.isHidden = true
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
//                if snapshot.childrenCount == 1 {
//                    self.constWidthTbl.constant = 0
////                    self.constLeadingColl.constant = 20
//                    self.lblOnline.isHidden = true
//                }
//                else {
//                    self.constWidthTbl.constant = 60
////                    self.constLeadingColl.constant = 0
//                    self.lblOnline.isHidden = false
//                }
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }

                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
//            else {
//                self.constWidthTbl.constant = 0
//                self.constLeadingColl.constant = 10
//
//                self.lblOnline.isHidden = true
//            }
        }

        self.tbl_parklist.register(UINib(nibName: "ParkLinkListCell", bundle: nil), forCellReuseIdentifier: "ParkLinkListCell")
        self.LoadParkListNearMe(str_radius: str_radius)
        let notificationCenter = NotificationCenter.default
               notificationCenter.addObserver(self,
                                              selector: #selector(refreshlist),
                                              name: NSNotification.Name(rawValue: "LocationUpdated"),
                                              object: nil)
    }
    
    @objc func refreshlist(){
        self.LoadParkListNearMe(str_radius: str_radius)
    }
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "filter_icon"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.filter_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_filter_click(_ sender: UIButton) {
        let obj_ferrylistingVC : SliderVC = SliderVC(nibName: "SliderVC", bundle: nil)
        obj_ferrylistingVC.delegate = self
        obj_ferrylistingVC.str_title = "Park Filter"
        let color_obj:UIColor = UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 0.2)
        
        presentPopup(obj_ferrylistingVC,
                     animated: true,
                     backgroundStyle: .color(color_obj), // present the popup with a blur effect has background
            constraints: [.width(300), .height(225)], // fix leading edge and the width
            transitioning: .zoom, // the popup come and goes from the left side of the screen
            autoDismiss: true, // when touching outside the popup bound it is not dismissed
            completion: nil)
    }
    
    
    @objc func filter_btn_clicked(){
        
        let obj_ferrylistingVC : SliderVC = SliderVC(nibName: "SliderVC", bundle: nil)
        obj_ferrylistingVC.delegate = self
        obj_ferrylistingVC.str_title = "Trainer Filter"
        obj_ferrylistingVC.str_current = str_radius
        let color_obj:UIColor = UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 0.6)
        presentPopup(obj_ferrylistingVC,
                     animated: true,
                     backgroundStyle: .color(color_obj), // present the popup with a blur effect has background
            constraints: [.width(300), .height(225)], // fix leading edge and the width
            transitioning: .zoom, // the popup come and goes from the left side of the screen
            autoDismiss: true, // when touching outside the popup bound it is not dismissed
            completion: nil)
        
    }
    
    func filter_button_clicked(value: String) {
        print("Slider value ", value)
        str_radius = value
        self.LoadParkListNearMe(str_radius: value)
    }
    
    
    func LoadParkListNearMe(str_radius: String){
        
        let strLat:String = DataUtil.appdelegate().str_lattitude
        let strLong:String = DataUtil.appdelegate().str_longitude
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam: Parameters = ["lat":strLat, "lon":strLong, "radius":str_radius]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        self.arr_parklist.removeAll()
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.ParkLinkList, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_Park:Park = Park(objPark: dict)
                        self.arr_parklist.append(obj_Park)
                    }
                
                    if self.arr_parklist.count == 0{
                        self.lbl_Noresult.isHidden = false
                    }else{
                        self.lbl_Noresult.isHidden = true
                    }
                    self.tbl_parklist.reloadData()
                }else{
                        self.lbl_Noresult.isHidden = false
                    //DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                self.lbl_Noresult.isHidden = false
                //DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == tblOnline {
            return 60
        }
        else {
            return 125
        }
    }
       
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblOnline {
//            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
//            let objFriendsChatVC:UserProfileVC = UserProfileVC(nibName: "UserProfileVC", bundle: nil)
//            //            let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
//            objFriendsChatVC.str_user_id = dict["userId"] as! String
//            objFriendsChatVC.strEditorBack = "Edit"
//            //            objFriendsChatVC.myFriendID = dict["userId"] as! String
//            //            objFriendsChatVC.friendName = dict["name"] as! String
//            self.navigationController?.pushViewController(objFriendsChatVC, animated: true)
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            flagPark = false
            flagGym = false
            flagGallery = false
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            obj_phonelistvc.str_user_id = dict["userId"] as! String
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)

        }
        else {
            let obj_requestmodal:Park!
            
            if searchEnabled {
                obj_requestmodal = arrFavSearchHeader[indexPath.row] as? Park
            }
            else {
                obj_requestmodal = self.arr_parklist[indexPath.row]
            }
            let obj_DetailParkvc:ParkLinkDetailVC = ParkLinkDetailVC(nibName: "ParkLinkDetailVC", bundle: nil)
            obj_DetailParkvc.str_parkID = obj_requestmodal.id
            self.navigationController?.pushViewController(obj_DetailParkvc, animated: true)
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblOnline {
            return arrOnlineFriends.count
            
        }
        else {
            if searchEnabled {
                return self.arrFavSearchHeader.count
            }
            else {
                return self.arr_parklist.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView ==  tblOnline {
            let identifier = "onlineTVCTableViewCell"
            var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
            
            if logincell == nil {
                tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
            }
            logincell?.selectionStyle = .none
    //        logincell?.layer.cornerRadius = 25
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            let url = URL(string: dict["pic"] as! String)
            logincell?.imgOnlineFriends.setImageFrom(url)
            logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
            
            return logincell!
        }
        else {
            let identifier = "ParkLinkListCell"
            let cell: ParkLinkListCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? ParkLinkListCell
            let objpark : Park!
            if searchEnabled {
                objpark = arrFavSearchHeader[indexPath.row] as? Park
            }
            else {
                objpark = self.arr_parklist[indexPath.row]
            }
            cell!.updateCellwithModal(obj_park: objpark)
            cell?.selectionStyle = .none
            return cell!
        }
    }
    
    
}
