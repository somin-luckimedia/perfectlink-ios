//
//  ParkLinkDetailVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 10/11/20.
//

import UIKit
import Alamofire

struct ParkTrainer {
    var user_id = ""
    var park_id = ""
    var name = ""
    var profile_picture = ""
    var rating = "0"
    
    init(objTrainer : NSDictionary) {
        let tempParkID = objTrainer["park_id"] as? NSNumber ?? NSNumber()
        self.user_id = objTrainer["user_id"] as? String ?? ""
        self.park_id = tempParkID.stringValue
        self.name = objTrainer["name"] as? String ?? ""
        self.profile_picture = objTrainer["profile_picture"] as? String ?? ""
        let ratingtemp =  objTrainer["rating"] as? NSNumber ?? NSNumber()
        self.rating = String(format: "%.1f", ratingtemp.doubleValue)
    }
    
    init() {
        self.user_id = ""
        self.park_id = ""
        self.name = ""
        self.profile_picture = ""
        self.rating = "0"
    }
}

struct ParkUser {
    var user_id = ""
    var park_id = ""
    var name = ""
    var profile_picture = ""
    
    init(objUser : NSDictionary) {
        let tempParkID = objUser["park_id"] as? NSNumber ?? NSNumber()
        self.user_id = objUser["user_id"] as? String ?? ""
        self.park_id = tempParkID.stringValue
        self.name = objUser["name"] as? String ?? ""
        self.profile_picture = objUser["profile_picture"] as? String ?? ""
    }
    
    init() {
        self.user_id = ""
        self.park_id = ""
        self.name = ""
        self.profile_picture = ""
    }
    
}



struct  ParkDetail {
    
    var id = "0"
    var park_name = ""
    var park_type  = ""
    var lat = ""
    var lon = ""
    var park_image = ""
    var park_address = ""
    var total_trainers = "0"
    var total_users = "0"
    var distance = "0.0"
    var galary_count = "0"
    var going_in_status = false
    var aboutPark = ""
    var arrTrainer = [ParkTrainer]()
    var arrUser = [ParkUser]()
    var str_star_rating = ""

    init(objParkDetail : NSDictionary) {
        
        let tempUserCount = objParkDetail["total_users"] as? NSNumber ?? NSNumber()
        let tempTrainerCount = objParkDetail["total_trainers"] as? NSNumber ?? NSNumber()
        let tempGalleryCount = objParkDetail["galary_count"] as? NSNumber ?? NSNumber()
        let tempParkID = objParkDetail["id"] as? NSNumber ?? NSNumber()
        let tempParytype = objParkDetail["park_type"] as? NSNumber ?? NSNumber()
        let tempgoing_in = objParkDetail["going_in_status"] as? NSNumber ?? NSNumber()

        
        self.id = tempParkID.stringValue
        self.park_name = objParkDetail["park_name"] as? String ?? ""
        let rating = objParkDetail["star_rating"] as? String ?? ""
        self.str_star_rating = "\(rating)"
        self.park_type  = tempParytype.stringValue
        self.lat = objParkDetail["lat"] as? String ?? ""
        self.lon = objParkDetail["lon"] as? String ?? ""
        self.park_image = objParkDetail["park_image"] as? String ?? ""
        self.park_address = objParkDetail["park_address"] as? String ?? ""
        self.total_trainers = tempTrainerCount.stringValue
        self.total_users = tempUserCount.stringValue
        let distancetemp = objParkDetail["distance"] as? NSNumber ?? NSNumber()
        self.distance = String(format: "%.2f Miles", distancetemp.doubleValue)
        self.aboutPark = objParkDetail["about_park"] as? String ?? ""
        self.galary_count = tempGalleryCount.stringValue
        self.going_in_status = tempgoing_in.boolValue
        self.arrTrainer = [ParkTrainer]()
        self.arrUser = [ParkUser]()
        
        let temparrTrainer = objParkDetail["trainer_list"] as? [NSDictionary] ?? [NSDictionary]()
        for trainerProfile in temparrTrainer{
            let tempTrainer = ParkTrainer(objTrainer: trainerProfile)
            self.arrTrainer.append(tempTrainer)
        }
        let temparrUser = objParkDetail["user_list"] as? [NSDictionary] ?? [NSDictionary]()
        for userProfile in temparrUser{
            let tempUser = ParkUser(objUser: userProfile)
            self.arrUser.append(tempUser)
        }
    }
    
    init() {
        self.id = "0"
        self.park_name = ""
        self.str_star_rating = ""
        self.park_type  = ""
        self.lat = ""
        self.lon = ""
        self.park_image = ""
        self.park_address = ""
        self.total_trainers = "0"
        self.total_users = "0"
        self.distance = "0.0"
        self.galary_count = "0"
        self.going_in_status = false
        self.aboutPark = ""
        self.arrTrainer = [ParkTrainer]()
        self.arrUser = [ParkUser]()
    }
    
    
    
}

class ParkLinkDetailVC: UIViewController {

    @IBAction func btnMapTapped(_ sender: UIButton) {
        let obj_GymDetailVC:ParkLinkDetailMapVC = ParkLinkDetailMapVC(nibName: "ParkLinkDetailMapVC", bundle: nil)
        obj_GymDetailVC.strLat = objPark.lat
        obj_GymDetailVC.strLon = objPark.long
        obj_GymDetailVC.strTitle = "Park Location"
        self.navigationController?.pushViewController(obj_GymDetailVC, animated: true)

    }
    @IBAction func btnAddTapped(_ sender: UIButton) {
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Add gym to profile", style: .default) { action -> Void in
            self.getAddGymProfile()
        }
        let secondAction: UIAlertAction = UIAlertAction(title: "Add users/trainers from gym", style: .default) { action -> Void in
            let obj_GymDetailVC:GymFriendUserTrainer = GymFriendUserTrainer(nibName: "GymFriendUserTrainer", bundle: nil)
            obj_GymDetailVC.str_gymId = self.str_parkID
            obj_GymDetailVC.strType = "PARK"
            self.navigationController?.pushViewController(obj_GymDetailVC, animated: true)
        }
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        
        // add actions
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(cancelAction)
        
        //khushbu_new
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad )
        {
//            if let currentPopoverpresentioncontroller = actionSheetController.popoverPresentationController{
//                currentPopoverpresentioncontroller.sourceView = btnTYpeOutlt
//                currentPopoverpresentioncontroller.sourceRect = btnTYpeOutlt.bounds;
//                //
//
//                currentPopoverpresentioncontroller.permittedArrowDirections = UIPopoverArrowDirection.up;
//                self.present(actionSheetController, animated: true, completion: nil)
//            }
        }else{
            self.present(actionSheetController, animated: true, completion: nil)
        }
    }
    
    func getAddGymProfile()
    {
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
 
        let postParam = ["id":str_parkID,"user_id" : str_userid,"type" : "PARK"]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.add_to_Profile, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
//                if let arr = successDict.value(forKey:"response"){
//
//                    self.obj_gymDetail = GymDetailModel(dictUserInfo: arr as! NSDictionary)
//                  //  self.tableview_trainerHome.reloadData()
//                    self.imgCover.setImageFromURL(stringImageUrl: self.obj_gymDetail.str_cover_image)
//                    self.lblGymTitle.text = self.obj_gymDetail.str_gym_title
//                    self.rateView.rate = CGFloat(Float(self.obj_gymDetail.str_star_rating)!)
//                    self.lblAddress.text = self.obj_gymDetail.str_address
//                    self.lblTime.text = self.obj_gymDetail.str_open_close
//                    self.txtGymDetail.isScrollEnabled = false
//                    self.txtGymDetail.text = self.obj_gymDetail.str_full_description
//                    self.constheightTextView.constant = self.txtGymDetail.contentSize.height + 20
//                    self.collEquipment.reloadData()
//                    if self.obj_gymDetail.array_userList.count == 0 {
//                        self.constHeightUser.constant = 0
//                    }
//                    else {
//                    self.collUsers.reloadData()
//                    let height = self.collUsers.collectionViewLayout.collectionViewContentSize.height
//                    self.constCollUserHeight.constant = height
//                    self.constHeightUser.constant = height + 70
//                    self.view.setNeedsLayout()
//                    self.collUsers.reloadData()
//                    }
//                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
//                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func vwRateTapped(_ sender: UIButton) {
        self.vwRateView.isHidden = false
        rateView_view.alignment = RateViewAlignmentCenter
        rateView_view.editable = true
        self.rateView_view.rate = 0
    }
    @IBAction func actbtn_ShareExperiancebtnClicked(_ sender: Any) {
        
        if (self.objParkDetail.going_in_status){
            let obj_ShareExperiance:ShareYourExperianceVC = ShareYourExperianceVC(nibName: "ShareYourExperianceVC", bundle: nil)
            obj_ShareExperiance.str_parkID = self.objParkDetail.id
            self.navigationController?.pushViewController(obj_ShareExperiance, animated: true)
            
        }else{
            DataUtil.alertMessage("Please join community first to share your experience with other's.", viewController: self)
        }
        
    }
    
    @IBAction func actbtn_GoingInbtnClicked(_ sender: Any) {
        
        let myUserID = (DataUtil.appdelegate().objUserInfo?.userId)!
        let postParam: Parameters = ["park_id":self.objParkDetail.id, "user_id":myUserID]
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        print("Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.ParkGoingIn, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                
                let tempResponse = successDict.value(forKey: "response") as? NSNumber ?? NSNumber(1)
                if tempResponse.boolValue
                {
                    self.objParkDetail.going_in_status = true
                    self.btn_goingIn.setTitle("Leave Community", for: .normal)
                    self.btn_goingIn.setTitle("Leave Community", for: .highlighted)
                    self.btn_goingIn.setTitle("Leave Community", for: .selected)
                }else
                {
                    self.objParkDetail.going_in_status = false
                    self.btn_goingIn.setTitle("GOING IN", for: .normal)
                    self.btn_goingIn.setTitle("GOING IN", for: .highlighted)
                    self.btn_goingIn.setTitle("GOING IN", for: .selected)
                }
                
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
        
        
        
    }
    
    @IBAction func btnTrainerTapped(_ sender: UIButton) {
        self.strType = "Trainer"
      //  self.lblUserLine.isHidden = true
//        self.lblTrainerLine.isHidden = false
        self.lblUserLine.backgroundColor = UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        self.lblTrainerLine.backgroundColor = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 9.0/255.0, alpha: 1.0)
        self.collUsers.reloadData()
        let height = self.collUsers.collectionViewLayout.collectionViewContentSize.height
        self.constCollUserHeight.constant = height
        self.constHeightUser.constant = height + 100
        self.view.setNeedsLayout()
        self.collUsers.reloadData()
        
    }
    
    @IBAction func btnUserTapped(_ sender: UIButton) {
        self.strType = "User"
//        self.lblUserLine.isHidden = false
//        self.lblTrainerLine.isHidden = true
        self.lblTrainerLine.backgroundColor = UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        self.lblUserLine.backgroundColor = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 9.0/255.0, alpha: 1.0)
        self.collUsers.reloadData()
        let height = self.collUsers.collectionViewLayout.collectionViewContentSize.height
        self.constCollUserHeight.constant = height
        self.constHeightUser.constant = height + 100
        self.view.setNeedsLayout()
        self.collUsers.reloadData()
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        strRate =  String(format: "%.f", rateView.rate)
        print(strRate)
        self.getAddRate()
    }
    func getAddRate()
    {
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
 
        let postParam = ["park_id":str_parkID,"type" : "PARK","rating" : strRate]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.add_star_rating, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            self.vwRateView.isHidden = true
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                }))
//                if let arr = successDict.value(forKey:"response"){
//
//                    self.obj_gymDetail = GymDetailModel(dictUserInfo: arr as! NSDictionary)
//                  //  self.tableview_trainerHome.reloadData()
//                    self.imgCover.setImageFromURL(stringImageUrl: self.obj_gymDetail.str_cover_image)
//                    self.lblGymTitle.text = self.obj_gymDetail.str_gym_title
//                    self.rateView.rate = CGFloat(Float(self.obj_gymDetail.str_star_rating)!)
//                    self.lblAddress.text = self.obj_gymDetail.str_address
//                    self.lblTime.text = self.obj_gymDetail.str_open_close
//                    self.txtGymDetail.isScrollEnabled = false
//                    self.txtGymDetail.text = self.obj_gymDetail.str_full_description
//                    self.constheightTextView.constant = self.txtGymDetail.contentSize.height + 20
//                    self.collEquipment.reloadData()
//                    if self.obj_gymDetail.array_userList.count == 0 {
//                        self.constHeightUser.constant = 0
//                    }
//                    else {
//                    self.collUsers.reloadData()
//                    let height = self.collUsers.collectionViewLayout.collectionViewContentSize.height
//                    self.constCollUserHeight.constant = height
//                    self.constHeightUser.constant = height + 70
//                    self.view.setNeedsLayout()
//                    self.collUsers.reloadData()
//                    }
//                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
//                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        self.vwRateView.isHidden = true
    }
    @IBOutlet var imgCover: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var collUsers: UICollectionView!
    
    @IBOutlet var lblAboutTitle: UILabel!
    @IBOutlet var rateView: DYRateView!
    @IBOutlet var txtAbout: UITextView!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblMiles: UILabel!
    @IBOutlet var constheightTextView: NSLayoutConstraint!
    @IBOutlet var constCollUserHeight: NSLayoutConstraint!
    @IBOutlet var constHeightUser: NSLayoutConstraint!

    @IBOutlet var lblUserLine: UILabel!
    @IBOutlet var lblTrainerLine: UILabel!
    var objPark = Park()
    var str_parkID = "",strType = "Trainer"
    var objParkDetail = ParkDetail()
    @IBOutlet var rateView_view: DYRateView!
    @IBOutlet var vwRateView: UIView!
    var strRate = ""
    @IBOutlet weak var btn_goingIn: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
//        lblUserLine.isHidden = true
        self.collUsers.register(UINib(nibName: "joinedUserCell", bundle: nil), forCellWithReuseIdentifier: "joinedUserCell")
        self.vwRateView.isHidden = true

        loadParkDetail()
        

        // Do any additional setup after loading the view.
    }
}


//MARK: WEBSERVICE CLL METHODS
extension ParkLinkDetailVC {
    func loadParkDetail(){
        
//        let strLat:String = DataUtil.appdelegate().str_lattitude
//        let strLong:String = DataUtil.appdelegate().str_longitude
        let strLat:String = DataUtil.appdelegate().str_lattitude
        let strLong:String = DataUtil.appdelegate().str_longitude

        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam =  ["lat":strLat, "lon":strLong, "park_id":self.str_parkID]
        print("Login Post Parameter : \(postParam)")
        
        let headers = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        self.objParkDetail.arrUser.removeAll()
        self.objParkDetail.arrTrainer.removeAll()
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.ParkLinkDetail, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let dict = successDict.value(forKey:"response"){
                    let tempdict = dict as! NSDictionary
                    self.objParkDetail = ParkDetail(objParkDetail: tempdict)
                    let url = URL(string: self.objParkDetail.park_image)
                    self.imgCover.setImageFrom(url)
                    self.lblTitle.text = self.objParkDetail.park_name
                    self.lblAboutTitle.text = "About \(self.objParkDetail.park_name)"
                    self.lblMiles.text = self.objParkDetail.distance//String(format: "%.2f Miles", self.objParkDetail.distance)
                    self.rateView.rate = CGFloat(Float(self.objParkDetail.str_star_rating)!)
                    self.lblAddress.text = self.objParkDetail.park_address
                    self.txtAbout.isScrollEnabled = false
                    self.txtAbout.text = self.objParkDetail.aboutPark
                    self.constheightTextView.constant = self.txtAbout.contentSize.height + 20
                    if self.objParkDetail.arrTrainer.count > 0 {
                        self.strType = "Trainer"
                        self.collUsers.reloadData()
                        let height = self.collUsers.collectionViewLayout.collectionViewContentSize.height
                        self.constCollUserHeight.constant = height
                        self.constHeightUser.constant = height + 100
                        self.view.setNeedsLayout()
                        self.collUsers.reloadData()
                    }
                    if (self.objParkDetail.going_in_status){
                        self.btn_goingIn.setTitle("Leave Community", for: .normal)
                        self.btn_goingIn.setTitle("Leave Community", for: .highlighted)
                        self.btn_goingIn.setTitle("Leave Community", for: .selected)
                    }else{
                        self.btn_goingIn.setTitle("GOING IN", for: .normal)
                        self.btn_goingIn.setTitle("GOING IN", for: .highlighted)
                        self.btn_goingIn.setTitle("GOING IN", for: .selected)
                    }
//                    self.lbl_parkDistance.text = self.objParkDetail.distance
//                    self.lbl_parkName.text = self.objParkDetail.park_name
//                    self.lbl_parkAddress.text = self.objParkDetail.park_address
//                    self.img_park.setImageFromURL(stringImageUrl: self.objParkDetail.park_image)
//                    self.lbl_aboutPark.text = self.objParkDetail.aboutPark
//                    self.btn_galleryCount.setTitle(self.objParkDetail.galary_count, for: .normal)
//                    
//                    self.tbl_userView.reloadData()
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
}

//MARK : COLLECTION VIEW DELEGATE & DATASOURCE METHODS
extension ParkLinkDetailVC : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if strType == "User" {
            return self.objParkDetail.arrUser.count
        }
        else {
            return self.objParkDetail.arrTrainer.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow:CGFloat = 3
        let spacingBetweenCells:CGFloat = 0
        
        let totalSpacing = (2 * 5) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        // if let collection = self.collectionData{
        let width = (collUsers.bounds.width - totalSpacing)/numberOfItemsPerRow
        return CGSize(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:joinedUserCell = collectionView.dequeueReusableCell(withReuseIdentifier: "joinedUserCell", for: indexPath) as! joinedUserCell
        if strType == "Trainer" {
            let dict = self.objParkDetail.arrTrainer[indexPath.row]
            let url = URL(string: dict.profile_picture)
            cell.img_user.setImageFrom(url)
            cell.lbl_name.text = dict.name
        }
        else {
            let dict = self.objParkDetail.arrUser[indexPath.row]
            let url = URL(string: dict.profile_picture)
            cell.img_user.setImageFrom(url)

//            cell.img_user.setImageFromURL(stringImageUrl: dict.profile_picture)
            cell.lbl_name.text = dict.name
        }
        
        //            let dict_equipement:NSDictionary = self.obj_gymDetail.array_userList[indexPath.row] as!NSDictionary
        //cell.updateCellwithModal(obj_userModel: dict_equipement)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if strType == "Trainer" {
            let dict = self.objParkDetail.arrTrainer[indexPath.row]
//            cell.img_user.setImageFromURL(stringImageUrl: dict.profile_picture)
//            cell.lbl_name.text = dict.name
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            obj_phonelistvc.str_user_id = dict.user_id
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)

        }
        else {
            let dict = self.objParkDetail.arrUser[indexPath.row]
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            obj_phonelistvc.str_user_id = dict.user_id
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
    }
}
    

