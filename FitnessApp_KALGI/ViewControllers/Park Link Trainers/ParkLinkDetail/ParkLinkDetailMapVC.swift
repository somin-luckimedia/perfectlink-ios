//
//  ParkLinkDetailMapVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 25/03/21.
//

import UIKit
import MapKit
import CoreLocation

class ParkLinkDetailMapVC: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate {
    
    @IBOutlet var lblTitle: UILabel!
    var locationManager = CLLocationManager()
    @IBOutlet var mapView: MKMapView!
    let newPin = MKPointAnnotation()
    var strLat : String = "",strLon : String = "",strTitle = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = strTitle
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            //            locationManager.startUpdatingLocation()
        }
        else if  CLLocationManager.authorizationStatus() != .authorizedAlways     {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            //            locationManager.startUpdatingLocation()
        }
        mapView.delegate = self
        mapView.mapType = .standard
        if #available(iOS 13.0, *) {
            mapView.overrideUserInterfaceStyle = .dark
        }
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        locationManager.startUpdatingLocation()
        
        //        let strLat:String = "40.67556768581136"//DataUtil.appdelegate().str_lattitude
        //        let strLong:String = "-73.67202505200265"//DataUtil.appdelegate().str_longitude
        
        
        //         let regionDistance:CLLocationDistance = 10000
        //         let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        //        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        //         let options = [
        //             MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
        //             MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        //         ]
        //         let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        //         let mapItem = MKMapItem(placemark: placemark)
        //         mapItem.openInMaps(launchOptions: options)
        //        newPin.coordinate = coordinates
        //        mapView.addAnnotation(newPin)
        
        // Do any additional setup after loading the view.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let latitude_ = (strLat as NSString).doubleValue
        let longitude_ = (strLon as NSString).doubleValue
        let latitude: CLLocationDegrees = latitude_
        let longitude: CLLocationDegrees = longitude_
        mapView.removeAnnotation(newPin)
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        
        let viewRegion = MKCoordinateRegion(center: coordinates, latitudinalMeters: 2000, longitudinalMeters: 2000)
        mapView.setRegion(viewRegion, animated: true)
        newPin.coordinate = coordinates
        mapView.addAnnotation(newPin)
        locationManager.stopUpdatingLocation()
    }
}

//MARK: BUTTON METHODS
extension ParkLinkDetailMapVC {
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
