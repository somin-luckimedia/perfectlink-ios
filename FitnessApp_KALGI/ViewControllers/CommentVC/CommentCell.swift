//
//  CommentCell.swift
//  FitnessApp
//
//  Created by Ashish on 25/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//
import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_comment: UILabel!
    @IBOutlet weak var imageView_profile: UIImageView!
    @IBOutlet weak var lbl_minutes: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_bookingmodal:CommentModel){
        lbl_name.text = String(format: "%@", obj_bookingmodal.str_name!)
        lbl_comment.text = String(format: "%@", obj_bookingmodal.str_comment!)
        let url = URL(string: obj_bookingmodal.str_profile_picture)
        imageView_profile.setImageFrom(url)
        
        
        if let timeResult = Double(obj_bookingmodal.str_posted_at) {
            let date = Date(timeIntervalSince1970: timeResult)
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.short //Set time style
            dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
            dateFormatter.timeZone = NSTimeZone() as TimeZone
            let localDate = dateFormatter.string(from: date as Date)
            lbl_minutes.text = localDate
        }
        
    }
}
