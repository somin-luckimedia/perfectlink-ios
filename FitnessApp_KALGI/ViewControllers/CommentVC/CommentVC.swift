//
//  CommentVC.swift
//  FitnessApp
//
//  Created by Ashish on 25/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire

class CommentVC: UIViewController , UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate  {
    
    fileprivate var array_mybookings:NSMutableArray = NSMutableArray()
    @IBOutlet weak var imageView_BG: UIImageView!
    var obj_wallpostModel:WallListModel? = nil
    @IBOutlet weak var tableview_bookings: UITableView!
     @IBOutlet weak var textview_message: UITextView!
    var wallPost_id : Int = 0
    //@IBOutlet weak var inputToolbar: UIView!
    //@IBOutlet weak var textView: GrowingTextView!
    //@IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    
    
    var dict_item:NSDictionary!
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.title = "COMMENTS"
        self.navigationController?.navigationBar.isHidden = true
        tableview_bookings.delegate  = self as UITableViewDelegate
        tableview_bookings.dataSource = self as UITableViewDataSource
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.getMyBookings()
    //    self.addLeftMenuButtonWithImage()
        self.textview_message.placeholder = "Post your comment..."
        self.textview_message.placeholderColor = UIColor.white
        
       
        //tableview_bookings.rowHeight = 110
        //tableview_bookings.estimatedRowHeight = UITableViewAutomaticDimension
    }
    
    
    
    
//    func addLeftMenuButtonWithImage(){
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//        let button = UIButton.init(type: .custom)
//        button.setImage(UIImage(named: "close"), for: UIControl.State.normal)
//        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
//        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
//        let barButton = UIBarButtonItem(customView: button)
//        self.navigationItem.leftBarButtonItem = barButton
//    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "filter_icon"), for: UIControl.State.normal)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func back_btn_clicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        let notificationCenter = NotificationCenter.default
        //DataUtil.loadIndicatorView()
        notificationCenter.post(name: NSNotification.Name(rawValue: "WallPost"), object: nil)
//
//        self.navigationController?.dismiss(animated: true, completion: nil)
//        self.dismiss(animated: true, completion: nil)
    }

    
    func getMyBookings(){
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam: Parameters = ["user_id":str_userid, "wall_id":wallPost_id]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.get_wall_comment, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:CommentModel = CommentModel(dictUserInfo: dict)
                        self.array_mybookings.add(obj_bookingmodal)
                    }
                    self.title = String(format: "COMMENTS(%ld)", self.array_mybookings.count)
                    self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
   
    @IBAction func post_btn_clicked(_ sender: UIButton) {
        print("delegate called")
        
        let str_message  : String = (textview_message.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let str_wall_id:String = "\(wallPost_id)"
        
        let postParam: Parameters = ["wall_id":str_wall_id, "comment":str_message,"user_id":str_userid]
        
        if !changepasswordValidation() {
            return
        }
        
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.add_wall_comment, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                postClick = true
                if let arr = successDict.value(forKey:"response"){
                    self.textview_message.text = ""
                    let dict:NSDictionary = arr as! NSDictionary
                    let obj_bookingmodal:CommentModel = CommentModel(dictUserInfo: dict)
                    self.array_mybookings.insert(obj_bookingmodal, at: 0)
                    self.title = String(format: "COMMENTS(%ld)", self.array_mybookings.count)
                    self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                
                
                
                
                
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func changepasswordValidation() -> Bool {
        if (textview_message.text?.isEmpty)!{
            DataUtil.alertMessage("Comment field is Empty", viewController: self)
            return false
        }
        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        let obj_requestmodal:CommentModel = array_mybookings[indexPath.row] as! CommentModel
        let font = UIFont(name: "Helvetica", size: 12.0)
        let height = heightForView(text: String(format: "%@", obj_requestmodal.str_comment!), font: font!, width: UIScreen.main.bounds.width-40)
        return  height + 90
        
       // return UITableViewAutomaticDimension
    }
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.array_mybookings.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "CommentCell"
        var logincell: CommentCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? CommentCell
        if logincell == nil {
            tableView.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? CommentCell)!
        }
        //logincell!.delegate = self
        let obj_requestmodal:CommentModel = array_mybookings[indexPath.row] as! CommentModel
        logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal)
        return logincell!
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let obj_phonelistvc:ProtienDetailVC = ProtienDetailVC(nibName: "ProtienDetailVC", bundle: nil)
//        let obj_requestmodal:ProteinListModel = array_mybookings[indexPath.row] as! ProteinListModel
//        obj_phonelistvc.str_productId = obj_requestmodal.str_product_id
//        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        let obj_requestmodal:CommentModel = array_mybookings[indexPath.row] as! CommentModel
        let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
        obj_phonelistvc.str_user_id = obj_requestmodal.str_user_id
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        
    }
    
    
}

