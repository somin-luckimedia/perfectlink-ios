//
//  AddActivityCell.swift
//  FitnessApp
//
//  Created by Ashish on 05/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

protocol MakeChallengeActivityCellDelegate:class {
    func camera_btn_clicked(cell: MakeChallengeCell, btn: UIButton)
    func video_btn_clicked(cell: MakeChallengeCell, btn: UIButton)
    func image_btn_clicked(cell: MakeChallengeCell, btn: UIButton)
    func post_btn_clicked(cell: MakeChallengeCell, btn: UIButton)
    func close_video_btn_clicked(cell: MakeChallengeCell, btn: UIButton)
    func close_image_btn_clicked(cell: MakeChallengeCell, btn: UIButton)
    func play_btn_clicked(cell: MakeChallengeCell, btn: UIButton)
}

class MakeChallengeCell: UITableViewCell {

    
    weak var delegate:MakeChallengeActivityCellDelegate?
    @IBOutlet weak var textview_message: UITextView!
    
     @IBOutlet weak var view1: UIView!
     @IBOutlet weak var view2: UIView!
     @IBOutlet weak var view3: UIView!
    
    
    @IBOutlet weak var view_video: UIView!
    @IBOutlet weak var view_image: UIView!
    @IBOutlet weak var constraint_top: NSLayoutConstraint!
    
    @IBOutlet weak var imageview_thumb: UIImageView!
    @IBOutlet weak var video_thumb: UIImageView!
    
    
    @IBOutlet weak var constraint_view1: NSLayoutConstraint!
    @IBOutlet weak var constraint_view2: NSLayoutConstraint!
    @IBOutlet weak var constraint_view3: NSLayoutConstraint!
    
    @IBOutlet weak var btn_complete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.textview_message.placeholder = "Enter your challenge here..."
        self.textview_message.placeholderColor = UIColor.white
       
        // Initialization code
    }

    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func camera_btn_clicked(_ sender: UIButton) {
        self.delegate?.camera_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func video_btn_clicked(_ sender: UIButton) {
         self.delegate?.video_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func image_btn_clicked(_ sender: UIButton) {
         self.delegate?.image_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func post_btn_clicked(_ sender: UIButton) {
        self.delegate?.post_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func close_video_btn_clicked(_ sender: UIButton) {
       self.delegate?.close_video_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func close_image_btn_clicked(_ sender: UIButton) {
        self.delegate?.close_image_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func play_btn_clicked(_ sender: UIButton) {
        self.delegate?.play_btn_clicked(cell: self, btn: sender)
    }
    
    
    
}
