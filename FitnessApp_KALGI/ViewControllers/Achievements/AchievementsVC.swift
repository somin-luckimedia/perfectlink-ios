//
//  AchievementsVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 05/03/21.
//

import UIKit

class AchievementsVC: UIViewController,AchievementsAddDelegate {
    func Achievementsadded_clicked() {
        self.apiAchievements()
    }
    

    fileprivate var array_achievements:NSMutableArray = NSMutableArray()
    @IBOutlet var tblAchievements: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblAchievements.rowHeight = UITableView.automaticDimension
        tblAchievements.estimatedRowHeight = 60.0

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.apiAchievements()
    }
}

//MARK: BUTTON ACTION METHODS
extension AchievementsVC {
    @IBAction func btnAddNewTapped(_ sender: UIButton) {
        let obj_parkeAddressVC:AddAchievementsVC = AddAchievementsVC(nibName: "AddAchievementsVC", bundle: nil)
        obj_parkeAddressVC.delegate = self
        let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

//MARK: TABLEVIEW DELEGATE & DATA SOURCE METHODS
extension AchievementsVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_achievements.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "AchievementTVC"
        var cell: AchievementTVC? = tableView.dequeueReusableCell(withIdentifier: identifier) as? AchievementTVC
        if cell == nil {
            tableView.register(UINib(nibName: "AchievementTVC", bundle: nil), forCellReuseIdentifier: identifier)
            cell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? AchievementTVC)!
        }
        let dict = array_achievements[indexPath.row] as! NSDictionary
        cell!.lblAchievements.text = dict["achievement"] as? String
        cell!.btnDelete.tag = indexPath.row
        cell!.btnDelete.addTarget(self, action: #selector(didTapPayButton), for: .touchUpInside)
        return cell!
    }
    
    @objc private func didTapPayButton(sender:UIButton) {
        
        let refreshAlert = UIAlertController(title: "", message: "Are you sure you want to delete this achievement?", preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.apiAchievementDelete(index: sender.tag)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    

}

//MARK: WEBAPI method
extension AchievementsVC {
    func apiAchievements() {
        self.array_achievements.removeAllObjects()
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam = ["user_id":str_userid]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.trainer_achievement_list, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let dict : NSArray = arr as! NSArray
                    print(dict)
                    for item in dict {
                        self.array_achievements.add(item)
                    }
                    self.tblAchievements.delegate = self
                    self.tblAchievements.dataSource = self
                    self.tblAchievements.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func apiAchievementDelete(index : Int) {
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let dict = array_achievements[index] as! NSDictionary
        let postParam = ["user_id":str_userid,"id" : dict["id"] as! Int] as [String : Any]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.remove_trainer_achievement, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                self.apiAchievements()
//                self.getMyBookings()
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
}
