//
//  AddAchievementsVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 13/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire

protocol AchievementsAddDelegate:class {
    func Achievementsadded_clicked()
}

class AddAchievementsVC: UIViewController {

    weak var delegate: AchievementsAddDelegate?
   @IBOutlet weak var textview_achievement: UITextView!
    
        override func viewDidLoad() {
            super.viewDidLoad()
            self.navigationController?.navigationBar.isHidden = true
//            self.title = "Add Achievements"
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//            self.addLeftMenuButtonWithImage()
        }
        
        
        func addLeftMenuButtonWithImage(){
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            let button = UIButton.init(type: .custom)
            button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
            button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
            button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
            let barButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = barButton
        }
        
    
        @objc func back_btn_clicked(){
            self.dismiss(animated: true, completion: nil)
        }
    
    @IBAction func submit_btn_clicked(_ sender: Any) {
        if self.textview_achievement.text?.isEmpty == true{
            DataUtil.alertMessage("Please add achievement", viewController: self)
            return
        }
        
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        
        let postParam: Parameters = ["achievement":(self.textview_achievement.text)!, "user_id":str_userid]
        print("Login Post Parameter : \(postParam)")
        
        
        
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.add_achievements, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                
                if let array:NSArray = successDict["response"] as? NSArray{
                    DataUtil.appdelegate().objUserInfo?.array_achievements.removeAllObjects()
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_model:AchievementsModel = AchievementsModel.init(dictUserInfo: dict)
                        DataUtil.appdelegate().objUserInfo?.array_achievements.add(obj_model)
                    }
                }
                
                
                
                let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    self.delegate?.Achievementsadded_clicked()
                    self.back_btn_clicked()
                }))
                self.present(alert, animated: true, completion: nil)
                
            }else{
                
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                self.back_btn_clicked()
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
    
        
        // 209, 9, 4
    
    
    

}
