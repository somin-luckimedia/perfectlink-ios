

import UIKit
import Alamofire

//struct ParkTrainer {
//    var user_id = ""
//    var park_id = ""
//    var name = ""
//    var profile_picture = ""
//    var rating = "0"
//    
//    init(objTrainer : NSDictionary) {
//        let tempParkID = objTrainer["park_id"] as? NSNumber ?? NSNumber()
//        self.user_id = objTrainer["user_id"] as? String ?? ""
//        self.park_id = tempParkID.stringValue
//        self.name = objTrainer["name"] as? String ?? ""
//        self.profile_picture = objTrainer["profile_picture"] as? String ?? ""
//        let ratingtemp =  objTrainer["rating"] as? NSNumber ?? NSNumber()
//        self.rating = String(format: "%.1f", ratingtemp.doubleValue)
//    }
//    
//    init() {
//        self.user_id = ""
//        self.park_id = ""
//        self.name = ""
//        self.profile_picture = ""
//        self.rating = "0"
//    }
//}
//
//struct ParkUser {
//    var user_id = ""
//    var park_id = ""
//    var name = ""
//    var profile_picture = ""
//    
//    init(objUser : NSDictionary) {
//        let tempParkID = objUser["park_id"] as? NSNumber ?? NSNumber()
//        self.user_id = objUser["user_id"] as? String ?? ""
//        self.park_id = tempParkID.stringValue
//        self.name = objUser["name"] as? String ?? ""
//        self.profile_picture = objUser["profile_picture"] as? String ?? ""
//    }
//    
//    init() {
//        self.user_id = ""
//        self.park_id = ""
//        self.name = ""
//        self.profile_picture = ""
//    }
//    
//}
//
//
//
//struct  ParkDetail {
//    
//    var id = "0"
//    var park_name = ""
//    var park_type  = ""
//    var lat = ""
//    var lon = ""
//    var park_image = ""
//    var park_address = ""
//    var total_trainers = "0"
//    var total_users = "0"
//    var distance = "0.0"
//    var galary_count = "0"
//    var going_in_status = false
//    var aboutPark = ""
//    var arrTrainer = [ParkTrainer]()
//    var arrUser = [ParkUser]()
//    
//    init(objParkDetail : NSDictionary) {
//        
//        let tempUserCount = objParkDetail["total_users"] as? NSNumber ?? NSNumber()
//        let tempTrainerCount = objParkDetail["total_trainers"] as? NSNumber ?? NSNumber()
//        let tempGalleryCount = objParkDetail["galary_count"] as? NSNumber ?? NSNumber()
//        let tempParkID = objParkDetail["id"] as? NSNumber ?? NSNumber()
//        let tempParytype = objParkDetail["park_type"] as? NSNumber ?? NSNumber()
//        let tempgoing_in = objParkDetail["going_in_status"] as? NSNumber ?? NSNumber()
//        
//        self.id = tempParkID.stringValue
//        self.park_name = objParkDetail["park_name"] as? String ?? ""
//        self.park_type  = tempParytype.stringValue
//        self.lat = objParkDetail["lat"] as? String ?? ""
//        self.lon = objParkDetail["lon"] as? String ?? ""
//        self.park_image = objParkDetail["park_image"] as? String ?? ""
//        self.park_address = objParkDetail["park_address"] as? String ?? ""
//        self.total_trainers = tempTrainerCount.stringValue
//        self.total_users = tempUserCount.stringValue
//        let distancetemp = objParkDetail["distance"] as? NSNumber ?? NSNumber()
//        self.distance = String(format: "%.2f", distancetemp.doubleValue)
//        self.aboutPark = objParkDetail["about_park"] as? String ?? ""
//        self.galary_count = tempGalleryCount.stringValue
//        self.going_in_status = tempgoing_in.boolValue
//        self.arrTrainer = [ParkTrainer]()
//        self.arrUser = [ParkUser]()
//        
//        let temparrTrainer = objParkDetail["trainer_list"] as? [NSDictionary] ?? [NSDictionary]()
//        for trainerProfile in temparrTrainer{
//            let tempTrainer = ParkTrainer(objTrainer: trainerProfile)
//            self.arrTrainer.append(tempTrainer)
//        }
//        let temparrUser = objParkDetail["user_list"] as? [NSDictionary] ?? [NSDictionary]()
//        for userProfile in temparrUser{
//            let tempUser = ParkUser(objUser: userProfile)
//            self.arrUser.append(tempUser)
//        }
//    }
//    
//    init() {
//        self.id = "0"
//        self.park_name = ""
//        self.park_type  = ""
//        self.lat = ""
//        self.lon = ""
//        self.park_image = ""
//        self.park_address = ""
//        self.total_trainers = "0"
//        self.total_users = "0"
//        self.distance = "0.0"
//        self.galary_count = "0"
//        self.going_in_status = false
//        self.aboutPark = ""
//        self.arrTrainer = [ParkTrainer]()
//        self.arrUser = [ParkUser]()
//    }
//    
//    
//    
//}




class DetailParkLinkVC: UIViewController, DetailParkLinkDelegate {
    
    
    
    @IBOutlet weak var btn_galleryCount: UIButton!
    @IBOutlet weak var tbl_userView: UITableView!
    @IBOutlet var view_header: UIView!
    @IBOutlet weak var img_park: UIImageView!
    @IBOutlet weak var lbl_parkName: UILabel!
    @IBOutlet weak var lbl_parkDistance: UILabel!
    @IBOutlet weak var lbl_parkAddress: UILabel!
    @IBOutlet weak var lbl_aboutPark: UILabel!
    @IBOutlet weak var btn_goingIn: UIButton!
    var objPark = Park()
    var str_parkID = ""
    var objParkDetail = ParkDetail()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Park Detail"
        // Do any additional setup after loading the view.
        self.tbl_userView.tableHeaderView = self.view_header
        let headerNib = UINib.init(nibName: "HeaderViewCell", bundle: Bundle.main)
        self.tbl_userView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderViewCell")
        self.tbl_userView.register(UINib(nibName: "DetailParkLinkCell", bundle: nil), forCellReuseIdentifier: "DetailParkLinkCell")
        //LocationUpdated
        
       
        self.loadHeaderView()
        self.addLeftMenuButtonWithImage()
        
        
        
    }
    
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.loadParkDetail()
    }
    
    @IBAction func btnLocation_clicked(_ sender: Any) {
        if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(self.objParkDetail.lat ),\(self.objParkDetail.lon )&directionsmode=driving") {
            UIApplication.shared.open(url, options: [:])
        }
        
        
    }
    
    func loadHeaderView(){
        self.lbl_parkDistance.text = objPark.distance
        self.lbl_parkName.text = objPark.Name
        self.lbl_parkAddress.text = objPark.Address
//        img_park.setImageFromURL(stringImageUrl: objPark.imageURL)
        let url = URL(string: objPark.imageURL)
        img_park.setImageFrom(url)
        self.btn_galleryCount.setTitle("0", for: .normal)
        
    }
    
    func loadParkDetail(){
        
        let strLat:String = DataUtil.appdelegate().str_lattitude
        let strLong:String = DataUtil.appdelegate().str_longitude
        let postParam: Parameters = ["lat":strLat, "lon":strLong, "park_id":self.str_parkID]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""


        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        self.objParkDetail.arrUser.removeAll()
        self.objParkDetail.arrTrainer.removeAll()
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.ParkLinkDetail, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let dict = successDict.value(forKey:"response"){
                    let tempdict = dict as! NSDictionary
                    self.objParkDetail = ParkDetail(objParkDetail: tempdict)
                    if (self.objParkDetail.going_in_status){
                        self.btn_goingIn.setTitle("Leave Community", for: .normal)
                        self.btn_goingIn.setTitle("Leave Community", for: .highlighted)
                        self.btn_goingIn.setTitle("Leave Community", for: .selected)
                    }else{
                        self.btn_goingIn.setTitle("GOING IN", for: .normal)
                        self.btn_goingIn.setTitle("GOING IN", for: .highlighted)
                        self.btn_goingIn.setTitle("GOING IN", for: .selected)
                    }
                    self.lbl_parkDistance.text = self.objParkDetail.distance
                    self.lbl_parkName.text = self.objParkDetail.park_name
                    self.lbl_parkAddress.text = self.objParkDetail.park_address
                    let url = URL(string: self.objParkDetail.park_image)
                    self.img_park.setImageFrom(url)
                    self.lbl_aboutPark.text = self.objParkDetail.aboutPark
                    self.btn_galleryCount.setTitle(self.objParkDetail.galary_count, for: .normal)
                    
                    self.tbl_userView.reloadData()
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    @IBAction func actbtn_ParkGalleryBtnClicked(_ sender: Any) {
        //http://perfectlinkfitness.com/api/park_going_in
        let postParam: Parameters = ["park_id":self.objParkDetail.id]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]

        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.ParkGallerylist, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let dict = successDict.value(forKey:"response"){
                    let temparrGallery = dict as! NSArray
                    
                    if temparrGallery.count > 0{
                        let objViewAllParkListVC:ViewAllParkListVC = ViewAllParkListVC(nibName: "ViewAllParkListVC", bundle: nil)
                        objViewAllParkListVC.isForPark = false
                        objViewAllParkListVC.arr_parklist = NSMutableArray(array: temparrGallery)
                        self.navigationController?.pushViewController(objViewAllParkListVC, animated: true)
                    }else{
                        DataUtil.alertMessage("Gallery is empty.", viewController: self)
                    }
                    
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
        
        
    }
    
    @IBAction func actbtn_GoingInbtnClicked(_ sender: Any) {
        
        let myUserID = (DataUtil.appdelegate().objUserInfo?.userId)!
        let postParam: Parameters = ["park_id":self.objParkDetail.id, "user_id":myUserID]
        print("Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]

        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.ParkGoingIn, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                
                let tempResponse = successDict.value(forKey: "response") as? NSNumber ?? NSNumber(1)
                if tempResponse.boolValue
                {
                    self.objParkDetail.going_in_status = true
                    self.btn_goingIn.setTitle("Leave Community", for: .normal)
                    self.btn_goingIn.setTitle("Leave Community", for: .highlighted)
                    self.btn_goingIn.setTitle("Leave Community", for: .selected)
                }else
                {
                    self.objParkDetail.going_in_status = false
                    self.btn_goingIn.setTitle("GOING IN", for: .normal)
                    self.btn_goingIn.setTitle("GOING IN", for: .highlighted)
                    self.btn_goingIn.setTitle("GOING IN", for: .selected)
                }
                self.loadParkDetail()
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
        
        
        
    }
    
    @IBAction func actbtn_ShareExperiancebtnClicked(_ sender: Any) {
        
        if (self.objParkDetail.going_in_status){
            let obj_ShareExperiance:ShareYourExperianceVC = ShareYourExperianceVC(nibName: "ShareYourExperianceVC", bundle: nil)
            obj_ShareExperiance.str_parkID = self.objParkDetail.id
            self.navigationController?.pushViewController(obj_ShareExperiance, animated: true)
            
        }else{
            DataUtil.alertMessage("Please join community first to share your experience with other's.", viewController: self)
        }
        
        
        
    }
    
    
    func trainerClickedWithIndex(index: Int) {
//        let temptrainer = self.objParkDetail.arrTrainer[index]
//        let obj_phonelistvc:TrainerDetailVC = TrainerDetailVC(nibName: "TrainerDetailVC", bundle: nil)
//        obj_phonelistvc.str_trainer_id = temptrainer.user_id
//        obj_phonelistvc.str_title = temptrainer.name
//        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        
    }
    
    func userClickedWithIndex(index: Int) {
        let tempUser = self.objParkDetail.arrUser[index]
        if DataUtil.appdelegate().objUserInfo?.userId == tempUser.user_id{
            
        }else{
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            obj_phonelistvc.str_user_id = tempUser.user_id
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
        
    }
    
}


extension DetailParkLinkVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderViewCell") as! HeaderViewCell
        
        if section == 0{
            let counttrainer = self.objParkDetail.arrTrainer.count
            headerView.lbl_tittle.text = "Trainers (" +  String(counttrainer) + ")"
            if counttrainer>4{
                headerView.btn_ViewAll.isHidden = false
            }else{
                headerView.btn_ViewAll.isHidden = true
            }
        }else{
            let countusers = self.objParkDetail.arrUser.count
            headerView.lbl_tittle.text = "Users (" + String(countusers) + ")"
            if countusers>4{
                headerView.btn_ViewAll.isHidden = false
            }else{
                headerView.btn_ViewAll.isHidden = true
            }
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "DetailParkLinkCell"
        let cell: DetailParkLinkCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? DetailParkLinkCell
        if indexPath.section == 0{
            cell!.updateCellModel(isForTrainer: true, arrTrainer: self.objParkDetail.arrTrainer, arrUser: self.objParkDetail.arrUser)
            
        }else{
            cell!.updateCellModel(isForTrainer: false, arrTrainer: self.objParkDetail.arrTrainer, arrUser: self.objParkDetail.arrUser)
        }
        cell?.selectionStyle = .none
        cell?.delegate = self
        return cell!
    }
    
    
    
    
    
    
    
    
    
    
}
