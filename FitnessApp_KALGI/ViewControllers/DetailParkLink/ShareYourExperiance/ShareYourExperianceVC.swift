

import UIKit
import Alamofire
import AVFoundation
import Photos
import AVKit


class ShareYourExperianceVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, MakeChallengeActivityCellDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate, VideoAddDelegate, UITextViewDelegate{
    func videoadded_clicked() {
        
    }
    var str_userType:String = "2"
    var str_parkID:String = ""
    var logincell: MakeChallengeCell!
    var selectedImage:UIImage? = nil
    var video_url:URL? = nil
    @IBOutlet weak var tableview_detail: UITableView!
    var height_table:CGFloat = 435
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview_detail.delegate  = self as UITableViewDelegate
        tableview_detail.dataSource = self as UITableViewDataSource
        //tableview_detail.rowHeight = 700
        //tableview_detail.estimatedRowHeight = UITableViewAutomaticDimension
       // self.tableview_detail.isHidden = true
        self.title = "Share Your Experience"
        tableview_detail.reloadData()
        self.addLeftMenuButtonWithImage()
      
        
        
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // self.navigationController?.navigationBar.isHidden = true
        //   self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func camera_btn_clicked(cell: MakeChallengeCell, btn: UIButton) {
        self.openCamera()
    }
    
    func video_btn_clicked(cell: MakeChallengeCell, btn: UIButton) {
        let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert) in
            self.openVideoRecorder()
            
        }))
        
        alert.addAction(UIAlertAction(title:"Video Library", style: .default, handler: { (alert) in
           self.openVideoGallery()
        }))
        
        alert.addAction(UIAlertAction(title:"Cancel", style: .default, handler: { (alert) in
        }))
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
    
    func image_btn_clicked(cell: MakeChallengeCell, btn: UIButton) {
        self.openGallery()
    }
    
    func post_btn_clicked(cell: MakeChallengeCell, btn: UIButton) {
        
            if self.video_url != nil{
                if !changepasswordValidation() {
                    return
                }
                self.upload_btn_clicked()
            }else if self.selectedImage != nil{
                if !changepasswordValidation() {
                    return
                }
                self.upload_image()
            }else{
                DataUtil.alertMessage("Please select video or image", viewController: self)
            }
    }
    
    func close_image_btn_clicked(cell: MakeChallengeCell, btn: UIButton) {
        self.closeImagePreview()
    }
    
    func close_video_btn_clicked(cell: MakeChallengeCell, btn: UIButton) {
        self.closeVideoPreview()
    }
    
    func play_btn_clicked(cell: MakeChallengeCell, btn: UIButton) {
        let player = AVPlayer(url: self.video_url!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.present(playerController, animated: true) {
            player.play()
        }
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
    //
    //
    //        return UITableViewAutomaticDimension
    //       // return 710
    //
    //    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "MakeChallengeCell"
        logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? MakeChallengeCell
        if logincell == nil {
            tableView.register(UINib(nibName: "MakeChallengeCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? MakeChallengeCell
        }
        logincell.btn_complete.setTitle("Share", for: .normal)
        logincell.textview_message.placeholder = "Enter your experience here..."
        logincell.textview_message.delegate = self
        logincell.delegate = self
        return logincell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return height_table
    }
    
    
    
    
    
    func camera_btn_clicked(cell: ProfileInfoCell, btn: UIButton) {
        
        
        let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert) in
            self.openCamera()
            
        }))
        
        alert.addAction(UIAlertAction(title:"Photo Library", style: .default, handler: { (alert) in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction(title:"Cancel", style: .default, handler: { (alert) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func openGallery(){
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera(){
        /*
         
         imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
         imagePicker.cameraCaptureMode = .video
         */
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    
    
    
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //var selectedImage: UIImage?
        
        
        if let videoUrl = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.mediaURL.rawValue)] as? URL
            
        {
            self.selectedImage = nil
            self.video_url = videoUrl
            
            //self.dismiss(animated: true, completion: nil)
            //DataUtil.loadIndicatorView()
            self.dismiss(animated: true) {
                self.openVideoPreview()
            }
            return
            
        }
        
        
        if let editedImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")]   as? UIImage {
            selectedImage = editedImage
            self.video_url = nil
        } else if let originalImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerOriginalImage")] as? UIImage {
            selectedImage = originalImage
            self.video_url = nil
        }
        if selectedImage != nil {
           self.video_url = nil
            // signupcell.imageview_profile.image = selectedImage;
           // signupcell.imageview_profile.layer.cornerRadius = signupcell.imageview_profile.frame.size.height/2
            //signupcell.imageview_profile.layer.masksToBounds = true
            self.openImagePreview()
        }
        
           self.dismiss(animated: true, completion: nil)
        
    }
    
    func openVideoGallery(){
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.mediaTypes = ["public.movie"]
            imagePicker.allowsEditing = true
            imagePicker.videoMaximumDuration = 60.0
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openVideoRecorder(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            imagePicker.cameraCaptureMode = .video
            imagePicker.videoMaximumDuration = 60.0
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openImagePreview(){
        self.logincell.view_image.isHidden = false
        self.logincell.constraint_top.constant = 260
        self.height_table = 675
        self.logincell.view1.isHidden = true
        self.logincell.view2.isHidden = true
        self.logincell.view3.isHidden = true
        self.logincell.imageview_thumb.image = selectedImage
        self.tableview_detail.reloadData()
    }
    
    func closeImagePreview(){
        self.logincell.view_image.isHidden = true
        self.logincell.constraint_top.constant = 20
        self.height_table = 435
        self.logincell.view1.isHidden = false
        self.logincell.view2.isHidden = false
        self.logincell.view3.isHidden = false
        self.selectedImage = nil
        self.tableview_detail.reloadData()
    }
    
    func openVideoPreview(){
        self.logincell.view_video.isHidden = false
        self.logincell.constraint_top.constant = 260
        self.height_table = 675
        self.logincell.view1.isHidden = true
        self.logincell.view2.isHidden = true
        self.logincell.view3.isHidden = true
        self.getThumbnailImageFromVideoUrl(url: self.video_url!) { (thumbImage) in
            self.logincell.video_thumb.image = thumbImage
        }
        self.tableview_detail.reloadData()
    }
    
    func closeVideoPreview(){
        self.logincell.view_video.isHidden = true
        self.logincell.constraint_top.constant = 20
        self.height_table = 435
        self.logincell.view1.isHidden = false
        self.logincell.view2.isHidden = false
        self.logincell.view3.isHidden = false
        self.video_url = nil
        self.tableview_detail.reloadData()
    }
    
    
    

    
    @objc func upload_btn_clicked(){
        if !changepasswordValidation() {
            return
        }
        //   self.uploadMedia(url_video: videoUrl)
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
//        let headers: HTTPHeaders = [
//            "Authorization": String(format: "Bearer %@",str_accessToken),
//            "Accept": "application/json"
//        ]

        DataUtil.loadIndicatorView()
        let postParam: Parameters = ["text":logincell.textview_message.text!, "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!, "file_type":"VIDEO", "park_id":self.str_parkID]
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(self.video_url!, withName: "file")
            for (key, value) in postParam {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
            
        },
                         usingThreshold:UInt64.init(),
                         to:ConstantFiles.ParkShareExperiance,
                         method:.post,
                         headers:[
                            "Authorization": String(format: "Bearer %@",str_accessToken),
                            "Accept": "application/json"
            ],
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    guard let successDict = response.result.value as? NSDictionary else
                                    {
                                        DataUtil.removeIndicatorView()
                                        return
                                    }
                                    DataUtil.removeIndicatorView()
                                    if let status = successDict["STATUS"] as? String{
                                        if status == "true"{
                                            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                                                self.closeVideoPreview()
                                                self.logincell.textview_message.text = ""
                                                
                                                
                                                
                                                let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                                                
                                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                                                    //self.delegate?.parkadded_clicked()
                                                    DispatchQueue.main.async {
                                                        if let arr = successDict.value(forKey:"response"){
                                                            let item:NSDictionary = arr as! NSDictionary
                                                        }
                                                        self.back_btn_clicked()
                                                    }
                                                    
                                                    
                                                }))
                                                
                                                self.present(alert, animated: true, completion: nil)
                                                
                                                
                                                print("Success ------", successDict)
                                                if let array:NSArray = successDict["response"] as? NSArray{
                                                    DataUtil.appdelegate().objUserInfo?.array_videos.removeAllObjects()
                                                    for item in array {
                                                       // let dict:NSDictionary =  item as! NSDictionary
                                                       // let obj_model:VideosModel = VideosModel.init(dictUserInfo: dict)
                                                        //DataUtil.appdelegate().objUserInfo?.array_videos.add(obj_model)
                                                    }
                                                    //self.delegate?.videoadded_clicked()
                                                  //  self.dismiss(animated: true, completion: nil)
                                                }
                                                
                                                
                                                
                                               
                                                //  self.present(alert, animated: true, completion: nil)
                                                
                                                // DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                                                //self.back_btn_clicked()
                                                
                                                /*   if let arr = successDict.value(forKey:"response"){
                                                 
                                                 
                                                 }else{
                                                 
                                                 }*/
                                            }else{
                                                
                                                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                                                self.back_btn_clicked()
                                            }
                                            
                                        }
                                    }
                                    debugPrint(response)
                                }
                            case .failure(let encodingError):
                                DataUtil.removeIndicatorView()
                                print(encodingError)
                            }
        })
        
        
    }
    
    
    func upload_image(){
        if !changepasswordValidation() {
            return
        }
        let postParam: Parameters = ["text":logincell.textview_message.text!, "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!, "file_type":"IMAGE", "park_id":self.str_parkID]
            print("Login Post Parameter : \(postParam)")
        
            ServerCommunication.getDataWithPostImageForWall(url: ConstantFiles.ParkShareExperiance, image: selectedImage as Any,parameter: postParam, viewController: self, isImage:true,success: { (successDict) in
                print("Success ------", successDict)
                if (successDict.value(forKey: "STATUS") as? String) == "true"{
                    
                    
                    
                    self.closeImagePreview()
                    self.logincell.textview_message.text = ""
                    let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        //self.delegate?.parkadded_clicked()
                        DispatchQueue.main.async {
                            if let arr = successDict.value(forKey:"response"){
                                let item:NSDictionary = arr as! NSDictionary
                                
                            }
                            self.back_btn_clicked()
                        }
                        
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
                
                // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
            }) { (dictFailure) in
                if (dictFailure.value(forKey: "message") as? String) != nil {
                    DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
                }
            }
            
            
            
    }
    
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
    
    
    func changepasswordValidation() -> Bool {
        if (logincell.textview_message.text?.isEmpty)!{
            DataUtil.alertMessage("Description field is Empty", viewController: self)
            return false
        }
        return true
    }
    
    
    
}
