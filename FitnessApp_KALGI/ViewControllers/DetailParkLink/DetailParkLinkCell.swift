

import UIKit

protocol DetailParkLinkDelegate: class {
   
    func trainerClickedWithIndex(index: Int)
    func userClickedWithIndex(index: Int)
}

class DetailParkLinkCell: UITableViewCell {
    var delegate : DetailParkLinkDelegate?
    @IBOutlet weak var collection_List: UICollectionView!
    var isForTrainer = false
    var arrTrainer = [ParkTrainer]()
    var arrUser = [ParkUser]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCellModel(isForTrainer: Bool, arrTrainer: [ParkTrainer], arrUser: [ParkUser])  {
        self.isForTrainer = isForTrainer
        self.arrTrainer = arrTrainer
        self.arrUser = arrUser
        self.collection_List.register(UINib(nibName: "DetailParkLinkUserCell", bundle: nil), forCellWithReuseIdentifier: "DetailParkLinkUserCell")
        self.collection_List.reloadData()
    }
    
}

extension DetailParkLinkCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isForTrainer{
            return self.arrTrainer.count
        }else{
            return self.arrUser.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell:DetailParkLinkUserCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailParkLinkUserCell", for: indexPath) as! DetailParkLinkUserCell
        if self.isForTrainer{
            let objParkTrainer = arrTrainer[indexPath.row]
            cell.lbl_Name.text = objParkTrainer.name
            let url = URL(string: objParkTrainer.profile_picture)
        cell.img_UserImage.setImageFrom(url)
            cell.lbl_Rating.text = objParkTrainer.rating
            cell.lbl_Rating.isHidden = false
            cell.imgStar.isHidden = false
        }else{
            let objParkUser = arrUser[indexPath.row]
            cell.lbl_Name.text = objParkUser.name
            let url = URL(string: objParkUser.profile_picture)
            cell.img_UserImage.setImageFrom(url)
            cell.lbl_Rating.isHidden = true
            cell.imgStar.isHidden = true
        }
            
            return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
         if self.isForTrainer{
            self.delegate?.trainerClickedWithIndex(index: indexPath.row)
         }else{
            self.delegate?.userClickedWithIndex(index: indexPath.row)
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 150)
    }
    
    
    
    
}
