

import UIKit

class DetailParkLinkUserCell: UICollectionViewCell {
    
    @IBOutlet weak var img_UserImage: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Rating: UILabel!
    @IBOutlet weak var imgStar: UIImageView!
    @IBOutlet weak var heightRating: NSLayoutConstraint!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
