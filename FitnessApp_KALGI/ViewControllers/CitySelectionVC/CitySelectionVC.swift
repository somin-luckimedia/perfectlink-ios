//
//  CitySelectionVC.swift
//  Spolu
//
//  Created by Ashish Mishra on 28/05/19.
//  Copyright © 2019 Ashish Mishra. All rights reserved.
//

import UIKit
import Alamofire
import Foundation
import Contacts
import ContactsUI
import FirebaseCrashlytics

class CitySelectionVC: UIViewController, SwiftMultiSelectDelegate,SwiftMultiSelectDataSource {
    
    @IBOutlet weak var collectioneview_city: UICollectionView!
    var array_city:NSMutableArray = NSMutableArray(objects: "", "", "", "", "", "")
    let array_icons:[String] = ["Gym_icon", "Trainer_icon","Park_Links","Free Run","Discount_icon", "Meals_icon", "Protien_icon", "Acceessories_icon"]
    let array_title:[String] = ["Gym", "Trainer","Park Links","Free Run", "Discount", "Meals", "Supplements", "Accessories"]
    //let array_quantity:[String] = ["300+ Gyms", "80+ Personal Trainer", "214+ Offer", " 100+ Meals", "30+ Products", "100+ Acceessories"]
    var isFromHome:Bool = false
    var isFromSignup:Bool = false
    var items:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    var initialValues:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    var selectedItems:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    var array_emails:[String] = [String]()
    @IBOutlet weak var lbl_name: UILabel!
    
    @IBOutlet weak var lbl_weight: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
        self.collectioneview_city.register(UINib(nibName: "CityCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CityCollectionCell")
        self.collectioneview_city.reloadData()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.lbl_name.text = String(format:"Hey!, %@", (DataUtil.appdelegate().objUserInfo?.name)!)
        self.lbl_weight.text = String(format:"Your Weight: %@ lbs", (DataUtil.appdelegate().objUserInfo?.str_weight)!)
        self.getHomeCount()
        // Do any additional setup after loading the view.
        createItems()
        Config.doneString = "Sync"
        SwiftMultiSelect.dataSource     = self
        SwiftMultiSelect.delegate       = self
        
        if self.isFromSignup == true{
            self.isFromSignup = false
            DataUtil.appdelegate().isFromSignup = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                SwiftMultiSelect.initialSelected = []
                SwiftMultiSelect.Show(to: self)
            })
        }
        
    
    }
    
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func getHomeCount(){
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getData(url: ConstantFiles.home_count, viewController: self,headers:headers   ,success: { (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    DataUtil.appdelegate().objHomeCount = HomeCountModel(dictUserInfo: arr as! NSDictionary)
                    self.collectioneview_city.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /// Create a custom items set
    func createItems(){
        
        self.items.removeAll()
        self.initialValues.removeAll()
        for i in 0..<50{
            items.append(SwiftMultiSelectItem(row: i, title: "test\(i)", description: "description for: \(i)", imageURL : (i == 1 ? "https://randomuser.me/api/portraits/women/68.jpg" : nil)))
        }
        self.initialValues = [self.items.first!,self.items[1],self.items[2]]
        self.selectedItems = items
        SwiftMultiSelect.dataSourceType = .phone
    }
    
    
    /// selector for switch addressbook
    ///
    /// - Parameter sender
    @IBAction func useAddr(_ sender: Any) {
        
        SwiftMultiSelect.dataSourceType = .phone
    }
    
    
    /// Function to launch selector from button
    ///
    /// - Parameter sender
    @IBAction func launch(_ sender: Any) {
        
        //Example to start a selector with initial values
        
        
    }
    
    
    //MARK: - SwiftMultiSelectDelegate
    
    func userDidSearch(searchString: String) {
        if searchString == ""{
            selectedItems = items
        }else{
            selectedItems = items.filter({$0.title.lowercased().contains(searchString.lowercased()) || ($0.description != nil && $0.description!.lowercased().contains(searchString.lowercased())) })
        }
    }
    
    func numberOfItemsInSwiftMultiSelect() -> Int {
        return selectedItems.count
    }
    
    func swiftMultiSelect(didUnselectItem item: SwiftMultiSelectItem) {
        print("row: \(item.title) has been deselected!")
    }
    
    func swiftMultiSelect(didSelectItem item: SwiftMultiSelectItem) {
        print("item: \(item.title) \(String(describing: item.userInfo)) has been selected!")
        
        
        //CNContact
    }
    
    func didCloseSwiftMultiSelect() {
        //badge.isHidden = true
        //badge.text = ""
    }
    
    func swiftMultiSelect(itemAtRow row: Int) -> SwiftMultiSelectItem {
        return selectedItems[row]
    }
    
    func swiftMultiSelect(didSelectItems items: [SwiftMultiSelectItem]) {
        
        initialValues   = items
        // badge.isHidden  = (items.count <= 0)
        ///badge.text      = "\(items.count)"
        print("you have been selected: \(items.count) items!")
        
        for item in items{
            
            if let contact = item.userInfo as? CNContact
            {
                if let emailValue : CNLabeledValue = contact.emailAddresses.first
                {
                    self.array_emails.append(emailValue.value as String)
                    print("Email : ", emailValue.value)
                }
            }
            
        }
        self.syncContacts()
        
    }
    
    
    
    
    func syncContacts(){
        //friends_email
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam: Parameters = ["user_id":str_userid, "friends_email":array_emails.joined(separator: ",")]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.contact_sync, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    
}

extension CitySelectionVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_title.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CityCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CityCollectionCell", for: indexPath) as! CityCollectionCell
        cell.lbl_title.text = array_title[indexPath.row]
        if array_title[indexPath.row] == "Meals" || array_title[indexPath.row] == "Free Run" {
            cell.lbl_quantity.text = "Comming Soon"
            cell.lbl_quantity.textColor = .red
        }else{
            cell.lbl_quantity.text = (DataUtil.appdelegate().objHomeCount.array_quantity[indexPath.row] as! String)
            cell.lbl_quantity.textColor = .white
        }
        cell.imageview_icon.image = UIImage(named: array_icons[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenwidth:CGFloat = UIScreen.main.bounds.size.width/2
        return CGSize(width: screenwidth, height: 170)
    }
    
    
    //MealsVC
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if indexPath.row == 0{
            let obj_phonelistvc:GymCategoryListVC = GymCategoryListVC(nibName: "GymCategoryListVC", bundle: nil)
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }else if indexPath.row == 1{
            let obj_phonelistvc:TrainerListingVC = TrainerListingVC(nibName: "TrainerListingVC", bundle: nil)
            obj_phonelistvc.str_trainer_category = "2"
            obj_phonelistvc.str_title = "Home Service"
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
//            let obj_phonelistvc:PersonalTrainerVC = PersonalTrainerVC(nibName: "PersonalTrainerVC", bundle: nil)
//            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
        else if indexPath.row == 2{
             DataUtil.appdelegate().isAuthorizedtoGetUserLocation()
           let obj_parkLinkVC:ParkLinkListVC = ParkLinkListVC(nibName: "ParkLinkListVC", bundle: nil)
           self.navigationController?.pushViewController(obj_parkLinkVC, animated: true)
        }
        else if indexPath.row == 3{
            DataUtil.alertMessage("Comming soon!", viewController: self)
        }
            
        else if indexPath.row == 4{
            //DiscountsVC
            let obj_phonelistvc:DiscountsVC = DiscountsVC(nibName: "DiscountsVC", bundle: nil)
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }else if indexPath.row == 5{
            //DiscountsVC
            DataUtil.alertMessage("Comming soon!", viewController: self)
            //            let obj_phonelistvc:MealsVC = MealsVC(nibName: "MealsVC", bundle: nil)
            //            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
        else if indexPath.row == 6{
            // ProtienVC
            let obj_phonelistvc:ProtienVC = ProtienVC(nibName: "ProtienVC", bundle: nil)
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }else if indexPath.row == 7{
            // AccessoriesVC
            let obj_phonelistvc:AccessoriesVC = AccessoriesVC(nibName: "AccessoriesVC", bundle: nil)
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
    }
}
