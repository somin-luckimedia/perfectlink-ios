//
//  ImageViewerVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 19/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//


import UIKit
import AVFoundation
import Alamofire
import Photos
import AVKit


protocol ImageAddDelegate:class {
    func Imageadded_clicked()
}

class ImageViewerVC: UIViewController {
    
    @IBOutlet weak var imageview_thumb: UIImageView!
    var isViewer:Bool = false
    var isDelete:Bool = true
    var video_url:URL! = nil
    var selectedImage:UIImage?
    var str_url:String? = nil
    var dictTemp : NSDictionary = [:]
    var name : String = ""
    weak var delegate: ImageAddDelegate?
    @IBOutlet var strLikeCount: UILabel!
    @IBOutlet var strCommentCount: UILabel!
    @IBOutlet var strShareCount: UILabel!
    @IBOutlet var vwVideoPlaceholder: UIView!
    var dictComment : WallListModel!
    
    @IBOutlet var constHeight: NSLayoutConstraint!
    @IBAction func onBackButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onLikeButtonTapped(_ sender: UIButton) {
        var like_count:Int = dictTemp["likes"] as! Int//Int(obj_requestmodal.str_likes)!
        var dislike_count:Int = dictTemp["unlikes"] as! Int
        var is_like : Int = dictTemp["is_liked"] as! Int
        var is_unlike : Int = dictTemp["is_unlike"] as! Int
        if is_like == 1 {
            is_like = 0
            like_count = like_count - 1
        }else{
            is_like = 1
            like_count = like_count + 1
            if is_unlike == 1 {
                dislike_count = dislike_count - 1
                is_unlike = 0
            }
        }
        //obj_requestmodal.str_likes = String(format: "%ld", like_count)
//        obj_requestmodal.str_unlikes = String(format: "%ld", dislike_count)
        strLikeCount.text = "\(like_count)"
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam: Parameters = ["user_id":str_userid, "wall_id": dictTemp["id"] as! Int, "like_unlike":"1"]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.like_post, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
               
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
//        //cell.lbl_dislike_count.text = String(format: "%@", obj_requestmodal.str_unlikes!)
//        self.array_mybookings.replaceObject(at: cell.rowIndex, with: obj_requestmodal)
//
//        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
//        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
//        let postParam: Parameters = ["user_id":str_userid, "wall_id":obj_requestmodal.str_id!, "like_unlike":"1"]
//        print("Login Post Parameter : \(postParam)")
//        let headers: HTTPHeaders = [
//            "Authorization": String(format: "Bearer %@", str_accessToken),
//            "Accept": "application/json"
//        ]
//        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.like_post, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
//            print(successDict)
//            if (successDict.value(forKey: "STATUS") as? String) == "true"{
//
//
//            }else{
//                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
//            }
//        }) { (dictFailure) in
//            if (dictFailure.value(forKey: "message") as? String) != nil {
//                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
//            }
//        }
    }
    
    @IBAction func onCommentButtonTapped(_ sender: UIButton) {
//        let obj_requestmodal:WallListModel = dictTemp as! WallListModel//array_mybookings[cell.rowIndex] as! WallListModel
        let obj_parkeAddressVC:CommentVC = CommentVC(nibName: "CommentVC", bundle: nil)
        // let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
        if #available(iOS 13.0, *) {
            obj_parkeAddressVC.modalPresentationStyle = .fullScreen
        } else {
            // Fallback on earlier versions
        }
        obj_parkeAddressVC.wallPost_id = dictTemp["id"] as! Int
        self.navigationController?.pushViewController(obj_parkeAddressVC, animated: true)
//        obj_parkeAddressVC.obj_wallpostModel = obj_requestmodal
//        self.present(obj_parkeAddressVC, animated: true, completion: nil)
        //        self.navigationController?.present(nav, animated: true, completion: nil)

    }
    
    @IBAction func onPlayTapped(_ sender: UIButton) {
        let imgURl = dictTemp["file"] as? String ?? ""
        let player = AVPlayer(url: URL(string: imgURl)!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.present(playerController, animated: true) {
            player.play()
        }
    }
    @IBAction func onShareButtonTapped(_ sender: UIButton) {
//        let obj_requestmodal:WallListModel = array_mybookings[cell.rowIndex] as! WallListModel
        let shareNumber  : Int = dictTemp["share_count"] as! Int//NumberFormatter().number(from: obj_requestmodal.str_share_count) as! Int
        strShareCount.text = "\(shareNumber + 1)"
        shareBtnClick(wall_id: "\(dictTemp["id"] as! Int)",shareCount: shareNumber + 1)
        let str_message:String = String(format: "Please find the wall item posted by %@:", self.name)
        let name = NSURL(string: dictTemp["file"] as! String)
        let objectsToShare = [str_message, name!] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)

    }
    
    func shareBtnClick(wall_id : String,shareCount : Int) {
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam  = ["wall_id":wall_id]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.add_wall_share, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{

            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    var obj_locationModel:GalleryImageModel? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        if strType_camera == "camera" {
            vwVideoPlaceholder.isHidden = true
        }
        else {
            vwVideoPlaceholder.isHidden = false
        }
        self.addLeftMenuButtonWithImage()
        if flagChallengeImageviewer {
            strCommentCount.text = "\(dictTemp["comment_count"] as! Int)"
            strShareCount.text = "\(dictTemp["share_count"] as! Int)"
            strLikeCount.text = "\(dictTemp["likes"] as! Int)"
        }
        else {
            constHeight.constant = 0
        }
        self.navigationController?.navigationBar.isHidden = true
        if isViewer == true{
            
            if isDelete == true{
            self.addRightMenuButtonWithImageForDelete()
            }
            imageview_thumb.setImageFromURL(stringImageUrl: (obj_locationModel?.str_image)!)
        }else if self.str_url != nil{
            imageview_thumb.setImageFromURL(stringImageUrl: str_url!)
        }
        else{
            self.addRightMenuButtonWithImage()
            self.imageview_thumb.image = selectedImage
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if postClick {
            let commentCount = dictTemp["comment_count"] as! Int
            strCommentCount.text = "\(commentCount + 1)"
            postClick = false
        }
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "close"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        //button.setImage(UIImage(named: "filter_icon"), for: UIControlState.normal)
        button.setTitle("UPLOAD", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(self.upload_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    
    func addRightMenuButtonWithImageForDelete(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "delete_navigation"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.delete_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func upload_btn_clicked(){
        //   self.uploadMedia(url_video: videoUrl)
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        DataUtil.loadIndicatorView()
        let postParam: Parameters = ["title":"image", "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!]
       
        ServerCommunication.getDataWithPostImageForTrainer(url: ConstantFiles.add_image, image: selectedImage as Any,parameter: postParam, viewController: self, isImage:true,success: { (successDict) in
            
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                
                if let array:NSArray = successDict["response"] as? NSArray{
                    DataUtil.appdelegate().objUserInfo?.array_gallery_image.removeAllObjects()
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_model:GalleryImageModel = GalleryImageModel.init(dictUserInfo: dict)
                        DataUtil.appdelegate().objUserInfo?.array_gallery_image.add(obj_model)
                    }
                    self.delegate?.Imageadded_clicked()
                    self.dismiss(animated: true, completion: nil)
                }
                
                
                
                let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    //self.delegate?.parkadded_clicked()
                    self.back_btn_clicked()
                }))
                //  self.present(alert, animated: true, completion: nil)
                
                // DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                //self.back_btn_clicked()
                
                /*   if let arr = successDict.value(forKey:"response"){
                 
                 
                 }else{
                 
                 }*/
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
            
            
            // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    @objc func delete_btn_clicked(){
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        
        let postParam: Parameters = ["id":(obj_locationModel?.str_id)!, "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!]
        print("Login Post Parameter : \(postParam)")
        
        
        
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.remove_gallery_image, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                //obj_locationModel
                DataUtil.appdelegate().objUserInfo!.array_gallery_image.remove(self.obj_locationModel!)
                self.delegate?.Imageadded_clicked()
                self.dismiss(animated: true, completion: nil)
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
}

