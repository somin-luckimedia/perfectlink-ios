

import UIKit
import Alamofire
import AVFoundation
import Photos
import AVKit
import Firebase

class UserProfileVC:  UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, FriendDetailCellDelegate  {
    func chat_btn_clicked() {
         let objFriendsChatVC:FriendsChatVC = FriendsChatVC(nibName: "FriendsChatVC", bundle: nil)
         let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        objFriendsChatVC.myUserID = str_userid
        objFriendsChatVC.myFriendID = obj_friendDetail.str_user_id
        objFriendsChatVC.friendName = self.obj_friendDetail.str_name
        self.navigationController?.pushViewController(objFriendsChatVC, animated: true)
    }
    @IBAction func btnChatTapped(_ sender: UIButton) {
        let objFriendsChatVC:FriendsChatVC = FriendsChatVC(nibName: "FriendsChatVC", bundle: nil)
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        objFriendsChatVC.myUserID = str_userid
        objFriendsChatVC.myFriendID = obj_friendDetail.str_user_id
        objFriendsChatVC.friendName = self.obj_friendDetail.str_name
        self.navigationController?.pushViewController(objFriendsChatVC, animated: true)
        
    }
    
    
    var obj_friendDetail:FriendDetailModel! = FriendDetailModel()
    var str_userType:String = "2"
    var logincell: FriendDetailCell!
    var str_gymId:String!
    var str_user_id:String = ""
    var strEditorBack : String = "Back"
    let transiton = SlideInTransition()
    var topView: UIView?
    let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate
    @IBOutlet var btnMenu: UIButton!
    @IBOutlet var btnEdit: UIButton!

    @IBOutlet var tblOnline: UITableView!

    var arrOnlineFriends : NSMutableArray = []
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")

    
    @IBOutlet weak var tableview_detail: UITableView!
    var obj_susbcription_dict:NSDictionary? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        if strEditorBack == "Edit" {
            print(self.strEditorBack)
            btnEdit.isHidden = true
        }
        else {
            self.str_user_id = DataUtil.appdelegate().objUserInfo?.userId! ?? ""
            btnEdit.isHidden = false

        }
//        self.title = "Profile"
        tableview_detail.delegate  = self as UITableViewDelegate
        tableview_detail.dataSource = self as UITableViewDataSource
        self.tableview_detail.isHidden = true
        tableview_detail.reloadData()
        //self.addLeftMenuButtonWithImage()
        self.addRightMenuButtonWithImage()
        //if DataUtil.appdelegate().objUserInfo?.str_userType == "2" {
        if strEditorBack == "Back" {
            btnMenu.setImage(UIImage(named: "menu"), for: .normal)
        }
        else {
            btnMenu.setImage(UIImage(named: "Back_icon"), for: .normal)

        }
        let notificationCenter = NotificationCenter.default
               notificationCenter.addObserver(self,
                                              selector: #selector(dismissProfile),
                                              name: NSNotification.Name(rawValue: "DismissProfile"),
                                              object: nil)
        
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }

                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
        }

    }
    
    @objc func dismissProfile(){
       // flagProfile = false
        self.dismissControllerAnimated()
    }

    //edit_icon
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "profile_edit"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.actbtn_EditProfilebtnClicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    
    @objc func actbtn_EditProfilebtnClicked(){
      let objProfileInfoVC:ProfileInfoVC = ProfileInfoVC(nibName: "ProfileInfoVC", bundle: nil)
        self.navigationController?.pushViewController(objProfileInfoVC, animated: true)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.dismissControllerAnimated()

        objAppDelgate1.sideMenuIndex = 0
        flagGym = true
        flagPark = true
        flagGallery = true
        self.getMyBookings()
        self.tabBarController?.tabBar.isHidden = false

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func getMyBookings(){
        let postParam: Parameters = ["user_id":self.str_user_id]
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.get_friend_info, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.tableview_detail.isHidden = false
                if let arr = successDict.value(forKey:"response"){
                    self.obj_friendDetail = FriendDetailModel(dictUserInfo: arr as! NSDictionary)
                    self.tableview_detail.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func selectedGalleryTapped(index : Int) {
       // if !flagParkLink {
        let dictemp = self.obj_friendDetail.array_gallery[index] as? NSDictionary ?? NSDictionary()
        let refreshAlert = UIAlertController(title: "Delete image", message: "Are you sure, you want to delete image?", preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            let park_id = dictemp["wallpost_id"] as? String ?? ""
            self.deleteWallPost(wallPost_id: park_id)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
            
        present(refreshAlert, animated: true, completion: nil)
//        }
//        else {
////            let dictemp = self.obj_friendDetail.array_vistedpark[index] as? NSDictionary ?? NSDictionary()
////            let obj_DetailParkvc:DetailParkLinkVC = DetailParkLinkVC(nibName: "DetailParkLinkVC", bundle: nil)
////            obj_DetailParkvc.str_parkID = dictemp["park_id"] as? String ?? ""
////            self.navigationController?.pushViewController(obj_DetailParkvc, animated: true)
//
//            let dictemp = self.obj_friendDetail.array_vistedpark[index] as? NSDictionary ?? NSDictionary()
//            let obj_DetailParkvc:ParkLinkDetailVC = ParkLinkDetailVC(nibName: "ParkLinkDetailVC", bundle: nil)
//            obj_DetailParkvc.str_parkID = dictemp["park_id"] as? String ?? ""
//            self.navigationController?.pushViewController(obj_DetailParkvc, animated: true)
      //  }

           
        
    }
    
    func deleteWallPost(wallPost_id : String) {
        let postParam: Parameters = ["wallpost_id":wallPost_id]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.delete_Wall_post, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                self.getMyBookings()
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func UnsubscribePark(park_id:String){
        let postParam: Parameters = ["user_id":self.str_user_id,"park_id" : park_id]
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.parkUnsubscribed, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.tableview_detail.isHidden = false
                if let arr = successDict.value(forKey:"response"){
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                    self.getMyBookings()
//                    self.obj_friendDetail = FriendDetailModel(dictUserInfo: arr as! NSDictionary)
//                    self.tableview_detail.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func UnsubscribeGym(subscription_id:String){
        let postParam: Parameters = ["subscription_id":subscription_id,"subscription_for" : "GYM"]
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.gymUnsubscribed, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.tableview_detail.isHidden = false
                if let arr = successDict.value(forKey:"response"){
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                    self.getMyBookings()
//                    self.obj_friendDetail = FriendDetailModel(dictUserInfo: arr as! NSDictionary)
//                    self.tableview_detail.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if tableView == tblOnline {
            return arrOnlineFriends.count
        }
        else {
        return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblOnline {
            let identifier = "onlineTVCTableViewCell"
            var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
            
            if logincell == nil {
                tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
            }
            logincell?.selectionStyle = .none
    //        logincell?.layer.cornerRadius = 25
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            let url = URL(string: dict["pic"] as! String)
            logincell?.imgOnlineFriends.setImageFrom(url)
            logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
            
            return logincell!
        }
        else {
        let identifier = "FriendDetailCell"
        logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FriendDetailCell
        if logincell == nil {
            tableView.register(UINib(nibName: "FriendDetailCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FriendDetailCell
        }
        //logincell.controller = self
        logincell.delegate = self
      //  logincell.vwImagIcon.isHidden = true
        logincell.updateCellwithModal(obj_gymDetail:self.obj_friendDetail)
//        logincell.btn_Chat.isHidden = true
        return logincell
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == tblOnline {
            return 60
        }
        else {
            return 1050 //550
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblOnline {
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            flagPark = false
            flagGym = false
            flagGallery = false
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            obj_phonelistvc.str_user_id = dict["userId"] as! String
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Hello delegate")
    }
    
    func selectedParkAtIndex(index: Int){
        if !flagParkLink {
        let dictemp = self.obj_friendDetail.array_vistedpark[index] as? NSDictionary ?? NSDictionary()
        let refreshAlert = UIAlertController(title: "", message: "Are you sure you want to remove park from profile?", preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            let park_id = dictemp["park_id"] as? String ?? ""
            self.UnsubscribePark(park_id: park_id)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
            
        present(refreshAlert, animated: true, completion: nil)
        }
        else {
//            let dictemp = self.obj_friendDetail.array_vistedpark[index] as? NSDictionary ?? NSDictionary()
//            let obj_DetailParkvc:DetailParkLinkVC = DetailParkLinkVC(nibName: "DetailParkLinkVC", bundle: nil)
//            obj_DetailParkvc.str_parkID = dictemp["park_id"] as? String ?? ""
//            self.navigationController?.pushViewController(obj_DetailParkvc, animated: true)
            
            let dictemp = self.obj_friendDetail.array_vistedpark[index] as? NSDictionary ?? NSDictionary()
            let obj_DetailParkvc:ParkLinkDetailVC = ParkLinkDetailVC(nibName: "ParkLinkDetailVC", bundle: nil)
            obj_DetailParkvc.str_parkID = dictemp["park_id"] as? String ?? ""
            self.navigationController?.pushViewController(obj_DetailParkvc, animated: true)
            
        }

           
       }
    func selectedGymAtIndex(index: Int){
        if !flagGymLink {
        let dictemp  = self.obj_friendDetail.array_gyms_id[index] as? NSDictionary ?? NSDictionary()
        let refreshAlert = UIAlertController(title: "Unsubscribe Gym", message: "Are you sure, you want to unsubscribe gym?", preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            let park_id = dictemp["subscription_id"] as? String ?? ""//dictemp.subscription_id!
            self.UnsubscribeGym(subscription_id: park_id)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        }
        else {
            let obj_requestmodal:GymListModel!//
            let obj_GymDetailVC:GymPersonalDetailVC = GymPersonalDetailVC(nibName: "GymPersonalDetailVC", bundle: nil)
           // obj_GymDetailVC.str_gymId = obj_requestmodal.str_gym_id
//            if searchEnabled {
//                obj_requestmodal = arrFavSearchHeader[indexPath.row] as! GymListModel
//                obj_GymDetailVC.str_gymId = obj_requestmodal.str_gym_id
//            }
//            else {
            let dictemp = self.obj_friendDetail.array_gyms_id[index] as? NSDictionary ?? NSDictionary()
            obj_GymDetailVC.str_gymId = dictemp["gym_id"] as? String ?? ""
//            }
            self.navigationController?.pushViewController(obj_GymDetailVC, animated: true)
        }
       }
    func selectedGalleryAtIndex(index: Int){
        if !flagGalleryLink {
            var dictemp : NSDictionary = [:]
            if strType_camera == "camera" {
            dictemp = self.obj_friendDetail.array_gallery[index] as? NSDictionary ?? NSDictionary()
            }
            else {
                dictemp = self.obj_friendDetail.array_videos[index] as? NSDictionary ?? NSDictionary()
            }
            let refreshAlert = UIAlertController(title: "Delete wallpost", message: "Are you sure, you want to delete wallPost?", preferredStyle: UIAlertController.Style.alert)
            refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                let park_id = dictemp["wallpost_id"] as? Int ?? 0
                self.deleteWallPost(wallPost_id: "\(park_id)")
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
            
        }
        else {
            
            
            var dictemp = self.obj_friendDetail.array_gallery[index] as? NSDictionary ?? NSDictionary()
            
            let filetype = dictemp["file_type"] as? String ?? ""
            if strType_camera == "camera" {
                
                let imgURl = dictemp["file"] as? String ?? ""
                flagChallengeImageviewer = true
                let obj_parkeAddressVC:ImageViewerVC = ImageViewerVC(nibName: "ImageViewerVC", bundle: nil)
                //            let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
                //            obj_parkeAddressVC.modalPresentationStyle = .fullScreen
                //            obj_parkeAddressVC.transitioningDelegate = self
                obj_parkeAddressVC.dictTemp = dictemp
                obj_parkeAddressVC.name =  self.obj_friendDetail.str_name
                obj_parkeAddressVC.str_url = imgURl
                self.navigationController?.pushViewController(obj_parkeAddressVC, animated: true)
                //                      self.navigationController?.present(nav, animated: true, completion: nil)
                
            }else {
                 dictemp = self.obj_friendDetail.array_videos[index] as? NSDictionary ?? NSDictionary()
                let imgURl = dictemp["thumbnail"] as? String ?? ""
                flagChallengeImageviewer = true
                let obj_parkeAddressVC:ImageViewerVC = ImageViewerVC(nibName: "ImageViewerVC", bundle: nil)
                //            let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
                //            obj_parkeAddressVC.modalPresentationStyle = .fullScreen
                //            obj_parkeAddressVC.transitioningDelegate = self
                obj_parkeAddressVC.dictTemp = dictemp
                obj_parkeAddressVC.name =  self.obj_friendDetail.str_name
                obj_parkeAddressVC.str_url = imgURl
                self.navigationController?.pushViewController(obj_parkeAddressVC, animated: true)
                //            let player = AVPlayer(url: URL(string: imgURl)!)
                //            let playerController = AVPlayerViewController()
                //            playerController.player = player
                //            self.present(playerController, animated: true) {
                //                player.play()
                //            }
                
                //        }
            }
        }
        
    }
    
    func actbtn_ViewAllParkbtnClicked(){
       
      let objViewAllParkListVC:ViewAllParkListVC = ViewAllParkListVC(nibName: "ViewAllParkListVC", bundle: nil)
        objViewAllParkListVC.isForPark = true
        objViewAllParkListVC.arr_parklist = self.obj_friendDetail.array_vistedpark
        self.navigationController?.pushViewController(objViewAllParkListVC, animated: true)
        
        
    }
    func actbtn_ViewAllGalleryBtnClicked(){
        
        let objViewAllParkListVC:ViewAllParkListVC = ViewAllParkListVC(nibName: "ViewAllParkListVC", bundle: nil)
        objViewAllParkListVC.isForPark = false
        objViewAllParkListVC.arr_parklist = self.obj_friendDetail.array_gyms
        self.navigationController?.pushViewController(objViewAllParkListVC, animated: true)
        
    }
    
    @IBAction func btnNotificationTapped(_ sender: UIButton) {
        let obj_NotificationVC:NotificationVC = NotificationVC(nibName: "NotificationVC", bundle: nil)
        self.navigationController?.pushViewController(obj_NotificationVC, animated: true)

    }
    @IBAction func btnEditProfileTapped(_ sender: UIButton) {
        let rootVC : EditProfileVC = EditProfileVC(nibName: "EditProfileVC", bundle: nil)
        self.navigationController?.pushViewController(rootVC, animated: true)

    }
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        flagProfile = true
        if strEditorBack == "Back" {
            let rootVC : SideMenuController1 = SideMenuController1(nibName: "SideMenuController", bundle: nil)
            if DataUtil.appdelegate().objUserInfo?.str_userType == "2" {
                rootVC.didTapMenuType = { menuType in
                    self.transitionToNew(menuType)
                }
            }
            else {
                rootVC.didTapMenuType1 = {menuTypeTrainer in
                    self.transitionTrainer(menuTypeTrainer)
                }
            }
            rootVC.modalPresentationStyle = .overCurrentContext
            rootVC.transitioningDelegate = self
            present(rootVC, animated: true)
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    func transitionToNew(_ menuType: MenuType) {
        switch menuType {
        case .FitnessWall:
            let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Challenges:
            let rootVC : OrderHistoryVC = OrderHistoryVC(nibName: "OrderHistoryVC", bundle: nil)
            self.present(rootVC, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.dismissControllerAnimated()
                
            }
        case .Chat:
            let rootVC : FriendListVC = FriendListVC(nibName: "FriendListVC", bundle: nil)
//            self.present(rootVC, animated: true, completion: nil)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                self.dismissControllerAnimated()
//            }
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.dismissControllerAnimated()
            }
        default:
            break
        }
    }
    
    func transitionTrainer(_ menuType : MenuTypeTrainer) {
        switch menuType {
        case .FitnessWall:
            let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
            
        case .MyOrders:
            let rootVC : OrderHistoryVC = OrderHistoryVC(nibName: "OrderHistoryVC", bundle: nil)
            self.present(rootVC, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Friends:
            let rootVC : FriendListVC = FriendListVC(nibName: "FriendListVC", bundle: nil)
            //            self.present(rootVC, animated: true, completion: nil)
            //            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            //                self.dismissControllerAnimated()
            //            }
            // self.present(rootVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Achievements:
            let rootVC : AchievementsVC = AchievementsVC(nibName: "AchievementsVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .MyTips:
            let rootVC : FreeTipsVCViewController = FreeTipsVCViewController(nibName: "FreeTipsVCViewController", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        
        case .Wallet:
            let rootVC : WalletNewVC = WalletNewVC(nibName: "WalletNewVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
            
        default:
            break
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension UserProfileVC: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = true
        return transiton
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = false
        return transiton
    }
}


    
