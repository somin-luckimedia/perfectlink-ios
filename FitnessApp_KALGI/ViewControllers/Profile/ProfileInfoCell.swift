//
//  ProfileInfoCell.swift
//  IslandTransfer
//
//  Created by Ashish Mishra on 21/03/18.
//  Copyright © 2018 Ashish Mishra. All rights reserved.
//





import UIKit
//countrycode_btn_clicked
protocol ProfileCellDelegate:class {
    func signup_btn_clicked(cell: ProfileInfoCell, btn: UIButton)
    func countrycode_btn_clicked(cell: ProfileInfoCell, btn: UIButton)
    func camera_btn_clicked(cell: ProfileInfoCell, btn: UIButton)
    func check_box_clicked(cell: ProfileInfoCell, btn: UIButton)
    func gender_btn_clicked(cell: ProfileInfoCell, btn: UIButton)
    func dob_btn_clicked(cell: ProfileInfoCell, btn: UIButton)
    func changepassword_btn_clicked(cell: ProfileInfoCell, btn: UIButton)
    func height_btn_clicked(cell: ProfileInfoCell, btn: UIButton)
    func weight_btn_clicked(cell: ProfileInfoCell, btn: UIButton)
}

class ProfileInfoCell: UITableViewCell {
    
    weak var delegate: ProfileCellDelegate?
    var cellIndex:NSInteger = 0
    
    @IBOutlet weak var btn_checkBox: UIButton!
    @IBOutlet weak var textfield_name: UITextField!
    @IBOutlet weak var textfield_user_name: UITextField!
    @IBOutlet weak var textfield_countrycode: UITextField!
    @IBOutlet weak var textfield_email: UITextField!
    @IBOutlet weak var textfield_mobile: UITextField!
    @IBOutlet weak var imageview_profile: CircularImageView!
    
    
    @IBOutlet weak var textfield_gender: UITextField!
    
    
    @IBOutlet weak var textfield_dob: UITextField!
    
    @IBOutlet weak var textfield_weight: UITextField!
    @IBOutlet weak var textfield_height: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureWithOwner(controller:ProfileInfoVC){
        self.textfield_name.delegate = controller;
        self.textfield_email.delegate = controller;
        self.textfield_mobile.delegate = controller;
    }
    
    
    func updateFields(info:UserInfo) {
        let url = URL(string: info.profilePic)
        imageview_profile.setImageFrom(url)
        textfield_user_name.text = info.str_user_name
        textfield_name.text = info.name
        textfield_email.text = info.email
        textfield_mobile.text = info.phone
        textfield_countrycode.text = info.str_countryCode
        textfield_gender.text = info.gender
        textfield_dob.text = info.str_dob
        textfield_height.text = info.str_height
        textfield_weight.text = String(format: "%@ lbs", info.str_weight) 
        if DataUtil.appdelegate().array_height_final.contains(info.str_height){
            let index:Int = DataUtil.appdelegate().array_height_final.firstIndex(of: info.str_height)!
            textfield_height.text = DataUtil.appdelegate().array_height[index]
        }
        if DataUtil.appdelegate().array_weight.contains(info.str_weight){
            let index:Int = DataUtil.appdelegate().array_weight.firstIndex(of: info.str_weight)!
            textfield_weight.text = String(format: "%@ lbs", DataUtil.appdelegate().array_weight[index])
        }
    }
    
    
    @IBAction func country_btn_clicked(_ sender: UIButton) {
        self.delegate?.countrycode_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func signup_btn_clicked(_ sender: UIButton) {
        self.delegate?.signup_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func camera_btn_clicked(_ sender: UIButton) {
        self.delegate?.camera_btn_clicked(cell: self, btn: sender)
    }
    
    
    @IBAction func check_box_clicked(_ sender: UIButton) {
        self.delegate?.check_box_clicked(cell: self, btn: sender)
    }
    
    @IBAction func gender_btn_clicked(_ sender: UIButton) {
        self.delegate?.gender_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func dob_btn_clicked(_ sender: UIButton) {
        self.delegate?.dob_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func changepassword_btn_clicked(_ sender: UIButton) {
        self.delegate?.changepassword_btn_clicked(cell: self, btn: sender)
        
    }
    @IBAction func height_btn_clicked(_ sender: UIButton) {
        self.delegate?.height_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func weight_btn_clicked(_ sender: UIButton) {
        self.delegate?.weight_btn_clicked(cell: self, btn: sender)
    }
    
}
