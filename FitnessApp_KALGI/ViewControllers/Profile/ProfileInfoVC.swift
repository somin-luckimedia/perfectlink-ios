//
//  ProfileInfoVC.swift
//  IslandTransfer
//
//  Created by Ashish Mishra on 21/03/18.
//  Copyright © 2018 Ashish Mishra. All rights reserved.
//




import UIKit
import Alamofire



class ProfileInfoVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate,ProfileCellDelegate, IQActionSheetPickerViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var tableview_signup: UITableView!
    var dictSideMenu:NSArray = []
    var signupcell: ProfileInfoCell!
    var strCountryCode:String = ""
    var str_gender:String = ""
    var str_dob:String = ""
    var str_height:String = ""
    var str_weight:String = ""
    var strIdtype:String = "1"
    var str_provider:String = ""
    var selectedImage:UIImage?
    let array_geneder:[String] = ["Male", "Female"]
    var dict_facebook:Dictionary<String,Any>? = nil
    var str_category:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "EDIT PROFILE"
        tableview_signup.delegate  = self as UITableViewDelegate
        tableview_signup.dataSource = self as UITableViewDataSource
        tableview_signup.reloadData()
        //self.addLeftMenuButtonWithImage()
        dictSideMenu = DataUtil.readCountryJson(fileName: "CountryCodes") as NSArray
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        
        str_height = (DataUtil.appdelegate().objUserInfo?.str_height)!
        str_weight = (DataUtil.appdelegate().objUserInfo?.str_weight)!
        strCountryCode = (DataUtil.appdelegate().objUserInfo?.str_countryCode)!
        str_dob = (DataUtil.appdelegate().objUserInfo?.str_dob)!
        str_gender = (DataUtil.appdelegate().objUserInfo?.gender)!
        let category:Int = Int((DataUtil.appdelegate().objUserInfo?.str_trainer_category)!)!
        str_category = DataUtil.appdelegate().array_category[category-1]
        // Do any additional setup after loading the view.
    }
    
    
   
    
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        //   self.navigationController?.isNavigationBarHidden = true
    }
    /* - (void)addLeftMenuButtonWithImage{
     UIImage *buttonImage = [UIImage imageNamed:@"back"];
     UIImage *faceImage = buttonImage;
     UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
     [face addTarget:self
     action:@selector(backClicked)
     forControlEvents:UIControlEventTouchUpInside];
     face.bounds = CGRectMake(0, 0, 30, 20 );
     [face setImage:faceImage forState:UIControlStateNormal];
     UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:face];
     self.navigationItem.leftBarButtonItem = backButton;
     }
     */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 625
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "ProfileInfoCell"
        signupcell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ProfileInfoCell
        if signupcell == nil {
            tableView.register(UINib(nibName: "ProfileInfoCell", bundle: nil), forCellReuseIdentifier: identifier)
            signupcell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ProfileInfoCell
        }
        
        
        
        signupcell.cellIndex = indexPath.row
        signupcell.updateFields(info: DataUtil.appdelegate().objUserInfo!)
        signupcell.configureWithOwner(controller: self)
        signupcell.delegate = self
        
        //  cell.textLabel?.text = monthName
        return signupcell
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Hello delegate")
    }
    
    func signup_btn_clicked(cell: ProfileInfoCell, btn: UIButton) {
        let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate
        let strUserName  : String = (signupcell.textfield_name.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strEmail  : String = (signupcell.textfield_email.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strMobile  : String = (signupcell.textfield_mobile.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strDeviceToken:String = (objAppDelgate1.str_devicetoken)!
        let strDeviceType:String = "IOS"
        let strLat:String = (objAppDelgate1.str_longitude)
        let strLong:String = (objAppDelgate1.str_longitude)
        
        
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
//        let headers: HTTPHeaders = [
//            "Authorization": String(format: "Bearer %@",str_accessToken),
//            "Accept": "application/json"
//        ]

        let postParam: Parameters = ["name":strUserName,"email":strEmail, "country_code":strCountryCode, "phone_number":strMobile, "device_type":strDeviceType,"device_token":strDeviceToken,"lat":strLat, "lon":strLong, "gender":str_gender, "dob":str_dob, "height":str_height, "weight":str_weight, "about":"","address":"", "user_id":(DataUtil.appdelegate().objUserInfo?.userId)!]
        print("Login Post Parameter : \(postParam)")
        
        
        if !signupValidation() {
            return
        }
        var isImage:Bool = true
        if selectedImage == nil{
            isImage = false
        }
        
        // DataUtil.alertMessage("You have been registered succesfully", viewController: self)
        
        ServerCommunication.getDataWithPostImage(url: ConstantFiles.update_profile, image: selectedImage as Any,parameter: postParam, viewController: self, isImage:isImage,success: { (successDict) in
            
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                let str_token:String = String(format: "%@",str_accessToken)
                DataUtil.appdelegate().objUserInfo = UserInfo(dictUserInfo: successDict["response"] as! NSDictionary)
                DataUtil.appdelegate().objUserInfo?.str_accessToken = str_token
                self.tableview_signup.reloadData()
                
                //SideMenuController1
                let obj_controlller:SideMenuController1 =  self.sideMenuController?.sideViewController as! SideMenuController1
                obj_controlller.update_profileData()
                
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
            
            
            // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
 
        
    }
    
    
    
    
    func signupValidation() -> Bool {
        
        if (signupcell.textfield_name.text?.isEmpty)!{
            DataUtil.alertMessage("Name is Empty", viewController: self)
            return false
        }
        
        
        if (signupcell.textfield_email.text?.isEmpty)!{
            DataUtil.alertMessage("Email Id is Empty", viewController: self)
            return false
        }
        let isValidEmail = ValidateUtils.isValidEmail(signupcell.textfield_email.text!)
        if isValidEmail == false {
            DataUtil.alertMessage("Please provide the valid Email.", viewController: self)
            return false
        }
        
        if (signupcell.textfield_mobile.text?.isEmpty)!{
            DataUtil.alertMessage("Phone is Empty", viewController: self)
            return false
        }
        
        let isValidPhone = ValidateUtils.isValidPhoneNumber(signupcell.textfield_mobile.text!)
        
        if isValidPhone == false {
            DataUtil.alertMessage("Please provide the valid Phone.", viewController: self)
            return false
        }
        
        
        
        if (signupcell.textfield_countrycode.text?.isEmpty)!{
            DataUtil.alertMessage("Countrycode is Empty", viewController: self)
            return false
        }
        
        if (signupcell.textfield_gender.text?.isEmpty)!{
            DataUtil.alertMessage("Gender is Empty", viewController: self)
            return false
        }
        
        if (signupcell.textfield_dob.text?.isEmpty)!{
            DataUtil.alertMessage("Date of birth is Empty", viewController: self)
            return false
        }
        
        if (signupcell.textfield_height.text?.isEmpty)!{
            DataUtil.alertMessage("Height is Empty", viewController: self)
            return false
        }
        
        if (signupcell.textfield_weight.text?.isEmpty)!{
            DataUtil.alertMessage("Weight is Empty", viewController: self)
            return false
        }
        
        
        
        return true
    }
    
    func countrycode_btn_clicked(cell:ProfileInfoCell, btn:UIButton){
        let array: NSArray = (dictSideMenu.value(forKey: "dial_code") as! NSArray)
        let array1: NSArray = (dictSideMenu.value(forKey: "name") as! NSArray)
        var modifiedarray = [String]()
        //let modifiedarray:NSArray = []
        for dict in dictSideMenu {
            
            let name:String = (dict as AnyObject).value(forKey: "name") as! String
            let dialcode:String = (dict as AnyObject).value(forKey: "dial_code") as! String
            
            let finalstring = String(format:"(%@) %@", name, dialcode)
            modifiedarray.append(finalstring)
            
        }
        
        
        
        // var array1:Array = [String]
        
        var groups = [[String]]()
        if let swiftArray = modifiedarray as NSArray as? [String] {
            groups.append(swiftArray)
        }
        
        // print(array)
        
        // groups.append(array as! [String])
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()
        
    }
    
    /*func radio_btn_clicked(cell: SIgnupCell, btn: UIButton) {
     
     switch btn.tag {
     case 101:
     self.signupcell.btn_local.image = UIImage(named: "radioselected_btn")
     self.signupcell.btn_foreigner.image = UIImage(named: "radiodeselected_btn")
     self.hidePassportView()
     break
     
     case 102:
     self.signupcell.btn_local.image = UIImage(named: "radiodeselected_btn")
     self.signupcell.btn_foreigner.image = UIImage(named: "radioselected_btn")
     self.showPassportView()
     break
     
     case 103:
     self.signupcell.btn_workpermit.image = UIImage(named: "radioselected_btn")
     self.signupcell.btn_passport.image = UIImage(named: "radiodeselected_btn")
     self.strIdtype = "2"
     break
     
     case 104:
     self.signupcell.btn_passport.image = UIImage(named: "radioselected_btn")
     self.signupcell.btn_workpermit.image = UIImage(named: "radiodeselected_btn")
     self.strIdtype = "3"
     
     default: break
     
     }
     
     }*/
    
    
    
    
    func camera_btn_clicked(cell: ProfileInfoCell, btn: UIButton) {
        
        
        let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert) in
            self.openCamera()
            
        }))
        
        alert.addAction(UIAlertAction(title:"Photo Library", style: .default, handler: { (alert) in
            self.opneGallery()
        }))
        
        alert.addAction(UIAlertAction(title:"Cancel", style: .default, handler: { (alert) in
        }))
        self.present(alert, animated: true, completion: nil)
        
        
        
        
    }
    
    
    func opneGallery(){
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self 
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //var selectedImage: UIImage?
        if let editedImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")]   as? UIImage {
            selectedImage = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerOriginalImage")] as? UIImage {
            selectedImage = originalImage
        }
        if let selectedImage = selectedImage {
            signupcell.imageview_profile.image = selectedImage;
            signupcell.imageview_profile.layer.cornerRadius = signupcell.imageview_profile.frame.size.height/2
            signupcell.imageview_profile.layer.masksToBounds = true
        }
        
        /*if let selectedImages = selectedImage {
         
         
         if let data = UIImageJPEGRepresentation(selectedImages,1) {
         let parameters: Parameters = [
         "access_token" : "YourToken"
         ]
         // You can change your image name here, i use NSURL image and convert into string
         let imageURL:NSURL = info[UIImagePickerControllerReferenceURL] as! NSURL
         let fileName = imageURL.absoluteString
         // Start Alamofire
         
         }
         
         
         
         }*/
        
        
        
        
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    //camera_btn_clicked
    /*
     -(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray*)titles
     
     */
    
    /*  func actionSheetPickerView(_ pickerView: IQActionSheetPickerView, didSelectTitles titles: [String]) {
     let title:String = titles[0]
     strCountryCode = title
     signupcell?.textfield_countrycode?.text = strCountryCode
     print(title)
     }*/
    
    /*
     
     
     
     
     
     
     
     
     
     
     
     
     */
    
    
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView, didSelectTitlesAtIndexes indexes: [NSNumber]) {
        
        if pickerView.tag == 101{
            let index = indexes[0] as! NSInteger
            let array: NSArray = (dictSideMenu.value(forKey: "dial_code") as! NSArray)
            let title:String = array[index] as! String
            strCountryCode = title
            signupcell?.textfield_countrycode?.text = strCountryCode
            
        }else if pickerView.tag == 102{
            let index = indexes[0] as! NSInteger
            let title:String = array_geneder[index]
            str_gender = title
            signupcell?.textfield_gender?.text = str_gender
        }else if pickerView.tag == 104{
            let index = indexes[0] as! NSInteger
            let title:String = DataUtil.appdelegate().array_height[index]
            str_height = DataUtil.appdelegate().array_height_final[index]
            signupcell?.textfield_height?.text = title
        }else if pickerView.tag == 105{
            let index = indexes[0] as! NSInteger
            let title:String = DataUtil.appdelegate().array_weight[index]
            str_weight = title
            signupcell?.textfield_weight?.text = String(format: "%@ lbs", title)
        }
        
    }
    
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView, didSelect date: Date) {
        
        //DataUtil.stringFromDate(date: date_today, DateFormate: "yyyy-MM-dd")
        str_dob = DataUtil.stringFromDate(date: date, DateFormate: "yyyy-MM-dd")
        signupcell?.textfield_dob.text = str_dob
        // self.lbl_time.text = str_time
        
        
    }
    
    
    func check_box_clicked(cell: ProfileInfoCell, btn: UIButton) {
        if btn.isSelected {
            btn.isSelected = false
        }else{
            btn.isSelected = true
        }
    }
    
    func gender_btn_clicked(cell: ProfileInfoCell, btn: UIButton) {
        var groups = [[String]]()
        groups.append(array_geneder)
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()
    }
    
    func dob_btn_clicked(cell: ProfileInfoCell, btn: UIButton) {
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.actionSheetPickerStyle = .datePicker
        picker.delegate = self
        picker.tag = 103
        let date:Date =  Date()
        //  let calendar = Calendar.current
        // let date_today:Date = calendar.date(byAdding: .day, value: 7, to: date)!
        picker.maximumDate = date
        picker.show()
    }
    
    func changepassword_btn_clicked(cell: ProfileInfoCell, btn: UIButton) {
        let obj_ferrylistingVC : ChangePasswordVC = ChangePasswordVC(nibName: "ChangePasswordVC", bundle: nil)
        self.navigationController?.pushViewController(obj_ferrylistingVC, animated: true)
    }
    
    func height_btn_clicked(cell: ProfileInfoCell, btn: UIButton){
        var groups = [[String]]()
        groups.append(DataUtil.appdelegate().array_height)
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()
    }
    
    
    func weight_btn_clicked(cell: ProfileInfoCell, btn: UIButton){
        var groups = [[String]]()
        groups.append(DataUtil.appdelegate().array_weight)
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()
    }
    
}

