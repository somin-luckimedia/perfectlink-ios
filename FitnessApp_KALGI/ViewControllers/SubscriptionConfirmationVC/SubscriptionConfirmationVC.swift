//
//  SubscriptionConfirmationVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 23/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit
import Alamofire

class SubscriptionConfirmationVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var str_transactionId:String = ""
    @IBOutlet weak var tableview_detail: UITableView!
    var obj_orderSummaryCell: OrdarSummaryCell!
    var obj_orderShippingCell: OrderShippingCell!
    var obj_orderTrackCell: OrderTrackCell!
    var obj_orderDetailModel:OrderDetailModel = OrderDetailModel()
    var isFromHistory:Bool = false
    var isFromTrainerHome:Bool = false
    var str_subscription_type:String = "GYM"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview_detail.delegate  = self as UITableViewDelegate
        tableview_detail.dataSource = self as UITableViewDataSource
        self.title = "SUBSCRIPTION CONFIRMATION"
        self.addLeftMenuButtonWithImage()
        self.tableview_detail.reloadData()
        if (DataUtil.appdelegate().objUserInfo?.str_userType) == "2" {
            self.getMyBookings()
            
        }else{
            self.getTrainerMyBookings()
        }
        // Do any additional setup after loading the view.
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        if isFromHistory == true{
            button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
            button.addTarget(self, action: #selector(self.back_summary_btn_clicked), for: UIControl.Event.touchUpInside)
        }else{
            button.setImage(UIImage(named: "homeIcon"), for: UIControl.State.normal)
            button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        }
        
        button.frame = CGRect(x: 0, y: 0, width: 25 , height: 25)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func getMyBookings(){
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        let postParam: Parameters = ["transaction_id":str_transactionId]
        print("Login Post Parameter : \(postParam)")
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.get_subscriptionInfo, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    self.obj_orderDetailModel = OrderDetailModel(dictUserInfo: arr as! NSDictionary)
                    self.tableview_detail.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    func getTrainerMyBookings(){
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        let postParam: Parameters = ["transaction_id":str_transactionId]
        print("Login Post Parameter : \(postParam)")
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.trainer_subscription_info_by_transaction_id, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    self.obj_orderDetailModel = OrderDetailModel(dictUserInfo: arr as! NSDictionary)
                    self.tableview_detail.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    @objc func back_btn_clicked(){
        let rootVC : CitySelectionVC = CitySelectionVC(nibName: "CitySelectionVC", bundle: nil)
        let nav = UINavigationController(rootViewController: rootVC)
        sideMenuController?.embed(centerViewController: nav)
        /*
         let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
         objAppDelgate.loadSidePanel(objVC: self.navigationController!)
         */
    }
    
    @objc func back_summary_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 70
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header:OrderSummaryHeaderView =  UINib(nibName: "OrderSummaryHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! OrderSummaryHeaderView
        if section == 1{
            header.lbl_title.text = "Membership Detail"
        }else if section == 2{
            header.lbl_title.text = "Price Detail"
        }
        
        return header
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0{
            if indexPath.row == 0{
                return 73
            }else{
                return 120
            }
        }else if indexPath.section == 1{
            if isFromTrainerHome == true{
                return 0
            }
            return 55
        }else{
            return 55
        }
        
        
        // return 710
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0{
            return 2
        }else if section == 1{
            if str_subscription_type == "GYM"{
                return 4
            }
            return 6
        }else{
            if self.obj_orderDetailModel.str_is_coupon_applied == true{
                return 4
            }
            return 3
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        ////TrainerHomeAddressCell
        if indexPath.section == 0{
            if indexPath.row == 0{
                let identifier = "SubscriptionConfirmationCell"
                var obj_amountcell: SubscriptionConfirmationCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? SubscriptionConfirmationCell
                if obj_amountcell == nil {
                    tableView.register(UINib(nibName: "SubscriptionConfirmationCell", bundle: nil), forCellReuseIdentifier: identifier)
                    obj_amountcell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? SubscriptionConfirmationCell)!
                }
                obj_amountcell?.lbl_orderId.text = obj_orderDetailModel.str_order_id
                obj_amountcell?.lbl_valid.text = obj_orderDetailModel.obj_accessories_detail.str_valid_upto
                return obj_amountcell!
            }
                let identifier = "OrdarSummaryCell"
                obj_orderSummaryCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrdarSummaryCell
                if obj_orderSummaryCell == nil {
                    tableView.register(UINib(nibName: "OrdarSummaryCell", bundle: nil), forCellReuseIdentifier: identifier)
                    obj_orderSummaryCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrdarSummaryCell
                }
            obj_orderSummaryCell.str_type = self.str_subscription_type
                obj_orderSummaryCell.updateCellwithModal(obj_accessoryiesModel: self.obj_orderDetailModel.obj_accessories_detail, str_qty: String(format: "%@", obj_orderDetailModel.str_quantity))
                return obj_orderSummaryCell
        }else if indexPath.section == 1{
            var index:Int = 0
            if str_subscription_type == "GYM"{
                index = 3
             }else{
                 index = 5
             }
            if indexPath.row == index{
                    let identifier = "SubscriptionTrackingCell"
                    var obj_amountcell: SubscriptionTrackingCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? SubscriptionTrackingCell
                    if obj_amountcell == nil {
                        tableView.register(UINib(nibName: "SubscriptionTrackingCell", bundle: nil), forCellReuseIdentifier: identifier)
                        obj_amountcell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? SubscriptionTrackingCell)!
                    }
                    return obj_amountcell!
            }
            
            
            
            //SubscriptionMembershipCell
            let identifier = "SubscriptionMembershipCell"
            var obj_membershipcell: SubscriptionMembershipCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? SubscriptionMembershipCell
            if obj_membershipcell == nil {
                tableView.register(UINib(nibName: "SubscriptionMembershipCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_membershipcell = tableView.dequeueReusableCell(withIdentifier: identifier) as? SubscriptionMembershipCell
            }

            var str_title:String = ""
            var str_value:String = ""
            if str_subscription_type == "GYM"{
                if indexPath.row == 0{
                    str_title = "Start Date"
                    str_value = self.obj_orderDetailModel.obj_accessories_detail.str_start_date
                }else if indexPath.row == 1{
                    str_title = "End Date"
                    str_value = self.obj_orderDetailModel.obj_accessories_detail.str_end_date
                }else{
                    str_title = "Membership Plan"
                    str_value = String(format: "%@ Months", self.obj_orderDetailModel.obj_accessories_detail.str_months)
                }
            }else{
                if indexPath.row == 0{
                    str_title = "Start Date"
                    str_value = self.obj_orderDetailModel.obj_accessories_detail.str_start_date
                }else if indexPath.row == 1{
                    str_title = "End Date"
                    str_value = self.obj_orderDetailModel.obj_accessories_detail.str_end_date
                }else if indexPath.row == 2{
                    str_title = "Membership Plan"
                    str_value = String(format: "%@ Days", self.obj_orderDetailModel.obj_accessories_detail.str_months)
                }else if indexPath.row == 3{
                    str_title = "Membership Type"
                    str_value = self.obj_orderDetailModel.obj_accessories_detail.str_membership_type
                }else{
                    str_title = "Member Count"
                    str_value = String(format: "%@ Members", self.obj_orderDetailModel.obj_accessories_detail.str_member_count)
                }
            }
            obj_membershipcell?.lbl_title.text = str_title
            obj_membershipcell?.lbl_value.text = str_value
            return obj_membershipcell!
        }else{
            let identifier = "OrderAmountCell"
            var obj_amountcell: OrderAmountCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderAmountCell
            if obj_amountcell == nil {
                tableView.register(UINib(nibName: "OrderAmountCell", bundle: nil), forCellReuseIdentifier: identifier)
                obj_amountcell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderAmountCell)!
            }
            
            if self.obj_orderDetailModel.str_is_coupon_applied == true{
                if indexPath.row == 0{
                    obj_amountcell?.lbl_title.text = String(format: "Total Amount")
                    obj_amountcell?.lbl_value.text = String(format: "%@%@", obj_orderDetailModel.obj_accessories_detail.str_currency, obj_orderDetailModel.str_membership_amount)
                }else if indexPath.row == 1{
                    obj_amountcell?.lbl_title.text = String(format: "Promo - (%@)", self.obj_orderDetailModel.str_coupon_code)
                    obj_amountcell?.lbl_value.text = String(format: "%@%@", obj_orderDetailModel.obj_accessories_detail.str_currency, obj_orderDetailModel.str_discounted_amount)
                }
                else if indexPath.row == 2{
                    obj_amountcell?.lbl_title.text = String(format: "Amount Paid")
                    obj_amountcell?.lbl_value.text = String(format: "%@%@", obj_orderDetailModel.obj_accessories_detail.str_currency, obj_orderDetailModel.str_total_amount_paid)
                }
                else{
                    obj_amountcell?.lbl_title.text = String(format: "Payment Method")
                    obj_amountcell?.lbl_value.text = String(format: "%@", obj_orderDetailModel.str_payment_method)
                }
            }else{
                if indexPath.row == 0{
                    obj_amountcell?.lbl_title.text = String(format: "Total Amount")
                    obj_amountcell?.lbl_value.text = String(format: "%@%@", obj_orderDetailModel.obj_accessories_detail.str_currency, obj_orderDetailModel.str_total_amount_paid)
                }else if indexPath.row == 1{
                    obj_amountcell?.lbl_title.text = String(format: "Amount Paid")
                    obj_amountcell?.lbl_value.text = String(format: "%@%@", obj_orderDetailModel.obj_accessories_detail.str_currency, obj_orderDetailModel.str_total_amount_paid)
                }
                else{
                    obj_amountcell?.lbl_title.text = String(format: "Payment Method")
                    obj_amountcell?.lbl_value.text = String(format: "%@", obj_orderDetailModel.str_payment_method)
                }
            }
            return obj_amountcell!
        }
    }
}
