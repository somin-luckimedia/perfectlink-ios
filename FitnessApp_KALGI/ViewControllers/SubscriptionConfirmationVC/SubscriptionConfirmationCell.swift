//
//  SubscriptionConfirmationCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 23/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class SubscriptionConfirmationCell: UITableViewCell {
    //73
    @IBOutlet weak var lbl_orderId: UILabel!
    @IBOutlet weak var lbl_valid: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
