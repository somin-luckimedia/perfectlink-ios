//
//  ChallengeDetailCell.swift
//  FitnessApp
//
//  Created by Ashish on 14/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//


import UIKit


protocol ChallengeDetailCellDelegate: class {
    func accept_btn_clicked(cell: ChallengeDetailCell, obj_challenge:ChallengeListModel ,btn: UIButton)
    func complete_btn_clicked(cell: ChallengeDetailCell, obj_challenge:ChallengeListModel , btn: UIButton)
    func share_btn_clicked(cell: ChallengeDetailCell, obj_challenge:ChallengeListModel ,btn: UIButton)
    func popup_clicked(cell: ChallengeDetailCell,  obj_participantDetail: ParticipantModel, index:Int)
    func image_btn_clicked(cell: ChallengeDetailCell,  obj_participantDetail: ParticipantModel, index:Int)
    
    func play_btn_clicked(cell: ChallengeDetailCell, obj_participantDetail:ParticipantModel  ,index:Int)
    func preview_btn_clicked(cell: ChallengeDetailCell, obj_participantDetail:ParticipantModel  ,index:Int)
    
    func detail_btn_clicked(cell: ChallengeDetailCell, obj_participantDetail:ParticipantModel  , obj_participantcell:ChallengeMemberCell ,index:Int)
}


class ChallengeDetailCell: UITableViewCell, ChallengeMemberCellDelegate, ChallengeGalleryCellDelegate {
    func play_btn_clicked(cell: ChallengeGalleryCell, obj_participantDetail: ParticipantModel, index: Int) {
        self.delegate?.play_btn_clicked(cell: self, obj_participantDetail: obj_participantDetail, index: index)
    }
    
    func preview_btn_clicked(cell: ChallengeGalleryCell, obj_participantDetail: ParticipantModel, index: Int) {
        self.delegate?.preview_btn_clicked(cell: self, obj_participantDetail: obj_participantDetail, index: index)
    }
    
    func popup_btn_clicked(cell: ChallengeMemberCell, obj_participantDetail: ParticipantModel, index: Int) {
        self.delegate?.popup_clicked(cell: self, obj_participantDetail: obj_participantDetail, index: index)
    }
    
    func image_btn_clicked(cell: ChallengeMemberCell, obj_participantDetail:ParticipantModel  ,index:Int){
        //self.delegate?.image_btn_clicked(cell: self, obj_participantDetail: obj_participantDetail, index: index)
        let indexPath:IndexPath = IndexPath(row: index, section: 0)
        if cell.obj_collectionview == collectionview_pager{
            return
        }
        let cell:ChallengeMemberCell = cell.obj_collectionview.cellForItem(at: indexPath) as! ChallengeMemberCell
        if cell.obj_collectionview == collectionview_pager{
            return
        }else if cell.obj_collectionview == collectionview_completed{
            cell.selected()
            if let selected = selectedIndexPathCompleted {
                let cell:ChallengeMemberCell = cell.obj_collectionview.cellForItem(at: selected) as! ChallengeMemberCell
                cell.deselected()
                if selectedIndexPathCompleted == indexPath{
                    selectedIndexPathCompleted = nil
                    return
                }
                
            }
            selectedIndexPathCompleted = indexPath
            
            
            
        }else if cell.obj_collectionview == collectionview_accepted{
            cell.selected()
            if let selected = selectedIndexPathAccepted {
                let cell:ChallengeMemberCell = cell.obj_collectionview.cellForItem(at: selected) as! ChallengeMemberCell
                cell.deselected()
                if selectedIndexPathAccepted == indexPath{
                    selectedIndexPathAccepted = nil
                    return
                }
            }
            selectedIndexPathAccepted = indexPath
        }else{
            cell.selected()
            if let selected = selectedIndexPathRequested {
                let cell:ChallengeMemberCell = cell.obj_collectionview.cellForItem(at: selected) as! ChallengeMemberCell
                cell.deselected()
                if selectedIndexPathRequested == indexPath{
                    selectedIndexPathRequested = nil
                    return
                }
            }
            selectedIndexPathRequested = indexPath
        }
    
        
        
        
        
    }
    
    

    @IBOutlet weak var imageView_profile: UIImageView!
    @IBOutlet weak var lbl_message: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var btn_accept: UIButton!
    
    @IBOutlet weak var lbl_total_description: UILabel!
    
    @IBOutlet weak var lbl_completed_description: UILabel!
    @IBOutlet weak var lbl_accepted_description: UILabel!
    @IBOutlet weak var lbl_requested_description: UILabel!
    
    var selectedIndexPathCompleted : IndexPath?
    var selectedIndexPathAccepted : IndexPath?
    var selectedIndexPathRequested : IndexPath?
    var obj_challengeDetail:ChallengeListModel! = ChallengeListModel()
    weak var delegate:ChallengeDetailCellDelegate?
    
    @IBOutlet weak var collectionview_pager: UICollectionView!
    @IBOutlet weak var collectionview_completed: UICollectionView!
    @IBOutlet weak var collectionview_accepted: UICollectionView!
    @IBOutlet weak var collectionview_requested: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    var str_requestType:Int = 0
    
    @IBOutlet weak var constraint_view_message: NSLayoutConstraint!
    @IBOutlet weak var constraint_collection_completed: NSLayoutConstraint!
    @IBOutlet weak var constraint_collection_accepted: NSLayoutConstraint!
    @IBOutlet weak var constraint_collection_requested: NSLayoutConstraint!
    @IBOutlet weak var constraint_collection_background: NSLayoutConstraint!
    @IBOutlet weak var constraint_collection_gallery: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    
    func updateCellwithModal(obj_challengeDetail:ChallengeListModel){
        self.obj_challengeDetail = obj_challengeDetail
        self.collectionview_pager.register(UINib(nibName: "ChallengeGalleryCell", bundle: nil), forCellWithReuseIdentifier: "ChallengeGalleryCell")
        self.collectionview_pager.reloadData()
        self.collectionview_accepted.register(UINib(nibName: "ChallengeMemberCell", bundle: nil), forCellWithReuseIdentifier: "ChallengeMemberCell")
        self.collectionview_accepted.reloadData()
        self.collectionview_completed.register(UINib(nibName: "ChallengeMemberCell", bundle: nil), forCellWithReuseIdentifier: "ChallengeMemberCell")
        self.collectionview_completed.reloadData()
        self.collectionview_requested.register(UINib(nibName: "ChallengeMemberCell", bundle: nil), forCellWithReuseIdentifier: "ChallengeMemberCell")
        self.collectionview_requested.reloadData()
        var url = URL(string: obj_challengeDetail.str_profile_picture)
        imageView_profile.setImageFrom(url)

//        imageView_profile.setImageFromURL(stringImageUrl: obj_challengeDetail.str_profile_picture )
        
        pageControl.hidesForSinglePage = true
        pageControl.numberOfPages = self.obj_challengeDetail.array_participant_pager.count
        lbl_message.text = String(format: "%@ would like to challenge you to complete this activity.", obj_challengeDetail.str_name)
        lbl_description.text = obj_challengeDetail.str_text
        lbl_total_description.text = String(format: "Challenge Members (%ld)", obj_challengeDetail.array_participant.count)
        lbl_completed_description.text = String(format: "Challenge Completed (%ld)", obj_challengeDetail.array_participant_completed.count)
        lbl_accepted_description.text = String(format: "Challenge Accepted (%ld)", obj_challengeDetail.array_participant_accepted.count)
        lbl_requested_description.text = String(format: "Challenge Requested (%ld)", obj_challengeDetail.array_participant_requested.count)
        
        if str_requestType == 1{
             self.btn_accept.isHidden = false
            if obj_challengeDetail.str_challenge_status == "0"{
                self.btn_accept.setTitle("Accept", for: .normal)
            }else{
                self.btn_accept.setTitle("Complete", for: .normal)
            }
            self.constraint_view_message.constant = 60
        }else if str_requestType == 0{
            self.btn_accept.isHidden = false
            self.btn_accept.setTitle("Share", for: .normal)
            self.constraint_view_message.constant = 0
            //self.constraint_collection_background.constant = self.btn_accept.frame.origin.y + self.btn_accept.frame.size.height + 20
        }
        else{
            self.btn_accept.isHidden = true
            self.constraint_view_message.constant = 0
            //self.constraint_collection_background.constant = self.btn_accept.frame.origin.y + self.btn_accept.frame.size.height + 20
        }
        
        
        
        
        if self.obj_challengeDetail.array_participant_completed.count == 0{
            self.constraint_collection_completed.constant = 26
        }else{
            self.constraint_collection_completed.constant = 128
        }
        
        
        if self.obj_challengeDetail.array_participant_accepted.count == 0{
            self.constraint_collection_accepted.constant = 26
        }else{
            self.constraint_collection_accepted.constant = 128
        }
        
        if self.obj_challengeDetail.array_participant_requested.count == 0{
        self.constraint_collection_requested.constant = 26
        }else{
            self.constraint_collection_requested.constant = 128
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            if self.str_requestType == 2{
                self.constraint_collection_background.constant = self.lbl_description.frame.origin.y + self.lbl_description.frame.size.height + 40
            }else{
                self.constraint_collection_background.constant = self.btn_accept.frame.origin.y + self.btn_accept.frame.size.height + 40
            }
        })
  
        
        
        
        
        
        
        
        
    }
    
    @IBAction func accept_btn_clicked(_ sender: UIButton) {
        if str_requestType == 1{
            self.btn_accept.isHidden = true
            if obj_challengeDetail.str_challenge_status == "0"{
                self.delegate?.accept_btn_clicked(cell: self, obj_challenge: self.obj_challengeDetail, btn:sender)
            }else{
                self.delegate?.complete_btn_clicked(cell: self, obj_challenge: self.obj_challengeDetail, btn:sender)
            }
        }else if str_requestType == 0{
            self.delegate?.share_btn_clicked(cell: self, obj_challenge: self.obj_challengeDetail, btn:sender)
            
//            let str_message:String = String(format: "%@ created challenge.", self.obj_challengeDetail.str_name)
//            let name = NSURL(string: self.obj_challengeDetail.str_file)
//            let objectsToShare = [str_message, name!] as [Any]
//            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//            self.present(activityVC, animated: true, completion: nil)
        }
    }
}




extension ChallengeDetailCell:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionview_pager{
            return self.obj_challengeDetail.array_participant_pager.count
        }else if collectionView == collectionview_completed{
            return self.obj_challengeDetail.array_participant_completed.count
        }else if collectionView == collectionview_accepted{
            return self.obj_challengeDetail.array_participant_accepted.count
        }else{
            return self.obj_challengeDetail.array_participant_requested.count
        }
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1     //return number of sections in collection view
    }
    
        
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionview_pager{
            let cell:ChallengeGalleryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChallengeGalleryCell", for: indexPath) as! ChallengeGalleryCell
           let obj_participant:ParticipantModel = self.obj_challengeDetail.array_participant_pager[indexPath.row] as! ParticipantModel
            cell.updateCellwithModal(obj_participantdetail: obj_participant)
            
            cell.rowIndex = indexPath.row
            cell.delegate = self
            return cell
        }
        
        let cell:ChallengeMemberCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChallengeMemberCell", for: indexPath) as! ChallengeMemberCell
        
        var obj_participant:ParticipantModel = ParticipantModel()
        if collectionView == collectionview_completed{
            obj_participant = self.obj_challengeDetail.array_participant_completed[indexPath.row] as! ParticipantModel
        }else if collectionView == collectionview_accepted{
            obj_participant = self.obj_challengeDetail.array_participant_accepted[indexPath.row] as! ParticipantModel
        }else{
            obj_participant = self.obj_challengeDetail.array_participant_requested[indexPath.row] as! ParticipantModel
        }
        cell.obj_collectionview = collectionView
        cell.updateCellwithModal(obj_participantdetail: obj_participant)
        cell.rowIndex = indexPath.row
        cell.delegate = self
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView == collectionview_pager{
            return
        }
        let cell:ChallengeMemberCell = collectionView.cellForItem(at: indexPath) as! ChallengeMemberCell
        if collectionView == collectionview_pager{
            return
        }else if collectionView == collectionview_completed{
            cell.selected()
            if let selected = selectedIndexPathCompleted {
                let cell:ChallengeMemberCell = collectionView.cellForItem(at: selected) as! ChallengeMemberCell
                cell.deselected()
            }
            selectedIndexPathCompleted = indexPath
        }else if collectionView == collectionview_accepted{
            cell.selected()
            if let selected = selectedIndexPathAccepted {
                let cell:ChallengeMemberCell = collectionView.cellForItem(at: selected) as! ChallengeMemberCell
                cell.deselected()
            }
            selectedIndexPathAccepted = indexPath
        }else{
            cell.selected()
            if let selected = selectedIndexPathRequested {
                let cell:ChallengeMemberCell = collectionView.cellForItem(at: selected) as! ChallengeMemberCell
                cell.deselected()
            }
            selectedIndexPathRequested = indexPath
        }
       
        
       // self.delegate?.detail_btn_clicked(cell: self, obj_participantDetail: obj_participant, obj_participantcell: cell, index: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.collectionview_pager{
            let screenwidth:CGFloat = UIScreen.main.bounds.size.width
            return CGSize(width: screenwidth, height: 200)
        }
        
        return CGSize(width: 70, height: 97)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == collectionview_pager{
        self.pageControl.currentPage = indexPath.row
        let obj_participant:ParticipantModel = self.obj_challengeDetail.array_participant_pager[indexPath.row] as! ParticipantModel
            self.lbl_description.text = obj_participant.str_text
        }
    }
}
