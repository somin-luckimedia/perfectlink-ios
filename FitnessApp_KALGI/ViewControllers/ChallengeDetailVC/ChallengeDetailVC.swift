//
//  ChallengeDetailVC.swift
//  FitnessApp
//
//  Created by Ashish on 14/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//




import UIKit
import Alamofire
import AVFoundation
import Photos
import AVKit

class ChallengeDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, ChallengeDetailCellDelegate, ChallengeCompletedDelegate  {
    func challenge_completed(obj_model: ChallengeListModel) {
        self.obj_challengeDetail = obj_model
        self.obj_challengeDetail.str_challenge_status = "2"
        self.str_requestType = 0
        self.tableview_detail.reloadData()
    }
    
    func accept_btn_clicked(cell: ChallengeDetailCell, obj_challenge: ChallengeListModel, btn: UIButton) {
        self.accept_challenge(obj_challengModel: obj_challenge, rowIndex: 0)
    }
    
    func complete_btn_clicked(cell: ChallengeDetailCell, obj_challenge: ChallengeListModel, btn: UIButton) {
        let obj_phonelistvc:MakeChallengeVC = MakeChallengeVC(nibName: "MakeChallengeVC", bundle: nil)
        obj_phonelistvc.is_fromComplete = true
        obj_phonelistvc.obj_challengeModel = obj_challenge
        obj_phonelistvc.delegate = self
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
    
    func share_btn_clicked(cell: ChallengeDetailCell, obj_challenge: ChallengeListModel, btn: UIButton) {
                    let str_message:String = String(format: "%@ created challenge.", obj_challenge.str_name)
                    let name = NSURL(string: obj_challenge.str_file)
                    let objectsToShare = [str_message, name!] as [Any]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    self.present(activityVC, animated: true, completion: nil)
    }
    
    
    
    func popup_clicked(cell: ChallengeDetailCell, obj_participantDetail: ParticipantModel, index: Int) {
        let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
        obj_phonelistvc.str_user_id = obj_participantDetail.str_user_id
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
    
    func play_btn_clicked(cell: ChallengeDetailCell, obj_participantDetail: ParticipantModel, index: Int) {
        let url = URL(string: obj_participantDetail.str_file)
        let player = AVPlayer(url: url!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.present(playerController, animated: true) {
            player.play()
        }
    }
    
    func preview_btn_clicked(cell: ChallengeDetailCell, obj_participantDetail: ParticipantModel, index: Int) {
        
//        let obj_parkeAddressVC:ImageViewerVC = ImageViewerVC(nibName: "ImageViewerVC", bundle: nil)
//        flagChallengeImageviewer = false
//        let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
//        obj_parkeAddressVC.str_url = obj_participantDetail.str_file
//        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    func detail_btn_clicked(cell: ChallengeDetailCell, obj_participantDetail: ParticipantModel, obj_participantcell: ChallengeMemberCell, index: Int) {
        
    }
    
    func image_btn_clicked(cell: ChallengeDetailCell,  obj_participantDetail: ParticipantModel, index:Int){
        
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func accept_challenge(obj_challengModel:ChallengeListModel, rowIndex:Int){
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam: Parameters = ["user_id":str_userid, "challange_id":obj_challengModel.str_challenge_id!]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.accept_challange, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                obj_challengModel.str_challenge_status = "1"
                if let arr = successDict.value(forKey:"response"){
                    let item:NSDictionary = arr as! NSDictionary
                    self.obj_challengeDetail = ChallengeListModel(dictUserInfo: item)
                    self.obj_challengeDetail.str_challenge_status = "1"
                    self.tableview_detail.reloadData()
                }
                
                
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
 
    
    
    var str_requestType:Int = 0
    var logincell: ChallengeDetailCell!
    @IBOutlet weak var tableview_detail: UITableView!
    var obj_challengeDetail:ChallengeListModel! = ChallengeListModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview_detail.delegate  = self as UITableViewDelegate
        tableview_detail.dataSource = self as UITableViewDataSource
        tableview_detail.reloadData()
        self.addLeftMenuButtonWithImage()
        self.title = "CHALLENGE DETAIL"
    }
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // self.navigationController?.navigationBar.isHidden = true
        //   self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "ChallengeDetailCell"
        logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ChallengeDetailCell
        if logincell == nil {
            tableView.register(UINib(nibName: "ChallengeDetailCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ChallengeDetailCell
        }
        logincell.delegate = self
        logincell.str_requestType = self.str_requestType
        logincell.updateCellwithModal(obj_challengeDetail: self.obj_challengeDetail)
        return logincell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 945
    }
    
}
