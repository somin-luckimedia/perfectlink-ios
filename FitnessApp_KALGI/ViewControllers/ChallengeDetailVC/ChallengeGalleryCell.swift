//
//  ChallengeGalleryCell.swift
//  FitnessApp
//
//  Created by Ashish on 15/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import AVKit

protocol ChallengeGalleryCellDelegate: class {
    func play_btn_clicked(cell: ChallengeGalleryCell, obj_participantDetail:ParticipantModel  ,index:Int)
    func preview_btn_clicked(cell: ChallengeGalleryCell, obj_participantDetail:ParticipantModel  ,index:Int)
}

class ChallengeGalleryCell: UICollectionViewCell {
    var rowIndex:Int = 0
    var obj_participantDetail:ParticipantModel! = ParticipantModel()
    weak var delegate:ChallengeGalleryCellDelegate?
    @IBOutlet weak var view_video: UIView!
    @IBOutlet weak var imageView_file: UIImageView!
    @IBOutlet weak var imageview_thumb: UIImageView!
    @IBOutlet weak var imageview_profile: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func updateCellwithModal(obj_participantdetail:ParticipantModel){
        self.obj_participantDetail = obj_participantdetail
        if obj_participantdetail.str_file_type == "VIDEO"{
            self.view_video.isHidden = false
            self.imageView_file.isHidden = true
            let url = URL(string: obj_participantdetail.str_thumb_image)
            imageview_thumb.setImageFrom(url)//(stringImageUrl: obj_participantdetail.str_thumb_image)
        }else{
            self.view_video.isHidden = true
            self.imageView_file.isHidden = false
            self.imageView_file.image = nil
            let url = URL(string: obj_participantdetail.str_file)
            imageView_file.setImageFrom(url)//(stringImageUrl: obj_participantdetail.str_thumb_image)
        }
        
        self.lbl_name.text = obj_participantdetail.str_name
        let url = URL(string: obj_participantdetail.str_profile_picture)
        imageview_profile.setImageFrom(url)
    }
    
    
    @IBAction func play_btn_clicked(_ sender: UIButton) {
        self.delegate?.play_btn_clicked(cell: self, obj_participantDetail: self.obj_participantDetail, index: self.rowIndex)
    }
    
    @IBAction func preview_btn_clicked(_ sender: UIButton) {
        self.delegate?.preview_btn_clicked(cell: self, obj_participantDetail: self.obj_participantDetail, index: self.rowIndex)
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
}
