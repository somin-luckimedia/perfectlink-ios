//
//  ChallengeMemberCell.swift
//  FitnessApp
//
//  Created by Ashish on 14/04/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//




import UIKit

protocol ChallengeMemberCellDelegate: class {
    func popup_btn_clicked(cell: ChallengeMemberCell, obj_participantDetail:ParticipantModel  ,index:Int)
    func image_btn_clicked(cell: ChallengeMemberCell, obj_participantDetail:ParticipantModel  ,index:Int)
}


class ChallengeMemberCell: UICollectionViewCell {
    @IBOutlet weak var imageView_member: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var view_popup: UIView!
    var rowIndex:Int = 0
    var obj_participantDetail:ParticipantModel! = ParticipantModel()
    weak var delegate:ChallengeMemberCellDelegate?
    var obj_collectionview: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    
    func updateCellwithModal(obj_participantdetail:ParticipantModel){
            self.obj_participantDetail = obj_participantdetail
            let url = URL(string: obj_participantdetail.str_profile_picture)

            imageView_member.setImageFrom(url)//(stringImageUrl: obj_participantdetail.str_profile_picture)
            lbl_name.text = obj_participantdetail.str_name
    }
    
    @IBAction func popup_btn_clicked(_ sender: UIButton) {
        self.delegate?.popup_btn_clicked(cell: self, obj_participantDetail: self.obj_participantDetail, index:self.rowIndex)
    }
    
    @IBAction func image_btn_clicked(_ sender: UIButton) {
        self.delegate?.image_btn_clicked(cell: self, obj_participantDetail: self.obj_participantDetail, index:self.rowIndex)
    }
    
}


extension ChallengeMemberCell {
    func selected() {
        //self.backgroundColor = UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
        self.view_popup.isHidden = false
    }
    func deselected() {
        self.view_popup.isHidden = true
        //self.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 0.25)
    }
}
