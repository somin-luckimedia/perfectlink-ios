//
//  SettingVC.swift
//  FitnessApp_KALGI
//
//  Created by Yogesh on 2020-10-07.
//

import UIKit
import Firebase

class SettingVC: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet var tblSettings: UITableView!
    let transiton = SlideInTransition()
    var topView: UIView?
    var arrSettings : NSArray = ["Change Password","Edit Profile","Address Book","About Us","Privacy Policy","Terms of Use","Help","Logout"]
    var arrSettingTrainer : NSArray = ["Change Password","About Us","Privacy Policy","Terms of Use","Help","Rate This App"]
    let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate
    @IBOutlet var btnMenu: UIButton!
    let userDefault = UserDefaults.standard
    let userref = Database.database().reference(withPath:"online")
    @IBOutlet var tblOnline: UITableView!
    var arrOnlineFriends : NSMutableArray = []


    @IBAction func btnCancel(_ sender: UIButton) {
        vwLogout.isHidden = true
    }
    
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        flagSetting = true
        let rootVC : SideMenuController1 = SideMenuController1(nibName: "SideMenuController", bundle: nil)
        if DataUtil.appdelegate().objUserInfo?.str_userType == "2" {
            rootVC.didTapMenuType = { menuType in
                self.transitionToNew(menuType)
            }
        }
        else {
            rootVC.didTapMenuType1 = {menuTypeTrainer in
                self.transitionTrainer(menuTypeTrainer)
            }
        }
        
        rootVC.modalPresentationStyle = .overCurrentContext
        rootVC.transitioningDelegate = self
        flagSetting = true
        present(rootVC, animated: true)
    }

    
    @IBAction func btnYes(_ sender: UIButton) {
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        
        let postParam = [ "user_id":str_userid]

        let headers = [
            "Authorization": String(format: "Bearer %@",  (str_accessToken)),
            "Accept": "application/json"
        ]
            ServerCommunication.getDataWithPostHeader(url: ConstantFiles.logOut_user, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
                print(successDict)
                
            if (successDict.value(forKey: "success") as? String) == "true"{
                
                let onlineRef = self.userref.child(str_userid)
                onlineRef.removeValue { (error, _ ) in
                    if error != nil {
                        print(error?.localizedDescription)
                    }
                }
                UserDefaults.standard.set("", forKey: "LogedInUserID")
                UserDefaults.standard.set("", forKey: "LogedInUserAccessToken")
                UserDefaults.standard.set(false, forKey: "LogedIn")
                UserDefaults.standard.synchronize()
                let splavhVcObj = SignInVC(nibName: "SignInVC", bundle: nil)
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.window!.rootViewController = UINavigationController(rootViewController: splavhVcObj)
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet var vwLogout: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true

        vwLogout.isHidden = true
        tblSettings.register(UINib(nibName: "SettingCVC", bundle: nil), forCellReuseIdentifier: "SettingCVC")
       // if DataUtil.appdelegate().objUserInfo?.str_userType == "2" {
            btnMenu.setImage(UIImage(named: "menu"), for: .normal)
//        }
//        else {
//            btnMenu.setImage(UIImage(named: "Back_icon"), for: .normal)
//
//        }
//        tblSettings.backgroundColor = .clear
        // Do any additional setup after loading the view.
        
        self.userref.observe(.value) { (snapshot) in
            if snapshot.exists() {
                print("-------------",snapshot.children.allObjects)
                //                if snapshot.childrenCount == 1 {
                //                    self.constWidthTbl.constant = 0
                ////                    self.constLeadingColl.constant = 20
                //                    self.lblOnline.isHidden = true
                //                }
                //                else {
                //                    self.constWidthTbl.constant = 60
                ////                    self.constLeadingColl.constant = 0
                //                    self.lblOnline.isHidden = false
                //                }
                let dict = snapshot.value as? [String : AnyObject] ?? [:]
                print("snapshot" , dict.keys)
                self.arrOnlineFriends.removeAllObjects()
                for (key,value) in dict {
                    let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                    if key == str_userid {
                        
                    }
                    else {
                        print("value-----",value)
                        self.arrOnlineFriends.add(value as! NSDictionary)
                    }
                }
                
                self.tblOnline.delegate = self
                self.tblOnline.dataSource = self
                self.tblOnline.reloadData()
            }
        }
        
        let notificationCenter = NotificationCenter.default
               notificationCenter.addObserver(self,
                                              selector: #selector(dismissSetting),
                                              name: NSNotification.Name(rawValue: "DismissSetting"),
                                              object: nil)

    }
    
    @objc func dismissSetting(){
//        flagSetting = false
        self.dismissControllerAnimated()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        objAppDelgate1.sideMenuIndex = 0
        dismissControllerAnimated()


    }
}

extension SettingVC {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblOnline {
            return arrOnlineFriends.count
        }
        else {
            return arrSettings.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblOnline {
            let identifier = "onlineTVCTableViewCell"
            var logincell: onlineTVCTableViewCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell
            
            if logincell == nil {
                tableView.register(UINib(nibName: "onlineTVCTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? onlineTVCTableViewCell)!
            }
            logincell?.selectionStyle = .none
            //        logincell?.layer.cornerRadius = 25
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            let url = URL(string: dict["pic"] as! String)
            logincell?.imgOnlineFriends.setImageFrom(url)
            logincell?.imgOnlineFriends.layer.cornerRadius = 25.0
            
            return logincell!
        }
        else {
            let cell : SettingCVC = tblSettings.dequeueReusableCell(withIdentifier: "SettingCVC") as! SettingCVC
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            cell.backgroundColor = .clear
            cell.imgSetting.image = UIImage(named: arrSettings[indexPath.row] as! String)
            cell.lblTitle.text = arrSettings[indexPath.row] as? String
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblOnline {
            let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
            flagPark = false
            flagGym = false
            flagGallery = false
            let dict = arrOnlineFriends[indexPath.row] as! NSDictionary
            obj_phonelistvc.str_user_id = dict["userId"] as! String
            self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
        }
        else {
            if indexPath.row == 0 {
                let rootVC : ChangePasswordVC = ChangePasswordVC(nibName: "ChangePasswordVC", bundle: nil)
                self.navigationController?.pushViewController(rootVC, animated: true)
            }
            else if indexPath.row == 1 {
                let rootVC : EditProfileVC = EditProfileVC(nibName: "EditProfileVC", bundle: nil)
                self.navigationController?.pushViewController(rootVC, animated: true)
            }
            else if indexPath.row == 2 {
                let rootVC : AddressVC = AddressVC(nibName: "AddressVC", bundle: nil)
                self.navigationController?.pushViewController(rootVC, animated: true)
                
            }
            else if indexPath.row == 3 {
                let rootVC : PrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                rootVC.strTitle = "About Us"
                self.navigationController?.pushViewController(rootVC, animated: true)
                
            }
            else if indexPath.row == 4 {
                let rootVC : PrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                rootVC.strTitle = "Privacy Policy"
                self.navigationController?.pushViewController(rootVC, animated: true)
                
            }
            else if indexPath.row == 5 {
                let rootVC : PrivacyPolicyVC = PrivacyPolicyVC(nibName: "PrivacyPolicyVC", bundle: nil)
                rootVC.strTitle = "Term & Conditions"
                self.navigationController?.pushViewController(rootVC, animated: true)
            }
            else if indexPath.row == 6 {
                let rootVC : HelpVC = HelpVC(nibName: "HelpVC", bundle: nil)
                self.navigationController?.pushViewController(rootVC, animated: true)
            }
            
            else if indexPath.row == 7 {
                vwLogout.isHidden = false
            }
        }
    }
    func transitionToNew(_ menuType: MenuType) {
        switch menuType {
        case .FitnessWall:
            let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Challenges:
            let rootVC : OrderHistoryVC = OrderHistoryVC(nibName: "OrderHistoryVC", bundle: nil)
            self.present(rootVC, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.dismissControllerAnimated()
                
            }
        case .Chat:
            let rootVC : FriendListVC = FriendListVC(nibName: "FriendListVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.dismissControllerAnimated()
            }
//            self.present(rootVC, animated: true, completion: nil)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                self.dismissControllerAnimated()
//            }
        default:
            break
        }
    }
    
    func transitionTrainer(_ menuType : MenuTypeTrainer) {
        switch menuType {
        case .FitnessWall:
            let rootVC : WallPostListingVC = WallPostListingVC(nibName: "WallPostListingVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
            
        case .MyOrders:
            let rootVC : OrderHistoryVC = OrderHistoryVC(nibName: "OrderHistoryVC", bundle: nil)
            self.present(rootVC, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Friends:
            let rootVC : FriendListVC = FriendListVC(nibName: "FriendListVC", bundle: nil)
            //            self.present(rootVC, animated: true, completion: nil)
            //            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            //                self.dismissControllerAnimated()
            //            }
            // self.present(rootVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .Achievements:
            let rootVC : AchievementsVC = AchievementsVC(nibName: "AchievementsVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        case .MyTips:
            let rootVC : FreeTipsVCViewController = FreeTipsVCViewController(nibName: "FreeTipsVCViewController", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
        
        case .Wallet:
            let rootVC : WalletNewVC = WalletNewVC(nibName: "WalletNewVC", bundle: nil)
            self.navigationController?.pushViewController(rootVC, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                flagMenu = false
                self.dismissControllerAnimated()
            }
            
        default:
            break
        }
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SettingVC: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = true
        return transiton
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transiton.isPresenting = false
        return transiton
    }
}

