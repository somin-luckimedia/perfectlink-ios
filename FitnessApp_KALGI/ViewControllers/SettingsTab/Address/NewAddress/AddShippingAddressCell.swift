//
//  AddShippingAddressCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 25/01/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//


import UIKit
//countrycode_btn_clicked
protocol AddShippingAddressCellDelegate:class {
    func signup_btn_clicked(cell: AddShippingAddressCell, btn: UIButton)
    func countrycode_btn_clicked(cell: AddShippingAddressCell, btn: UIButton)
    func check_box_clicked(cell: AddShippingAddressCell, btn: UIButton)
}

class AddShippingAddressCell: UITableViewCell {
    
    weak var delegate: AddShippingAddressCellDelegate?
    var cellIndex:NSInteger = 0
    @IBOutlet weak var textfield_name: UITextField!
    @IBOutlet weak var textfield_countrycode: UITextField!
    @IBOutlet weak var textfield_mobile: UITextField!
    @IBOutlet weak var textfield_address1: UITextField!
    @IBOutlet weak var textfield_address2: UITextField!
    @IBOutlet weak var textfield_landmark: UITextField!
    @IBOutlet weak var textfield_city: UITextField!
    @IBOutlet weak var textfield_state: UITextField!
    @IBOutlet weak var textfield_postalcode: UITextField!
    

    @IBOutlet var btnAddEdit: UIButton!
    @IBOutlet var btn_check_box: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureWithOwner(controller:NewAddressVC){
        self.textfield_name.delegate = controller
        self.textfield_countrycode.delegate = controller
        self.textfield_mobile.delegate = controller
        self.textfield_address1.delegate = controller
        self.textfield_address2.delegate = controller
        self.textfield_landmark.delegate = controller
        self.textfield_city.delegate = controller
        self.textfield_state.delegate = controller
        self.textfield_postalcode.delegate = controller
    }
    
    
    func updateFields(obj_address:ShippingAddressModel) {
        self.textfield_name.text = obj_address.str_recipient_name
        self.textfield_countrycode.text = obj_address.str_country_code
        self.textfield_mobile.text = obj_address.str_phone_number
        self.textfield_address1.text = obj_address.str_line1
        self.textfield_address2.text = obj_address.str_line2
        self.textfield_landmark.text = obj_address.str_land_mark
        self.textfield_city.text = obj_address.str_city
        self.textfield_state.text = obj_address.str_state
        self.textfield_postalcode.text = obj_address.str_postal_code
    }
    
    
    @IBAction func country_btn_clicked(_ sender: UIButton) {
        self.delegate?.countrycode_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func signup_btn_clicked(_ sender: UIButton) {
        self.delegate?.signup_btn_clicked(cell: self, btn: sender)
    }
    
    @IBAction func check_box_tapped(_ sender: UIButton) {
        self.delegate?.check_box_clicked(cell: self, btn: sender)
    }
    
  
    
}
