//
//  NewAddressVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 12/10/20.
//

import UIKit

class NewAddressVC: UIViewController,AddShippingAddressCellDelegate, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate,IQActionSheetPickerViewDelegate{

    

    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var tblNewAddress: UITableView!
    var signupcell: AddShippingAddressCell!
    var dictSideMenu:NSArray = []
    var strCountryCode:String = "+91"
    var strDefault_shipping = 0
    var array_Booking : NSMutableArray = []
    var inedexRow : Int = 0
    var str_Entry = "",str_id = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        tblNewAddress.delegate  = self as UITableViewDelegate
        tblNewAddress.dataSource = self as UITableViewDataSource
        lblAddress.text = str_Entry
        
        tblNewAddress.reloadData()
//        self.tabBarController?.tabBar.isHidden = true

        dictSideMenu = DataUtil.readCountryJson(fileName: "CountryCodes") as NSArray
        
 
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 650
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "AddShippingAddressCell"
        signupcell = tableView.dequeueReusableCell(withIdentifier: identifier) as? AddShippingAddressCell
        if signupcell == nil {
            tableView.register(UINib(nibName: "AddShippingAddressCell", bundle: nil), forCellReuseIdentifier: identifier)
            signupcell = tableView.dequeueReusableCell(withIdentifier: identifier) as? AddShippingAddressCell
        }
        
        
        signupcell.selectionStyle = .none
        signupcell.cellIndex = indexPath.row
        //signupcell.updateFields(info: DataUtil.appdelegate().objUserInfo!)
        signupcell.configureWithOwner(controller: self)
        signupcell.delegate = self
        
        if str_Entry == "Edit Address" {
            let dict = array_Booking[inedexRow] as! ShippingAddressModel
            signupcell.textfield_name.text = dict.str_recipient_name
            signupcell.textfield_countrycode.text = dict.str_country_code
            signupcell.textfield_mobile.text = dict.str_phone_number
            signupcell.textfield_address1.text = dict.str_line1
            signupcell.textfield_address2.text = dict.str_line2
            signupcell.textfield_landmark.text = dict.str_land_mark
            signupcell.textfield_city.text = dict.str_city
            signupcell.textfield_state.text = dict.str_state
            signupcell.textfield_postalcode.text = dict.str_postal_code
            signupcell.btnAddEdit.setTitle("Update", for: .normal)
            str_id = dict.str_id
            if dict.str_default_shipping1 == 1 {
                signupcell.btn_check_box.isSelected = true
                strDefault_shipping = 1
            }
            else {
                signupcell.btn_check_box.isSelected = false
                strDefault_shipping = 0
            }
        }
        //  cell.textLabel?.text = monthName
        return signupcell
    }
    
    func check_box_clicked(cell: AddShippingAddressCell, btn: UIButton) {
        
        if btn.isSelected {
            btn.isSelected = false
            strDefault_shipping = 0
        }else{
            btn.isSelected = true
            strDefault_shipping = 1

        }
    }

    func signup_btn_clicked(cell: AddShippingAddressCell, btn: UIButton) {
        let strUserName  : String = (signupcell.textfield_name.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strMobile  : String = (signupcell.textfield_mobile.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strAddress1  : String = (signupcell.textfield_address1.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strAddress2  : String = (signupcell.textfield_address2.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strLandMark  : String = (signupcell.textfield_landmark.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strCity  : String = (signupcell.textfield_city.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strState  : String = (signupcell.textfield_state.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strPostalCode  : String = (signupcell.textfield_postalcode.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        let str_userId:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""

        var postParam = ["recipient_name":strUserName, "user_id":str_userId, "country_code":strCountryCode, "phone_number":strMobile, "line1":strAddress1,"line2":strAddress2,"land_mark":strLandMark, "city":strCity, "state":strState, "postal_code":strPostalCode,"default_shipping" : strDefault_shipping] as [String : Any]
        print("Login Post Parameter : \(postParam)")
        
        if !signupValidation() {
            return
        }
        
        var url : String = ""
        if str_Entry == "Edit Address" {
            url = ConstantFiles.update_shipping_address
            postParam["id"] = str_id
        }
        else {
            url = ConstantFiles.add_shipping_address
        }
        ServerCommunication.getDataWithPostHeader(url: url, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
//                self.delegate?.Addressadded_clicked()
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
            }else{
                showAlertWithTitleFromVC(vc: self, andMessage: (successDict.value(forKey: "message") as? String)!)
//                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
            
            
            // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                showAlertWithTitleFromVC(vc: self, andMessage: (dictFailure.value(forKey: "message") as? String)!)

//                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
        
        
    }
    
    
    
    
    func signupValidation() -> Bool {
        
        if (signupcell.textfield_name.text?.isEmpty)!{
            DataUtil.alertMessage("Name is Empty", viewController: self)
            return false
        }
        
      
        
        if (signupcell.textfield_mobile.text?.isEmpty)!{
            DataUtil.alertMessage("Phone is Empty", viewController: self)
            return false
        }
        
        let isValidPhone = ValidateUtils.isValidPhoneNumber(signupcell.textfield_mobile.text!)
        
        if isValidPhone == false {
            DataUtil.alertMessage("Please provide the valid Phone.", viewController: self)
            return false
        }
        
        
        
        if (signupcell.textfield_countrycode.text?.isEmpty)!{
            DataUtil.alertMessage("Countrycode is Empty", viewController: self)
            return false
        }
        
        if (signupcell.textfield_address1.text?.isEmpty)!{
            DataUtil.alertMessage("Address Line 1 is Empty", viewController: self)
            return false
        }
        
        if (signupcell.textfield_address2.text?.isEmpty)!{
            DataUtil.alertMessage("Address Line 2 is Empty", viewController: self)
            return false
        }
        
        if (signupcell.textfield_landmark.text?.isEmpty)!{
            DataUtil.alertMessage("Landmark is Empty", viewController: self)
            return false
        }
        
        if (signupcell.textfield_city.text?.isEmpty)!{
            DataUtil.alertMessage("City is Empty", viewController: self)
            return false
        }
        
        if (signupcell.textfield_state.text?.isEmpty)!{
            DataUtil.alertMessage("State is Empty", viewController: self)
            return false
        }
        if (signupcell.textfield_postalcode.text?.isEmpty)!{
            DataUtil.alertMessage("Postalcode is Empty", viewController: self)
            return false
        }
        
        
        
        return true
    }
    
    func countrycode_btn_clicked(cell:AddShippingAddressCell, btn:UIButton){
        let array: NSArray = (dictSideMenu.value(forKey: "dial_code") as! NSArray)
        let array1: NSArray = (dictSideMenu.value(forKey: "name") as! NSArray)
        var modifiedarray = [String]()
        //let modifiedarray:NSArray = []
        for dict in dictSideMenu {
            
            let name:String = (dict as AnyObject).value(forKey: "name") as! String
            let dialcode:String = (dict as AnyObject).value(forKey: "dial_code") as! String
            
            let finalstring = String(format:"(%@) %@", name, dialcode)
            modifiedarray.append(finalstring)
            
        }
        
        
        
        // var array1:Array = [String]
        
        var groups = [[String]]()
        if let swiftArray = modifiedarray as NSArray as? [String] {
            groups.append(swiftArray)
        }
        
        // print(array)
        
        // groups.append(array as! [String])
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()
        
    }
    
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView, didSelectTitlesAtIndexes indexes: [NSNumber]) {
            let index = indexes[0] as! NSInteger
            let array: NSArray = (dictSideMenu.value(forKey: "dial_code") as! NSArray)
            let title:String = array[index] as! String
            strCountryCode = title
            signupcell?.textfield_countrycode?.text = strCountryCode
            
    }


}
