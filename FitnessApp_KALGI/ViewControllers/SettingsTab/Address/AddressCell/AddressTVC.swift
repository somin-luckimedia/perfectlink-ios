//
//  AddressTVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 12/10/20.
//

import UIKit

class AddressTVC: UITableViewCell {

    @IBOutlet var lblName: UILabel!
    
    @IBOutlet var imgDefault_Shipping: UIImageView!
    @IBOutlet var lblPhoneNumber: UILabel!
    @IBOutlet var lblAddress: UILabel!

    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var btnDefaultShipping: UIButton!
    
    @IBOutlet var constHeight: NSLayoutConstraint!
    @IBOutlet var constBottom: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
