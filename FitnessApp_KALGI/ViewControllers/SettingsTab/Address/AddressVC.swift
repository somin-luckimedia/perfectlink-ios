//
//  AddressVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 12/10/20.
//

import UIKit

class AddressVC: UIViewController {

    @IBOutlet var tblAddressBook: UITableView!
    

    fileprivate var array_address:NSMutableArray = NSMutableArray()
    var obj_protienDetailModel:ProtienDetailModel! = ProtienDetailModel()
    var isfromProtien:Bool = false
    var productCount:Int = 1
    var totalCost:Double = 0.00
    override func viewDidLoad() {
        super.viewDidLoad()
        tblAddressBook.delegate = self
        tblAddressBook.dataSource = self
        tblAddressBook.rowHeight = 80
        tblAddressBook.estimatedRowHeight = UITableView.automaticDimension

        tblAddressBook.register(UINib(nibName: "AddressTVC", bundle: nil), forCellReuseIdentifier: "AddressTVC")
        

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.tabBarController?.tabBar.isHidden = true
        getMyBookings()
        
    }
}


//MARK: TABLEVIEW DELEGATE & DATASOURCE METHODS
extension AddressVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_address.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : AddressTVC = tblAddressBook.dequeueReusableCell(withIdentifier: "AddressTVC") as! AddressTVC
        cell.selectionStyle = .none
        let dict = array_address[indexPath.row] as! ShippingAddressModel
        cell.lblName.text = dict.str_recipient_name
        cell.lblPhoneNumber.text = "\(dict.str_country_code!) \(dict.str_phone_number!)"
        cell.lblAddress.text = "\(dict.str_line1!) \(dict.str_line2!) \(dict.str_city!) \(dict.str_state!) \(dict.str_postal_code!)"
        if dict.str_default_shipping1 == 1 {
            cell.imgDefault_Shipping.isHidden = false
            cell.btnDefaultShipping.isHidden = false
            cell.constHeight.constant = 26
            cell.constBottom.constant = 20
        }
        else  {
            cell.imgDefault_Shipping.isHidden = true
            cell.btnDefaultShipping.isHidden = true
            cell.constHeight.constant = 0
            cell.constBottom.constant = 0

        }
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(didTapEditButton), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
        let rootVC : NewAddressVC = NewAddressVC(nibName: "NewAddressVC", bundle: nil)
        rootVC.str_Entry = "Edit Address"
        rootVC.array_Booking = array_address
        rootVC.inedexRow = indexPath.row
        rootVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(rootVC, animated: true)

        
    }

}

//MARK: BUTTON ACTION METHODS
extension AddressVC {
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddNewAddress(_ sender: Any) {
        let rootVC : NewAddressVC = NewAddressVC(nibName: "NewAddressVC", bundle: nil)
        rootVC.str_Entry = "New Address"

        self.navigationController?.pushViewController(rootVC, animated: true)
        
    }
    @objc func didTapEditButton(sender:UIButton) {
        print(sender.tag)
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        
        let headers = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        let str_userId:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let dict = array_address[sender.tag] as! ShippingAddressModel
        
        let postParam = ["user_id":str_userId, "id" : dict.str_id!]

        print("Login Post Parameter : \(postParam)")
        self.array_address.removeAllObjects()
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.remove_shipping_address, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:ShippingAddressModel = ShippingAddressModel(dictUserInfo: dict)
                        self.array_address.add(obj_bookingmodal)
                    }
                    self.tblAddressBook.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
}

//MARK : WEBSERVICE CALL METHODS
extension AddressVC {
    func getMyBookings(){
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers = [
            "Authorization": String(format: "Bearer %@",  str_accessToken),
            "Accept": "application/json"
        ]
        let str_userId:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let postParam = ["user_id":str_userId]
        print("Login Post Parameter : \(postParam)")
        self.array_address.removeAllObjects()
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.get_shipping_address, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:ShippingAddressModel = ShippingAddressModel(dictUserInfo: dict)
                        self.array_address.add(obj_bookingmodal)
                    }
                    self.tblAddressBook.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
}
