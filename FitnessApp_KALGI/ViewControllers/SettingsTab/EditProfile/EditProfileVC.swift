//
//  EditProfileVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 12/10/20.
//

import UIKit

class EditProfileVC: UIViewController,UITextFieldDelegate,IQActionSheetPickerViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,EditCellDelegate {
    
    func signup_btn_clicked(cell: EditCell, btn: UIButton) {
        self.EditCall()
    }

    func countrycode_btn_clicked(cell: EditCell, btn: UIButton) {
        let array: NSArray = (dictSideMenu.value(forKey: "dial_code") as! NSArray)
        let array1: NSArray = (dictSideMenu.value(forKey: "name") as! NSArray)
        var modifiedarray = [String]()
        //let modifiedarray:NSArray = []
        for dict in dictSideMenu {
            
            let name:String = (dict as AnyObject).value(forKey: "name") as! String
            let dialcode:String = (dict as AnyObject).value(forKey: "dial_code") as! String
            
            let finalstring = String(format:"(%@) %@", name, dialcode)
            modifiedarray.append(finalstring)
            
        }
        // var array1:Array = [String]
        
        var groups = [[String]]()
        if let swiftArray = modifiedarray as NSArray as? [String] {
            groups.append(swiftArray)
        }
        
        // print(array)
        
        // groups.append(array as! [String])
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()
    }
    
    func opneGallery(){
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openCamera(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }

    func camera_btn_clicked(cell: EditCell, btn: UIButton) {
        let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert) in
            self.openCamera()
            
        }))
        
        alert.addAction(UIAlertAction(title:"Photo Library", style: .default, handler: { (alert) in
            self.opneGallery()
        }))
        
        alert.addAction(UIAlertAction(title:"Cancel", style: .default, handler: { (alert) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView, didSelectTitlesAtIndexes indexes: [NSNumber]) {
        
        if pickerView.tag == 101{
            let index = indexes[0] as! NSInteger
            let array: NSArray = (dictSideMenu.value(forKey: "dial_code") as! NSArray)
            let title:String = array[index] as! String
            strCountryCode = title
            editcell?.textfield_countrycode?.text = strCountryCode
            
        }else if pickerView.tag == 102{
            let index = indexes[0] as! NSInteger
            let title:String = array_geneder[index]
            str_gender = title
            editcell?.textfield_gender?.text = str_gender
        }else if pickerView.tag == 104{
            let index = indexes[0] as! NSInteger
            let title:String = DataUtil.appdelegate().array_height[index]
            str_height = DataUtil.appdelegate().array_height_final[index]
            editcell?.textfield_height?.text = title
        }else if pickerView.tag == 105{
            let index = indexes[0] as! NSInteger
            let title:String = DataUtil.appdelegate().array_weight[index]
            str_weight = title
            editcell?.textfield_weight?.text = String(format: "%@ lbs", title)
        }else if pickerView.tag == 110{
            let index = indexes[0] as! NSInteger
            let title:String = DataUtil.appdelegate().array_category[index]
            str_category = title
            editcell?.textfield_about?.text = title
        }
        
    }
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView, didSelect date: Date) {
        
        //DataUtil.stringFromDate(date: date_today, DateFormate: "yyyy-MM-dd")
        str_dob = DataUtil.stringFromDate(date: date, DateFormate: "yyyy-MM-dd")
        editcell?.textfield_dob.text = str_dob
        // self.lbl_time.text = str_time
        
        
    }

//    func check_box_clicked(cell: EditCell, btn: UIButton) {
//        <#code#>
//    }

    func signin_btn_clicked(cell: EditCell, btn: UIButton) {
    
    }

    func gender_btn_clicked(cell: EditCell, btn: UIButton) {
        var groups = [[String]]()
        groups.append(array_geneder)
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()
    }

    func dob_btn_clicked(cell: EditCell, btn: UIButton) {
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.actionSheetPickerStyle = .datePicker
        picker.delegate = self
        picker.tag = 103
        let date:Date =  Date()
        //  let calendar = Calendar.current
        // let date_today:Date = calendar.date(byAdding: .day, value: 7, to: date)!
        picker.maximumDate = date
        picker.show()
    }

    func height_btn_clicked(cell: EditCell, btn: UIButton) {
        var groups = [[String]]()
        groups.append(DataUtil.appdelegate().array_height)
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()
    }

    func weight_btn_clicked(cell: EditCell, btn: UIButton) {
        var groups = [[String]]()
        groups.append(DataUtil.appdelegate().array_weight)
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = btn.tag;
        picker.show()

    }

    func trainer_btn_clicked(cell: EditCell, btn: UIButton) {
        var groups = [[String]]()
        groups.append(DataUtil.appdelegate().array_category)
        let picker:IQActionSheetPickerView = IQActionSheetPickerView.init(title: "", delegate: self)
        picker.titlesForComponents =  groups;
        picker.tag = 110
        picker.show()
    }

    func terms_btn_clicked(type: String) {
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //var selectedImage: UIImage?
        if let editedImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")]   as? UIImage {
            selectedImage = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerOriginalImage")] as? UIImage {
            selectedImage = originalImage
        }
        if let selectedImage = selectedImage {
            editcell.imageview_profile.image = selectedImage;
            editcell.imageview_profile.layer.cornerRadius = editcell.imageview_profile.frame.size.height/2
            editcell.imageview_profile.layer.masksToBounds = true
        }
        
        /*if let selectedImages = selectedImage {
         
         
         if let data = UIImageJPEGRepresentation(selectedImages,1) {
         let parameters: Parameters = [
         "access_token" : "YourToken"
         ]
         // You can change your image name here, i use NSURL image and convert into string
         let imageURL:NSURL = info[UIImagePickerControllerReferenceURL] as! NSURL
         let fileName = imageURL.absoluteString
         // Start Alamofire
         
         }
         
         
         
         }*/
        
        
        
        
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    

    
    
    var dictSideMenu:NSArray = []
    var editcell: EditCell!
    var strCountryCode:String = "+91"
    var str_gender:String = ""
    var str_height:String = ""
    var str_weight:String = ""
    var str_dob:String = ""
    var strUserType: String = "2"
    var strIdtype:String = "1"
    var selectedImage:UIImage?
    let array_geneder:[String] = ["Male", "Female"]
    var str_category:String = ""
    var str_provider:String = ""
    var dict_facebook:Dictionary<String,Any>? = nil
    var social_id : String = ""
 

    @IBOutlet var tblEditProfile: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.getProfile()
        tblEditProfile.delegate = self
        tblEditProfile.dataSource = self
        dictSideMenu = DataUtil.readCountryJson(fileName: "CountryCodes") as NSArray
//        self.tabBarController?.tabBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    

}

extension EditProfileVC {
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: TABLEVIEW DELEGATE & DATASOURCE METHODS
extension EditProfileVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "EditCell"
        editcell = tableView.dequeueReusableCell(withIdentifier: identifier) as? EditCell
        
        if editcell == nil {
            tableView.register(UINib(nibName: "EditCell", bundle: nil), forCellReuseIdentifier: identifier)
            editcell = tableView.dequeueReusableCell(withIdentifier: identifier) as? EditCell
            editcell.selectionStyle = UITableViewCell.SelectionStyle.none
            
        }
        editcell.cellIndex = indexPath.row
        editcell.configureWithOwner(controller: self)
        editcell.delegate = self
        let url = URL(string: DataUtil.appdelegate().objUserInfo!.profilePic)
        editcell.imageview_profile.setImageFrom(url)
        editcell.textfield_firstname.text = DataUtil.appdelegate().objUserInfo?.str_first_name
        editcell.textfield_lastname.text = DataUtil.appdelegate().objUserInfo?.str_last_name
        editcell.textfield_username.text = DataUtil.appdelegate().objUserInfo?.str_user_name
        editcell.textfield_email.text = DataUtil.appdelegate().objUserInfo?.email
        editcell.textfield_countrycode.text = DataUtil.appdelegate().objUserInfo?.str_countryCode
        editcell.textfield_mobile.text = DataUtil.appdelegate().objUserInfo?.phone
        editcell.textfield_gender.text = DataUtil.appdelegate().objUserInfo?.gender
        editcell.textfield_dob.text = DataUtil.appdelegate().objUserInfo?.str_dob
        editcell.textfield_height.text = DataUtil.appdelegate().objUserInfo?.str_height
        editcell.textfield_weight.text = DataUtil.appdelegate().objUserInfo?.str_weight
        editcell.txtAbout.text = DataUtil.appdelegate().objUserInfo?.str_about
        var category:Int = Int((DataUtil.appdelegate().objUserInfo?.str_trainer_category)!)!
        if DataUtil.appdelegate().objUserInfo?.str_userType == "3" {
            if category == 0 {
                category = 1
            }
        editcell.textfield_about.text = DataUtil.appdelegate().array_category[category-1]
        }
        
        if DataUtil.appdelegate().objUserInfo?.str_userType == "2"{

            editcell.view_about.isHidden = true
            editcell.view_height.isHidden = false
            editcell.view_weight.isHidden = false

        }else{

            editcell.view_about.isHidden = false
            editcell.view_height.isHidden = true
            editcell.view_weight.isHidden = true
//            signupcell.constraint_height.constant = 80
        }
        
        //  cell.textLabel?.text = monthName
        return editcell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 680
    }
}

//MARK: WEBSERVICE CALL
extension EditProfileVC {
    func getProfile() {
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        
        let postParam = [ "user_id":str_userid]
        
        let headers = [
            "Authorization": String(format: "Bearer %@",  (str_accessToken)),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.get_friend_info, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true" {
                DataUtil.appdelegate().objUserInfo = UserInfo(dictUserInfo: successDict["response"] as! NSDictionary)
                let responseDict:NSDictionary = successDict["response"] as! NSDictionary
                print(responseDict)
                self.tblEditProfile.delegate = self
                self.tblEditProfile.dataSource = self
                self.tblEditProfile.reloadData()
                UserDefaults.standard.synchronize()
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
    }
    func EditCall() {
        let str_name  : String = (editcell.textfield_name.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        
        let objAppDelgate1 =  UIApplication.shared.delegate as! AppDelegate
        let strUserName  : String = (editcell.textfield_username.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strFirstName  : String = (editcell.textfield_firstname.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strLastName  : String = (editcell.textfield_lastname.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        
        let strEmail  : String = (editcell.textfield_email.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let strMobile  : String = (editcell.textfield_mobile.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        //  let strPassword  : String = (editcell.textfield_password.text)!
        //let strDeviceToken:String = (objAppDelgate1.str_devicetoken)!
        let strDeviceType:String = "IOS"
        let strAboutType : String = (editcell.txtAbout.text)!
        let strLat:String = (objAppDelgate1.str_lattitude)
        let strLong:String = (objAppDelgate1.str_longitude)
        str_gender = editcell.textfield_gender.text!
        str_dob = editcell.textfield_dob.text!
        str_height = editcell.textfield_height.text!
        str_weight = editcell.textfield_weight.text!
        var str_trainer_category : String = ""
        if DataUtil.appdelegate().array_category.firstIndex(of: str_category) == nil {
            str_trainer_category = DataUtil.appdelegate().objUserInfo!.str_trainer_category
        }
        else {
            str_trainer_category = "\(DataUtil.appdelegate().array_category.firstIndex(of: str_category)! + 1)"
        }
        //editcell.textfield_about.text ?? ""
        
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        
        let headers = [
            "Authorization": String(format: "Bearer %@",  (str_accessToken)),
            "Accept": "application/json"
        ]
        //        let headers : [String : String] = ["Accept": "application/json"]
        let postParam = ["name":str_name,"email":strEmail, "country_code":strCountryCode,"firstname" : strFirstName,"username" : strUserName ,"lastname":strLastName, "phone_number":strMobile, "device_type":strDeviceType,"lat":strLat, "lon":strLong, "gender":str_gender, "dob":str_dob, "height":str_height, "weight":str_weight,"address":"", "user_id":(DataUtil.appdelegate().objUserInfo?.userId)!,"about": strAboutType,"trainer_category" : str_trainer_category,"profile_picture" : ""]
        
        var isImage:Bool = true
        if selectedImage == nil{
            isImage = false
        }
        
        // DataUtil.alertMessage("You have been registered succesfully", viewController: self)
        
        ServerCommunication.getDataWithPostImage(url: ConstantFiles.update_profile, image: selectedImage as Any,parameter: postParam, viewController: self, isImage:isImage,success: { (successDict) in
            
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.appdelegate().objUserInfo = UserInfo(dictUserInfo: successDict["response"] as! NSDictionary)
                //                DataUtil.appdelegate().objUserInfo?.str_accessToken = successDict["access_token"] as! String
                let responseDict:NSDictionary = successDict["response"] as! NSDictionary
                print(responseDict)
                
                showAlertWithTitleFromVC(vc: self, title: "", andMessage: (successDict.value(forKey: "message") as? String)!, buttons: ["OK"]) { (index) in
                    if index == 0 {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "errors") as? String)!, viewController: self)
            }
            
            
            // objAppDelgate.loadSidePanel(objVC: self.navigationController!)
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
    }
}
