//
//  PrivacyPolicyVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 10/10/20.
//

import UIKit
import WebKit

class PrivacyPolicyVC: UIViewController {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var webPrivacyPolicy: WKWebView!
    var strTitle : String = ""
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        var url : URL!
        if strTitle == "Privacy Policy" {
            lblTitle.text = "Privacy Policy"
        url = URL(string: "http://admin.perfectlinkfitness.com/api/privacy_policy")
        }
        else if strTitle == "About Us" {
            lblTitle.text = "About Us"
            url = URL(string: "http://admin.perfectlinkfitness.com/api/about_us")

        }
        else if strTitle == "Term & Conditions" {
            lblTitle.text = "Term & Conditions"
            url = URL(string: "http://admin.perfectlinkfitness.com/api/terms_of_use_user")
        }

        do {
            let contents = try String(contentsOf: url!)
            print(contents)
            let font = UIFont.appFont_TitilliumWebRegular(fontSize: 50.0)
            self.webPrivacyPolicy.loadHTMLString("<span style=\"font-family: \(font.fontName); font-size: \(font.pointSize); color: #ffffff\">\(contents)</span>", baseURL: nil)
            webPrivacyPolicy.isOpaque = false
            webPrivacyPolicy.backgroundColor = .clear
            webPrivacyPolicy.scrollView.backgroundColor = .clear
            webPrivacyPolicy.layer.backgroundColor = UIColor.clear.cgColor
//            let webContent = "<html><body><p><font size=25>" + contents + "</font></p></body></html>"
//            vwWebView.loadHTMLString(webContent, baseURL: nil)
        } catch {
            // contents could not be loaded
        }

        // Do any additional setup after loading the view.
    }
    



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
