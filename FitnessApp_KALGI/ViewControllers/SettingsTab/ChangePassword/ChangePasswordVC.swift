//
//  ChangePasswordVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 10/10/20.
//

import UIKit

class ChangePasswordVC: UIViewController {
    @IBOutlet weak var textfield_currentpassword: UITextField!
    @IBOutlet weak var textfield_newpassword: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


}

extension ChangePasswordVC {
    
    
    @IBAction func btn_back_tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func submit_btn_clicked(_ sender: Any) {
        print("delegate called")
        
        let str_currentpassword  : String = (textfield_currentpassword.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let str_newpassword  : String = (textfield_newpassword.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        
//        let str_access_token:String = (DataUtil.appdelegate().objUserInfo?.str_accessToken)!
        let str_email:String = UserDefaults.standard.value(forKey: "Email") as? String ?? ""
      //  let str_token:String = (DataUtil.appdelegate().objUserInfo?.devicetoken)!
       // let email =
        
        let postParam = [ "current_password":str_currentpassword,"new_password" : str_newpassword,"email":str_email]

        if !changepasswordValidation() {
            return
        }
        let headers = [
            "Authorization": String(format: "Bearer %@",  (str_accessToken)),
            "Accept": "application/json"
        ]
            ServerCommunication.getDataWithPostHeader(url: ConstantFiles.change_password, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
                print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.textfield_currentpassword.text = ""
                self.textfield_newpassword.text = ""
                showAlertWithTitleFromVC(vc: self, title: "", andMessage: (successDict.value(forKey: "message") as? String)!, buttons: ["OK"]) { (index) in
                    if index == 0 {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
//                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
        
        
        
        
    }
    func changepasswordValidation() -> Bool {
        if (textfield_currentpassword.text?.isEmpty)!{
//            DataUtil.alertMessage("Current password field is Empty", viewController: self)
            showAlertWithTitleFromVC(vc: self, andMessage: "Current password field is Empty")
            return false
        }
        if (textfield_newpassword.text?.isEmpty)!{
//            DataUtil.alertMessage("New password field is Empty", viewController: self)
            showAlertWithTitleFromVC(vc: self, andMessage: "New password field is Empty")
            return false
        }
//        if (textfield_confirmpassword.text?.isEmpty)!{
//            DataUtil.alertMessage("Confirm password field is Empty", viewController: self)
//            return false
//        }
//        if (textfield_confirmpassword.text != textfield_newpassword.text){
//            DataUtil.alertMessage("New password and confirm password are not same", viewController: self)
//            return false
//        }
        return true
    }
}
