//
//  HelpVC.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 11/10/20.
//

import UIKit

class HelpVC: UIViewController {

    @IBOutlet weak var textview_message: UITextView!
    @IBOutlet weak var textfield_subject: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

        textview_message.placeholder = "Message…"

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_back_clicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submit_btn_clicked(_ sender: Any) {
        print("delegate called")
        
        let str_subject  : String = (textfield_subject.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let str_message  : String = (textview_message.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        let str_userId:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let postParam = ["subject":str_subject, "message":str_message,"user_id":str_userId]
        
        if !changepasswordValidation() {
            return
        }
        
        let headers = [
            "Authorization": String(format: "Bearer %@",  (str_accessToken)),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.helpRequest, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
                print(successDict)
                if (successDict.value(forKey: "STATUS") as? String) == "true"{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
             }
            }) { (dictFailure) in
                if (dictFailure.value(forKey: "message") as? String) != nil {
                    DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
                }
        }
    }
    
    func changepasswordValidation() -> Bool {
        if (textfield_subject.text?.isEmpty)!{
            DataUtil.alertMessage("Subject field is Empty", viewController: self)
            return false
        }
        if (textview_message.text?.isEmpty)!{
            DataUtil.alertMessage("Message field is Empty", viewController: self)
            return false
        }
      
        return true
    }


}
