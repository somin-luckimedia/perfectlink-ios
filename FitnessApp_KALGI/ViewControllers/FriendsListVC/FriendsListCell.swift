//
//  FriendsListCell.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 23/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//




import UIKit

class FriendsListCell: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var imageView_thumb: UIImageView!
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_bookingmodal:FriendsListModel){
        lbl_title.text = String(format: "%@", obj_bookingmodal.str_name!)
        lbl_description.text = String(format: "%@", obj_bookingmodal.str_about!)
        imageView_thumb.sd_setImage(with: URL(string: (obj_bookingmodal.str_profile_picture)), placeholderImage: UIImage(named: "appLogo"))
        lbl_price.text = String(format: "%@", obj_bookingmodal.str_email)
    }
    
    
}

