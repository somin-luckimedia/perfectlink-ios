//
//  FriendsListVC.swift
//  FitnessApp
//
//  Created by Ashish Mishra on 23/02/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//




import UIKit
import Alamofire
import Foundation
import Contacts
import ContactsUI

class FriendsListVC: UIViewController , UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate, SwiftMultiSelectDelegate,SwiftMultiSelectDataSource, FriendListCellDelegate  {
    func reload_list() {
        self.getMyBookings()
    }
    
    
    var items:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    var initialValues:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    var selectedItems:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    
    fileprivate var array_mybookings:NSMutableArray = NSMutableArray()
    @IBOutlet weak var imageView_BG: UIImageView!
    
    @IBOutlet weak var tableview_bookings: UITableView!
    var dict_item:NSDictionary!
    var my_switch:UISwitch = UISwitch()
    var array_emails:[String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "FRIENDS"
        tableview_bookings.delegate  = self as UITableViewDelegate
        tableview_bookings.dataSource = self as UITableViewDataSource
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        //self.addLeftMenuButtonWithImage()
        self.addRightMenuButtonWithImage()
       // self.addRightSearchButtonWithImage()
        
        createItems()
        Config.doneString = "Sync"
        SwiftMultiSelect.dataSource     = self
        SwiftMultiSelect.delegate       = self
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getMyBookings()
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        my_switch = UISwitch(frame: .zero)
        my_switch.tintColor =  UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
        my_switch.onTintColor =  UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
        my_switch.isOn = false // or false
        my_switch.addTarget(self, action: #selector(switchToggled(_:)), for: .valueChanged)
        let switch_display = UIBarButtonItem(customView: my_switch)
        
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.search_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        
        
         navigationItem.rightBarButtonItems = [barButton, switch_display]
       // navigationItem.rightBarButtonItem = switch_display
    }
    
    @objc func search_btn_clicked(){
        let rootVC : FriendSearchVC = FriendSearchVC(nibName: "FriendSearchVC", bundle: nil)
        self.navigationController?.pushViewController(rootVC, animated: true)
    }
    
    
    func addRightSearchButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func switchToggled(_ sender: UISwitch) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        switch authorizationStatus {
        case .authorized:
            if sender.isOn {
                print( "The switch is now true!" )
                SwiftMultiSelect.initialSelected = []
                SwiftMultiSelect.Show(to: self)
            }
            else{
                print( "The switch is now false!" )
            }
            break
        case .denied, .notDetermined:
            sender.isOn = false
            DataUtil.alertMessage("Please allow acces of your contacts from settings", viewController: self)
            return

        default:
            sender.isOn = false
            DataUtil.alertMessage("Please allow acces of your contacts from settings", viewController: self)
            break
        }
        
        
        
        
    }
    
    
    
    
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getMyBookings(){
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam: Parameters = ["user_id":str_userid]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",  (DataUtil.appdelegate().objUserInfo?.str_accessToken)!),
            "Accept": "application/json"
        ]
        self.array_mybookings.removeAllObjects()
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.get_friendsList, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.array_mybookings.removeAllObjects()
                if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:FriendsListModel = FriendsListModel(dictUserInfo: dict)
                        self.array_mybookings.add(obj_bookingmodal)
                    }
                    self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    
    func syncContacts(){
        //friends_email
        let str_userid:String = String(format:(DataUtil.appdelegate().objUserInfo?.userId)!)
        let postParam: Parameters = ["user_id":str_userid, "friends_email":array_emails.joined(separator: ",")]
        print("Login Post Parameter : \(postParam)")
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",  (DataUtil.appdelegate().objUserInfo?.str_accessToken)!),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.contact_sync, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                self.getMyBookings()
               /* if let arr = successDict.value(forKey:"response"){
                    let array:NSArray = arr as! NSArray
                    for item in array {
                        let dict:NSDictionary =  item as! NSDictionary
                        let obj_bookingmodal:FriendsListModel = FriendsListModel(dictUserInfo: dict)
                        self.array_mybookings.add(obj_bookingmodal)
                    }
                    self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }*/
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        let obj_requestmodal:FriendsListModel = array_mybookings[indexPath.row] as! FriendsListModel
        if obj_requestmodal.str_request_status == "1"{
        return 120
        }
        return 150
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.array_mybookings.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let obj_requestmodal:FriendsListModel = array_mybookings[indexPath.row] as! FriendsListModel
         if obj_requestmodal.str_request_status == "1"{
            let identifier = "FriendsListCell"
            var logincell: FriendsListCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? FriendsListCell
            if logincell == nil {
                tableView.register(UINib(nibName: "FriendsListCell", bundle: nil), forCellReuseIdentifier: identifier)
                logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? FriendsListCell)!
            }
            logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal)
            return logincell!
        }
        
        
        
        
        
        let identifier = "FriendsListAcceptCell"
        var logincell: FriendsListAcceptCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? FriendsListAcceptCell
        if logincell == nil {
            tableView.register(UINib(nibName: "FriendsListAcceptCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? FriendsListAcceptCell)!
        }
        logincell?.controller = self
        logincell?.delegate = self
        logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal)
        return logincell!
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj_phonelistvc:FriendDetailVC = FriendDetailVC(nibName: "FriendDetailVC", bundle: nil)
        let obj_requestmodal:FriendsListModel = array_mybookings[indexPath.row] as! FriendsListModel
        obj_phonelistvc.str_user_id = obj_requestmodal.str_user_id
        self.navigationController?.pushViewController(obj_phonelistvc, animated: true)
    }
    
    
    
    /// Create a custom items set
    func createItems(){
        
        self.items.removeAll()
        self.initialValues.removeAll()
        for i in 0..<50{
            items.append(SwiftMultiSelectItem(row: i, title: "test\(i)", description: "description for: \(i)", imageURL : (i == 1 ? "https://randomuser.me/api/portraits/women/68.jpg" : nil)))
        }
        self.initialValues = [self.items.first!,self.items[1],self.items[2]]
        self.selectedItems = items
        SwiftMultiSelect.dataSourceType = .phone
    }
    
    
    /// selector for switch addressbook
    ///
    /// - Parameter sender
    @IBAction func useAddr(_ sender: Any) {
        
        SwiftMultiSelect.dataSourceType = .phone
    }
    
    
    /// Function to launch selector from button
    ///
    /// - Parameter sender
    @IBAction func launch(_ sender: Any) {
        
        //Example to start a selector with initial values
        
        
    }
    
    
    //MARK: - SwiftMultiSelectDelegate
    
    func userDidSearch(searchString: String) {
        if searchString == ""{
            selectedItems = items
        }else{
            selectedItems = items.filter({$0.title.lowercased().contains(searchString.lowercased()) || ($0.description != nil && $0.description!.lowercased().contains(searchString.lowercased())) })
        }
    }
    
    func numberOfItemsInSwiftMultiSelect() -> Int {
        return selectedItems.count
    }
    
    func swiftMultiSelect(didUnselectItem item: SwiftMultiSelectItem) {
        print("row: \(item.title) has been deselected!")
    }
    
    func swiftMultiSelect(didSelectItem item: SwiftMultiSelectItem) {
        print("item: \(item.title) \(String(describing: item.userInfo)) has been selected!")
        
        
        //CNContact
    }
    
    func didCloseSwiftMultiSelect() {
        //badge.isHidden = true
        //badge.text = ""
        my_switch.isOn = false
    }
    
    func swiftMultiSelect(itemAtRow row: Int) -> SwiftMultiSelectItem {
        return selectedItems[row]
    }
    
    func swiftMultiSelect(didSelectItems items: [SwiftMultiSelectItem]) {
        
        initialValues   = items
       // badge.isHidden  = (items.count <= 0)
        ///badge.text      = "\(items.count)"
        my_switch.isOn = false
        print("you have been selected: \(items.count) items!")
        
        for item in items{
            
            if let contact = item.userInfo as? CNContact
            {
                    if let emailValue : CNLabeledValue = contact.emailAddresses.first
                    {
                        self.array_emails.append(emailValue.value as String)
                        print("Email : ", emailValue.value)
                    }
            }
            
        }
        self.syncContacts()
        
    }
    
    
    
}

