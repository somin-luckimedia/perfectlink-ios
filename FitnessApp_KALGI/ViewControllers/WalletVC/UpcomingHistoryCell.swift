//
//  UpcomingHistoryCell.swift
//  FitnessApp
//
//  Created by Ashish on 10/05/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//

import UIKit

class UpcomingHistoryCell: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_member_count: UILabel!
    @IBOutlet weak var lbl_valid: UILabel!
    @IBOutlet weak var lbl_status: UILabel!
    @IBOutlet weak var imageView_thumb: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCellwithModal(obj_bookingmodal:WalletModel){
        lbl_title.text = String(format: "%@, $%@", obj_bookingmodal.str_name, obj_bookingmodal.str_total_amount_paid)
        lbl_date.text = String(format: "From: %@ To: %@", obj_bookingmodal.str_start_date, obj_bookingmodal.str_end_date)
        let url = URL(string: obj_bookingmodal.str_profile_pic)

        imageView_thumb.setImageFrom(url)
        if obj_bookingmodal.str_member_count == "1"{
        lbl_member_count.text = "Single Member"
    }
        else{
        lbl_member_count.text = String(format: "Group of %@ member", obj_bookingmodal.str_member_count)
    }
        lbl_valid.text = String(format: "Valid upto: %@", obj_bookingmodal.str_days)
}

}
