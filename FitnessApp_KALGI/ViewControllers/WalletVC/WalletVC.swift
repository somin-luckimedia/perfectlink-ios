//
//  WalletVC.swift
//  FitnessApp
//
//  Created by Ashish on 09/05/20.
//  Copyright © 2020 Ashish Mishra. All rights reserved.
//



import UIKit
import Alamofire
//OrderHistoryCell
class WalletVC: UIViewController , UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate  {
    
    fileprivate var array_upcoming:NSMutableArray = NSMutableArray()
    fileprivate var array_history:NSMutableArray = NSMutableArray()
    @IBOutlet weak var imageView_BG: UIImageView!
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var tableview_bookings: UITableView!
    
    @IBOutlet weak var lbl_from: UILabel!
    @IBOutlet weak var lbl_to: UILabel!
    @IBOutlet weak var lbl_balance: UILabel!
    
    
    var str_amount:String = "0"
    var str_fromDate:String = ""
    var str_toDate:String = ""
    
    var selectedIndex:Int = 0
    var dict_item:NSDictionary!
    
    /*
     "from_date" = "2020-03-22";
     "wallet_admin_received" = 0;
     "wallet_hundred_percent" = 0;
     "wallet_seventyfive_percent" = 0;
     "to_date" = "2020-05-10";
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Wallet History"
        tableview_bookings.delegate  = self as UITableViewDelegate
        tableview_bookings.dataSource = self as UITableViewDataSource
        tableview_bookings.tableHeaderView = self.view_header
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
//        if (DataUtil.appdelegate().objUserInfo?.str_userType) == "2" {
//            self.getMyBookings()
//
//        }else{
//            self.getTrainerOrders()
//        }
        self.getUpcomingHistory()
        
        //self.addLeftMenuButtonWithImage()
            self.addRightMenuButtonWithImage()
        
        
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func addLeftMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "back_image"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.back_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func addRightMenuButtonWithImage(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "history"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.history_btn_clicked), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30 , height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func back_btn_clicked(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func history_btn_clicked(){
        
        let obj_parkeAddressVC:HistoryVC = HistoryVC(nibName: "HistoryVC", bundle: nil)
            let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
            //obj_parkeAddressVC.obj_wallpostModel = obj_requestmodal
            self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    
    @IBAction func transfer_btn_clicked(_ sender: UIButton) {
        //ANGNQSENSPRZ
        //let postParam: Parameters = ["user_id":"TOD2DR3TMJQ0","start_date": self.str_fromDate, "end_date": str_toDate ]
        
        if self.str_amount == "0"{
            DataUtil.alertMessage("There is no any amount in wallet to transfer", viewController: self)
            return
        }
        
        let postParam: Parameters = ["user_id":(DataUtil.appdelegate().objUserInfo?.userId)!,"start_date": self.str_fromDate, "end_date": str_toDate ]
        //let postParam: Parameters = ["user_id":(DataUtil.appdelegate().objUserInfo?.userId)!]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        //WalletModel
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.request_to_admin, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)

                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    @IBAction func header_btn_clicked(_ sender: UIButton) {
        let imageview1:UIImageView = self.view_header.viewWithTag(201) as! UIImageView
        let imageview2:UIImageView = self.view_header.viewWithTag(202) as! UIImageView
        let btn1:UIButton = self.view_header.viewWithTag(101) as! UIButton
        let btn2:UIButton = self.view_header.viewWithTag(102) as! UIButton
        btn1.setTitleColor(.lightGray, for: .normal)
        btn2.setTitleColor(.lightGray, for: .normal)
        sender.setTitleColor(.white, for: .normal)
        
        let imageview_selected:UIImageView = self.view_header.viewWithTag(sender.tag + 100) as! UIImageView
        imageview1.backgroundColor = UIColor.clear
        imageview2.backgroundColor = UIColor.clear
        imageview_selected.backgroundColor = UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
        if sender.tag == 101{
            self.selectedIndex = 0
            self.tableview_bookings.reloadData()
        }else if sender.tag == 102{
            self.selectedIndex = 1
             self.tableview_bookings.reloadData()
        }
    }
    
    
    
    @IBAction func upcoming_btn_clicked(_ sender: UIButton) {
        
    }
    
   
    
    
    
    func getUpcomingHistory(){
        //ANGNQSENSPRZ
        //let postParam: Parameters = ["user_id":"TOD2DR3TMJQ0"]
        let postParam: Parameters = ["user_id":(DataUtil.appdelegate().objUserInfo?.userId)!]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        //WalletModel
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.trainer_wallet, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let obj_dict:NSDictionary = arr as! NSDictionary
                    
                    if let result_number = obj_dict.value(forKey: "wallet_seventyfive_percent") as? NSNumber
                    {
                        self.str_amount = "\(result_number)"
                        self.lbl_balance.text = String(format: "$%@", self.str_amount)
                    }
                    
                    if let result_number = obj_dict.value(forKey: "from_date") as? String
                    {
                        self.str_fromDate = "\(result_number)"
                        self.lbl_from.text = String(format: "From: %@", self.str_fromDate)
                    }
                    
                    if let result_number = obj_dict.value(forKey: "to_date") as? String
                    {
                        self.str_toDate = "\(result_number)"
                        self.lbl_to.text = String(format: "To: %@", self.str_toDate)
                    }
                    
                    
                    /*
                     "from_date" = "2020-03-22";
                     "wallet_admin_received" = 0;
                     "wallet_hundred_percent" = 0;
                     "wallet_seventyfive_percent" = 0;
                     "to_date" = "2020-05-10";
                     */
                    
                    if let arr = obj_dict.value(forKey:"upcoming"){
                        let array:NSArray = arr as! NSArray
                        for item in array {
                            let dict:NSDictionary =  item as! NSDictionary
                            let obj_bookingmodal:WalletModel = WalletModel(dictUserInfo: dict)
                            self.array_upcoming.add(obj_bookingmodal)
                        }
                        self.tableview_bookings.reloadData()
                    }
                    
                    if let arr = obj_dict.value(forKey:"history"){
                        let array:NSArray = arr as! NSArray
                        for item in array {
                            let dict:NSDictionary =  item as! NSDictionary
                            let obj_bookingmodal:WalletModel = WalletModel(dictUserInfo: dict)
                            self.array_history.add(obj_bookingmodal)
                        }
                       // self.tableview_bookings.reloadData()
                    }
                    
                    
                    //self.tableview_bookings.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if self.selectedIndex == 0{
        return 118
        }
        
        return 138
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if self.selectedIndex == 0{
        return self.array_upcoming.count
        }
        
        return self.array_history.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.selectedIndex == 0{
        let identifier = "UpcomingHistoryCell"
        var logincell: UpcomingHistoryCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? UpcomingHistoryCell
        if logincell == nil {
            tableView.register(UINib(nibName: "UpcomingHistoryCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? UpcomingHistoryCell)!
        }
        let obj_requestmodal:WalletModel = array_upcoming[indexPath.row] as! WalletModel
        logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal)
        return logincell!
        }
        
        let identifier = "HistoryCell"
        var logincell: HistoryCell? = tableView.dequeueReusableCell(withIdentifier: identifier) as? HistoryCell
        if logincell == nil {
            tableView.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: identifier)
            logincell = (tableView.dequeueReusableCell(withIdentifier: identifier) as? HistoryCell)!
        }
        let obj_requestmodal:WalletModel = array_history[indexPath.row] as! WalletModel
        logincell!.updateCellwithModal(obj_bookingmodal: obj_requestmodal)
        return logincell!
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var obj_requestmodal:WalletModel!
        if self.selectedIndex == 0{
            obj_requestmodal = array_upcoming[indexPath.row] as? WalletModel
        }else{
            obj_requestmodal = array_history[indexPath.row] as? WalletModel
        }
        let rootVC : SubscriptionConfirmationVC = SubscriptionConfirmationVC(nibName: "SubscriptionConfirmationVC", bundle: nil)
        rootVC.str_transactionId = String(format: "%@", obj_requestmodal.str_transaction_id)
        rootVC.isFromHistory = true
        rootVC.str_subscription_type = "TRAINER"
        rootVC.isFromTrainerHome = true
        self.navigationController?.pushViewController(rootVC, animated: true)
    }
    
}
