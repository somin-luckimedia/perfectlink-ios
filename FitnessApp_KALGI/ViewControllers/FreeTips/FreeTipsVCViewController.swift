//
//  FreeTipsVCViewController.swift
//  FitnessApp_KALGI
//
//  Created by kalgi bhavsar on 08/03/21.
//

import UIKit
import Photos
import AVFoundation
import AVKit
import Alamofire

class FreeTipsVCViewController: UIViewController, ImageVideoPickerDelegate ,UIImagePickerControllerDelegate,UINavigationControllerDelegate,VideoAddDelegate,TrainerVideoCellDelegate {

    @IBOutlet var collFreeTips: UICollectionView!
    fileprivate var array_FreeTips:NSMutableArray = NSMutableArray()
    private let spacing:CGFloat = 5.0
    var indexTips : Int = -1
    func videoadded_clicked() {
        self.apiFreeTips()
//        self.collFreeTips.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiFreeTips()
        self.collFreeTips.register(UINib(nibName: "FreeTipsCVC", bundle: nil), forCellWithReuseIdentifier: "FreeTipsCVC")
    }
}


//MARK : BUTTON ACTION METHODS
extension FreeTipsVCViewController {
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnAddTipsTapped(_ sender: UIButton) {
        self.opneGallery()
    }
}

//MARK : COLLECTION VIEW DELEGATE & DATA SOURCE METHIDS
extension FreeTipsVCViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_FreeTips.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:FreeTipsCVC = collectionView.dequeueReusableCell(withReuseIdentifier: "FreeTipsCVC", for: indexPath) as! FreeTipsCVC

        cell.contentView.layer.cornerRadius = 10.0
        let dict = array_FreeTips[indexPath.row] as! NSDictionary
        let url = URL(string: dict["thumbnail"] as! String)
        cell.imgFreeTips.setImageFrom(url)
        cell.btnDeleteTips.tag = indexPath.row
        cell.btnDeleteTips.addTarget(self, action: #selector(didTapPayButton), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow:CGFloat = 2
        let spacingBetweenCells:CGFloat = 0
        
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        // if let collection = self.collectionData{
        let width = (collFreeTips.bounds.width - totalSpacing)/numberOfItemsPerRow
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let dict = array_FreeTips[indexPath.row] as! NSDictionary
        let fileUrl = URL(string: dict["video"] as! String)
        let player = AVPlayer(url: fileUrl!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.present(playerController, animated: true) {
            player.play()
        }
    }
}

//MARK : API CALL & CUSTOM METHODS
extension FreeTipsVCViewController {
    func apiFreeTips() {
        self.array_FreeTips.removeAllObjects()
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        let postParam = ["user_id":str_userid]
        print("Login Post Parameter : \(postParam)")
        let headers = [
            "Authorization": String(format: "Bearer %@",str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.trainer_video_list, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                if let arr = successDict.value(forKey:"response"){
                    let dict : NSArray = arr as! NSArray
                    print(dict)
                    for item in dict {
                        self.array_FreeTips.add(item)
                    }
                    self.collFreeTips.delegate = self
                    self.collFreeTips.dataSource = self
                    self.collFreeTips.reloadData()
                }else{
                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                }
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    func apiDeleteFreeTips(index : Int) {
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
        let dict = array_FreeTips[index] as! NSDictionary
        let postParam: Parameters = ["user_id":str_userid,"id" : dict["id"] as! String]
        print("Login Post Parameter : \(postParam)")
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@", str_accessToken),
            "Accept": "application/json"
        ]
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.remove_trainer_video, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                self.apiFreeTips()
//                self.getMyBookings()
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
    }
    
    @objc private func didTapPayButton(sender:UIButton) {
        
        let refreshAlert = UIAlertController(title: "", message: "Are you sure you want to delete this tips?", preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.apiDeleteFreeTips(index: sender.tag)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func opneGallery(){
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.mediaTypes = ["public.movie"]
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
  /*  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let videoURL:URL = (info[UIImagePickerControllerMediaURL] as? URL)!
        print("videoURL:\(String(describing: videoURL))")
        self.dismiss(animated: true, completion: nil)
    }*/
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
        
    {
        
        
        if let videoUrl = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.mediaURL.rawValue)] as? URL
            
        {
            //self.dismiss(animated: true, completion: nil)
             //DataUtil.loadIndicatorView()
            self.dismiss(animated: true) {

                let obj_parkeAddressVC:VideoViewerVC = VideoViewerVC(nibName: "VideoViewerVC", bundle: nil)
                let nav:UINavigationController = UINavigationController(rootViewController: obj_parkeAddressVC)
                obj_parkeAddressVC.video_url = videoUrl
                obj_parkeAddressVC.delegate = self
                self.navigationController?.present(nav, animated: true, completion: nil)
                
                
            }
            /*
            
            
            
         //   self.uploadMedia(url_video: videoUrl)
           
            let postParam: Parameters = ["title":"Test iOS12", "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!]
            Alamofire.upload(multipartFormData:{ multipartFormData in
                multipartFormData.append(videoUrl, withName: "video")
                for (key, value) in postParam {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
                
            },
                             usingThreshold:UInt64.init(),
                             to:"http://perfectlinkfitness.com/api/trainer_add_video",
                             method:.post,
                             headers:[
                                "Authorization": String(format: "Bearer %@",  (DataUtil.appdelegate().objUserInfo?.str_accessToken)!),
                                "Accept": "application/json"
                ],
                             encodingCompletion: { encodingResult in
                                switch encodingResult {
                                case .success(let upload, _, _):
                                     DataUtil.removeIndicatorView()
                                    upload.responseJSON { response in
                                        guard let successDict = response.result.value as? NSDictionary else
                                        {
                                            return
                                        }
                                        if let status = successDict["STATUS"] as? String{
                                            if status == "true"{
                                                if (successDict.value(forKey: "STATUS") as? String) == "true"{
                                                    
                                                    if let array:NSArray = successDict["response"] as? NSArray{
                                                        DataUtil.appdelegate().objUserInfo?.array_videos.removeAllObjects()
                                                        for item in array {
                                                            let dict:NSDictionary =  item as! NSDictionary
                                                            let obj_model:VideosModel = VideosModel.init(dictUserInfo: dict)
                                                            DataUtil.appdelegate().objUserInfo?.array_videos.add(obj_model)
                                                        }
                                                        self.collectioneview_video.reloadData()
                                                    }
                                                    
                                                    
                                                    
                                                    let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: (successDict.value(forKey: "message") as? String), preferredStyle: UIAlertControllerStyle.alert)
                                                    
                                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                                                        //self.delegate?.parkadded_clicked()
                                                        self.back_btn_clicked()
                                                    }))
                                                    self.present(alert, animated: true, completion: nil)
                                                    
                                                    // DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                                                    //self.back_btn_clicked()
                                                    
                                                    /*   if let arr = successDict.value(forKey:"response"){
                                                     
                                                     
                                                     }else{
                                                     
                                                     }*/
                                                }else{
                                                    
                                                    DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                                                    self.back_btn_clicked()
                                                }
                                                
                                            }
                                        }
                                        debugPrint(response)
                                    }
                                case .failure(let encodingError):
                                     DataUtil.removeIndicatorView()
                                    print(encodingError)
                                }
            })
            
            */

        }
        
        
        
    }
    
    func onCancel() {
        
    }
    
    func uploadMedia(url_video:URL){
        
        
        guard let url = URL(string: "http://admin.perfectlinkfitness.com/api/trainer_add_video") else {
            return
        }
        var request = URLRequest(url: url)
        let boundary = "------------------------your_boundary"
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        request.httpMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue(String(format: "Bearer %@",str_accessToken), forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        var movieData: Data?
        do {
            movieData = try Data(contentsOf: url_video, options: Data.ReadingOptions.alwaysMapped)
        } catch _ {
            movieData = nil
            return
        }
        
        var body = Data()
        
        // change file name whatever you want
        let filename = "video.mov"
        let mimetype = "video/mov"
        //"user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"video\"; user_id=\"DPYZC68VR5WS\";title=\"test ios\";filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(movieData!)
        let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""

        let dict = ["title":"Test iOS", "user_id":str_userid]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: [])
        body.append(jsonData!)
        
        
        
        
        
        request.httpBody = body
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, reponse: URLResponse?, error: Error?) in
            if let `error` = error {
                print(error)
                return
            }
            if let `data` = data {
                print(String(data: data, encoding: String.Encoding.utf8))
            }
        }
        
        task.resume()
    }
    
    func onDoneSelection(assets: [PHAsset]) {
        if assets.count > 0{
            ImageVideoPicker.getDataFrom(asset: assets[0]) { data in
                let str_userid:String = UserDefaults.standard.value(forKey: "LogedInUserID") as? String ?? ""
                let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

                let headers: HTTPHeaders = [
                        "Authorization": String(format: "Bearer %@", str_accessToken),
                        "Accept": "application/json"
                    ]
                    
                let postParam: Parameters = ["title":"Test iOS", "user_id": str_userid]
                    print("Login Post Parameter : \(postParam)")

                ServerCommunication.getDataWithPostVideo(url: ConstantFiles.add_video, image: data!  ,parameter: postParam,headers:headers, viewController: self, isImage:true,success: { (successDict) in
                        print(successDict)
                    if (successDict.value(forKey: "STATUS") as? String) == "true"{
                        
                        
                        
                    }else{
                            
                            DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
                        self.navigationController?.popViewController(animated: true)
                        }
                    }) { (dictFailure) in
                        if (dictFailure.value(forKey: "message") as? String) != nil {
                            DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
                        }
                    }
                    
                    
                    
            }
            
        }
    }
    
    func delete_btn_clicked(cell: TrainerVideoCell, btn: UIButton) {
        
        let obj_locationModel:VideosModel = DataUtil.appdelegate().objUserInfo!.array_videos[cell.rowIndex] as! VideosModel
        let headers: HTTPHeaders = [
            "Authorization": String(format: "Bearer %@",  (DataUtil.appdelegate().objUserInfo?.str_accessToken)!),
            "Accept": "application/json"
        ]
        
        let postParam: Parameters = ["id":(obj_locationModel.str_id)!, "user_id":(DataUtil.appdelegate().objUserInfo?.str_trainer_user_id)!]
        print("Login Post Parameter : \(postParam)")
        
        
        
        ServerCommunication.getDataWithPostHeader(url: ConstantFiles.remove_video, parameter: postParam, viewController: self, headers: headers, success: {  (successDict) in
            print(successDict)
            if (successDict.value(forKey: "STATUS") as? String) == "true"{
                //obj_locationModel
                DataUtil.appdelegate().objUserInfo!.array_videos.remove(obj_locationModel)
                self.collFreeTips.reloadData()
                
            }else{
                DataUtil.alertMessage((successDict.value(forKey: "message") as? String)!, viewController: self)
            }
        }) { (dictFailure) in
            if (dictFailure.value(forKey: "message") as? String) != nil {
                DataUtil.alertMessage((dictFailure.value(forKey: "message") as? String)!, viewController: self)
            }
        }
        
        
        
    }
}
