//
//  ConstantFIle.swift
//  WealthGenerators
//
//  Created by Mithilesh on 20/02/17.
//  Copyright © 2016 Mithilesh. All rights reserved.
//

import Foundation
import UIKit

public typealias Parameter = [String:Any]

//let objSynapseObjectParser = SynapseObjectParser.singleton()
let indicatorViewTag = 10001
let errorToastViewTag = 10002
///SEAT/V1/

struct ConstantFiles
{
    
    struct Square {
        static let SQUARE_LOCATION_ID: String = "<# REPLACE_ME #>"
        static let APPLICATION_ID: String  = "sandbox-sq0idb-aZS7z8BK5k6jVBEOYyLQhg"
        static let CHARGE_SERVER_HOST: String = "http://admin.perfectlinkfitness.com/api"
        static let CHARGE_URL_Trainer: String = "\(CHARGE_SERVER_HOST)/subscription-payment"
        static let CHARGE_URL_Accessories: String = "\(CHARGE_SERVER_HOST)/creat-payment"

    }
    static let appVersion =  Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
//    static let baseURL = "https://perfectlink.luckistore.in/api/"
    static let baseURL = "http://admin.perfectlinkfitness.com/api/"
    
    // Ashish
    
    static let ParkLinkList = baseURL + "trainer_park_list"
    static let ParkLinkDetail = baseURL + "trainer_park_detail"
    static let ParkGoingIn = baseURL + "park_going_in"
    static let ParkShareExperiance = baseURL + "share_your_experience"
    static let ParkGallerylist = baseURL + "park_gallery"
    // Till here
    
    
    static let loginUrl =  baseURL+"login"
    static let register =  baseURL+"signup"
    static let socialLoginUrl =  baseURL+"socialLogin"
    static let reset_password =  baseURL+"password/reset"
    static let helpRequest =  baseURL+"help_request"
    static let gym_categories =  baseURL+"gym_category"
    static let freeRun_get = baseURL+"freeRun"
    static let gym_listing =  baseURL+"gym_list"
    static let gym_detail =  baseURL+"gym_details"
    static let add_star_rating =  baseURL+"add_star_rating"
    static let gym_add_Profile =  baseURL+"addgym_to_profile"
    static let add_to_Profile =  baseURL+"add_to_profile"
    static let add_wall_share =  baseURL+"add_wall_share"

    static let user_trainers =  baseURL+"users_trainers"
    static let product_category =  baseURL+"product_by_category"
    static let protein_listing =  baseURL+"product_lists"
    static let accessories_listing =  baseURL+"accessories_lists"
    static let discount_listing =  baseURL+"coupon_codes"
    static let protien_detail =  baseURL+"product_details"
    static let accessories_detail =  baseURL+"accessories_details"
    static let park_list =  baseURL+"park_list"
    static let add_park =  baseURL+"trainer_add_park"
    static let remove_park =  baseURL+"remove_trainer_park"
    static let remove_achievements =  baseURL+"remove_trainer_achievement"
    static let add_achievements =  baseURL+"trainer_add_achievement"
    static let updat_membershiprate =  baseURL+"trainer_update_membership_rate"
    static let add_video =  baseURL+"trainer_add_video"
    static let remove_video =  baseURL+"remove_trainer_video"
    static let update_profile =  baseURL+"update_profile"
    static let stopFreeRun =  baseURL+"stopFreeRun"

    static let add_image =  baseURL+"trainer_galary_image"
    static let remove_gallery_image =  baseURL+"remove_trainer_galary_image"
    static let trainer_list =  baseURL+"trainer_list"
    static let trainer_detail =  baseURL+"trainer_details"
    static let trainerFollow =  baseURL+"follow_trainer"

    static let forget_password =  baseURL+"password/email"
    static let home_count =  baseURL+"home_screen_counts"
    static let add_shipping_address =  baseURL+"add_shipping_address"
    static let update_shipping_address =  baseURL+"update_shipping_address"
    static let get_shipping_address =  baseURL+"get_shipping_address"
    static let remove_shipping_address =  baseURL+"remove_shipping_address"
    static let get_orderInfo =  baseURL+"order_info_by_transaction_id"
    static let get_my_orders =  baseURL+"my_order"
    static let get_subscriptionInfo =  baseURL+"subscription_info_by_transaction_id"
    static let get_friendsList =  baseURL+"friends_list"
    static let add_friendsList =  baseURL+"add_friends"
    static let search_friendsList =  baseURL+"search_friend"
    static let contact_sync =  baseURL+"contact_sync"
    static let check_user =  baseURL+"check_avilable_user"
    static let get_friend_info =  baseURL+"get_friend_info"
    static let parkUnsubscribed = baseURL+"parkUnsubscribed"
    static let gymUnsubscribed = baseURL+"gymUnsubscribed"

    static let send_friend_request =  baseURL+"send_friend_request"
    static let cancel_friend_request =  baseURL+"cancel_friend_request"
    static let reject_friend_request =  baseURL+"reject_friend_request"
    static let accept_friend_request =  baseURL+"accept_friend_request"
    static let get_wall_list =  baseURL+"wall_list"
    static let delete_Wall_post = baseURL+"removeWallPost"
    static let remove_trainer_video = baseURL+"remove_trainer_video"
    static let like_post =  baseURL+"like"
    static let dislike_post =  baseURL+"dislike"
    static let add_wall_post =  baseURL+"add_wall"
    static let add_challange =  baseURL+"add_challange"
    static let my_order_trainer =  baseURL+"my_order_trainer"
  static let trainer_subscription_info_by_transaction_id =  baseURL+"trainer_subscription_info_by_transaction_id"
    static let trainer_wallet =  baseURL+"trainer_wallet"
    static let trainer_wallet_history =  baseURL+"trainer_wallet_history"
    static let request_to_admin =  baseURL+"request_to_admin"
    static let update_trainer_account_details =  baseURL+"update_trainer_account_details"
    static let add_trainer_home_service =  baseURL+"add_trainer_home_service"
    static let update_trainer_home_service =  baseURL+"update_trainer_home_service"
    static let challange_completed_list =  baseURL+"challange_request_completed_list"
    static let challange_received_list =  baseURL+"challange_received_list"
    static let challange_sent_list =  baseURL+"challange_sent_list"
    static let challange_friends_list =  baseURL+"challange_friends_list"
    static let accept_challange =  baseURL+"accept_challange"
    static let complete_challange =  baseURL+"complete_challange"
    static let get_wall_comment =  baseURL+"get_wall_comment"
    static let add_wall_comment =  baseURL+"add_wall_comment"
    static let trainer_achievement_list =  baseURL+"trainer_achievement_list"
    static let remove_trainer_achievement = baseURL+"remove_trainer_achievement"
    static let trainer_video_list =  baseURL+"trainer_video_list"


    
    //add_challange accept_challange   complete_challange
    //add_wall_comment
    //http://perfectlinkfitness.com/api/get_wall_comment
    //http://perfectlinkfitness.com/api/challange_friends_list
    //http://perfectlinkfitness.com/api/challange_request_completed_list
    //http://perfectlinkfitness.com/api/challange_received_list.
    //http://perfectlinkfitness.com/api/challange_sent_list
    //http://perfectlinkfitness.com/api/add_wall
    static let getMobileListing =  baseURL+"api.php?mobile=oo"
    static let getAllBookings =  baseURL+"api.php"
    static let updateprofile =  baseURL+"update_user_profile"
    static let forgetpassword =  baseURL+"forget_password"

    static let logOut_user = baseURL + "logout"
    
    static let user_info = baseURL + "profile_info"
    
    static let change_password = baseURL + "change_password"
    
    static let getUserInfo =  baseURL+"userinfo_by_id"

    static let addTicket =  baseURL+"public/api/addsupporttickets"

    static let updateRating =  baseURL+"public/api/updatetaskrating"
    static let viewAllTickets =  baseURL+"public/api/usersupporttickets"
    static let urlLoginVerification = baseURL + "checkGen?"
    static let urlCreatePassword = baseURL + "createPassword?"
    static let urlNotification = baseURL + "alertListing?"
    static let notificationURL =   baseURL+"notificationListing?"
    static let notificationReadUnreadURL =  baseURL+"ReadUnreadnotifications?"
    static let notificationDetailUrl =  baseURL+"notificationDetail?"
    static let urlAlertRead =  baseURL+"myMsg?"
    static let urlAnnouncement = baseURL + "announcementListing?"
    static let urlDeleteNotification = baseURL + "deleteAlertNotification?"
    static let profileSettingFetchUrl =  baseURL+"getProfile?"
    static let profileSettingSaveUrl =  baseURL+"profileSetting?"
    static let profileSetttingMediaUrl = baseURL+"profileSettingSpecificRecords?"
    static let urlPresentationAll = baseURL + "presentationCategory?"
    static let urlPresentation =  baseURL+"presentationCategory?"
    static let urlPresentationListing =  baseURL+"presentationListing?"
    static let urlPresentationDetail =  baseURL+"presentationDetail?"
    static let urlMediaCenter =  baseURL+"trainingCategory?"
    static let urlMediaCenterListing =  baseURL+"trainingListing?"
    static let urlMediaCenterDetail =  baseURL+"trainingDetail?"
    static let urlTradeAlertList =  baseURL+"tradeAlertListing?"
    static let urlTradeAlertReadUnread =  baseURL+"readUnreadTrades?"
    static let urlTradeMails =  baseURL+"trademails?"
    static let urlTradeMailReadUnread = baseURL + "readUnreadTrademaillist"
    static let urlTradeMailsDetail = baseURL + "readtrademails?"
    static let urlTradeAlertUnreadCount = baseURL + "tradeCountUnreadBothSide"
    //https://api.instagram.com/v1/users/self/?access_token=4999312421.3eb2ffd.dd180086b59d40b38180edafe69c89de
    //https://www.spolu.in/carbon/public/api/usersupporttickets
    //update_number
    static let keyDeviceID = "deviceID"
    
    static let str4Letters =  "First name must containt 4 charectors"
    static let strValidCompanyUrl = "Please enter valid company URL."
    static let strValidEnrollmentUrl = "Please enter valid enrollment URL."
    static let strValidEventUrl = "Please enter valid Event URL."
    static let strValidFacebookUrl = "Please enter valid Facebook URL."
    static let strValidTwitterUrl = "Please enter valid twitter URL."
    static let strValidInstagrammUrl = "Please enter valid Instagram URL."
    static let strValidPinterestUrl = "Please enter valid pinterest URL."
    static let strValidLinkedInUrl = "Please enter valid linked in URL."
    static let strValidGooglePlusUrl = "Please enter valid google Plus URL."
    static let strValidYoutubeUrl = "Please enter valid youtube URL."
    
    static let msgTitleOfApp : String = "PerfectLinkFitness"
   
    static let genId = "genId"
    static let password = "password"
    static let userId = "userId"
    static let deviceToken = "deviceToken"
    static let strNotificationTitle = "Notifications"
    static let strNotificationDetailTitle = "Notification Message"
    static let langCode = "LangCode"
    static let strLanguage = "AppLanguage"
    static let strPresentaion = "Presentation"
    static let strMediaCenter = "Training"
    static var indicatorViewTag = 1001
    static let navigationBarTitleColor: UIColor = UIColor.white
    static let pagination = 50
    static let supportMailId = "support@wealthgenerators.com"
    static let userNotFoundStatusCode = 1500
    
    //new
    static let strNotificationByUser = baseURL + "get_notifications"
    
}

