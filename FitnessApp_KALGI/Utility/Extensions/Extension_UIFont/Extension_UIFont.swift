//
//  Extension_UIFont.swift
//  uClinic
//
//  Created by Viral Shah on 26/04/18.
//  Copyright © 2018 Viral Shah. All rights reserved.
//

import UIKit

extension UIFont {
    
    class func appFont_TitilliumWebBold(fontSize: CGFloat) -> UIFont {
        return UIFont(name: "Lato-Bold", size: fontSize)!
    }
    
    class func appFont_TitilliumWebRegular(fontSize: CGFloat) -> UIFont {
        return UIFont(name: "Lato-Regular", size: fontSize)!
    }
     
    class func proportionalFontSize(size:CGFloat) -> CGFloat {
        return (size + (UIDevice.current.type == .iPhone8plus || UIDevice.current.type == .iPhone7plus || UIDevice.current.type == .iPhone6Splus || UIDevice.current.type == .iPhone6plus ? 0 : 0)
            + (UIDevice.current.type == .iPhone6 || UIDevice.current.type == .iPhone6S || UIDevice.current.type == .iPhone7 || UIDevice.current.type == .iPhone8 || UIDevice.current.type == .iPhoneX ? -1 : 0)
            + (UIDevice.current.type == .iPhone5 || UIDevice.current.type == .iPhone5S || UIDevice.current.type == .iPhone5C || UIDevice.current.type == .iPhoneSE ? -2 : 0) + (UIDevice.current.type == .iPhone4 ? -2 : 0))
    }
}
