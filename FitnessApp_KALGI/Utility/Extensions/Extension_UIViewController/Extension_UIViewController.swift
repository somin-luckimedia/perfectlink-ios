//
//  Extension_UIViewController.swift
//  HeyU
//
//  Created by viral shah on 31/12/17.
//  Copyright © 2017 viral shah. All rights reserved.
//

import UIKit
//import SlideMenuControllerSwift

extension UIViewController {
    
    func setNavigationBar(_ tintColor: UIColor = .white) {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.isExclusiveTouch = true
        self.navigationController?.navigationBar.tintColor = UIColor.appBlue
        self.navigationController?.navigationBar.barTintColor = UIColor.gray
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.navigationBar.backIndicatorImage = UIImage()
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
//        self.navigationController?.view.backgroundColor = UIColor.appMenuBg
        self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage() , for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue) : UIColor.appBlack,
                                                                        NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont.appFont_TitilliumWebRegular(fontSize: 19)]
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back"), style: .plain, target: self, action: #selector(self.buttonActionMethodName))
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    func setNavigationBarWithoutTitleImage(_ tintColor: UIColor = .white) {
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.shadowImage = UIImage()
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_back"), style: .plain, target: self, action: nil)
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue) : UIColor.white,
                                                                        NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont.appFont_TitilliumWebRegular(fontSize: 18)]
        
        let searchImage:UIImage = UIImage(named: "ic_back")!
        let backBtn:UIBarButtonItem = UIBarButtonItem(image: searchImage,  style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.buttonActionMethodName))
        
        self.navigationItem.leftBarButtonItem = backBtn
    }
    
    @objc func buttonActionMethodName(){
        self.navigationController!.popViewController(animated: true)
    }
    
    func setNavigationTitleImage(_ tintColor: UIColor = .white) { 
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "logo_small")
        self.navigationController?.navigationBar.topItem?.titleView = imageView
    }
    
    func setNavigationWithBack(_ tintColor: UIColor = .white) {
        let searchImage:UIImage = UIImage(named: "ic_back")!
        let backBtn:UIBarButtonItem = UIBarButtonItem(image: searchImage,  style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.buttonActionMethodName))
        self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.leftBarButtonItem = backBtn
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "logo_small")
        self.navigationController?.navigationBar.topItem?.titleView = imageView
    }
    
    
    func setNavigationClearColor(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barStyle = .blackOpaque
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : UIFont.appFont_TitilliumWebRegular(fontSize: 22),NSAttributedString.Key.foregroundColor : UIColor.white]
    }
}


extension UIApplication {
    
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
    
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        
//        if let slide = viewController as? SideMenuController{
//            return topViewController(slide.menuViewController)
//        }
        return viewController
    }
}

extension UIViewController {
    func topMostViewController() -> UIViewController {
        
        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
        }
        
        return self
    }
}

extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
}
