//
//  Extension_UIColor.swift
//  uClinic
//
//  Created by Viral Shah on 26/04/18.
//  Copyright © 2018 Viral Shah. All rights reserved.
//

import UIKit

extension UIColor {
    static var appBlack = UIColor(red: 32/255, green: 40/255, blue: 53/255, alpha: 1)
    static var appBlue = UIColor(red: 96/255, green: 89/255, blue: 255/255, alpha: 1)

}
