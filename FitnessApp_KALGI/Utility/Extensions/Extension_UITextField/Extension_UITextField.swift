//
//  Extension_UITextField.swift
//  GoRate
//
//  Created by Bhavin Mistry on 11/09/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

extension UITextField {
    func setupUIForTextField(font: UIFont, textColor: UIColor, placeHolderString: String) {
        self.placeholder = placeHolderString
        self.font = font
        self.textColor = textColor
    }
}
