
//
//  ServerCommunication.swift
//  WealthGenerators
//
//  Created by Mithilesh on 2/28/17.
//  Copyright © 2017 Mithilesh. All rights reserved.

import UIKit
import Alamofire
import Foundation
import Photos
import AVFoundation

class ServerCommunication: NSObject {

    class func getData(url: String,viewController: UIViewController,headers:HTTPHeaders ,success: @escaping(NSDictionary) -> Void,failure: @escaping (NSDictionary) -> Void){
        DataUtil.loadIndicatorView()
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            if response.data != nil{
                print("******************Json Data Start************************************")
                print(NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)!)
                print("******************Json Data End ************************************")
            }
            switch (response.result){
            case .success(_):
                print("******************success******************")
                guard let json = response.result.value as? [String: Any] else
                {
                    print("Error: \(String(describing: response.result.error))")
                    DataUtil.removeIndicatorView()
                    DispatchQueue.main.async {
                        failure(response.result as Any as! NSDictionary)
                    }
                    return
                }
                print(json)
              
                    DispatchQueue.main.async {
                        DataUtil.removeIndicatorView()
                        success(json as NSDictionary)
                    }
                
                DataUtil.removeIndicatorView()
            case .failure(_):
                print("******************failure******************")
                DataUtil.removeIndicatorView()
                print(response.result.error!.localizedDescription)
            }
        }
    }
    
    class func getDataWithPost(url: String,parameter postParam: Parameters,viewController: UIViewController,success: @escaping(NSDictionary) -> Void,failure: @escaping (NSDictionary) -> Void){
        print("URL: \(url) ")
        print("Post Parameter: \(postParam) ")
        DataUtil.loadIndicatorView()
        Alamofire.request(url, method: .post, parameters: postParam, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (response:DataResponse<Any>) in
            if let data = response.data{
                print("******************Json Data Start************************************")
                if let strJson = NSString(data: data, encoding: String.Encoding.utf8.rawValue){
                    print(strJson)
                    if strJson.contains("Unauthenticated.") {
                        UserDefaults.standard.set("", forKey: "LogedInUserID")
                        UserDefaults.standard.set("", forKey: "LogedInUserAccessToken")
                        UserDefaults.standard.set(false, forKey: "LogedIn")
                        UserDefaults.standard.synchronize()
                        let splavhVcObj = SignInVC(nibName: "SignInVC", bundle: nil)
                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                        appdelegate.window!.rootViewController = UINavigationController(rootViewController: splavhVcObj)
                    }
                }
                print("******************Json Data End ************************************")
            }
            switch (response.result){
            case .success(_):
                print("******************success******************")
                guard let json = response.result.value as? NSDictionary else
                {
                    print("Error: \(String(describing: response.result.error))")
                    DataUtil.removeIndicatorView()
                    DispatchQueue.main.async {
                        failure(response.result as Any as! NSDictionary )
                    }
                    return
                }
                if let status = json["status"] as? String{
                    if status == "false"{
                        DispatchQueue.main.async {
                            print("Failed: \(response.result.value!)")
                            if  let statusCode = (response.result.value as! NSDictionary).value(forKey: "statusCode") as? Int{
                                if statusCode == ConstantFiles.userNotFoundStatusCode{
                                    
                                    
                                    
                                }
                            }
                            if (json["message"] as? String) != nil{
                                let str_message:String = json["message"] as! String

                                if str_message == "Token mismatch!"{
                                    let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: "We are experiencing that your session has been expired. Kindly Re-Login to resume the services.", preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                                        
                                        let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
                                       // objAppDelgate.load_login_view()
                                        
                                    }))
                                    
                                    DataUtil.appdelegate().window?.rootViewController?.present(alert, animated: true, completion: nil)
                                    
                                }
                               
                                
                            }
                            
//                            if let message = (response.result.value as! NSDictionary).value(forKey: "message") as? String {
//                                DataUtil.alertMessage(localizedString(message), viewController: viewController)
//                            }
                            failure(response.result.value as! NSDictionary)
                        }
                    }
                    else if status == "true"{
                        print(json)
                        DispatchQueue.main.async {
                            success(json)
                        }
                    }
                }
                else{
                    print(json)
                    DispatchQueue.main.async {
                        success(json)
                    }
                }
                DataUtil.removeIndicatorView()
            case .failure(_):
                print("******************failure******************")
                print(response.result.error!.localizedDescription)
                DispatchQueue.main.async {
                    if ((response.result.error) as NSError?)?.code == -1009{
                        //DataUtil.alertMessage(response.result.error!.localizedDescription, viewController: viewController)
                        DispatchQueue.main.async {
                            let dictFail = NSMutableDictionary()
                            dictFail.setValue(-1009, forKey: "errorCode")
                            failure(dictFail)
                        }
                        return
                    }
                    //DataUtil.alertMessage(response.result.error!.localizedDescription, viewController: viewController)
                }
                DataUtil.removeIndicatorView()
            }
        }
    }
    
    
    class func getDataWithPostHeader(url: String,parameter postParam: Parameters,viewController: UIViewController,headers:HTTPHeaders ,success: @escaping(NSDictionary) -> Void,failure: @escaping (NSDictionary) -> Void){
        print("URL: \(url) ")
        print("Post Parameter: \(postParam) ")
        DataUtil.loadIndicatorView()
        Alamofire.request(url, method: .post, parameters: postParam, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            if let data = response.data{
                print("******************Json Data Start************************************")
                if let strJson = NSString(data: data, encoding: String.Encoding.utf8.rawValue){
                    print(strJson)
                    if strJson.contains("Unauthenticated.") {
                        UserDefaults.standard.set("", forKey: "LogedInUserID")
                        UserDefaults.standard.set("", forKey: "LogedInUserAccessToken")
                        UserDefaults.standard.set(false, forKey: "LogedIn")
                        UserDefaults.standard.synchronize()
                        let splavhVcObj = SignInVC(nibName: "SignInVC", bundle: nil)
                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                        appdelegate.window!.rootViewController = UINavigationController(rootViewController: splavhVcObj)
                    }
                }
                print("******************Json Data End ************************************")
            }
            switch (response.result){
            case .success(_):
                print("******************success******************")
                guard let json = response.result.value as? NSDictionary else
                {
                    print("Error: \(String(describing: response.result.error))")
                    DataUtil.removeIndicatorView()
                    DispatchQueue.main.async {
                        failure(response.result as Any as! NSDictionary )
                    }
                    return
                }
                if let status = json["status"] as? String{
                    if status == "false"{
                        DispatchQueue.main.async {
                            print("Failed: \(response.result.value!)")
                            if  let statusCode = (response.result.value as! NSDictionary).value(forKey: "statusCode") as? Int{
                                if statusCode == ConstantFiles.userNotFoundStatusCode{
                                    
                                    
                                    
                                }
                            }
                            if (json["message"] as? String) != nil{
                                let str_message:String = json["message"] as! String
                                
                                if str_message == "Token mismatch!"{
                                    let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: "We are experiencing that your session has been expired. Kindly Re-Login to resume the services.", preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                                        
                                        let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
                                        // objAppDelgate.load_login_view()
                                        
                                    }))
                                    
                                    DataUtil.appdelegate().window?.rootViewController?.present(alert, animated: true, completion: nil)
                                    
                                }
                                
                                
                            }
                            
                            //                            if let message = (response.result.value as! NSDictionary).value(forKey: "message") as? String {
                            //                                DataUtil.alertMessage(localizedString(message), viewController: viewController)
                            //                            }
                            failure(response.result.value as! NSDictionary)
                        }
                    }
                    else if status == "true"{
                        print(json)
                        DispatchQueue.main.async {
                            success(json)
                        }
                    }
                }
                else{
                    print(json)
                    DispatchQueue.main.async {
                        success(json)
                    }
                }
                DataUtil.removeIndicatorView()
            case .failure(_):
                print("******************failure******************")
                print(response.result.error!.localizedDescription)
                DispatchQueue.main.async {
                    if ((response.result.error) as NSError?)?.code == -1009{

                        DispatchQueue.main.async {
                            let dictFail = NSMutableDictionary()
                            dictFail.setValue(-1009, forKey: "errorCode")
                            failure(dictFail)
                        }
                        return
                    }
                    //DataUtil.alertMessage(response.result.error!.localizedDescription, viewController: viewController)
                }
                DataUtil.removeIndicatorView()
            }
        }
    }
    
    
    class func getDataWithPut(url: String,parameter postParam: Parameters,viewController: UIViewController,success: @escaping(NSDictionary) -> Void,failure: @escaping (NSDictionary) -> Void){
        print("URL: \(url) ")
        print("Post Parameter: \(postParam) ")
        DataUtil.loadIndicatorView()
        Alamofire.request(url, method: .put, parameters: postParam, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (response:DataResponse<Any>) in
            if let data = response.data{
                print("******************Json Data Start************************************")
                if let strJson = NSString(data: data, encoding: String.Encoding.utf8.rawValue){
                    print(strJson)
                    if strJson.contains("Unauthenticated.") {
                        UserDefaults.standard.set("", forKey: "LogedInUserID")
                        UserDefaults.standard.set("", forKey: "LogedInUserAccessToken")
                        UserDefaults.standard.set(false, forKey: "LogedIn")
                        UserDefaults.standard.synchronize()
                        let splavhVcObj = SignInVC(nibName: "SignInVC", bundle: nil)
                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                        appdelegate.window!.rootViewController = UINavigationController(rootViewController: splavhVcObj)
                    }
                }
                print("******************Json Data End ************************************")
            }
            switch (response.result){
            case .success(_):
                print("******************success******************")
                guard let json = response.result.value as? NSDictionary else
                {
                    print("Error: \(String(describing: response.result.error))")
                    DataUtil.removeIndicatorView()
                    DispatchQueue.main.async {
                        failure(response.result as Any as! NSDictionary )
                    }
                    return
                }
                if let status = json["status"] as? String{
                    if status == "false"{
                        DispatchQueue.main.async {
                            print("Failed: \(response.result.value!)")
                            if  let statusCode = (response.result.value as! NSDictionary).value(forKey: "statusCode") as? Int{
                                if statusCode == ConstantFiles.userNotFoundStatusCode{
                                    
                                    
                                    
                                }
                            }
                            if (json["message"] as? String) != nil{
                                let str_message:String = json["message"] as! String
                                
                                if str_message == "Token mismatch!"{
                                    let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: "We are experiencing that your session has been expired. Kindly Re-Login to resume the services.", preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                                        
                                        let objAppDelgate =  UIApplication.shared.delegate as! AppDelegate
                                      //  objAppDelgate.load_login_view()
                                        
                                    }))
                                    
                                    DataUtil.appdelegate().window?.rootViewController?.present(alert, animated: true, completion: nil)
                                    
                                }
                                
                                
                            }
                            
                            //                            if let message = (response.result.value as! NSDictionary).value(forKey: "message") as? String {
                            //                                DataUtil.alertMessage(localizedString(message), viewController: viewController)
                            //                            }
                            failure(response.result.value as! NSDictionary)
                        }
                    }
                    else if status == "true"{
                        print(json)
                        DispatchQueue.main.async {
                            success(json)
                        }
                    }
                }
                else{
                    print(json)
                    DispatchQueue.main.async {
                        success(json)
                    }
                }
                DataUtil.removeIndicatorView()
            case .failure(_):
                print("******************failure******************")
                print(response.result.error!.localizedDescription)
                DispatchQueue.main.async {
                    if ((response.result.error) as NSError?)?.code == -1009{
                        //DataUtil.alertMessage(response.result.error!.localizedDescription, viewController: viewController)
                        DispatchQueue.main.async {
                            let dictFail = NSMutableDictionary()
                            dictFail.setValue(-1009, forKey: "errorCode")
                            failure(dictFail)
                        }
                        return
                    }
                    //DataUtil.alertMessage(response.result.error!.localizedDescription, viewController: viewController)
                }
                DataUtil.removeIndicatorView()
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    class func getDataWithPostImageForWall(url: String,image:Any,parameter postParam: Parameters,viewController: UIViewController, isImage:Bool ,success: @escaping(NSDictionary) -> Void,failure: @escaping (NSDictionary) -> Void){
        print("URL: \(url) ")
        print("Post Parameter: \(postParam) ")
        DataUtil.loadIndicatorView()
        
        
        
        let parameters = [
            "file_name": "file.jpeg"
        ]
        
        
        
        var headers: HTTPHeaders! = nil
        
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""
        if DataUtil.appdelegate().objUserInfo != nil{
            headers = [
                "Authorization": String(format: "Bearer %@",str_accessToken),
                "Accept": "application/json"
            ]
        }else{
            headers = [
                "Accept": "application/json"
            ]
        }
        
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if isImage == true{
                multipartFormData.append((image as! UIImage).jpegData(compressionQuality: 1)!, withName: "file", fileName: "file.jpeg", mimeType: "image/png")
                
                // multipartFormData.appendBodyPart(data: UIImageJPEGRepresentation(image as! UIImage, 1)!, name: "profile_picture", fileName: "profile_picture.png", mimeType: "image/png")
                
            }
            for (key, value) in postParam {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:url,
           method:.post,
           headers:headers)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    DispatchQueue.main.async {
                        DataUtil.removeIndicatorView()
                        guard let json = response.result.value as? NSDictionary else
                        {
                            print("Error: \(String(describing: response.result.error))")
                            DataUtil.removeIndicatorView()
                            DispatchQueue.main.async {
                                failure(response.result as Any as! NSDictionary )
                            }
                            return
                        }
                        success(json)
                    }
                }
            case .failure(let encodingError): break
            DataUtil.removeIndicatorView()
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    class func getDataWithPostImage(url: String,image:Any,parameter postParam: Parameters,viewController: UIViewController, isImage:Bool ,success: @escaping(NSDictionary) -> Void,failure: @escaping (NSDictionary) -> Void){
        print("URL: \(url) ")
        print("Post Parameter: \(postParam) ")
        DataUtil.loadIndicatorView()
        
        
        
        let parameters = [
            "file_name": "profile_picture.jpeg"
        ]

        

        var headers: HTTPHeaders! = nil
        
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        if DataUtil.appdelegate().objUserInfo != nil{
             headers = [
                "Authorization": String(format: "Bearer %@",  str_accessToken),
                "Accept": "application/json"
            ]
        }else{
             headers = [
                 "Accept": "application/json"
            ]
        }
        
       
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if isImage == true{
                multipartFormData.append((image as! UIImage).jpegData(compressionQuality: 1)!, withName: "profile_picture", fileName: "profile_picture.jpeg", mimeType: "image/png")
                
               // multipartFormData.appendBodyPart(data: UIImageJPEGRepresentation(image as! UIImage, 1)!, name: "profile_picture", fileName: "profile_picture.png", mimeType: "image/png")

            }
            for (key, value) in postParam {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:url,
           method:.post,
           headers:headers)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    DispatchQueue.main.async {
                         DataUtil.removeIndicatorView()
                        guard let json = response.result.value as? NSDictionary else
                        {
                            print("Error: \(String(describing: response.result.error))")
                            DataUtil.removeIndicatorView()
                            DispatchQueue.main.async {
                                failure(response.result as Any as! NSDictionary )
                            }
                            return
                        }
                        
                        
                        success(json)
                    }
                }
                
            case .failure(let encodingError): break
                 DataUtil.removeIndicatorView()
            }
        }
    }
    
    
    class func getDataWithPostImageForTrainer(url: String,image:Any,parameter postParam: Parameters,viewController: UIViewController, isImage:Bool ,success: @escaping(NSDictionary) -> Void,failure: @escaping (NSDictionary) -> Void){
        print("URL: \(url) ")
        print("Post Parameter: \(postParam) ")
        DataUtil.loadIndicatorView()
        
        
        
        let parameters = [
            "file_name": "image.jpeg"
        ]
        
        
        
        var headers: HTTPHeaders! = nil
        let str_accessToken = UserDefaults.standard.value(forKey: "LogedInUserAccessToken") as? String ?? ""

        
        if DataUtil.appdelegate().objUserInfo != nil{
            headers = [
                "Authorization": String(format: "Bearer %@",str_accessToken),
                "Accept": "application/json"
            ]
        }else{
            headers = [
                "Accept": "application/json"
            ]
        }
        
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if isImage == true{
                multipartFormData.append((image as! UIImage).jpegData(compressionQuality: 1)!, withName: "image", fileName: "image.jpeg", mimeType: "image/png")
                
                // multipartFormData.appendBodyPart(data: UIImageJPEGRepresentation(image as! UIImage, 1)!, name: "profile_picture", fileName: "profile_picture.png", mimeType: "image/png")
                
            }
            for (key, value) in postParam {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:url,
           method:.post,
           headers:headers)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    DispatchQueue.main.async {
                        DataUtil.removeIndicatorView()
                        guard let json = response.result.value as? NSDictionary else
                        {
                            print("Error: \(String(describing: response.result.error))")
                            DataUtil.removeIndicatorView()
                            DispatchQueue.main.async {
                                failure(response.result as Any as! NSDictionary )
                            }
                            return
                        }
                        
                        
                        success(json)
                    }
                }
                
            case .failure(let encodingError): break
            DataUtil.removeIndicatorView()
            }
        }
    }
    
    
  
    class func getDataWithPostVideo(url: String,image:Data,parameter postParam: Parameters,headers:HTTPHeaders , viewController: UIViewController, isImage:Bool ,success: @escaping(NSDictionary) -> Void,failure: @escaping (NSDictionary) -> Void){
        print("URL: \(url) ")
        print("Post Parameter: \(postParam) ")
        DataUtil.loadIndicatorView()
        
        
        
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(image, withName: "video")
            //    multipartFormData.append(image, withName: "video", fileName: "video.mp4", mimeType: "video/mp4")
            for (key, value) in postParam {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:url)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    DispatchQueue.main.async {
                        DataUtil.removeIndicatorView()
                        guard let json = response.result.value as? NSDictionary else
                        {
                            print("Error: \(String(describing: response.result.error))")
                            DataUtil.removeIndicatorView()
                            DispatchQueue.main.async {
                                failure(response.result as Any as! NSDictionary )
                            }
                            return
                        }
                        
                        
                        success(json)
                    }
                }
                
            case .failure(let encodingError): break
            DataUtil.removeIndicatorView()
            }
        }
    }
    
    
    
    class func getDataWithPostImageForattachment(url: String,image1:UIImage?,image2:UIImage?,image3:UIImage?,parameter postParam: Parameters,viewController: UIViewController, success: @escaping(NSDictionary) -> Void,failure: @escaping (NSDictionary) -> Void){
        print("URL: \(url) ")
        print("Post Parameter: \(postParam) ")
        DataUtil.loadIndicatorView()
        
        
   
        
        
        // image1,image2, image3
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if image1 != nil{
                multipartFormData.append(image1!.jpegData(compressionQuality: 1)!, withName: "image1", fileName: "image1.jpeg", mimeType: "image/jpeg")
            }
            if image2 != nil{
                multipartFormData.append(image2!.jpegData(compressionQuality: 1)!, withName: "image2", fileName: "image2.jpeg", mimeType: "image/jpeg")
            }
            if image3 != nil{
                multipartFormData.append(image3!.jpegData(compressionQuality: 1)!, withName: "image3", fileName: "image3.jpeg", mimeType: "image/jpeg")
            }
            for (key, value) in postParam {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:url)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    DispatchQueue.main.async {
                        DataUtil.removeIndicatorView()
                        guard let json = response.result.value as? NSDictionary else
                        {
                            print("Error: \(String(describing: response.result.error))")
                            DataUtil.removeIndicatorView()
                            DispatchQueue.main.async {
                                failure(response.result as Any as! NSDictionary )
                            }
                            return
                        }
                        
                        
                        success(json)
                    }
                }
                
            case .failure(let encodingError): break
            DataUtil.removeIndicatorView()
                
                
                
                //print encodingError.description
            }
        }
        
        
        
        
        
        /*   Alamofire.request(url, method: .post, parameters: postParam, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (response:DataResponse<Any>) in
         if let data = response.data{
         print("******************Json Data Start************************************")
         if let strJson = NSString(data: data, encoding: String.Encoding.utf8.rawValue){
         print(strJson)
         }
         print("******************Json Data End ************************************")
         }
         switch (response.result){
         case .success(_):
         print("******************success******************")
         guard let json = response.result.value as? NSDictionary else
         {
         print("Error: \(String(describing: response.result.error))")
         DataUtil.removeIndicatorView()
         DispatchQueue.main.async {
         failure(response.result as Any as! NSDictionary )
         }
         return
         }
         if let status = json["status"] as? String{
         if status == "fail"{
         DispatchQueue.main.async {
         print("Failed: \(response.result.value!)")
         if  let statusCode = (response.result.value as! NSDictionary).value(forKey: "statusCode") as? Int{
         if statusCode == ConstantFiles.userNotFoundStatusCode{
         
         
         
         }
         }
         //                            if let message = (response.result.value as! NSDictionary).value(forKey: "message") as? String {
         //                                DataUtil.alertMessage(localizedString(message), viewController: viewController)
         //                            }
         failure(response.result.value as! NSDictionary)
         }
         }
         else if status == "success"{
         print(json)
         DispatchQueue.main.async {
         success(json)
         }
         }
         }
         else{
         print(json)
         DispatchQueue.main.async {
         success(json)
         }
         }
         DataUtil.removeIndicatorView()
         case .failure(_):
         print("******************failure******************")
         print(response.result.error!.localizedDescription)
         DispatchQueue.main.async {
         if ((response.result.error) as NSError?)?.code == -1009{
         //DataUtil.alertMessage(response.result.error!.localizedDescription, viewController: viewController)
         DispatchQueue.main.async {
         let dictFail = NSMutableDictionary()
         dictFail.setValue(-1009, forKey: "errorCode")
         failure(dictFail)
         }
         return
         }
         //DataUtil.alertMessage(response.result.error!.localizedDescription, viewController: viewController)
         }
         DataUtil.removeIndicatorView()
         }
         }*/
    }
    
    
    class func getStringDataWithPost(url: String,parameter postParam: Parameters,viewController: UIViewController,success: @escaping(String) -> Void){
        print("URL: \(url) ")
        print("Post Parameter: \(postParam) ")
        DataUtil.loadIndicatorView()
        Alamofire.request(url, method: .post, parameters: postParam, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (response:DataResponse<Any>) in
            if let data = response.data{
                print("******************Json Data Start************************************")
                if let strValue = String(data: data, encoding: String.Encoding.ascii){
                    print(strValue)
                    DispatchQueue.main.async {
                        success(strValue)
                    }
                }
                else{
                    print("nil")
                }
                DataUtil.removeIndicatorView()
                print("******************Json Data End ************************************")
            }
        }
    }
    
    class func getXmlData(url: String, success:@escaping (XMLParser) ->Void,failure:@escaping (String) ->Void){
        DataUtil.loadIndicatorView()
        let request = URLRequest(url: URL(string: url)!)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil{
                DispatchQueue.main.async {
                    failure("Error \(String(describing: error?.localizedDescription))")
                }
                DataUtil.removeIndicatorView()
            }
            else{
                if data != nil{
                    DispatchQueue.main.async {
                        success(XMLParser(data: data!))
                    }
                    DataUtil.removeIndicatorView()
                }
            }
        }
        task.resume()
    }
    
    class func getServerData(url: String,parameter postParam: Parameters,viewController: UIViewController,success: @escaping(NSDictionary) -> Void,failure: @escaping (NSDictionary) -> Void){
        var request = URLRequest(url: URL(string: url)!)
        var postString: String?
        let dict =  NSMutableDictionary()
        dict.setValue("71", forKey: "userId")
        dict.setValue("1", forKey: "page_no")
        postString = DataUtil.jsonStringWithJSONObject(dict)
        if (postString != nil) {
            let postBody = postString?.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let contentType = "application/json"
            request.addValue(contentType, forHTTPHeaderField: "Content-type")
            request.httpMethod = "POST"
            request.httpBody = postBody
        }else{
            request.httpMethod = "GET"
        }
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil {
                do{
                    let dictS = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    DispatchQueue.main.async {
                        success(dictS)
                    }
                }
                catch {
                    print("")
                }
            }
            
        }
        task.resume()
    }
}
