//
//  CircularImageView.swift
//  IslandTransfer
//
//  Created by Ashish Mishra on 19/03/18.
//  Copyright © 2018 Ashish Mishra. All rights reserved.
//

import UIKit

class CircularImageView: UIImageView {

    override open func awakeFromNib() {
        super.awakeFromNib()
        initializeButton()
    }
    
    private func initializeButton() {
        layer.cornerRadius = frame.size.height/2
        //self.layer.shadowColor = UIColor.black.cgColor
        //self.layer.shadowOffset = CGSize(width: 0, height: 10)
        //self.layer.shadowOpacity = 0.25
        //self.layer.shadowRadius = 5
        self.layer.masksToBounds = true
    }

}
