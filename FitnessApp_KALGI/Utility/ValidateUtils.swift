//
//  ValidateUtils.swift
//  Oodles
//
//  Created by Randeep Singh on 3/29/17.
//  Copyright © 2017 createx. All rights reserved.
//

import UIKit

class ValidateUtils: NSObject {

    static func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{0,25}"
        let predicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return predicate.evaluate(with: testStr)
    }
    
    static func isValidString(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Za-z]+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return predicate.evaluate(with: testStr)
    }
    
    static func isValidPhoneNumber(_ testStr:String) -> Bool {
        let emailRegEx = "^([0-9]*)$"
        let predicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return predicate.evaluate(with: testStr)
    }
    
    static func isValidNumber(_ testStr:String) -> Bool {
        let emailRegEx = "\\b([0-9%.+\\-]+)\\b"
        let predicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return predicate.evaluate(with: testStr)
    }
    
    static func isValidNumberDecimal(_ testStr:String) -> Bool {
        let emailRegEx = "^([0-9]*|[0-9]*[.][0-9]*)$"
        let predicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return predicate.evaluate(with: testStr)
    }
    
}
