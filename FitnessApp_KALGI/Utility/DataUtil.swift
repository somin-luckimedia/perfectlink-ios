//
//  DataUtil.swift
//  WealthGenerators
//
//  Created by Mithilesh on 2/7/17.
//  Copyright © 2017 Mithilesh. All rights reserved.
//

import UIKit
import CoreData
import CoreImage

class DataUtil: NSObject {
    
    class func isValidUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url  = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
                
            }
        }
        return false
    }
    class func stringFromDate(date : Date,DateFormate formate: String) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = formate
        return formatter.string(from: date)
    }
    
    class func dateFromString(strDate: String,DateFormate formate: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        return dateFormatter.date(from: strDate)!
    }
    
    class func nullToBlank(value : AnyObject?) -> String {
        if value is NSNull {
            return ""
        } else {
            return String(format:"%@", value as! CVarArg)
        }
    }
    
    
    class func createQRFromString(_ str: String) -> UIImage? {
        let stringData = str.data(using: String.Encoding.utf8)
        
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter?.setValue(stringData, forKey: "inputMessage")
        
        filter?.setValue("H", forKey: "inputCorrectionLevel")
        
        // if let img = createQRFromString("Hello world program created by someone") {
        let somImage = UIImage(ciImage: (filter?.outputImage)!, scale: 1.0, orientation: UIImage.Orientation.down)
      //  }
        
        return somImage
    }
    
    class func setGradientBackgroundImage() -> UIImage {
        let middleColor:UIColor = UIColor(red: 252/255, green: 238/255, blue: 209/255, alpha: 1.0)
        let endColor:UIColor = UIColor(red: 216/255, green: 256/255, blue: 214/255, alpha: 1.0)
        let startColor:UIColor = UIColor(red: 216/255, green: 256/255, blue: 214/255, alpha: 1.0)
        let updatedFrame = UIScreen.main.bounds
        //updatedFrame.size.height + = self.frame.origin.y
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: [startColor, middleColor, endColor], startPoint: .centerTop, endPoint: .centerBottom)
        return gradientLayer.createGradientImage()!
    }
    /*
    class func alertMessage (_ msgString : String,viewController: UIViewController)
    {
        let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: msgString, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: localizedString(LocalizedConstant.ok), style: UIAlertActionStyle.default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    class func alertMessage (_ msgString : String,viewController: UIViewController, okAlertAction: @escaping (String) -> Void,cancelAlertAction:@escaping (String) -> Void)
    {
        let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: msgString, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: localizedString(LocalizedConstant.cancel), style: .default, handler: { (alert) in
            cancelAlertAction("cancel")
        }))
        
        alert.addAction(UIAlertAction(title: localizedString(LocalizedConstant.ok), style: .default, handler: { (alert) in
            okAlertAction("ok")
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    class func alertMessage (_ msgString : String,viewController: UIViewController, okAlertAction: @escaping (String) -> Void)
    {
        let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: msgString, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: localizedString(LocalizedConstant.ok), style: .default, handler: { (alert) in
            okAlertAction("ok")
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    */
    
    class func alertMessage (_ msgString : String,viewController: UIViewController)
    {
        let alert = UIAlertController(title: ConstantFiles.msgTitleOfApp , message: msgString, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    
    class func loadIndicatorView(indicatorVwBgColor: UIColor? = UIColor.groupTableViewBackground,indicatorVwColor: UIColor? = UIColor.black){
        if UIApplication.shared.delegate?.window??.viewWithTag(ConstantFiles.indicatorViewTag) == nil{
            let loadView = UIView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width,  height: UIScreen.main.bounds.size.height))
            loadView.backgroundColor = UIColor.clear
            loadView.tag = ConstantFiles.indicatorViewTag
            let widthBgIndicationView: CGFloat = 80.0
            let bgIndicatorView = UIView(frame: CGRect(x: CGFloat(UIScreen.main.bounds.size.width/2 - widthBgIndicationView/2), y: CGFloat(UIScreen.main.bounds.size.height/2 - widthBgIndicationView/2), width: widthBgIndicationView, height: widthBgIndicationView))
            bgIndicatorView.backgroundColor = indicatorVwBgColor
            bgIndicatorView.layer.cornerRadius = 5.0
            loadView.addSubview(bgIndicatorView)
            let activityIndicator = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.whiteLarge)
            activityIndicator.tag = ConstantFiles.indicatorViewTag+1
            activityIndicator.color = indicatorVwColor
            activityIndicator.frame.origin = CGPoint(x: bgIndicatorView.frame.size.width/2 - activityIndicator.frame.size.width/2, y: bgIndicatorView.frame.size.height/2 - activityIndicator.frame.size.height/2)
            bgIndicatorView.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            UIApplication.shared.delegate?.window??.addSubview(loadView)
        }
    }
    
    class func removeIndicatorView(){
        let appWindow = UIApplication.shared.delegate?.window
        appWindow??.viewWithTag(ConstantFiles.indicatorViewTag)?.removeFromSuperview()
    }
    
    
    //Mark: Read Json  File
    class func readJson(fileName: String) ->NSDictionary
    {
        let dictJsonError: NSDictionary = ["Error": "FAIL"]
        let path = Bundle.main.path(forResource: fileName, ofType: "geojson")
        let jsonData = NSData(contentsOfFile: path!)
        do{
            let  dictVal = try JSONSerialization.jsonObject(with: jsonData! as Data, options: JSONSerialization.ReadingOptions.allowFragments)
            return dictVal as! NSDictionary
        }
        catch {
            
        }
        return dictJsonError
    }
    
    
    class func readCountryJson(fileName: String) -> NSArray
    {
       // let dictJsonError: NSDictionary = ["Error": "FAIL"]
        let path = Bundle.main.path(forResource: fileName, ofType: "json")
        let jsonData = NSData(contentsOfFile: path!)
        do{
            let  dictVal = try JSONSerialization.jsonObject(with: jsonData! as Data, options: JSONSerialization.ReadingOptions.allowFragments)
            return dictVal as! NSArray
        }
        catch {
            
        }
        return [""]
    }
    
    class func appdelegate() -> AppDelegate {
        let appdelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        return appdelegate
    }
    
    
    class func synapseParserString(_ string : String) -> String{
        return  "WealthGenerators." + string
    }
    
    class  func readXmlFile() -> Data{
        let path = Bundle.main.path(forResource: "note", ofType: "xml")
        let xmlData = NSData(contentsOfFile: path!)
        return xmlData! as Data
    }
    
    class func jsonStringWithJSONObject(_ jsonObject: AnyObject) -> String? {
        let data: Data? = try? JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions.prettyPrinted)        
        var jsonStr: String?
        if data != nil {
            jsonStr = String(data: data!, encoding: String.Encoding.utf8)
        }
        return jsonStr
    }
    
   class func removeNull(value : AnyObject?) -> String? {
        if value is NSNull {
            return ""
        } else {
            return value as! String
        }
    }
    
class func mapImageFrom(str_source_lat:String, str_source_long:String, str_destination_lat:String, str_destination_long:String) -> URL{
        let str_mapurl:String = String(format: "http://maps.google.com/maps/api/staticmap?markers=color:blue|label:S|%@,%@&markers=color:green|label:D|%@,%@&%@&sensor=true&key=AIzaSyCkY0gnNhffo5o423y8JprarNBraClbAa8", str_source_lat, str_source_long,str_destination_lat, str_destination_long, String(format: "zoom=12&size=%dx%d", 375, 150))
        let mapurl:URL =  URL(string: str_mapurl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!
        return mapurl
    }
    
    enum Climate {
        case India
        case America
        case Africa
        case Australia
    }
    
}

/*extension MemberShipCell {
    func selected() {
        self.backgroundColor = UIColor(red: 209.0/255.0, green: 9.0/255.0, blue: 4.0/255.0, alpha: 1.0)
    }
    func deselected() {
        self.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 174.0/255.0, alpha: 0.25)
    }
}*/
