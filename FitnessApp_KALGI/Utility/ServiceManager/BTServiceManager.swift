//
//  BTServiceManager.swift
//  uClinic
//
//  Created by Viral Shah on 26/04/18.
//  Copyright © 2018 Viral Shah. All rights reserved.
//

import Foundation
import Alamofire
import NVActivityIndicatorView

let ServiceManager = BTServiceManager.shared

class BTServiceManager: NSObject {
    
    var request: Alamofire.Request? {
        didSet {
            oldValue?.cancel()
        }
    }
    var isLoderRequire: Bool = true
    var viewController : UIViewController = UIViewController()
    // MARK: - SHARED MANAGER
    static let shared = BTServiceManager()
    
    func callGETApi(_ url: String, isLoaderRequired: Bool, andCompletion completion: @escaping (_ isSuccess: Bool, _ statusCode: Int, _ message: String, _ data: Any) -> Void) {
        callApi(true, url: url, paramters: nil, header: nil, isLoaderRequired: isLoaderRequired, andCompletion: completion)
    }
    
    func callPostAPI(_ url: String, isLoaderRequired: Bool, paramters: [String: Any]?, header: [String: String]?, andCompletion completion: @escaping (_ isSuccess: Bool, _ statusCode: Int, _ message: String, _ data: Any) -> Void) {
        callApi(false, url: url, paramters: paramters, header: header, isLoaderRequired: isLoaderRequired, andCompletion: completion)
    }
    func callPostAPIAnother(_ url: String, isLoaderRequired: Bool, paramters: [String: Any]?, header: [String: String]?, andCompletion completion: @escaping (_ isSuccess: Bool, _ statusCode: Int, _ message: String, _ data: Any) -> Void) {
        callApiAnther(false, url: url, paramters: paramters, header: header, isLoaderRequired: isLoaderRequired, andCompletion: completion)
    }
    
    fileprivate func callApi(_ isGet: Bool, url: String, paramters: [String: Any]?, header: [String: String]?, isLoaderRequired: Bool, andCompletion completion: @escaping (_ isSuccess: Bool, _ statusCode: Int, _ message: String, _ data: Any) -> Void) {
        
        if !IS_INTERNET_AVAILABLE() {
//            SHOW_INTERNET_ALERT()
            return
        }
        
        //        let headers:[String:String] = (languageOfApp == "ar") ? ["lancode" : "ar"] : ["lancode" : "en"]
        //        let headers : [String : String] = ["Authorization" : "Bearer \(SharedUser.token)"]
        
        if isLoaderRequired {
            ActivityIndicator.sharedInstance.showHUD(show: true, VC: viewController)
        }
        
        print("*************** URL : \(url) ***************** \n ************** PARAMTERS **************** \n \(String(describing: paramters)) \n **************** HEADERS *************** \n \(String(describing: header))")
        
        Alamofire.request(url, method: isGet ? .get : .post, parameters: paramters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            switch response.result {
            case .success(let JSON):
                let dictJSON = JSON as! NSDictionary
                print("RESPONSE: ", dictJSON)
                let status = dictJSON.object_forKeyWithValidationForClass_String(aKey: "status")
                let message = dictJSON.object_forKeyWithValidationForClass_String(aKey: "message")
                if status == "success" {
                    completion(true, 200, message, dictJSON["data"] as Any)
                } else {
                    completion(false, 400, message, dictJSON["data"] as Any)
                }
            case .failure( _):
                completion(false, 0, "There might be some internal server issue. Please try after sometime.", NSDictionary())
            }
        }
    }
    

    
    fileprivate func callApiAnther(_ isGet: Bool, url: String, paramters: [String: Any]?, header: [String: String]?, isLoaderRequired: Bool, andCompletion completion: @escaping (_ isSuccess: Bool, _ statusCode: Int, _ message: String, _ data: Any) -> Void) {
        
        if !IS_INTERNET_AVAILABLE() {
//            SHOW_INTERNET_ALERT()
            return
        }
        
        //        let headers:[String:String] = (languageOfApp == "ar") ? ["lancode" : "ar"] : ["lancode" : "en"]
        //        let headers : [String : String] = ["Authorization" : "Bearer \(SharedUser.token)"]
        
        if isLoaderRequired {
            ActivityIndicator.sharedInstance.showHUD(show: true, VC: viewController)
        }
        
        print("*************** URL : \(url) ***************** \n ************** PARAMTERS **************** \n \(String(describing: paramters)) \n **************** HEADERS *************** \n \(String(describing: header))")
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: paramters!, options: JSONSerialization.WritingOptions.prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            NSLog("Parameter : \(jsonString)")
            NSLog("API Call Started")
        } catch let error as NSError {
           
            print("error",error)
        }
        
        Alamofire.request(url, method: isGet ? .get : .post, parameters: paramters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            switch response.result {
            case .success(let JSON):
                let dictJSON = JSON as! NSDictionary
                print("RESPONSE: ", dictJSON)
                let status = dictJSON.object_forKeyWithValidationForClass_String(aKey: "status")
                let message = dictJSON.object_forKeyWithValidationForClass_String(aKey: "message")
                if status == "success" {
                    completion(true, 200, message, dictJSON["result"] as Any)
                } else {
                    completion(true, 400, message, dictJSON["result"] as Any)
                }
            case .failure( _):
                completion(false, 0, "There might be some internal server issue. Please try after sometime.", NSDictionary())
            }
        }
    }
    
    func callUPLOADApi(url : String, image:UIImage?, isLoaderRequired: Bool , params : [String : Any]?, header: [String: String]?, onSuccess : @escaping (_ isSuccess: Bool, _ statusCode: Int, _ message: String, _ data: Any) -> Void) {
        
        //        let headers:[String:String] = ["lancode" : "en"]
        //        print("\n\n\n\n\nURL : \(url) \nPARAM : \(params!) \nHEADERS : \(headers)")
        
        if isLoaderRequired {
            ActivityIndicator.sharedInstance.showHUD(show: true, VC: viewController)
        }
        
        print("*************** URL : \(url) ***************** \n ************** PARAMTERS **************** \n \(String(describing: params)) \n **************** HEADERS *************** \n \(String(describing: header))")
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            
            if let params = params {
                for eachKey in params.keys {
                    if let value = params[eachKey] as? String {
                        multipartFormData.append(value.data(using: .utf8)!, withName: eachKey)
                    }
                }
            }
            
            if let image = image {
                if let imageData = image.jpegData(compressionQuality: 0.5) {
                    multipartFormData.append(imageData, withName: "image", fileName: "image.jpg", mimeType: "image/jpeg")
                }
            }
        },
                         usingThreshold:UInt64.init(),
                         to:url,
                         method:.post,
                         headers:header,
                         encodingCompletion: { encodingResult in
                            
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    if let result = response.result.value {
                                        let dictJSON = result as! NSDictionary
                                        print("RESPONSE: ", dictJSON)
                                        let status = dictJSON.object_forKeyWithValidationForClass_String(aKey: "status")
                                        let message = dictJSON.object_forKeyWithValidationForClass_String(aKey: "message")
                                        if status == "success" {
                                            onSuccess(true, 200, message, dictJSON["data"] as Any)
                                        } else {
                                            onSuccess(false, 400, message, NSDictionary())
                                        }
                                    } else {
                                        onSuccess(false, 0, "", NSDictionary())
                                    }
                                }
                            case .failure(let encodingError):
                                print("ERR: UPLOAD: \(encodingError.localizedDescription)")
                                ActivityIndicator.sharedInstance.showHUD(show: true, VC: self.viewController)
                                onSuccess(false, 0, "There might be some internal server issue. Please try after sometime.", NSDictionary())
                            }
        })
    }
}
