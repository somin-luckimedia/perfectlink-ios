//
//  API_URLS.swift
//  uClinic
//
//  Created by Viral Shah on 15/05/18.
//  Copyright © 2018 Viral Shah. All rights reserved.
//

import Foundation

//MARK: - ************* LOCAL API BASE URL **************
let BASE_URL = "http://peponigruppe.com/peponistaging/"

//MARK: - ************* LIVE API BASE URL **************
//let BASE_URL = "http://www.peponigruppe.com/peponigruppeadmin/"


//MARK: - ********** END POINT URL ************

let apiLogin = GET_FULL_URL("signin") 
let apiSignup = GET_FULL_URL("signup")
let apiVerifyOTP = GET_FULL_URL("verifyotp")
let apiUploadImage = GET_FULL_URL("uploadimage")
let apiForgotPw = GET_FULL_URL("forgotpasswordapp")
let apiService = GET_FULL_URL("servicelist")
let apiSubService = GET_FULL_URL("subservicelist")
let apiAddToCart = GET_FULL_URL("addtocart")
let apiMyChatList = GET_FULL_URL("mychatlist")
let apiChatDetails = GET_FULL_URL("chatdetails")
let apiLoginFB = GET_FULL_URL("loginfb")
let apiLogout = GET_FULL_URL("logoutapp")
let apigetMyProfile = GET_FULL_URL("getmyprofile")
let apisetMyProfile = GET_FULL_URL("setmyprofile")
let apiChangePassword = GET_FULL_URL("changepasswordapp")
let apigetNotificationStatus = GET_FULL_URL("getnotificationstatus")
let apisetnotificationstatus = GET_FULL_URL("setnotificationstatus")
let apimycartlist = GET_FULL_URL("mycartlist")
let apipackagelist = GET_FULL_URL("packagelist")
let apipackagedetails = GET_FULL_URL("packagedetails")
let apisaveJobs = GET_FULL_URL("addmysavedjobs")
let apideleteJobs = GET_FULL_URL("cartdelete")
let apicarteditladder = GET_FULL_URL("carteditladder")
let apicarteditdescription = GET_FULL_URL("carteditdescription")
let apicartdeletesubservice = GET_FULL_URL("cartdeletesubservice")
let apiCartDelete = GET_FULL_URL("cartdelete")
let apiCartReschedule = GET_FULL_URL("cartreschedule")
let apiSaveMyJobsList = GET_FULL_URL("mysavedjoblist")
let apiActivatedPackageList = GET_FULL_URL("activatedpackagelist")
let apiSendMessage = GET_FULL_URL("sendmessage")
let apiCustomerSupport = GET_FULL_URL("customersupport")
let apiPackageBuy = GET_FULL_URL("packagebuy")
let apiCheckout = GET_FULL_URL("checkout")
let apiHistoryList = GET_FULL_URL("historylist")
let apiOrderreschedule = GET_FULL_URL("orderreschedule")
let apiOrderDelete = GET_FULL_URL("orderdelete")
let apiWallet = GET_FULL_URL("walletlist")
let apigetsavedaddress = GET_FULL_URL("getsavedaddress")
let apiNotificationlist = GET_FULL_URL("notificationlist")
let apiSetsavedaddress = GET_FULL_URL("setsavedaddress")
let apibecomeapartner = GET_FULL_URL("becomeapartner")



//MARK: - ********** GET FULL URL FUNCTION ************

func GET_FULL_URL(_ endPoint: String) -> String {
    return BASE_URL + endPoint
}
