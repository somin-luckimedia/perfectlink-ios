//
//  Globals.swift
//  uClinic
//
//  Created by Viral Shah on 26/04/18.
//  Copyright © 2018 Viral Shah. All rights reserved.
//

import Foundation
import UIKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate
@available(iOS 13.0, *)
let scene = UIApplication.shared.connectedScenes.first 
@available(iOS 13.0, *)
//let appSceneDelegate : SceneDelegate = (scene?.delegate as! SceneDelegate)

let APP_NAME = ""
let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
let USER_DEFAULT = UserDefaults.standard
let IS_LOGGED_IN = "IS_LOGGED_IN"
let kDeviceToken = "kDeviceToken"
var deviceToken = ""
var userInfoNotification : [String : Any]?
var languageOfApp = "en"
var deviceId : String = ""
var SharedUser : LoginModel!
var SharedService : ServiceModel!
let kUserData = "kUserData"
var summaryDict : NSMutableDictionary = [:]
var arrSummarySubService : NSMutableArray = []
var serviceId : String = ""
var serviceName : String = ""
var strImageDescription : String = ""
var packageCost : String = ""
var dict_Package_global : NSMutableDictionary = [:]
var isJob_pkg : String = ""
var SharedHistory : HistoryModel!
var cartId_global : String = ""
var webview_str : String = ""
var pkg_Desc : String = ""
var webViewType : String =  ""
var isInternetAvailable : Bool = true
var receiver_id : String = "1", order_id = "1"
var imgChat : String = ""
var postClick : Bool = false
var strType_camera : String = ""
var flagPark : Bool = true
var flagGym : Bool = true
var flagGallery : Bool = true
var flagParkLink : Bool = true
var flagGymLink : Bool = true
var flagChallengeImageviewer : Bool = true
var flagGalleryLink : Bool = true
var flagMenu : Bool = false
var flagHome : Bool = false
var flagChallenge : Bool = false
var flagCChat : Bool = false
var flagProfile : Bool = false
var flagSetting : Bool = false
var struserType : String = ""
var flagNotification : Int = 0

//MARK:- *********** INTERNET CHECK *************

func IS_INTERNET_AVAILABLE() -> Bool {
    return BTReachabilityManager.shared.isInternetAvailableForAllNetworks()
}


var INTERNET_MESSAGE:String = ""

func SHOW_INTERNET_ALERT() {
    INTERNET_MESSAGE = "Your internet connection appears to be offline. Please check connection and try again."
    showAlertWithTitleFromVC(vc: (appDelegate.window?.rootViewController)!, title:APP_NAME, andMessage: INTERNET_MESSAGE, buttons: ["Dismiss"]) { (index) in
    }
}

struct ScreenSize {
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6_7_8          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P_7P_8P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0 
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}

func generateQRCode(from string: String) -> UIImage? {
    let data = string.data(using: String.Encoding.ascii)
    if let filter = CIFilter(name: "CIQRCodeGenerator") {
        filter.setValue(data, forKey: "inputMessage")
        let transform = CGAffineTransform(scaleX: 3, y: 3)
        if let output = filter.outputImage?.transformed(by: transform) {
            return UIImage(ciImage: output)
        }
    }
    return nil
}

//MARK: - ******** PROPOTIONAL FUNCTIONS **********

let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let SCREEN_WIDTH = UIScreen.main.bounds.size.width

let PROPOTIONAL_WIDTH : CGFloat = 34

func GET_PROPORTIONAL_WIDTH (width:CGFloat) -> CGFloat {
    return ((SCREEN_WIDTH * width)/750)
}
func GET_PROPORTIONAL_HEIGHT (height:CGFloat) -> CGFloat {
    return ((SCREEN_HEIGHT * height)/1334)
}


func getYearsBetweenDates(startDate:Date, andEndDate endDate:Date) -> [Date] {
    let gregorian: NSCalendar = NSCalendar.current as NSCalendar;
    let components = gregorian.components(NSCalendar.Unit.year, from: startDate, to: endDate, options: [])
    var arrDates = [Date]()
    for i in 0...components.year!{
        print(i)
        if let date = Calendar.current.date(byAdding: .year, value: i, to: startDate) {
            arrDates.append(date)
        }
    }
    return arrDates
}


func getDatesBetweenDates(startDate:Date, andEndDate endDate:Date) -> [Date] {
    let gregorian: NSCalendar = NSCalendar.current as NSCalendar;
    let components = gregorian.components(NSCalendar.Unit.day, from: startDate, to: endDate, options: [])
    var arrDates = [Date]()
    for i in 0...components.day!{
        arrDates.append(startDate.addingTimeInterval(60*60*24*Double(i)))
    }
    return arrDates
}

func timeAgoSinceDate(date:NSDate, numericDates:Bool) -> String {
    var calendar = NSCalendar.current
    calendar.timeZone = TimeZone.current
    let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .month, .year, .second]
    let now = NSDate()
    let earliest = now.earlierDate(date as Date)
    let latest = (earliest == now as Date) ? date : now
    let components = calendar.dateComponents(unitFlags, from: earliest.toLocalTime(), to: latest as Date)
    
    if (components.year! >= 2) {
        return "\(components.year!) \("Years ago")"
    } else if (components.year! >= 1){
        if (numericDates){
            return "1 Year ago"
        } else {
            return "Last year"
        }
    } else if (components.month! >= 2) {
        return "\(components.month!) \("Months ago")"
    } else if (components.month! >= 1){
        if (numericDates){
            return "1 Month ago"
        } else {
            return "Last month"
        }
    }
        //        else if (components.weekOfYear! >= 2) {
        //            return "\(components.weekOfYear!) \("weeks ago".localized)"
        //        } else if (components.weekOfYear! >= 1){
        //            if (numericDates){
        //                return "a week ago".localized
        //            } else {
        //                return "Last week".localized
        //            }
        //        }
    else if (components.day! >= 2) {
        if components.day! == 30 {
            return "1 Month ago"
        }
        else {
            return "\(components.day!) \("Days ago")"
        }
    } else if (components.day! >= 1){
        if (numericDates){
            return "1 Day ago"
        } else {
            return "Yesterday"
        }
    } else if (components.hour! >= 2) {
        return "\(components.hour!) \("Hours ago")"
    } else if (components.hour! >= 1){
        if (numericDates){
            return "1 Hour ago"
        } else {
            return "1 Hour ago"
        }
    } else if (components.minute! >= 2) {
        return "\(components.minute!) \("Minutes ago")"
    } else if (components.minute! >= 1){
        if (numericDates){
            return "1 Minute ago"
        } else {
            return "1 Minute ago"
        }
    } else if (components.second! >= 59) {
        return "\(components.second!) \("Seconds ago")"
    } else {
        return "Just now"
    }
}

var isIphoneXOrBigger: Bool {
    // 812.0 on iPhone X, XS.
    // 896.0 on iPhone XS Max, XR.
    return UIScreen.main.bounds.height >= 812
}

//func addBulltes(stringList: [String],
//                font: UIFont,
//                bullet: String = "\u{2022}",
//                indentation: CGFloat = 12,
//                lineSpacing: CGFloat = 2,
//                paragraphSpacing: CGFloat = 15,
//                textColor: UIColor = APP_LABEL_SECONDARYCOLOR,
//                bulletColor: UIColor) -> NSAttributedString {
//
//    let textAttributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: textColor]
//    let bulletAttributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16), NSAttributedStringKey.foregroundColor: bulletColor]
//
//    let paragraphStyle = NSMutableParagraphStyle()
//    let nonOptions = [NSTextTab.OptionKey: Any]()
//    paragraphStyle.tabStops = [
//        NSTextTab(textAlignment: .left, location: indentation, options: nonOptions)]
//    paragraphStyle.defaultTabInterval = indentation
//    paragraphStyle.firstLineHeadIndent = 0
//    paragraphStyle.lineSpacing = lineSpacing
//    paragraphStyle.paragraphSpacing = paragraphSpacing
//    paragraphStyle.headIndent = indentation
//
//    let bulletList = NSMutableAttributedString()
//    for string in stringList {
//        let formattedString = "\(bullet) \(string)\n"
//        let attributedString = NSMutableAttributedString(string: formattedString)
//
//        attributedString.addAttributes(
//            [NSAttributedStringKey.paragraphStyle : paragraphStyle],
//            range: NSMakeRange(0, attributedString.length))
//
//        attributedString.addAttributes(
//            textAttributes,
//            range: NSMakeRange(0, attributedString.length))
//
//        let string:NSString = NSString(string: formattedString)
//        let rangeForBullet:NSRange = string.range(of: bullet)
//        attributedString.addAttributes(bulletAttributes, range: rangeForBullet)
//        bulletList.append(attributedString)
//    }
//
//    return bulletList
//}

extension UIApplication {
    /// Dismiss keyboard from key window.
    public static func endEditing(_ force: Bool = false) {
        shared.keyWindow?.endEditing(force)
    }
}
