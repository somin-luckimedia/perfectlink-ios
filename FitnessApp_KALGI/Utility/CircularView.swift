//
//  CircularView.swift
//  IslandTransfer
//
//  Created by Ashish Mishra on 04/06/18.
//  Copyright © 2018 Ashish Mishra. All rights reserved.
//

import UIKit

class CircularView: UIView {

    override open func awakeFromNib() {
        super.awakeFromNib()
        initializeButton()
    }
    
    private func initializeButton() {
        layer.cornerRadius = frame.size.height/2
        self.layer.masksToBounds = true
    }

}
